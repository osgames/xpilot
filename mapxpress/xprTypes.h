/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

typedef char    map_data_t[MAX_MAP_SIZE+1][MAX_MAP_SIZE+1];
typedef char    max_str_t[255];


typedef struct {
   int	backx;
   int	backy;
   char		cdata;
} blockdata;

/*To Add options to the Program: 
1. Add the variable to the xpmap_t structure in the proper order. (as per xpilot option dump)
2. Add IDM constant(s) to mapxpmnu.h, be sure to add in the proper place and follow the format as
shown there. 
3. Add the entry fields to the dialog definitions in mapxpress.rc. Make sure to set the control
ID's to your IDM_ constant(s)
4. Add Your Option to the Prefs structure in the proper location (by page), 
following the specified format, don't forget to specify the correct page number.
5. Increase the constant NUMPREFS in mapxpress.h
6. Add a default value to the default_settings array in defualt_settings.h.
7. Add any special processing code that may be necessary for your mapoption, such as
special checks...etc.
8. Edit the HTML help files to include your new option.
/***************************************************************/
typedef struct {
   max_str_t	mapName, mapAuthor, mapFileName;
   int		width, height;
   char		width_str[4], height_str[4];
   blockdata	data[MAX_MAP_SIZE][MAX_MAP_SIZE];
   int		view_zoom;
   int		changed;
   char		*comments;

   char		gravity[7];
   char		shipMass[7];
   char		ballMass[7];
   char		shotMass[7];
   char		shotSpeed[7];
   char		shotLife[4];
   char		fireRepeatRate[7];
   char		minRobots[3];
   char		maxRobots[3];
   int		robotsTalk;
   int		robotsLeave;
   char		robotLeaveLife[7];
   char		robotLeaveScore[7];
   char		robotLeaveRatio[7];
   char		robotTeam[3];
   int		restrictRobots;
   int		reserveRobotTeam;
   max_str_t robotRealName;
   max_str_t robotHostName;
   max_str_t tankRealName;
   max_str_t tankHostName;
   char		tankScoreDecrement[7];
   int		turnThrust;
   int		selfImmunity;
   max_str_t defaultShipShape;
   max_str_t tankShipShape;
   char		maxPlayerShots[4];
   int		shotsGravity;
   int		allowPlayerCrashes;
   int		allowPlayerBounces;
   int		allowPlayerKilling;
   int		allowShields;
   int		playerStartsShielded;
   int		shotsWallBounce;
   int		ballsWallBounce;
   int		minesWallBounce;
   int		itemsWallBounce;
   int		missilesWallBounce;
   int		sparksWallBounce;
   int		debrisWallBounce;
   int		asteroidsWallBounce;
   int		cloakedExhaust;
   int		cloakedShield;
   char		maxObjectWallBounceSpeed[20];
   char		maxShieldedWallBounceSpeed[20];
   char		maxUnshieldedWallBounceSpeed[20];
   char		maxShieldedPlayerWallBounceAngle[20];
   char		maxUnshieldedPlayerWallBounceAngle[20];
   char		playerWallBounceBrakeFactor[20];
   char		objectWallBounceBrakeFactor[20];
   char		objectWallBounceLifeFactor[20];
   char		wallBounceFuelDrainMult[20];
   char		wallBounceDestroyItemProb[20];
   int		limitedVisibility;
   char		minVisibilityDistance[20];
   char		maxVisibilityDistance[20];
   int		limitedLives;
   char		worldLives[4];
   int		reset;
   char		resetOnHuman[4];
   int		teamPlay;
   int		teamCannons;
   int		teamFuel;
   char		cannonSmartness[1];
   int		cannonsUseItems;
   char		cannonDeadTime[20];
   int		keepShots;
   int		teamAssign;
   int		teamImmunity;
   int		ecmsReprogramMines;
   int		ecmsReprogramRobots;
   int		targetKillTeam;
   int		targetTeamCollision;
   int		targetSync;
   char		targetDeadTime[20];
   int		treasureKillTeam;
   int		captureTheFlag;
   int		treasureCollisionDestroys;
   char		ballConnectorSpringConstant[20];
   char		ballConnectorDamping[20];
   char		maxBallConnectorRatio[20];
   char		ballConnectorLength[20];
   int		treasureCollisionMayKill;
   int		wreckageCollisionMayKill;
   int		asteroidCollisionMayKill;
   int		timing;
   int		edgeWrap;
   int		edgeBounce;
   int		extraBorder;
   char		gravityPoint[8];
   char		gravityAngle[4];
   int		gravityPointSource;
   int		gravityClockwise;
   int		gravityAnticlockwise;
   int		gravityVisible;
   int		wormholeVisible;
   int		itemConcentratorVisible;
   char		wormTime[20];
   char		framesPerSecond[7];
   int		allowSmartMissiles;
   int		allowHeatSeekers;
   int		allowTorpedoes;
   int		allowNukes;
   int		allowClusters;
   int		allowModifiers;
   int		allowLaserModifiers;
   int		allowShipShapes;
   int		playersOnRadar;
   int		missilesOnRadar;
   int		minesOnRadar;
   int		nukesOnRadar;
   int		treasuresOnRadar;
   int		asteroidsOnRadar;
   int		distinguishMissiles;
   char		maxMissilesPerPack[7];
   char		maxMinesPerPack[7];
   int		identifyMines;
   int		shieldedItemPickup;
   int		shieldedMining;
   int		laserIsStunGun;
   char		nukeMinSmarts[7];
   char		nukeMinMines[7];
   char		nukeClusterDamage[20];
   char		mineFuseTime[20];
   char		mineLife[20];
   char		minMineSpeed[20];
   char		missileLife[20];
   char		baseMineRange[20];
   char		shotKillScoreMult[20];
   char		torpedoKillScoreMult[20];
   char		smartKillScoreMult[20];
   char		heatKillScoreMult[20];
   char		clusterKillScoreMult[20];
   char		laserKillScoreMult[20];
   char		tankKillScoreMult[20];
   char		runoverKillScoreMult[20];
   char		ballKillScoreMult[20];
   char		explosionKillScoreMult[20];
   char		shoveKillScoreMult[20];
   char		crashScoreMult[20];
   char		mineScoreMult[20];
   char		movingItemProb[20];
   char		randomItemProb[20];
   char		dropItemOnKillProb[20];
   char		detonateItemOnKillProb[20];
   char		destroyItemInCollisionProb[20];
   char		itemProbMult[20];
   char		cannonItemProbMult[20];
   char		maxItemDensity[20];
   char		asteroidProb[20];
   char		maxAsteroidDensity[20];
   char		itemConcentratorRadius[20];
   char		itemConcentratorProb[20];
   char		rogueHeatProb[20];
   char		rogueMineProb[20];
   char		itemEnergyPackProb[20];
   char		itemTankProb[20];
   char		itemECMProb[20];
   char		itemArmorProb[20];
   char		itemMineProb[20];
   char		itemMissileProb[20];
   char		itemCloakProb[20];
   char		itemSensorProb[20];
   char		itemWideangleProb[20];
   char		itemRearshotProb[20];
   char		itemAfterburnerProb[20];
   char		itemTransporterProb[20];
   char		itemMirrorProb[20];
   char		itemDeflectorProb[20];
   char		itemHyperJumpProb[20];
   char		itemPhasingProb[20];
   char		itemLaserProb[20];
   char		itemEmergencyThrustProb[20];
   char		itemTractorBeamProb[20];
   char		itemAutopilotProb[20];
   char		itemEmergencyShieldProb[20];
   char		initialFuel[20];
   char		initialTanks[20];
   char		initialArmor[20];
   char		initialECMs[20];
   char		initialMines[20];
   char		initialMissiles[20];
   char		initialCloaks[20];
   char		initialSensors[20];
   char		initialWideangles[20];
   char		initialRearshots[20];
   char		initialAfterburners[20];
   char		initialTransporters[20];
   char		initialMirrors[20];
   char		maxArmor[20];
   char		initialDeflectors[20];
   char		initialHyperJumps[20];
   char		initialPhasings[20];
   char		initialLasers[20];
   char		initialEmergencyThrusts[20];
   char		initialTractorBeams[20];
   char		initialAutopilots[20];
   char		initialEmergencyShields[20];
   char		maxFuel[20];
   char		maxTanks[20];
   char		maxECMs[20];
   char		maxMines[20];
   char		maxMissiles[20];
   char		maxCloaks[20];
   char		maxSensors[20];
   char		maxWideangles[20];
   char		maxRearshots[20];
   char		maxAfterburners[20];
   char		maxTransporters[20];
   char		maxMirrors[20];
   char		maxDeflectors[20];
   char		maxPhasings[20];
   char		maxHyperJumps[20];
   char		maxEmergencyThrusts[20];
   char		maxLasers[20];
   char		maxTractorBeams[20];
   char		maxAutopilots[20];
   char		maxEmergencyShields[20];
   char		gameDuration[7];
   int		allowViewing;
   char		friction[20];
   char		blockFriction[20];
   int		blockFrictionVisible;
   char		coriolis[7];
   char		checkpointRadius[20];
   char		raceLaps[7];
   int		lockOtherTeam;
   int		loseItemDestroys;
   int		useWreckage;
   char		maxOffensiveItems[20];
   char		maxDefensiveItems[20];
   char		roundDelay[7];
   char		maxRoundTime[20];
   char		roundsToPlay[20];

   int		cannonFlak;
   int		connectorIsString;
   int		ballCollisions;
   int		ballSparkCollisions;
   char		asteroidItemProb[20];
   char		asteroidMaxItems[20];
   int		teamShareScore;
   int		allowAlliances;
   int		announceAlliances;
   char		maxPauseTime[20];
   int		asteroidConcentratorVisible;
   char		asteroidConcentratorRadius[20];
   char		asteroidConcentratorProb[20];
   int		ballrace;
   int		ballraceConnected;
   char		mineShotDetonateDistance[20];
   char		cannonPoints[20];
   char		cannonMaxScore[20];
   int		cannonsDefend;
   char		asteroidPoints[20];
   char		asteroidMaxScore[20];
} xpmap_t;


typedef struct {
  char                   *name;
  char                   *value;
} charlie;

/*structures and macro's for tracking bases, checkpoints.
Used when finding the closest teams for treasure, target
ownership...etc*/
typedef struct {
    POINT	pos;
    u_short	team;
} base_t;
typedef struct {
    int NumBases;
    base_t	*base;
	int NumChecks;
	int checkpoints[26];
} World_map;

typedef struct undo_t {
   int                   x,y;
   char                  icon;
   BOOL			 rotate;
   struct undo_t         *next;
} undo_t;


typedef struct prefs_t {
   char                  *name;
   char					 *altname;
   int                   length, type;
   char                  *charvar;
   int                   *intvar;
   BOOL			output;
   int			id1;
} prefs_t;
typedef prefs_t* LPPREFSSTRUCT;

typedef struct {
	World_map MapItems;
	xpmap_t	MapStruct;
	undo_t  *undolist;
	prefs_t PrefsArray[NUMPREFS];
	int iVscrollMax, iHscrollMax, xcoord, ycoord; 
	int iHscrollPos, iVscrollPos, seldxbeg, seldybeg, seldxend, seldyend;
	BOOL fSelected;
	int strdx, strdy;
	POINT ptTempBeg, ptTempEnd;

} MAPDOCDATA, *LPMAPDOCDATA;
