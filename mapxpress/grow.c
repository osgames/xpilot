/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include                 "mapxpress.h"

int               grow_minx,grow_miny,grow_maxx,grow_maxy,
                         grow_w,grow_h,grow_centerx, grow_centery;
int               grow_filled = 0;

double                   grow_xa = 1.0, grow_ya = 1.0;
grow_t                   *grow = NULL;

//extern int mapicon_ptr[91];

double rfrac(void)
{
    /*
     * Return a pseudo-random value in the range { 0 <= x < 1 }.
     * Assume all RAND_MAXs are at least 32767 and divide by 32768.
     */
    return (double)((double)(rand() & 0x7FFF) * 0.000030517578125);
}
/***************************************************************************/
/* GrowMapArea                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose :  Majority of this code is the existing grow.c code from       */
/* xp-mapedit.                                                             */
/***************************************************************************/
int GrowMapArea(LPMAPDOCDATA lpMapDocData, BOOL selected)
{
   grow_t                *next, *delgrow;
   int                   i,j,growat;
   int                   angle;
   double                 x,y,dx,dy;
   double				 temp;
	
	
   RoundMapArea(lpMapDocData, selected);
   /* free grow structure */
   next = grow;
   while ( next != NULL) {
     delgrow = next->next;
     free(next);
     next = delgrow;
   }
   grow = NULL;

   grow_xa = grow_ya = 1.0;

   

	if (selected) /* area selected, do just this area */
	{
		SortSelectionArea(lpMapDocData);
		grow_minx = lpMapDocData->seldxbeg;
		grow_maxx = lpMapDocData->seldxend-1;
		grow_miny = lpMapDocData->seldybeg;
		grow_maxy = lpMapDocData->seldyend-1;
	}
	else /* no area selected, do entire map */
	{
         grow_minx = 0;
         grow_miny = 0;
         grow_maxx = lpMapDocData->MapStruct.width;
         grow_maxy = lpMapDocData->MapStruct.height;

	}

      grow_w = grow_maxx-grow_minx;
      grow_h = grow_maxy-grow_miny;
      grow_centerx = (grow_minx+grow_maxx)/2;
      grow_centery = (grow_miny+grow_maxy)/2;

      grow_filled = 0;
      for (i=grow_minx;i<grow_maxx;i++) {
         for (j=grow_miny;j<grow_maxy;j++) {
				if (!InsideMap(lpMapDocData, i, j))
					continue;
			 if (lpMapDocData->MapStruct.data[(int) i][(int) j].cdata != MAP_FILLED) {
               ;
            } else {
               grow_filled++;
               next = grow;
               grow = (grow_t *) malloc(sizeof(grow_t));
               grow->x = i;
               grow->y = j;
               grow->next = next;
            }
         }
      }

      /* place a square in the center if there are none */
      if (grow == NULL) {
         ChangeMapData(lpMapDocData, grow_centerx,grow_centery,MAP_FILLED,1);
         grow = (grow_t *) malloc(sizeof(grow_t));
         grow->x = grow_centerx;
         grow->y = grow_centery;
         grow->next = NULL;
         grow_filled=1;
         if (grow_w > grow_h) {
            grow_ya = ((double) grow_h)/ ((double) grow_w);
         } else {
            grow_xa = ((double) grow_w)/ ((double) grow_h);
         }
      }


   if (grow_filled > 1) {
      growat = rand() % (grow_filled-1);
      next = grow;
      while ( (next != NULL) && (growat != 0) ) {
         next = next->next;
         growat--;
      }
   } else {
      next = grow;
   }

   temp = rfrac() * 1000;
   angle = (int) temp % 1000;

   dx = grow_xa * cos ((2*3.14*angle/1000) );
   dy = grow_ya * sin ((2*3.14*angle/1000) );
   x = next->x + dx;
   y = next->y + dy;

   while (MapData(lpMapDocData, (int) x, (int) y) == MAP_FILLED) {
      x += dx;
      y += dy;
   }
   if (  ((int) x > grow_maxx) || ((int) y > grow_maxy) ||
        ((int) x < grow_minx) || ((int) y < grow_miny) ) {
      return 1;
   }

   ChangeMapData(lpMapDocData, (int) x,(int) y,MAP_FILLED,1);
   next = grow;
   grow = (grow_t *) malloc(sizeof(grow_t));
   grow->x = (int) x;
   grow->y = (int) y;
   grow->next = next;
   grow_filled++;

   return 0;
}
