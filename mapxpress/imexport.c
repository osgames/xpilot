/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/


#include "mapxpress.h"

/* Public variables
* ================
*/
int	xsize, ysize;
int	invert = 0;

/******************************************************************/
/* -USED IN LoadPbmFile-                                          */

/* The man-page for pbm says no line can exceed 70 chars (except, of
   course, the pixel-data in bit-packed mode, but we don't buffer
   that through 'buf' */
#define PBMSIZE 70
#define WHITESPACE " \t\r\n"
/******************************************************************/
/***************************************************************************/
/* LoadXbmFile                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   file                                                                  */
/* Purpose : Load a xbm file											   */
/***************************************************************************/
int LoadXbmFile(LPMAPDOCDATA lpMapDocData, char *file)
{
	FILE                  *fp;
	max_str_t             line;
	char				  *tmp;
	int                   bits,x=0,y=0;
	
	if ((fp = fopen(file, "r")) == NULL) {
		ErrorHandler("File Opening Error: %s", file);
		return 1;
	}
	fgets(line, sizeof(max_str_t), fp);
	tmp = strrchr(line, (int) ' ');
	if (tmp == NULL) return 1;
	tmp++;
	lpMapDocData->MapStruct.width = atoi(tmp);
	sprintf(lpMapDocData->MapStruct.width_str,"%d",lpMapDocData->MapStruct.width);
	
	fgets(line, sizeof(max_str_t), fp);
	tmp = strrchr(line, (int) ' ');
	if (tmp == NULL) return 1;
	tmp++;
	lpMapDocData->MapStruct.height = atoi(tmp);
	sprintf(lpMapDocData->MapStruct.height_str,"%d",lpMapDocData->MapStruct.height);
	
	while ( (fgets(line, sizeof(max_str_t), fp)) != 0 ) {
		tmp = strstr(line,"0x");
		while ( tmp != NULL) {
			tmp += 2;
			if ((int) tmp[0] > 96) {
				bits = ((int)(tmp[0])-87)*16;
			} else {
				bits = ((int)(tmp[0])-48)*16;
			}
			if ((int) tmp[1] > 96) {
				bits += (int)(tmp[1])-87;
			} else {
				bits += (int)(tmp[1])-48;
			}
			if ( (bits & 128) == 128) lpMapDocData->MapStruct.data[x+7][y].cdata = 'x';
			if ( (bits & 64) == 64) lpMapDocData->MapStruct.data[x+6][y].cdata = 'x';
			if ( (bits & 32) == 32) lpMapDocData->MapStruct.data[x+5][y].cdata = 'x';
			if ( (bits & 16) == 16) lpMapDocData->MapStruct.data[x+4][y].cdata = 'x';
			if ( (bits & 8) == 8) lpMapDocData->MapStruct.data[x+3][y].cdata = 'x';
			if ( (bits & 4) == 4) lpMapDocData->MapStruct.data[x+2][y].cdata = 'x';
			if ( (bits & 2) == 2) lpMapDocData->MapStruct.data[x+1][y].cdata = 'x';
			if ( (bits & 1) == 1) lpMapDocData->MapStruct.data[x][y].cdata = 'x';
			x += 8;
			if ( x>= lpMapDocData->MapStruct.width) {
				y++;
				x=0;
			}
			tmp = strstr(tmp,"0x");
		}
	}
	fclose(fp);
	return TRUE;
}

#define NEXT_TOKEN()							       \
    state++;								       \
    if (!(p = strtok(NULL, " \t\r\n")))					       \
break;

/***************************************************************************/
/* LoadPbmFile                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   file                                                                  */
/* Purpose : Load a pbm file                                               */
/* Ported from pbmtomap.c by Greg Renda (greg@ncd.com)                     */
/* and the 99/10/09 update to pbmtomap.c                                   */
/* by Ben Armstrong (synrg@sanctuary.nslug.ns.ca)                          */
/***************************************************************************/
int LoadPbmFile(LPMAPDOCDATA lpMapDocData, char *file)
{
	FILE                  *fp;
	int             state = 0, width, height, count;
    char            buf[PBMSIZE];
	char *p;
	int x, y;
    int bit_packed;
	int c, mask;
	
	
	if ((fp = fopen(file, "r")) == NULL) {
		ErrorHandler("File Opening Error: %s", file);
		return 1;
	}
	
	while (fgets(buf, PBMSIZE, fp))
    {
	/* Comments can only start at beginning of line.  I'm not sure
		if this is an accurate interpretation of the pbm spec. */
		p = strchr(buf, '#');
		if (p)
			*p = 0;
		
        /* Blank lines are ignored */
		if (!(p = strtok(buf, WHITESPACE)))
			continue;
		
			/* Each case is responsible for getting the next token before
			iterating thru the loop again, except in bit-packed mode,
			there can only be one whitespace char between height and the
			pixel data.  When all tokens are exhausted, we fall through
			to the outer loop to get another line, except in bit-packed
			mode which processes all remaining input and does not return. */
		
		
		while (p)
		{
			
			switch (state)
			{
			case 0:
				bit_packed = !strcmp(p, "P4");
				
				if (strcmp(p, "P1") && !bit_packed)
				{
					ErrorHandler("Unrecognized PBM format!");
				}
				NEXT_TOKEN();
			case 1:
				width = atoi(p);
				NEXT_TOKEN();
			case 2:
				height = atoi(p);
				lpMapDocData->MapStruct.width = width;
				sprintf(lpMapDocData->MapStruct.width_str,"%d",lpMapDocData->MapStruct.width);
				lpMapDocData->MapStruct.height = height;
				sprintf(lpMapDocData->MapStruct.height_str,"%d",lpMapDocData->MapStruct.height);
				count = 0;
				x = 0;
				y = 0;
				
				if (bit_packed)
					state++;
				else
				{
					NEXT_TOKEN();
				}
			default:
				if (bit_packed)
				{
				/* In bit-packed mode, there are no line-breaks,
				so all remaining chars in the input stream
				need to be read by process_bit_packed() and
					then we exit. */
					y=0;

					while ((c = fgetc(fp)) != EOF)
					{
						for (mask = 128 ; mask > 0 ; mask = mask >> 1)
						{
							if ((c & mask) == mask)
								lpMapDocData->MapStruct.data[x][y].cdata = 'x';
							else
								lpMapDocData->MapStruct.data[x][y].cdata = ' ';
							count++;
							x++;
							if (!(count % width))
							{
							/* end of row, skip over remaining bits
								to next char */
								y++;
								x = 0;
								break;
							}
						}
					}
					fclose(fp);
					return TRUE;
				}
				else
				{
				/* ASCII mode processes a single token and then
					iterates through the token loop again */
					
					while (*p)
					{
					/* Break output into 'width' wide rows, which is not
						necessarily at the end of a line of input */
						if (!(count % width))
							y++;
						x = 0;
						if (*p == '1')
						{
							lpMapDocData->MapStruct.data[x][y].cdata = 'x';
							count++;
							x++;
						}
						else if (*p == '0')
						{
							lpMapDocData->MapStruct.data[x][y].cdata = ' ';
							count++;
							x++;
						}
						p++;
					}
					
					
					NEXT_TOKEN();
				}
			}
		}
		
	}
	
	fclose(fp);
	return TRUE;
}
/***************************************************************************/
/* SaveXbmFile                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   pstrFileName                                                          */
/* Purpose : Save as a xbm file                                            */
/* Some code ported from maps2image.c by Andrew W. Scherpbier              */
/***************************************************************************/
int SaveXbmFile(LPMAPDOCDATA lpMapDocData, PSTR pstrFileName)
{
	FILE	*ofile;
	char		*image;
	int		x, y;
	int		i;
	int		value;
	char		*p;
	int	count, Xsize;	
	
	
	xsize = (lpMapDocData->MapStruct.width);
	
   	ysize = (lpMapDocData->MapStruct.height);
	
	image = (char *) malloc(xsize * ysize);
	memset(image, invert, xsize * ysize);
	
	/*
	*   Build the image
	*/
	build_image(lpMapDocData, image);
	
	if (NULL == (ofile = fopen (pstrFileName, "wb")))
	{
		ErrorHandler("Couldn't open the destination file: %s", pstrFileName);
		return FALSE ;
	}
	
	
	/*
	*   Output the image.  The output image is in xbm format.
	*/
	count = 0;
	Xsize = (xsize) & 0xfff8;
	
	fprintf(ofile, "#define maps_width %d\n#define maps_height %d\n", Xsize, ysize);
	fprintf(ofile, "static unsigned char maps_bits[] = {\n   ");
	p = image;
	value = 0;
	i = 0x80;
	for (y = 0; y < ysize; y++)
	{
		for (x = 0; x < Xsize; x++)
		{
			if (*p)
				value |= 0x80;
			i >>= 1;
			if (i == 0)
			{
				fprintf(ofile, "0x%02x, ", value & 0xff);
				count++;
				if (count >= 12)
				{
					count = 0;
					fprintf(ofile, "\n   ");
				}
				i = 0x80;
				value = 0;
			}
			value >>= 1;
			p++;
		}
		if (i != 0x80)
		{
			fprintf(ofile, "0x%02x, ", value & 0xff);
			count++;
			if (count >= 12)
			{
				count = 0;
				fprintf(ofile, "\n   ");
			}
			i = 0x80;
			value = 0;
		}
#if 0
		value = 0;
#endif
		p -= Xsize - xsize;
	}
	fprintf(ofile, "\n};\n");
	
	fclose(ofile);
	return FALSE;
}



/***************************************************************************/
/* SavePbmPlusFile                                                         */
/* Arguments :                                                             */
/*   file                                                                  */
/* Purpose : Save as a pbmplus file                                        */
/* Some code ported from maps2image.c by Andrew W. Scherpbier              */
/***************************************************************************/
int SavePbmPlusFile(LPMAPDOCDATA lpMapDocData, PSTR pstrFileName, int PBMPLUSTYPE)
{
	FILE	*ofile;
	char		*image;
	int x, y, numcolors;
	int		i;
	int		value;
	char		*p;
	
	numcolors = 255;

	
	xsize = (lpMapDocData->MapStruct.width);
	
   	ysize = (lpMapDocData->MapStruct.height);
	
	image = (char *) malloc(xsize * ysize);
	memset(image, invert, xsize * ysize);
	
	/*
	*   Build the image
	*/
	build_image(lpMapDocData, image);
	
	if (NULL == (ofile = fopen (pstrFileName, "wb")))
	{
		ErrorHandler("Couldn't open the destination file: %s", pstrFileName);
		return FALSE ;
	}
	
	
	/*
	*   Output the image.  The output image can be either pbm ppm or pgm
	*/
	switch (PBMPLUSTYPE)
	{
	case 0: /*PORTABLE BITMAP*/
		fprintf(ofile, "P4\n%d %d\n", xsize, ysize);
		p = image;
		for (y = 0; y < ysize; y++)
		{
			value = 0;
			i = 0x80;
			for (x = 0; x < xsize; x++)
			{
				if (*p)
					value |= i;
				i >>= 1;
				if (i == 0)
				{
					fprintf(ofile, "%c", value);
					i = 0x80;
					value = 0;
				}
				p++;
			}
			if (i != 0x80)
				fprintf(ofile, "%c", value);
			value = 0;
		}
	break;
	case 1: //PORTABLE PIXMAP SMALL SIZE NON BINARY
		fprintf(ofile, "P3\n%d %d\n%d\n", xsize, ysize,numcolors);
		for (y = 0; y < lpMapDocData->MapStruct.height; y++)
		{
			for (x = 0; x < lpMapDocData->MapStruct.width; x++)
			{
				switch (lpMapDocData->MapStruct.data[x][y].cdata)
				{
					//Walls are blue
				case 'q': case 'w': case 'x': case 'a':
				case 's': fprintf(ofile, "0 0 255");
						  break;
					//Gravities, currents, & wormholes are green
				case '+': case '-': case '<': case '>': case '@': case '(':
				case ')': case 'i': case 'j': case 'k': case 'm':
						  fprintf(ofile, "0 255 0");
						  break;
					//Fuels, targets, treasures, and item concentrators are red
				case '#': case '!': case '*': case '%': case '^':
						  fprintf(ofile, "255 0 0");
						  break;
				    //Bases and Cannons are white
				case 'd': case 'r': case 'c': case 'f':	case '_':
				case '1': case '2': case '3': case '4': case '5': case '6': 
				case '7': case '8': case '9': case '0': 
						  fprintf(ofile, "255 255 255");
						  break;
					//Deco are orange, so is friction
				case 't': case'y': case 'b': case 'g': case 'h': case 'z':
						  fprintf(ofile, "255 128 0"); 
						  break;

				default : fprintf(ofile, "0 0 0");
						  break;
				}
				fprintf(ofile, " ");
			}
			fprintf(ofile, "\n");
		}
	break;
	case 2: //PORTABLE PIXMAP SMALL SIZE BINARY
		fprintf(ofile, "P6\n%d %d\n%d\n", xsize, ysize,numcolors);
		for (y = 0; y < lpMapDocData->MapStruct.height; y++)
		{
			for (x = 0; x < lpMapDocData->MapStruct.width; x++)
			{
				switch (lpMapDocData->MapStruct.data[x][y].cdata)
				{
					//Walls are blue
				case 'q': case 'w': case 'x': case 'a':
				case 's': putc((char) 0, ofile);
						  putc((char) 0, ofile);
						  putc((char) 255, ofile);
						  break;
					//Gravities, currents, & wormholes are green
				case '+': case '-': case '<': case '>': case '@': case '(':
				case ')': case 'i': case 'j': case 'k': case 'm':
						  putc((char) 0, ofile);
						  putc((char) 255, ofile);
						  putc((char) 0, ofile);
						  break;
					//Fuels, targets, treasures, and item concentrators are red
				case '#': case '!': case '*': case '%': case '^':
						  putc((char) 255, ofile);
						  putc((char) 0, ofile);
						  putc((char) 0, ofile);
						  break;
				    //Bases and Cannons are white
				case 'd': case 'r': case 'c': case 'f':	case '_':
				case '1': case '2': case '3': case '4': case '5': case '6': 
				case '7': case '8': case '9': case '0': 
						  putc((char) 255, ofile);
						  putc((char) 255, ofile);
						  putc((char) 255, ofile);
						  break;
					//Deco are orange, so is friction
				case 't': case'y': case 'b': case 'g': case 'h': case 'z':
						  putc((char) 255, ofile);
						  putc((char) 128, ofile);
						  putc((char) 0, ofile);
						  break;

				default :
						  putc((char) 0, ofile);
						  putc((char) 0, ofile);
						  putc((char) 0, ofile);
						  break;
				}
			}
		}
	}
	
	fclose(ofile);
	return FALSE;
}
/***************************************************************************/
/* SaveXpmFile                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   pstrFileName                                                          */
/* Purpose :                                                               */
/***************************************************************************/
int SaveXpmFile(LPMAPDOCDATA lpMapDocData, PSTR pstrFileName)
{
	FILE *ofile = NULL;
	int x, y, numcolors;
	
	numcolors = 32;
	
	if (NULL == (ofile = fopen (pstrFileName, "wb")))
	{
		ErrorHandler("Couldn't write to file: %s", pstrFileName);
		return FALSE ;
	}
	
	/*Output the X11 pixmap header*/
	fprintf(ofile,"/* XPM */\n");
	fprintf(ofile,"static char *%s[] = {\n","xpilotmap");
	fprintf(ofile,"/* columns rows colors chars-per-pixel */\n");
	fprintf(ofile,"\"%d %d %d 1\",\n", lpMapDocData->MapStruct.width, lpMapDocData->MapStruct.height, numcolors);
	fprintf(ofile,"\" \tc #000000000000\",\n");
	
	/*Set up the colors & corresponding characters*/
	fprintf(ofile,"\"q\tc Blue\",\n");
	fprintf(ofile,"\"w\tc Blue\",\n");
	fprintf(ofile,"\"x\tc Blue\",\n");
	fprintf(ofile,"\"a\tc Blue\",\n");
	fprintf(ofile,"\"s\tc Blue\",\n");
	
	fprintf(ofile,"\"d\tc Yellow\",\n");
	fprintf(ofile,"\"r\tc Yellow\",\n");
	fprintf(ofile,"\"c\tc Yellow\",\n");
	fprintf(ofile,"\"f\tc Yellow\",\n");
	
	fprintf(ofile,"\"_\tc #000000000000\",\n");
	fprintf(ofile,"\"$\tc #000000000000\",\n");
	
	fprintf(ofile,"\"#\tc Red\",\n");
	fprintf(ofile,"\"!\tc Red\",\n");
	fprintf(ofile,"\"*\tc Red\",\n");
	fprintf(ofile,"\"%%\tc Red\",\n");
	fprintf(ofile,"\"^\tc Red\",\n");
	
	fprintf(ofile,"\"+\tc Green\",\n");
	fprintf(ofile,"\"-\tc Green\",\n");
	fprintf(ofile,"\"<\tc Green\",\n");
	fprintf(ofile,"\">\tc Green\",\n");
	fprintf(ofile,"\"@\tc Green\",\n");
	fprintf(ofile,"\")\tc Green\",\n");
	fprintf(ofile,"\"(\tc Green\",\n");
	
	fprintf(ofile,"\"i\tc Green\",\n");
	fprintf(ofile,"\"j\tc Green\",\n");
	fprintf(ofile,"\"k\tc Green\",\n");
	fprintf(ofile,"\"m\tc Green\",\n");
	
	fprintf(ofile,"\"t\tc Magenta\",\n");
	fprintf(ofile,"\"y\tc Magenta\",\n");
	fprintf(ofile,"\"b\tc Magenta\",\n");
	fprintf(ofile,"\"g\tc Magenta\",\n");
	fprintf(ofile,"\"h\tc Magenta\",\n");
	fprintf(ofile,"\"z\tc Magenta\",\n");
	fprintf(ofile,"/* pixels */\n");
	/*Output the image itself*/
	for (y = 0; y < lpMapDocData->MapStruct.height; y++)
	{
		fprintf(ofile, "\"");
		for (x = 0; x < lpMapDocData->MapStruct.width; x++)
			fprintf(ofile, "%c", lpMapDocData->MapStruct.data[x][y].cdata);
		fprintf(ofile, "\",\n");
	}
	fseek(ofile, -2L, SEEK_CUR);
	fprintf(ofile, "\n};");
	fclose(ofile);
	return FALSE;
}
/**************************************************************************
* static void build_image(char *image, int x, int y, FILE *fl)
*   Read a map and create the image for it. The image is placed
*   at location (x, y)
*/
static void build_image(LPMAPDOCDATA lpMapDocData, char *image)
{
	char	*p;
	int	j;
	
	
	p = convert_map(lpMapDocData);
	
	for (j = 0; j < lpMapDocData->MapStruct.height; j++)
	{
		memcpy(image, p, lpMapDocData->MapStruct.width);
		image += xsize;
		p += lpMapDocData->MapStruct.width;
	}
	
	
}

/**************************************************************************
* static char *convert_map(int width, int height, FILE *fl)
*   Create an image from map data.
*/
static char *convert_map(LPMAPDOCDATA lpMapDocData)
{
	int	x, y;
	int	value = 1;
	char	*output = (char *) malloc(lpMapDocData->MapStruct.width * lpMapDocData->MapStruct.height);
	
	
	value ^= invert;
	
	memset((char *) output, invert, lpMapDocData->MapStruct.width * lpMapDocData->MapStruct.height);
	
	
	for(y = 0; y < lpMapDocData->MapStruct.height; y++)
		for (x = 0; x < lpMapDocData->MapStruct.width; x++)
			if (strchr("xswqa#", lpMapDocData->MapStruct.data[x][y].cdata))
			{
				output[y * lpMapDocData->MapStruct.width + x] = lpMapDocData->MapStruct.data[x][y].cdata; 
			}
			
			return output;
}
