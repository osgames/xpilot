/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "mapxpress.h"

/*Should TransparentPaste affect functions like shifting, rotating, mirroring?
I don't know if this would be desirable in any situation, but if so, this should be defined*/
/*#define TRANSPASTEAFFECTS*/

/***************************************************************************/
/* DrawBlockLine                                                           */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwnd: Hangle to the window                                            */
/*   ptB: beginning point of line                                          */
/*   ptE: end point of line                                                */
/*   selected: current selected block                                      */
/*                                                                         */
/* Purpose : Translates current selected tool to mapicon, calls change to  */
/* mapdata, draws icon, for all the blocks along the current selected line.*/
/***************************************************************************/
void DrawBlockLine (LPMAPDOCDATA lpMapDocData, HWND hwnd, POINT ptB, POINT ptE, int selected)
{
	int vzoom = lpMapDocData->MapStruct.view_zoom;
	int deltax,deltay,sign,i,x2,y2, icon;
	BOOL ckpnting = FALSE;
	
	deltax = (ptE.x / vzoom + lpMapDocData->iHscrollPos) - (ptB.x / vzoom + lpMapDocData->iHscrollPos);
	deltay = (ptE.y / vzoom + lpMapDocData->iVscrollPos) - (ptB.y / vzoom + lpMapDocData->iVscrollPos);

	if  (selected == IDM_MAP_CHECKPOINT)
		ckpnting = TRUE;
	else 
		icon = selected;
	


	if ( (deltax == 0) && (deltay == 0) ) {
		if (ckpnting)
			icon = FindNextCheckPoint(lpMapDocData);
		SelectGrid(lpMapDocData, hwnd, ptB.x / vzoom + lpMapDocData->iHscrollPos, ptB.y / vzoom + lpMapDocData->iVscrollPos, icon, TRUE);
		return;
	}

	if (abs(deltax) >= abs(deltay))
	{
		sign = (deltax < 0) ? -1 : 1;
		for (i = 0; abs(i) <= abs(deltax); i += sign) {
			x2 = ptB.x / vzoom + lpMapDocData->iHscrollPos + i;
			y2 = ptB.y / vzoom + lpMapDocData->iVscrollPos + (i * deltay) / deltax;
			if (ckpnting)
				icon = FindNextCheckPoint(lpMapDocData);
			SelectGrid(lpMapDocData, hwnd, x2, y2, icon, TRUE);
		}
	} else {
		sign = (deltay < 0) ? -1 : 1;
		for (i = 0; abs(i) <= abs(deltay); i += sign) {
			x2 = ptB.x / vzoom + lpMapDocData->iHscrollPos + (i * deltax) / deltay ;
			y2 = ptB.y / vzoom + lpMapDocData->iVscrollPos + i;
			if (ckpnting)
				icon = FindNextCheckPoint(lpMapDocData);
			SelectGrid(lpMapDocData, hwnd, x2, y2, icon, TRUE);
		}
	}

}
/***************************************************************************/
/* DrawBlockCircle                                                         */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwnd: Hangle to the window                                            */
/*   ptB: beginning point of line                                          */
/*   ptE: end point of line                                                */
/*   selected: current selected block                                      */
/*                                                                         */
/* Purpose : Translates current selected tool to mapicon, calls change to  */
/* mapdata, draws for all the blocks along the current selected circle.    */
/***************************************************************************/
void DrawBlockCircle (LPMAPDOCDATA lpMapDocData, HWND hwnd, POINT ptB, POINT ptE, int selected)
{
	int vzoom = lpMapDocData->MapStruct.view_zoom;
	int startx, starty, x2, y2, icon;
	double deltax,deltay, radius, radius2;
	int scale = 1;

	BOOL ckpnting = FALSE;
	BOOL changeBool = FALSE;

	startx = (ptB.x / vzoom + lpMapDocData->iHscrollPos);
	starty = (ptB.y / vzoom + lpMapDocData->iVscrollPos);

	deltax = (ptE.x / vzoom + lpMapDocData->iHscrollPos) - startx;
	deltay = (ptE.y / vzoom + lpMapDocData->iVscrollPos) - starty;
	
	//This gives us the radius to check against.
	radius = sqrt(deltax*deltax + deltay*deltay);

	if  (selected == IDM_MAP_CHECKPOINT)
		ckpnting = TRUE;
	else 
		icon = selected;
	
	if ( radius == 0 ) {
		if (ckpnting)
			icon = FindNextCheckPoint(lpMapDocData);
		SelectGrid(lpMapDocData, hwnd, startx, starty, icon, TRUE);
		return;
	}
	

	for(x2 = startx - (int)radius; x2<= startx+(int)radius; x2++)
		for(y2 = starty - (int)radius; y2<= starty+(int)radius; y2++)
		{
			if (!InsideMap(lpMapDocData, x2, y2))
				continue;
			//The radius of the current block from the start point
			radius2 = sqrt(abs(startx-x2)*abs(startx-x2) + abs(starty-y2)*abs(starty-y2));
			if (iSelectionShape == IDM_CIRCLEEMPTY) //Empty circle
			{
				//If this block is on the desired radius, draw it
				if ((int) radius2 == (int) radius)
					changeBool=TRUE;
				else
					changeBool=FALSE;
			}
			else if (iSelectionShape == IDM_CIRCLEFILLED)//We want to draw a filled circle
			{
				//If this block is on or inside the desired radius...
				if ((int) radius2 <= (int) radius)
					changeBool=TRUE;
				else
					changeBool=FALSE;
			}
			if (changeBool)
			{
				if (ckpnting)
					icon = FindNextCheckPoint(lpMapDocData);
				SelectGrid(lpMapDocData, hwnd, x2, y2, icon, TRUE);
			}

		}

}
/***************************************************************************/
/* DrawBlockRectangle                                                      */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwnd: Hangle to the window                                            */
/*   ptB: beginning point of line                                          */
/*   ptE: end point of line                                                */
/*   selected: current selected block                                      */
/*                                                                         */
/* Purpose : Translates current selected tool to mapicon, calls change to  */
/* mapdata, draws for all the blocks along the current selected rect.      */
/***************************************************************************/
void DrawBlockRectangle(LPMAPDOCDATA lpMapDocData, HWND hwnd, POINT ptB, POINT ptE, int selected)
{
	int vzoom = lpMapDocData->MapStruct.view_zoom;
	int startx, starty, endx, endy, x, y, icon, temp;

	BOOL ckpnting = FALSE;
	BOOL changeBool = TRUE;

	startx = (ptB.x / vzoom + lpMapDocData->iHscrollPos);
	starty = (ptB.y / vzoom + lpMapDocData->iVscrollPos);

	endx = (ptE.x / vzoom + lpMapDocData->iHscrollPos);
	endy = (ptE.y / vzoom + lpMapDocData->iVscrollPos);

	if (endy < starty)
	{
		temp = endy;
		endy = starty;
		starty = temp;
	}
	if (endx < startx)
	{
		temp = endx;
		endx = startx;
		startx = temp;
	}

	/*We need to adjust the end points, because we have one block outside of the area
	we want selected, and the easiest way to correct this is to fix it here.*/
	endx += -1;
	endy += -1;

	
	if  (selected == IDM_MAP_CHECKPOINT)
		ckpnting = TRUE;
	else 
		icon = selected;
	
	if ( startx == endx && starty == endy ) {
		if (ckpnting)
			icon = FindNextCheckPoint(lpMapDocData);
		SelectGrid(lpMapDocData, hwnd, startx, starty, icon, TRUE);
		return;
	}
	

	for(x = startx; x <= endx; x++)
		for(y = starty; y <= endy; y++)
		{
			if (!InsideMap(lpMapDocData, x, y))
				continue;
			if (iSelectionShape == IDM_RECTEMPTY) //Empty Rectangle
			{
				//If this block is on the desired radius, draw it
				if ((x == startx) || (x == endx) || (y == starty) || (y == endy))
					changeBool=TRUE;
				else
					changeBool=FALSE;
			}
			if (changeBool)
			{
				if (ckpnting)
					icon = FindNextCheckPoint(lpMapDocData);
				SelectGrid(lpMapDocData, hwnd, x, y, icon, TRUE);
			}

		}

}
/***************************************************************************/
/* Fill Area                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwnd: Hangle to the window                                            */
/*   x: xcoord to start from                                               */
/*   y: ycoord to start from                                               */
/*   selected: current selected block                                      */
/*                                                                         */
/* Purpose : Translates current selected tool to mapicon, calls change to  */
/* mapdata, draws icon                                                     */
/***************************************************************************/
void FillArea(LPMAPDOCDATA lpMapDocData, HWND hwnd, int x, int y, int selected)
{
  char icon, old, ch;
  short temp_x, temp_y, hold_x, hold_y;

  if ((selected >= '0') && (selected <= '9'))
     {
     icon = selected;
     }
  switch (selected)
  {
	case IDM_MAP_REC_RD : 
	case IDM_MAP_REC_LD : 
	case IDM_MAP_FILLED : 
	case IDM_MAP_REC_RU : 
	case IDM_MAP_REC_LU : 
	case IDM_MAP_CAN_LEFT : 
	case IDM_MAP_CAN_UP : 
	case IDM_MAP_CAN_DOWN : 
	case IDM_MAP_CAN_RIGHT : 
	case IDM_MAP_BASE : 
	case IDM_MAP_BASE_ORNT :
	case IDM_MAP_FUEL : 
	case IDM_MAP_TARGET : 
	case IDM_MAP_TREASURE : 
	case IDM_MAP_ITEM_CONC : 
	case IDM_MAP_GRAV_POS :
	case IDM_MAP_GRAV_NEG : 
	case IDM_MAP_GRAV_ACWISE : 
	case IDM_MAP_GRAV_CWISE : 
	case IDM_MAP_WORM_NORMAL : 
	case IDM_MAP_WORM_OUT : 
	case IDM_MAP_WORM_IN : 
	case IDM_MAP_CRNT_UP : 
	case IDM_MAP_CRNT_LT : 
	case IDM_MAP_CRNT_RT : 
	case IDM_MAP_CRNT_DN : 
	case IDM_MAP_DEC_RD : 
	case IDM_MAP_DEC_LD : 
	case IDM_MAP_DEC_FLD : 
	case IDM_MAP_DEC_RU : 
	case IDM_MAP_DEC_LU : 
	case IDM_MAP_SPACE :
	case IDM_MAP_FRICTION:
	case IDM_MAP_EMPTYTREASURE:
	case IDM_MAP_ASTEROID_CONC:
		icon = mapicon_seg[selected].char_val;
		break;
	case IDM_MAP_CAN_UNSPEC:
		if (lpMapDocData->MapStruct.data[x][y].cdata == MAP_SPACE)
		{
			ch = MapData(lpMapDocData, x, y-1);
			if((ch == MAP_FILLED) || (ch == MAP_REC_LD) || (ch == MAP_REC_RD))
				icon = MAP_CAN_DOWN;
			else
			{
				ch = MapData(lpMapDocData, x+1, y);
				if((ch == MAP_FILLED) || (ch == MAP_REC_LD) || (ch == MAP_REC_LU))
					icon = MAP_CAN_LEFT;
				else
				{
					ch = MapData(lpMapDocData, x, y+1);
					if((ch == MAP_FILLED) || (ch == MAP_REC_RU) || (ch == MAP_REC_LU))
						icon = MAP_CAN_UP;
					else
					{
						ch = MapData(lpMapDocData, x-1, y);
						if((ch == MAP_FILLED) || (ch == MAP_REC_RD) || (ch == MAP_REC_RU))
							icon = MAP_CAN_RIGHT;
						else
							return;
					}
				}
			}
		}
		else
			return;
		break;
	case IDM_MAP_TEAMBASE:
		icon = selected;
		break;
	case IDM_MAP_CHECKPOINT:
		icon = MAP_SPACE;
		break;		
		
  }


/*Store the current block and coords*/
  old = lpMapDocData->MapStruct.data[x][y].cdata;
  if (icon == old)
	  return;

/*Change the current block*/
  ChangeMapData(lpMapDocData, x,y,icon,1);

/***************************/
/*Begin Recursively Filling*/
/*This is my own filling loop. It stores the previous location. then moves to the right, up,
/*down and left. If it cant move any farther in any direction....it moves back to the stored
/*location and tries in a different direction. All directions and possibilities should
/*eventually be tried...and we should end up all the way back where we started. If
/*anybody can come up with some more efficient code...by all means let me know cause
/*this block is pretty big.
/*JLM
/*jlmiller@ctitech.com
/***************************/
  temp_x = x;
  temp_y = y;

  while(TRUE)
  {
    if( (temp_x < lpMapDocData->MapStruct.width - 1) && lpMapDocData->MapStruct.data[temp_x+1][temp_y].cdata == old)
     {
     ChangeMapData(lpMapDocData, temp_x+1,temp_y,icon,1);
     lpMapDocData->MapStruct.data[temp_x+1][temp_y].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x+1][temp_y].backy = temp_y;
     temp_x++;
     continue;
     }
    if( (temp_x > 0) && lpMapDocData->MapStruct.data[temp_x-1][temp_y].cdata == old)
     {
     ChangeMapData(lpMapDocData, temp_x-1,temp_y,icon,1);
     lpMapDocData->MapStruct.data[temp_x-1][temp_y].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x-1][temp_y].backy = temp_y;
     temp_x--;
     continue;
     }
    if( (temp_y < lpMapDocData->MapStruct.height - 1) && lpMapDocData->MapStruct.data[temp_x][temp_y+1].cdata == old)
     {
     ChangeMapData(lpMapDocData, temp_x,temp_y+1,icon,1);
     lpMapDocData->MapStruct.data[temp_x][temp_y+1].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x][temp_y+1].backy = temp_y;
     temp_y++;
     continue;
     }
    if( (temp_y > 0) && lpMapDocData->MapStruct.data[temp_x][temp_y-1].cdata == old)
     {
     ChangeMapData(lpMapDocData, temp_x,temp_y-1,icon,1);
     lpMapDocData->MapStruct.data[temp_x][temp_y-1].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x][temp_y-1].backy = temp_y;
     temp_y--;
     continue;
     }

    if( (temp_x == lpMapDocData->MapStruct.width - 1 ) && (lpMapDocData->MapStruct.data[0][temp_y].cdata == old) && lpMapDocData->MapStruct.edgeWrap )
     {
     ChangeMapData(lpMapDocData, 0,temp_y,icon,1);
     lpMapDocData->MapStruct.data[0][temp_y].backx = temp_x;
     lpMapDocData->MapStruct.data[0][temp_y].backy = temp_y;
     temp_x = 0;
     continue;
     }
    if( (temp_x == 0) && (lpMapDocData->MapStruct.data[lpMapDocData->MapStruct.width-1][temp_y].cdata == old) && lpMapDocData->MapStruct.edgeWrap )
     {
     ChangeMapData(lpMapDocData, lpMapDocData->MapStruct.width-1,temp_y,icon,1);
     lpMapDocData->MapStruct.data[lpMapDocData->MapStruct.width-1][temp_y].backx = temp_x;
     lpMapDocData->MapStruct.data[lpMapDocData->MapStruct.width-1][temp_y].backy = temp_y;
     temp_x = lpMapDocData->MapStruct.width-1;
     continue;
     }
    if( (temp_y == lpMapDocData->MapStruct.height - 1) && (lpMapDocData->MapStruct.data[temp_x][0].cdata == old) && lpMapDocData->MapStruct.edgeWrap )
     {
     ChangeMapData(lpMapDocData, temp_x,0,icon,1);
     lpMapDocData->MapStruct.data[temp_x][0].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x][0].backy = temp_y;
     temp_y = 0;
     continue;
     }
    if( (temp_y == 0) && (lpMapDocData->MapStruct.data[temp_x][lpMapDocData->MapStruct.height-1].cdata == old) && lpMapDocData->MapStruct.edgeWrap )
     {
     ChangeMapData(lpMapDocData, temp_x,lpMapDocData->MapStruct.height-1,icon,1);
     lpMapDocData->MapStruct.data[temp_x][lpMapDocData->MapStruct.height-1].backx = temp_x;
     lpMapDocData->MapStruct.data[temp_x][lpMapDocData->MapStruct.height-1].backy = temp_y;
     temp_y = lpMapDocData->MapStruct.height-1;
     continue;
     }


    hold_x = lpMapDocData->MapStruct.data[temp_x][temp_y].backx;
    hold_y = lpMapDocData->MapStruct.data[temp_x][temp_y].backy;
    lpMapDocData->MapStruct.data[temp_x][temp_y].backx = VOIDBACK;
    lpMapDocData->MapStruct.data[temp_x][temp_y].backy = VOIDBACK;    
    temp_x = hold_x;
    temp_y = hold_y;
    if ( (temp_x == VOIDBACK) && (temp_y == VOIDBACK) )
      break;
  }
 /*End of filling loop!!*/


  /*Should we count up the checkpoints?*/
  if ((old >= 'A') && (old <= 'Z'))
   {
    CountCheckPoints(lpMapDocData);
   }

  /*Should we count up the bases?*/
  if  ( ((old >= '0') && (old <= '9')) && lpMapDocData->MapStruct.teamPlay)
   {
    CountBases(lpMapDocData);
    InvalidateRect(hwnd, NULL, TRUE);
   }

	

}
/***************************************************************************/
/* SelectGrid                                                              */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwnd: Hangle to the window                                            */
/*   x: xcoord                                                             */
/*   y: ycoord                                                             */
/*   selected: current selected block                                      */
/*   count: should we count bases?                                         */
/*                                                                         */
/* Purpose : Translates current selected tool to mapicon, calls change to  */
/* mapdata, draws icon                                                     */
/***************************************************************************/
void SelectGrid(LPMAPDOCDATA lpMapDocData, HWND hwnd, int x, int y, int selected, BOOL count)
{
	int vzoom = lpMapDocData->MapStruct.view_zoom;
	HFONT storeFont;
	HDC hdc;
	RECT rect;
	char icon, old, ch;
	int picnum=IDM_MAP_SPACE;

	if (!InsideMap(lpMapDocData, x, y))
		return;	
	
	if  ((toupper(selected) >= 'A') && (toupper(selected) <= 'Z'))
	{
		lpMapDocData->MapItems.checkpoints[toupper(selected)-65]++;
		icon = toupper((char)(selected));
		picnum = IDM_MAP_CHECKPOINT;
	}
	else 
 		if ((selected >= '0') && (selected <= '9'))
		{
			icon = selected;
			picnum = IDM_MAP_TEAMBASE;
		}
		else 
			switch (selected)
		{
		case IDM_MAP_REC_RD : 
		case IDM_MAP_REC_LD : 
		case IDM_MAP_FILLED : 
		case IDM_MAP_REC_RU : 
		case IDM_MAP_REC_LU : 
		case IDM_MAP_CAN_LEFT : 
		case IDM_MAP_CAN_UP : 
		case IDM_MAP_CAN_DOWN : 
		case IDM_MAP_CAN_RIGHT : 
		case IDM_MAP_BASE : 
		case IDM_MAP_BASE_ORNT :
		case IDM_MAP_FUEL : 
		case IDM_MAP_TARGET : 
		case IDM_MAP_TREASURE : 
		case IDM_MAP_ITEM_CONC : 
		case IDM_MAP_GRAV_POS :
		case IDM_MAP_GRAV_NEG : 
		case IDM_MAP_GRAV_ACWISE : 
		case IDM_MAP_GRAV_CWISE : 
		case IDM_MAP_WORM_NORMAL : 
		case IDM_MAP_WORM_OUT : 
		case IDM_MAP_WORM_IN : 
		case IDM_MAP_CRNT_UP : 
		case IDM_MAP_CRNT_LT : 
		case IDM_MAP_CRNT_RT : 
		case IDM_MAP_CRNT_DN : 
		case IDM_MAP_DEC_RD : 
		case IDM_MAP_DEC_LD : 
		case IDM_MAP_DEC_FLD : 
		case IDM_MAP_DEC_RU : 
		case IDM_MAP_DEC_LU : 
		case IDM_MAP_SPACE :
		case IDM_MAP_FRICTION:
		case IDM_MAP_EMPTYTREASURE:
		case IDM_MAP_ASTEROID_CONC:
			icon = mapicon_seg[selected].char_val;
			if (icon != MAP_SPACE)
				picnum = selected;
			break;
		case IDM_MAP_CAN_UNSPEC:
			if (lpMapDocData->MapStruct.data[x][y].cdata == MAP_SPACE)
			{
				ch = MapData(lpMapDocData, x, y-1);
				if((ch == MAP_FILLED) || (ch == MAP_REC_LD) || (ch == MAP_REC_RD))
				{
					icon = MAP_CAN_DOWN;
					picnum = IDM_MAP_CAN_DOWN;
				}
				else
				{
					ch = MapData(lpMapDocData, x+1, y);
					if((ch == MAP_FILLED) || (ch == MAP_REC_LD) || (ch == MAP_REC_LU))
					{
						icon = MAP_CAN_LEFT;
						picnum = IDM_MAP_CAN_LEFT;
					}
					else
					{
						ch = MapData(lpMapDocData, x, y+1);
						if((ch == MAP_FILLED) || (ch == MAP_REC_RU) || (ch == MAP_REC_LU))
						{
							icon = MAP_CAN_UP;
							picnum = IDM_MAP_CAN_UP;
						}
						else
						{
							ch = MapData(lpMapDocData, x-1, y);
							if((ch == MAP_FILLED) || (ch == MAP_REC_RD) || (ch == MAP_REC_RU))
							{
								icon = MAP_CAN_RIGHT;
								picnum = IDM_MAP_CAN_RIGHT;
							}
							else
								return;
						}
					}
				}
			}
			else
				return;
		}
		
		
		SaveUndoIcon(lpMapDocData, x,y,lpMapDocData->MapStruct.data[x][y].cdata,FALSE);
		
		hdc = GetDC(hwnd);
		if  (((toupper(icon) >= 'A') && (toupper(icon) <= 'Z')) ||
			((icon >= '0') && (icon <= '9')) ||
			(icon == MAP_TARGET) ||
			(icon == MAP_TREASURE) ||
			(icon == MAP_FUEL) ||
			(icon == MAP_BASE) || (icon == MAP_EMPTYTREASURE))
		{
			storeFont = GetZoomFont(hdc, lpMapDocData);
		}
		rect.left = (x-lpMapDocData->iHscrollPos)*vzoom;
		rect.top = (y-lpMapDocData->iVscrollPos)*vzoom;
		rect.right = (x-lpMapDocData->iHscrollPos)*vzoom + vzoom;
		rect.bottom = (y-lpMapDocData->iVscrollPos)*vzoom + vzoom;
		
		FillRect(hdc, &rect, (HBRUSH) GetStockObject (BLACK_BRUSH));
		old = lpMapDocData->MapStruct.data[x][y].cdata;
		
		ChangeMapData(lpMapDocData, x,y,icon,1);
		
		DrawMapPic(hdc, lpMapDocData, (x-lpMapDocData->iHscrollPos)*vzoom, (y-lpMapDocData->iVscrollPos)*vzoom, picnum, icon, vzoom, x, y);
		
		if  (((toupper(icon) >= 'A') && (toupper(icon) <= 'Z')) ||
			((icon >= '0') && (icon <= '9')) ||
			(icon == MAP_TARGET) ||
			(icon == MAP_TREASURE) ||
			(icon == MAP_FUEL) ||
			(icon == MAP_BASE) || (icon == MAP_EMPTYTREASURE))
		{
			DeleteZoomFont(hdc, storeFont);
		}
		
		
		if ( ((old == MAP_BASE_ORNT) || (icon == MAP_BASE_ORNT)) &&
			((x >= 0) && (x <= lpMapDocData->MapStruct.width-1)) &&
			((y >= 0) && (y <= lpMapDocData->MapStruct.height-1)) && !lpMapDocData->MapStruct.edgeWrap)
		{
			rect.left = (x-lpMapDocData->iHscrollPos)*vzoom-vzoom;
			rect.top = (y-lpMapDocData->iVscrollPos)*vzoom-vzoom;
			rect.right = (x-lpMapDocData->iHscrollPos)*vzoom + (2*vzoom);
			rect.bottom = (y-lpMapDocData->iVscrollPos)*vzoom + (2*vzoom);
			InvalidateRect(hwnd, &rect, TRUE);
		}
		else
			if ( ((old == MAP_BASE_ORNT) || (icon == MAP_BASE_ORNT)) &&
				((x >= 0) && (x <= lpMapDocData->MapStruct.width-1)) &&
				((y >= 0) && (y <= lpMapDocData->MapStruct.height-1)) )
				InvalidateRect(hwnd, NULL, TRUE);
			
			
			if  ( (((old >= '0') && (old <= '9')) || old == MAP_BASE) && count)
			{
				CountBases(lpMapDocData);
				if (lpMapDocData->MapStruct.teamPlay)
					InvalidateRect(hwnd, NULL, TRUE);
			}
			
			if  ((old >= 'A') && (old <= 'Z'))
			{
				CountCheckPoints(lpMapDocData);
			}
			
			ReleaseDC(hwnd,hdc);
			
}
/***************************************************************************/
/* ChangeMapData                                                           */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x: xcoord                                                             */
/*   y: ycoord                                                             */
/*   icon: the new character                                               */
/*   save: Store undo data?                                                */
/*                                                                         */
/* Purpose :  Changes data stored in map location x,y                      */
/***************************************************************************/
void ChangeMapData(LPMAPDOCDATA lpMapDocData, int x, int y, char icon, BOOL save)
{
	if ((x < 0) || (y < 0) || (x >= MAX_MAP_SIZE) || (y >= MAX_MAP_SIZE))
		return;
		

	if (lpMapDocData->MapStruct.data[x][y].cdata == icon)
		return;

	if (save)
		SaveUndoIcon(lpMapDocData, x,y,lpMapDocData->MapStruct.data[x][y].cdata,FALSE);

	lpMapDocData->MapStruct.data[x][y].cdata = icon;

	if (hwndMiniMap)
		UpdateSmallMap(lpMapDocData, x,y);
}
/***************************************************************************/
/* IncrementMapBlock                                                       */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x                                                                     */
/*   y                                                                     */
/*   dx                                                                    */
/*   dy                                                                    */
/*                                                                         */
/* Purpose : Switches block to next block of type on Right Mouse Click     */
/***************************************************************************/
void IncrementMapBlock(LPMAPDOCDATA lpMapDocData, HWND hwnd, int x, int y, int dx, int dy)
{
	int vzoom = lpMapDocData->MapStruct.view_zoom;
	HFONT storeFont;
	HDC hdc;
	RECT rect;
	char icon, old;
	int picnum=IDM_MAP_SPACE;
	int rotated = TRUE; /*We can rotate (toggle) to the next block in some cases.*/
	
	lpMapDocData->MapStruct.changed = TRUE;	
	
	hdc = GetDC(hwnd);
	rect.left = x;
	rect.top = y;
	rect.right = x + vzoom;
	rect.bottom = y + vzoom;
	
	switch(lpMapDocData->MapStruct.data[dx][dy].cdata)
	{
	case MAP_CAN_LEFT :
		icon = MAP_CAN_UP;	
		picnum = IDM_MAP_CAN_UP;
		break;
	case MAP_CAN_UP :
		icon = MAP_CAN_DOWN;
		picnum = IDM_MAP_CAN_DOWN;
		break;
	case MAP_CAN_DOWN :
		icon = MAP_CAN_RIGHT;
		picnum = IDM_MAP_CAN_RIGHT;
		break;
	case MAP_CAN_RIGHT :
		icon = MAP_CAN_LEFT;
		picnum = IDM_MAP_CAN_LEFT;
		break;
	case MAP_GRAV_CWISE :
		icon = MAP_GRAV_ACWISE;
		picnum = IDM_MAP_GRAV_ACWISE;
		break;
	case MAP_GRAV_ACWISE :
		icon = MAP_GRAV_CWISE;
		picnum = IDM_MAP_GRAV_CWISE;
		break;
	case MAP_GRAV_POS :
		icon = MAP_GRAV_NEG;
		picnum = IDM_MAP_GRAV_NEG;
		break;
	case MAP_GRAV_NEG :
		icon = MAP_GRAV_POS;
		picnum = IDM_MAP_GRAV_POS;
		break;
	case MAP_WORM_NORMAL :
		icon = MAP_WORM_OUT;
		picnum = IDM_MAP_WORM_OUT;
		break;
	case MAP_WORM_OUT :
		icon = MAP_WORM_IN;
		picnum = IDM_MAP_WORM_IN;
		break;
	case MAP_WORM_IN :
		icon = MAP_WORM_NORMAL;
		picnum = IDM_MAP_WORM_NORMAL;
		break;
	case MAP_CRNT_UP :		
		icon = MAP_CRNT_LT;
		picnum = IDM_MAP_CRNT_LT;
		break;
	case MAP_CRNT_LT :
		icon = MAP_CRNT_RT;
		picnum = IDM_MAP_CRNT_RT;
		break;
	case MAP_CRNT_RT :
		icon = MAP_CRNT_DN;
		picnum = IDM_MAP_CRNT_DN;
		break;
	case MAP_CRNT_DN :
		icon = MAP_CRNT_UP;
		picnum = IDM_MAP_CRNT_UP;
		break;
	case '0' :	
	case '1' :	
	case '2' :	
	case '3' :	
	case '4' :	
	case '5' :	
	case '6' :	
	case '7' :	
	case '8' :
		icon = lpMapDocData->MapStruct.data[dx][dy].cdata+1;
		picnum = IDM_MAP_TEAMBASE;
		break;
	case '9' :	icon = MAP_BASE;
		picnum = IDM_MAP_BASE;
		break;
		
	case MAP_BASE : icon = '0';
		picnum = IDM_MAP_TEAMBASE;
		break;
	default : rotated = FALSE;
		break;
	}
	
	/*Are we able to toggle? If so do it.*/
	if (rotated)
	{
		if  ( ((lpMapDocData->MapStruct.data[dx][dy].cdata >= 'A') && (lpMapDocData->MapStruct.data[dx][dy].cdata <= 'Z')) || ((lpMapDocData->MapStruct.data[dx][dy].cdata >= '0') && (lpMapDocData->MapStruct.data[dx][dy].cdata <= '9')) || (lpMapDocData->MapStruct.data[dx][dy].cdata == MAP_BASE) )
		{
			storeFont = GetZoomFont(hdc, lpMapDocData);
		}
		FillRect(hdc, &rect, (HBRUSH) GetStockObject (BLACK_BRUSH));
		
		old = lpMapDocData->MapStruct.data[dx][dy].cdata;
		ChangeMapData(lpMapDocData, dx,dy,icon,1);
		DrawMapPic(hdc, lpMapDocData, x, y, picnum, lpMapDocData->MapStruct.data[dx][dy].cdata, vzoom, x, y);
		
		if  (((lpMapDocData->MapStruct.data[dx][dy].cdata >= 'A') && (lpMapDocData->MapStruct.data[dx][dy].cdata <= 'Z')) || ((lpMapDocData->MapStruct.data[dx][dy].cdata >= '0') && (lpMapDocData->MapStruct.data[dx][dy].cdata <= '9')))
		{
			DeleteZoomFont(hdc, storeFont);
		}

		if  ( ((old >= '0') && (old <= '9')) || (old == MAP_BASE) )
		{
			CountBases(lpMapDocData);
			InvalidateRect(hwnd, NULL, TRUE);
		}
	}
	

	ReleaseDC(hwnd,hdc);
	
}
/***************************************************************************/
/* RotateMap                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose : Rotates the map 90 degrees clockwise                          */
/***************************************************************************/
void RotateMap(LPMAPDOCDATA lpMapDocData, BOOL selected)
{
	char icon;
	int x,y,i,j;
	int xbegin, xend, ybegin, yend, xlen, ylen, temp,
		xcent, ycent;
	BOOL base;

	lpMapDocData->MapStruct.changed = TRUE;
	
	EmptyTempData();
		
	if (selected)
	{
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		xlen = lpMapDocData->seldxend - lpMapDocData->seldxbeg;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;
		ylen = lpMapDocData->seldyend - lpMapDocData->seldybeg;
		
	}
	else
	{
		xbegin = 0;
		xend = lpMapDocData->MapStruct.width;
		ybegin = 0;
		yend = lpMapDocData->MapStruct.height;
	}
	
	/*fill the clipdata*/
	for (x = xbegin, i = 0; x < xend; x++, i++)
		for (y = ybegin, j = 0; y < yend; y++, j++)
		{
			if (!InsideMap(lpMapDocData, x, y))
				continue;
			clipdata[i][j] = lpMapDocData->MapStruct.data[x][y].cdata;
		}			

	/*empty the square*/
	for(x = xbegin; x < xend; x++)
		for(y = ybegin; y < yend; y++)
		{
			if (!InsideMap(lpMapDocData, x, y))
				continue;
			if (lpMapDocData->MapStruct.data[x][y].cdata >= '0' && lpMapDocData->MapStruct.data[x][y].cdata <= '9')
				base = TRUE; 
			ChangeMapData(lpMapDocData, x,y,MAP_SPACE,1);
		}
		
	if (!selected) 
		if (lpMapDocData->MapStruct.width != lpMapDocData->MapStruct.height)
		{
			SaveUndoIcon(lpMapDocData,0,0,'x',TRUE);
			SwapMapSize(lpMapDocData);
			temp = xend;
			xend = yend;
			yend = temp;
		}

	/*If this is a selected area, then it may not be a perfect square...we need
	to be able to rotate rectangles as well, so find the center of the selected area,
	and position from there.*/
	if (selected)
	{
		xcent = xbegin + xlen/2;
		ycent = ybegin + ylen/2;
		xbegin = xcent - ylen/2;
		ybegin = ycent - xlen/2;
		xend = xbegin + ylen;
		yend = ybegin + xlen;
		lpMapDocData->seldxend = xend;
		lpMapDocData->seldxbeg = xbegin;
		lpMapDocData->seldyend = yend;
		lpMapDocData->seldybeg = ybegin;
	}

	/* rotate the clipdata and refill the main map*/
	for (x = xbegin, i = xend-xbegin-1; x < xend; x++, i--)
		for (y = ybegin, j=0; y < yend; y++, j++)
		{
			switch(clipdata[j][i])
			{
			case MAP_REC_RD :
				icon = MAP_REC_LD;
				break;
			case MAP_REC_LD :
				icon = MAP_REC_LU;
				break;
			case MAP_REC_LU :
				icon = MAP_REC_RU;
				break;
			case MAP_REC_RU :
				icon = MAP_REC_RD;
				break;
			case MAP_CAN_LEFT :
				icon = MAP_CAN_UP;
				break;
			case MAP_CAN_UP :
				icon = MAP_CAN_RIGHT;
				break;
			case MAP_CAN_RIGHT :
				icon = MAP_CAN_DOWN;
				break;
			case MAP_CAN_DOWN : 									
				icon = MAP_CAN_LEFT;
				break;
			case MAP_CRNT_UP :
				icon = MAP_CRNT_RT;
				break;
			case MAP_CRNT_RT :
				icon = MAP_CRNT_DN;
				break;
			case MAP_CRNT_DN :
				icon = MAP_CRNT_LT;
				break;
			case MAP_CRNT_LT :
				icon = MAP_CRNT_UP;
				break;
			case MAP_DEC_RD :
				icon = MAP_DEC_LD;
				break;
			case MAP_DEC_LD :
				icon = MAP_DEC_LU;
				break;
			case MAP_DEC_LU :
				icon = MAP_DEC_RU;
				break;
			case MAP_DEC_RU :
				icon = MAP_DEC_RD;
				break;
			default :
				icon = clipdata[j][i];
				break;
				
			}
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (icon == MAP_SPACE)
					continue;
#endif
			ChangeMapData(lpMapDocData, x,y, icon, 1);
		}
	
		if (base)
			CountBases(lpMapDocData);

	return;
}
/***************************************************************************/
/* MirrorMap                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose :   Mirrors the map, horizontally or vertically                 */
/***************************************************************************/
void MirrorMap(LPMAPDOCDATA lpMapDocData, BOOL selected, BOOL direct)
{
	char icon;
	int x,y,i,j;
	int xbegin, xend, ybegin, yend, ibegin, iinc, jbegin, jinc;
	BOOL base;
	
	lpMapDocData->MapStruct.changed = TRUE;
	
	EmptyTempData();

	if (selected)
	{
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;
	}
	else
	{
		xbegin = 0;
		xend = lpMapDocData->MapStruct.width;
		ybegin = 0;
		yend = lpMapDocData->MapStruct.height;
	}

	/*fill the clipdata*/
	for (x = xbegin, i = 0; x < xend; x++, i++)
		for (y = ybegin, j = 0; y < yend; y++, j++)
		{
				if (!InsideMap(lpMapDocData, x, y))
					continue;
			clipdata[i][j] = lpMapDocData->MapStruct.data[x][y].cdata;
		}
		/* mirror the clipdata and refill the main map*/
	

	//We start from different places depending on the direction we are
	//mirroring.
	if (direct)
	{
		ibegin = 0;
		iinc = 1;
		jbegin = yend-ybegin-1;
		jinc = -1;
	}
	else
	{
		ibegin = xend-xbegin-1;
		iinc = -1;
		jbegin = 0;
		jinc = +1;
	}

		

	for (x = xbegin, i = ibegin; x < xend; x++, i=i+iinc)
		for (y = ybegin, j = jbegin; y < yend; y++, j=j+jinc)
		{
			switch(clipdata[i][j])
			{
				case MAP_REC_RD :
					icon = direct ? MAP_REC_RU : MAP_REC_LD;
					break;
				case MAP_REC_LD :
					icon = direct ? MAP_REC_LU : MAP_REC_RD;
					break;
				case MAP_REC_LU :
					icon = direct ? MAP_REC_LD : MAP_REC_RU;
					break;
				case MAP_REC_RU :
					icon = direct ? MAP_REC_RD : MAP_REC_LU;
					break;
				case MAP_CAN_UP :
					icon = direct ? MAP_CAN_DOWN : MAP_CAN_UP;
					break;
				case MAP_CAN_DOWN :
					icon = direct ? MAP_CAN_UP : MAP_CAN_DOWN;
					break;
				case MAP_CAN_LEFT :
					icon = direct ? MAP_CAN_LEFT : MAP_CAN_RIGHT;
					break;
				case MAP_CAN_RIGHT :
					icon = direct ? MAP_CAN_RIGHT : MAP_CAN_LEFT;
					break;
				case MAP_CRNT_UP :
					icon = direct ? MAP_CRNT_DN : MAP_CRNT_UP;
					break;
				case MAP_CRNT_DN :
					icon = direct ? MAP_CRNT_UP : MAP_CRNT_DN;
					break;
				case MAP_CRNT_RT :
					icon = direct ? MAP_CRNT_RT : MAP_CRNT_LT;
					break;
				case MAP_CRNT_LT :
					icon = direct ? MAP_CRNT_LT : MAP_CRNT_RT;
					break;
				case MAP_DEC_RD :
					icon = direct ? MAP_DEC_RU : MAP_DEC_LD;
					break;
				case MAP_DEC_LD :
					icon = direct ? MAP_DEC_LU : MAP_DEC_RD;
					break;
				case MAP_DEC_LU :
					icon = direct ? MAP_DEC_LD : MAP_DEC_RU;
					break;
				case MAP_DEC_RU :
					icon = direct ? MAP_DEC_RD : MAP_DEC_LU;
					break;
				default :
					icon = clipdata[i][j];
					break;
			}
			if (icon >= '0' && icon <= '9')
			  base = TRUE; 
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (icon == MAP_SPACE)
					continue;
#endif
			ChangeMapData(lpMapDocData, x,y, icon, 1);
		}

	
	if (base)
		CountBases(lpMapDocData);

	return;
}

/***************************************************************************/
/* Cycle Map Horizontal                                                    */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   direction: direction to shift.                                        */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose :   Cycles  the map horizontally                                */
/***************************************************************************/
void CycleMapHorizontal(LPMAPDOCDATA lpMapDocData, BOOL direction, BOOL selected)
{
	int x,y,i,j;
	int xbegin, xend, ybegin, yend;
	BOOL base;
	
	lpMapDocData->MapStruct.changed = TRUE;
	
	EmptyTempData();
		
	if (selected)
	{
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;
	}
	else
	{
		xbegin = 0;
		xend = lpMapDocData->MapStruct.width;
		ybegin = 0;
		yend = lpMapDocData->MapStruct.height;
	}
	
	/*fill the clipdata map*/
	for (x = xbegin, i = 0; x < xend; x++, i++)
		for (y = ybegin, j = 0; y < yend; y++, j++)
		{
				if (!InsideMap(lpMapDocData, x, y))
					continue;
			if (lpMapDocData->MapStruct.data[x][y].cdata >= '0' && lpMapDocData->MapStruct.data[x][y].cdata <= '9')
				base = TRUE; 
			clipdata[i][j] = lpMapDocData->MapStruct.data[x][y].cdata;
		}
		
	/*If "direction" is true scroll to the left*/
	if (direction)
	{
		/* cycle the clipdata and refill the main map*/
		for (x = xbegin, i = 1; x < xend; x++, i++)
			for (y = ybegin, j = 0; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
			
			x = xend-1;
			i = 0;
			for (y = ybegin, j = 0; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
	}
	/*otherwise scroll to the right*/
	else
	{
		/* cycle the clipdata and refill the main map*/
		for (x = xbegin+1, i = 0; x < xend; x++, i++)
			for (y = ybegin, j = 0; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
			x = xbegin;
			i = xend-xbegin-1;
			for (y = ybegin, j = 0; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
	}  

	
	if (base)   
		CountBases(lpMapDocData);

	return;
}
/***************************************************************************/
/* Cycle Map Vertical                                                      */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   direction: direction to shift.                                        */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose :   Cycles  the map vertically                                  */
/***************************************************************************/
void CycleMapVertical(LPMAPDOCDATA lpMapDocData, BOOL direction, BOOL selected)
{
	int x,y,i,j;
	int xbegin, xend, ybegin, yend;
	BOOL base;
	
	lpMapDocData->MapStruct.changed = TRUE;
	
	EmptyTempData();
		
	if (selected)
	{
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;
	}
	else
	{
		xbegin = 0;
		xend = lpMapDocData->MapStruct.width;
		ybegin = 0;
		yend = lpMapDocData->MapStruct.height;
	}
	
	/*fill the clipdata map*/
	for (x = xbegin, i = 0; x < xend; x++, i++)
		for (y = ybegin, j = 0; y < yend; y++, j++)
		{
				if (!InsideMap(lpMapDocData, x, y))
					continue;
			if (lpMapDocData->MapStruct.data[x][y].cdata >= '0' && lpMapDocData->MapStruct.data[x][y].cdata <= '9')
				base = TRUE; 
			clipdata[i][j] = lpMapDocData->MapStruct.data[x][y].cdata;
		}
		
	/*if "direction" is true, scroll upwards*/
	if (direction)
	{
		/* rotate the clipdata and refill the main map*/
		for (x = xbegin, i = 0; x < xend; x++, i++)
			for (y = ybegin, j = 1; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
			
		y = yend-1;
		j = 0;
		for (x = xbegin, i = 0; x < xend; x++, i++)
		{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
			ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
		}
	}
	/*otherwise scroll downwards*/
	else
	{
		/* rotate the clipdata and refill the main map*/
		for (x = xbegin, i = 0; x < xend; x++, i++)
			for (y = ybegin+1, j = 0; y < yend; y++, j++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}

			y = ybegin;
			j = yend-ybegin-1;
			for (x = xbegin, i = 0; x < xend; x++, i++)
			{
#ifdef TRANSPASTEAFFECTS
			if (TransPaste)
				if (clipdata[i][j] == MAP_SPACE)
					continue;
#endif
				ChangeMapData(lpMapDocData, x,y,clipdata[i][j],1);
			}
	}

	
	if (base)   
		CountBases(lpMapDocData);
	
	return;
}
/***************************************************************************/
/* NegativeMapArea                                                         */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   selected: is an area selected?                                        */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
void NegativeMapArea(LPMAPDOCDATA lpMapDocData, BOOL selected)
{
	int i,j;
	int xbegin, xend, ybegin, yend;
	char ch;
 
	lpMapDocData->MapStruct.changed = TRUE;
	
	if (selected)
	{
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;
	}
	else
	{
		xbegin = 0;
		xend = lpMapDocData->MapStruct.width;
		ybegin = 0;
		yend = lpMapDocData->MapStruct.height;
	}

	for (i=xbegin;i<xend;i++)
		for(j=ybegin;j<yend;j++)
		{
			if (!InsideMap(lpMapDocData, i, j))
				continue;
			switch(lpMapDocData->MapStruct.data[i][j].cdata)
			{		
            case MAP_SPACE:	ch = MAP_FILLED;
				break;				
            case MAP_FILLED: ch = MAP_SPACE;
				break;				
            case MAP_REC_RD: ch = MAP_REC_LU;
				break;				
            case MAP_REC_LU: ch = MAP_REC_RD;
				break;				
            case MAP_REC_LD: ch = MAP_REC_RU;
				break;				
            case MAP_REC_RU: ch = MAP_REC_LD;
				break;
			default: continue;
			}
			ChangeMapData(lpMapDocData, i,j,ch,1);
		}

}
/***************************************************************************/
/* SaveUndoIcon                                                            */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x: xcoord                                                             */
/*   y: ycoord                                                             */
/*   icon: the old character                                               */
/*   rotate: Are we rotating?                                              */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
int SaveUndoIcon(LPMAPDOCDATA lpMapDocData, int x, int y, char icon, BOOL rotate)
{
	struct undo_t        *undo = NULL;
	
	undo = (struct undo_t *) lpMapDocData->undolist;
	lpMapDocData->undolist = (undo_t *) malloc(sizeof(undo_t) );
	lpMapDocData->undolist->next = undo;
	lpMapDocData->undolist->x = x;
	lpMapDocData->undolist->y = y;
	lpMapDocData->undolist->icon = icon;
	lpMapDocData->undolist->rotate = rotate;
	return 0;
}

/***************************************************************************/
/* Undo                                                                    */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
int Undo(LPMAPDOCDATA lpMapDocData)
{
	undo_t                *traverse;
	char old;
	BOOL basechanged=FALSE;
	
	/* if the first icon is a breakpoint, skip it */
	if (lpMapDocData->undolist != NULL)
	{
		if (lpMapDocData->undolist->icon == '\n')
		{
			traverse = (undo_t *) lpMapDocData->undolist->next;
			free(lpMapDocData->undolist);
			lpMapDocData->undolist = traverse;
		}
	}


	while ( lpMapDocData->undolist != NULL )
	{
		/* check if we are at a breakpoint */
		if ( lpMapDocData->undolist->icon == '\n' )
		{
			break;
		}
		if ( lpMapDocData->undolist->rotate == TRUE)
		{
			SwapMapSize(lpMapDocData);
		}
		old = lpMapDocData->MapStruct.data[lpMapDocData->undolist->x][lpMapDocData->undolist->y].cdata;
		ChangeMapData(lpMapDocData, lpMapDocData->undolist->x, lpMapDocData->undolist->y, lpMapDocData->undolist->icon,0);
		if  ( lpMapDocData->MapStruct.teamPlay && (((old >= '0') && (old <= '9')) || ((lpMapDocData->undolist->icon >= '0') &&  (lpMapDocData->undolist->icon <= '9'))) )
		{
			basechanged=TRUE;
		}
		traverse = (undo_t *) lpMapDocData->undolist->next;
		free(lpMapDocData->undolist);
		lpMapDocData->undolist = traverse;
	}
	if (basechanged)
		CountBases(lpMapDocData);
	return 0;
}
/***************************************************************************/
/* ClearUndo                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
void ClearUndo(LPMAPDOCDATA lpMapDocData)
{
	/* insert a breakpoint */
	SaveUndoIcon(lpMapDocData, 0,0,'\n',FALSE);
}
/***************************************************************************/
/* VoidUndo                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
void VoidUndo(LPMAPDOCDATA lpMapDocData)
{
	/* insert a nullpoint */
	lpMapDocData->undolist = NULL;
}
/***************************************************************************/
/* SwapMapSize                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
void SwapMapSize(LPMAPDOCDATA lpMapDocData)
{
	int temp2;
	
	temp2 = lpMapDocData->MapStruct.width;
	lpMapDocData->MapStruct.width = lpMapDocData->MapStruct.height;
	lpMapDocData->MapStruct.height = temp2;
	itoa(lpMapDocData->MapStruct.width, lpMapDocData->MapStruct.width_str, 10);
	itoa(lpMapDocData->MapStruct.height, lpMapDocData->MapStruct.height_str, 10);
}
/***************************************************************************/
/* SortSelectionArea                                                       */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
void SortSelectionArea(LPMAPDOCDATA lpMapDocData)
{
	int temp;
	
	if (lpMapDocData->seldxbeg > lpMapDocData->seldxend)
	{
		temp = lpMapDocData->seldxbeg;
		lpMapDocData->seldxbeg = lpMapDocData->seldxend;
		lpMapDocData->seldxend = temp; 
	}
	if (lpMapDocData->seldybeg > lpMapDocData->seldyend)
	{
		temp = lpMapDocData->seldybeg;
		lpMapDocData->seldybeg = lpMapDocData->seldyend;
		lpMapDocData->seldyend = temp; 
	}
}
/***************************************************************************/
/* char EmptyTempData                                                      */
/* Arguments :                                                             */
/*   none                                                                  */
/*                                                                         */
/* Purpose :  Clean out the temporary array used for rotating, mirroring.. */
/* shifting, etc.                                                          */
/***************************************************************************/
void EmptyTempData()
{
	int x,y;
	
	for(x = 0; x < MAX_MAP_SIZE; x++)
		for(y = 0; y < MAX_MAP_SIZE; y++)
			clipdata[x][y] = MAP_SPACE;
}
/***************************************************************************/
/* void dragArea                                                           */
/* Arguments :                                                             */
/*   lpMapDocData: Pointer to map document                                 */
/*   transx: Distance to translate in x direction                          */
/*   transy: Distance to translate in y direction                          */
/*   drop: Drop at the specified translocate distance                      */
/*                                                                         */
/* Purpose : Drag & drop a specified area                                  */
/***************************************************************************/
void dragArea(LPMAPDOCDATA lpMapDocData, int transx, int transy, BOOL drop)
{
	int x,y,i,j;
	int xbegin, xend, ybegin, yend;
	BOOL base=FALSE;

	/*If we're not dropping the data, we must be storing it in the
	clipdata array.*/
	if (!drop)
	{
		EmptyTempData();
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;

			/*fill the clipdata map*/
	for (x = xbegin, i = 0; x < xend; x++, i++)
		for (y = ybegin, j = 0; y < yend; y++, j++)
		{
				if (!InsideMap(lpMapDocData, x, y))
					continue;
			clipdata[i][j] = lpMapDocData->MapStruct.data[x][y].cdata;
		}
		return;
	}

	/*We are dropping the data, so don't store anything.
	Make sure we aren't off the left or top of the map.*/
	//7/00 Commented code that kept us from dragging selections off edge of map.
	if (drop)
	{
		if (transx < 0)
		{
//			if (lpMapDocData->seldxbeg + transx < 0)
//			{
//				xbegin = 0;
//				xend = 0 + (lpMapDocData->seldxend-lpMapDocData->seldxbeg);
//			}
//			else
//			{
				xbegin = lpMapDocData->seldxbeg+transx;
				xend = lpMapDocData->seldxend+transx;
//			}
		}
		else
		{
//			if (lpMapDocData->seldxend + transx > lpMapDocData->MapStruct.width)
//			{
//				xbegin = lpMapDocData->MapStruct.width - (lpMapDocData->seldxend-lpMapDocData->seldxbeg);
//				xend = lpMapDocData->MapStruct.width;
//			}
//			else
//			{
				xbegin = lpMapDocData->seldxbeg+transx;
				xend = lpMapDocData->seldxend+transx;
//			}
		}


		if (transy < 0)
		{
//			if (lpMapDocData->seldybeg + transy < 0)
//			{
//				ybegin = 0;
//				yend = 0 + (lpMapDocData->seldyend-lpMapDocData->seldybeg);
//			}
//			else
//			{
				ybegin = lpMapDocData->seldybeg+transy;
				yend = lpMapDocData->seldyend+transy;
//			}
		}
		else
		{
//			if (lpMapDocData->seldyend + transy > lpMapDocData->MapStruct.height)
//			{
//				ybegin = lpMapDocData->MapStruct.height - (lpMapDocData->seldyend-lpMapDocData->seldybeg);
//				yend = lpMapDocData->MapStruct.height;
//			}
//			else
//			{
				ybegin = lpMapDocData->seldybeg+transy;
				yend = lpMapDocData->seldyend+transy;
//			}
		}

		for(x = lpMapDocData->seldxbeg; x < lpMapDocData->seldxend; x++)
			for(y = lpMapDocData->seldybeg; y < lpMapDocData->seldyend; y++)
			{
				ChangeMapData(lpMapDocData,x,y,MAP_SPACE,1);
			}

		for(x = xbegin, i = 0; x < xend; x++, i++)
			for(y = ybegin, j = 0; y < yend; y++, j++)
			{
				if (!InsideMap(lpMapDocData, x, y))
					continue;

				if ( (lpMapDocData->MapStruct.data[x][y].cdata >= '0') && (lpMapDocData->MapStruct.data[x][y].cdata <= '9') )
					base = TRUE;
				
				if ( (clipdata[i][j] >= '0') && (clipdata[i][j] <= '9') )
					base = TRUE;
					
				switch (TransPaste)
				{
				case FALSE :  ChangeMapData(lpMapDocData,x,y,clipdata[i][j],1);
						break;
				case TRUE  :	if (clipdata[i][j] == MAP_SPACE)
									break;
								else
								{
								ChangeMapData(lpMapDocData,x,y,clipdata[i][j],1);
								break;
								}
				}
			}
		lpMapDocData->seldxbeg = xbegin;
		lpMapDocData->seldxend = xend;
		lpMapDocData->seldybeg = ybegin;
		lpMapDocData->seldyend = yend;
	}

	if (base)   
		CountBases(lpMapDocData);

}
/***************************************************************************/
/* ClearMap                                                                */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   selected: do we have an area selected?                                */
/*   crop: are we cropping around an area?                                 */
/*                                                                         */
/* Purpose :   clear out the mapdata                                       */
/***************************************************************************/
void ClearMap(LPMAPDOCDATA lpMapDocData, BOOL selected, BOOL crop)
{
	int                   i,j;
	int startx, starty, endx, endy;
	BOOL base, checkp;
	char ch;

	lpMapDocData->MapStruct.changed = TRUE;
	
	if (selected)
	{
		SortSelectionArea(lpMapDocData);
	}

	startx = starty = 0;
	endx = lpMapDocData->MapStruct.width;
	endy = lpMapDocData->MapStruct.height;

	if (selected && !crop)
	{
		startx = lpMapDocData->seldxbeg;
		endx = lpMapDocData->seldxend;
		starty = lpMapDocData->seldybeg;
		endy = lpMapDocData->seldyend;
	}

	for (i = startx; i < endx; i++)
		for (j = starty; j < endy; j++)
		{
			if (!InsideMap(lpMapDocData, i, j))
				continue;

			ch = lpMapDocData->MapStruct.data[i][j].cdata;
			if (crop && selected)
			{
				if ((i >= lpMapDocData->seldxbeg) && (i < lpMapDocData->seldxend) &&
					(j >= lpMapDocData->seldybeg) && (j < lpMapDocData->seldyend) )
				continue;
			}
			
			if (ch >= '0' && ch <= '9')
				base = TRUE; 
			if (ch >= 'A' && ch <= 'Z')
				checkp = TRUE; 
			ChangeMapData(lpMapDocData,i,j,MAP_SPACE,1);
		}

	if (base && lpMapDocData->MapStruct.teamPlay)
		CountBases(lpMapDocData);
	
	if (checkp)
		CountCheckPoints(lpMapDocData);

}
/***************************************************************************/
/* MapData                                                                 */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x: xcoord                                                             */
/*   y: ycoord                                                             */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
char MapData(LPMAPDOCDATA lpMapDocData, int x, int y)
{
	if (lpMapDocData->MapStruct.edgeWrap)
	{
		if ( x<0 ) {
			x = lpMapDocData->MapStruct.width + x ;
		} else if (x>=lpMapDocData->MapStruct.width) {
			x = x - lpMapDocData->MapStruct.width;
		}
		if ( y<0 ) {
			y = lpMapDocData->MapStruct.height + y;
		} else if (y>=lpMapDocData->MapStruct.height) {
			y = y - lpMapDocData->MapStruct.height;
		}
	}
	else
	{
		if ( x<0 ) {
			x = 0;
		} else if (x>lpMapDocData->MapStruct.width) {
			x = lpMapDocData->MapStruct.width;
		}
		if ( y<0 ) {
			y = 0;
		} else if (y>lpMapDocData->MapStruct.height) {
			y = lpMapDocData->MapStruct.height;
		}
	}

	return lpMapDocData->MapStruct.data[(int) x][(int) y].cdata;
}
/***************************************************************************/
/* InsideMap                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x: xcoord                                                             */
/*   y: ycoord                                                             */
/*                                                                         */
/* Purpose :  Verifies a point is inside the map limits. Necessary because */
/* Dragging map data will allow us to end up with area outside the map	   */
/* selected, which is possibly outside the array as well, causeing memerr. */
/***************************************************************************/
BOOL InsideMap(LPMAPDOCDATA lpMapDocData, int x, int y)
{
	if ( (x < 0) || (x >= lpMapDocData->MapStruct.width)
			|| (y < 0) || (y >= lpMapDocData->MapStruct.height) ) 
	return FALSE;
	
	return TRUE;
}