Note: If you downloaded the binairy (pre-compiled) copy of MapXpress, ignore the stuff about
compiling. Just place the files in the Xpilot Directory (if you have one, otherwise be sure
all the files are in the same directory.)

To Compile MapXpress
!NOTE: THIS FILE MAKES THE ASSUMPTION YOU HAVE THE STANDARD PATHS AND VARIABLES DEFINED
FOR YOUR VISUAL C++ INSTALLATION. THIS INCLUDES THE PATH TO NMAKE, CL, LINK ETC. IT ALSO ASSUMES
YOU HAVE HTMLHELP WORKSHOP INSTALLED TO C:\Program Files\HTML Help Workshop

THE FILE MAPXPRESS.REG CONTAINS REGISTRY ENTRIES FOR MAPXPRESS, WHICH SHOULD BE ADDED TO THE
WINDOWS REGISTRY. This sets up icons and default actions for .xp files.

* Alter the default_colors.h if you wish to change any of the
editor colors (although I don't recommend it).

* Then, use the VC 6.0 files MapXpress.dsw & MapXpress.dsp.
-Double click the file MapXpress.dsw, or open it from Developer studio.
-Set the active configuration using "Build" "Set Active Configuration"
 to "MapXpress - Win32 Release"
-Press F7 or Select "Build", "Build MapXpress.exe"

*Finally place the generated executeable file in your Xpilot folder:
(XPILOT directory)\MapXpress.exe

This file should be found at (Xpilot Source Dir)\Release\MapXpress.exe
and the usual Xpilot Directory is C:\Xpilot

*********************************************************
To compile the HTML Help files- All files are found in
the directory "html_help".
*I use the 'Microsoft HTML Help Workshop' which is freely available, and comes
with Visual C++ 6.0 Standard.

Open the file "mapxpress.hhp" in "Microsoft HTML Help Workshop"

Push "Save & Compile"

*Place "mapxpress.chm" in your Xpilot folder, or in whatever folder you
placed the program executeable file: 
(XPILOT directory)\mapxpress.chm

**************************************************************

Send bugs, comments, etc. to  jlmiller@ctitech.com (the author)
or 			      xpilot@xpilot.org	   (the xpilot guru's)

10:40 AM 5/17/01

Authorship Disclaimer:
11:32 AM 5/14/99 - Please note that much of the code for MapXpress is altered from MapEdit or
some other programs source code. Much of the code has been altered to unrecognizability. The author of MapXpress
does not claim to have authored all the code contained in MapXpress, just to have modified it to
fit the needed purpose.
