/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include <windows.h>
#include <windowsx.h>
//#include <shellapi.h>
#include <commdlg.h>
#include <commctrl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <memory.h>
#include <math.h>
#include <float.h>
#include <process.h>
#include <limits.h>

#include <Winreg.h> /*file for windows registry*/
#include <direct.h>

#ifdef USEHTMLHELP
#include <htmlhelp.h>
#endif

/*must be defined before several include files*/
#define NUMPREFS 260

/*program specific include files*/
#include "defaults.h"
#include "proto.h"
#include "map.h"
#include "mapxpmnu.h"
#include "expose.h"
#include "default_colors.h"
#include "grow.h"

#define VOIDBACK	999

/*The total number of map options supported...must be updated to add new prefs*/
/*Don't forget to change index in MAPDOCDATA definintion in xprTypes.h to correspond*/


/***************************************************************/
/*These values represent the different pages of the preferences dialog box.
One of these must be specified in each prefs option, or the edit fields won't be
initialized with the default value when the prefs box is opened.*/
#define PREFS1 1
#define PREFS2 2
#define PREFS3 3
#define PREFS4 4
#define PREFS5 5
#define PREFS6 6
#define PREFS7 7
#define PREFS8 8
#define PREFS9 9

/********************************************/
/* This is the version number used for all version identification purposes.
/* Note to stupid...."Don't forget to update this when you change something!"
/*
/*
/********************************************/
static char szAppName[] = "MapXpress 4.5.0" ;
static char szXpilotVersion[] = "Xpilot 4.5.0" ;

#ifdef USEHTMLHELP
/*The new HTML help scheme.*/
#define CHMHELPFILE "mapxpress.chm"
#else
/*The old winhelp help scheme*/
#define HELPFILE "mapxpress.hlp"
#endif

#define MXPTEMPFILE "mapxpress_tmp.xp"

extern int               numprefs;

#define MAPWIDTH         0
#define MAPHEIGHT        1
#define MAPDATA          2
#define STRING           3
#define YESNO            4
#define FLOAT            5
#define POSFLOAT         6
#define INT              7
#define POSINT           8
#define COORD            9
#define LISTINT			 10

extern int cWidth, cHeight;
/*temporary array for doing special operations*/
extern map_data_t        clipdata;
/*the actual clipboard array*/
extern map_data_t		ClipBoard;

extern HINSTANCE hInst;
extern HWND hwndFrame;
extern HWND hwndClient;
extern HWND hwndChild;
extern HWND hwndClipBoard;
extern HWND hwndMiniMap;
extern HWND hwndThumb;
extern HMENU hMenuInit, hMenuMap;

static HWND hwndTBmapsyms;
static HWND hwndTBfile;
static HWND hwndTBtools;
static HWND hwndTBshape;

/*The currently selected buttons*/
extern int iSelectionMapSyms;
extern int iSelectionTools;
extern int iSelectionShape;

/*The last team base entered from keyboard*/
extern char lastTeamBase;


extern POINT ptOutbeg, ptOutend;

#define LENGTH(x, y)		( hypot( (double) (x), (double) (y) ) )
/*
 * Two macros for edge wrap of differences in position.
 * If the absolute value of a difference is bigger than
 * half the map size then it is wrapped.
 */
#define WRAP_DX(dx)	\
	(lpMapDocData->MapStruct.edgeWrap \
	    ? ((dx) < - (lpMapDocData->MapStruct.width >> 1) \
		? (dx) + lpMapDocData->MapStruct.width \
		: ((dx) > (lpMapDocData->MapStruct.width >> 1) \
		    ? (dx) - lpMapDocData->MapStruct.width \
		    : (dx))) \
	    : (dx))
#define WRAP_DY(dy)	\
	(lpMapDocData->MapStruct.edgeWrap \
	    ? ((dy) < - (lpMapDocData->MapStruct.height >> 1) \
		? (dy) + lpMapDocData->MapStruct.height \
		: ((dy) > (lpMapDocData->MapStruct.height >> 1) \
		    ? (dy) - lpMapDocData->MapStruct.height \
		    : (dy))) \
	    : (dy))


/*Filled World brushes*/
extern HBRUSH hBrushSolidWall;
extern HBRUSH hBrushFuel;
extern HBRUSH hBrushCannon;
extern HBRUSH hBrushBase;
extern HBRUSH hBrushDecorWall;
extern HBRUSH hBrushWormhole;
extern HBRUSH hBrushTreasure;
extern HBRUSH hBrushTarget;
extern HBRUSH hBrushItemConc;
extern HBRUSH hBrushGravity;
extern HBRUSH hBrushCurrent;
extern HBRUSH hBrushFriction;

extern HPEN hPenSolidWall;
extern HPEN hPenCannon;
extern HPEN hPenBase;
extern HPEN hPenDecorWall;
extern HPEN hPenWormhole;
extern HPEN hPenTreasure;
extern HPEN hPenTarget;
extern HPEN hPenItemConc;
extern HPEN hPenGravity;
extern HPEN hPenCurrent;
extern HPEN hPenFriction;

/*external variable for Wildmap random map generator*/
extern char wmapwidth[4];
extern char wmapheight[4];
extern char wmapseed[20];
extern char wmapseed_ratio[10];
extern char wmapfill_ratio[10];
extern char wmapnum_bases[4];
extern char wmapnum_teams[4];
extern char wmapcannon_ratio[10];
extern char wmapfuel_ratio[10];
extern char wmapgrav_ratio[10];
extern char wmapworm_ratio[10];

/*Registry Key Stuff*/
extern HKEY xpilotkey;
extern BOOL FilledWorld;
extern BOOL TransPaste;
extern BOOL SaveAllPrefs;
extern BOOL Mini_Map;
extern unsigned char *xpilotmap_dir;
extern unsigned long *xpilotmap_dir_length;
extern unsigned char *editor_command;
extern unsigned long *editor_command_length;
extern unsigned char *mini_map;
extern unsigned long *mini_map_length;
extern unsigned char *client_command;
extern unsigned long *client_command_length;
extern unsigned char *server_command;
extern unsigned long *server_command_length;

/*File...Open Dialog box filter*/
extern unsigned long *zfilterIndex;

extern char *mapErrors;
