/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"

#define ToolBar_ButtonStructSize(hwnd) \
(void)SendMessage((hwnd), TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0L)

#define ToolBar_AddBitmap(hwnd, nButtons, lptbab) \
(int)SendMessage((hwnd), TB_ADDBITMAP, (WPARAM)nButtons, (LPARAM)(LPTBADDBITMAP) lptbab)

#define ToolBar_AddButtons(hwnd, uNumButtons, lpButtons) \
(BOOL)SendMessage((hwnd), TB_ADDBUTTONS, (WPARAM)(UINT)uNumButtons, (LPARAM)(LPTBBUTTON)lpButtons)


DWORD dwToolBarStyles = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS |
TBSTYLE_WRAPABLE | TBSTYLE_TOOLTIPS;

int cyToolBar ;

char szTbStrings[] ="New\0Open\0Save\0Cut\0Copy\0Paste\0Undo\0Pen\0Erase\0Line\0Line Fill\0Shape\0Fill\0Clear Map\0"
"Crop\0Select\0Round Map\0Zoom In\0Zoom Out\0Rotate Map\0"
"Mirror Vertically\0Mirror Horizontally\0Adjust Horizontally Left\0Adjust Horizontally Right\0"
"Adjust Vertically Up\0Adjust Vertically Down\0Negative Map\0Map Properties\0Lower Right Solid\0"
"Lower Left Solid\0Full Solid\0Upper Right Solid\0Upper Left Solid\0"
"Left Cannon\0Upwards Cannon\0Down Cannon\0Right Cannon\0Base\0"
"Start Orientation\0Fuel Block\0Target\0Treasure\0Empty Treasure\0Item Concentrator\0Asteroid Concentrator\0"
"Push Gravity\0Pull Gravity\0Counter Clockwise Current\0Clockwise Current\0"
"I\\O Wormhole\0Out Wormhole\0In Wormhole\0Upwards Current\0Left Current\0"
"Right Current\0Down Current\0Lower Right Deco\0Lower Left Deco\0Full Deco\0"
"Upper Right Deco\0Upper Left Deco\0Empty Space\0Quick Cannon\0Grow Map\0Check Map\0"
"View Source\0Launch Map\0Last Team Base\0Next Check Point\0Friction\0"
"Empty Circle\0Filled Circle\0Empty Rectangle\0Filled Rectangle\0";

TBBUTTON tbshape[] = 
{
	0, IDM_CIRCLEEMPTY, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	1, IDM_CIRCLEFILLED, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	2, IDM_RECTEMPTY, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	3, IDM_RECTFILLED, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
} ;

TBBUTTON tbfile[] = 
{
	STD_FILENEW, IDM_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0,0,0,
	STD_FILEOPEN, IDM_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0,0,0,
	STD_FILESAVE, IDM_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON,0,0,0,0,
	0,0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	STD_CUT, IDM_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0,
	STD_COPY, IDM_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0,
	STD_PASTE, IDM_EDIT_PASTE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0,
	0,0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	STD_UNDO, IDM_EDIT_UNDO, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0,0,0,0,
} ;
TBBUTTON tbtools[] = 
{
	0, IDM_PEN, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	1, IDM_ERASE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	2, IDM_LINE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	19, IDM_LINEFILL, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	24, IDM_SHAPE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	3, IDM_SELECT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	12, IDM_FILL, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	0, 0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	5, IDM_CLEARMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	17, IDM_CROP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	4, IDM_ROUNDMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	20, IDM_GROWMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	0, 0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	6, IDM_ZOOMIN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	7, IDM_ZOOMOUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	0, 0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	8, IDM_ROTATEMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	9, IDM_MIRRORMAPV, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	10, IDM_MIRRORMAPH, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	13, IDM_CYCLEMAPH, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	15, IDM_CYCLEMAPHNEG, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	14, IDM_CYCLEMAPV, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	16, IDM_CYCLEMAPVNEG, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	18, IDM_NEGATIVEMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	0, 0, TBSTATE_ENABLED, TBSTYLE_SEP,0,0,0,0,
	11, IDM_PROPERTIES, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	21, IDM_CHECKMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	22, IDM_MAPSOURCE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
	23, IDM_LAUNCHMAP, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0, 0, 0,
//	22, IDM_OPENBITMAP, TBSTATE_HIDDEN, TBSTYLE_BUTTON, 0, 0, 0, 0,
} ;

TBBUTTON tbmapsyms[] = 
{
	0, IDM_MAP_REC_RD, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	1, IDM_MAP_REC_LD, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	2, IDM_MAP_FILLED, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	3, IDM_MAP_REC_RU, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	4, IDM_MAP_REC_LU, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	32, IDM_MAP_CAN_UNSPEC, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	5, IDM_MAP_CAN_LEFT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	6, IDM_MAP_CAN_UP, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	7, IDM_MAP_CAN_DOWN, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	8, IDM_MAP_CAN_RIGHT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	9, IDM_MAP_BASE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	10, IDM_MAP_BASE_ORNT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	11, IDM_MAP_FUEL, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	12, IDM_MAP_TARGET, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	13, IDM_MAP_TREASURE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	36, IDM_MAP_EMPTYTREASURE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	14, IDM_MAP_ITEM_CONC, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	37, IDM_MAP_ASTEROID_CONC, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	15, IDM_MAP_GRAV_POS, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	16, IDM_MAP_GRAV_NEG, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	17, IDM_MAP_GRAV_ACWISE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	18, IDM_MAP_GRAV_CWISE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	19, IDM_MAP_WORM_NORMAL, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	20, IDM_MAP_WORM_OUT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	21, IDM_MAP_WORM_IN, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	22, IDM_MAP_CRNT_UP, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	23, IDM_MAP_CRNT_LT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	24, IDM_MAP_CRNT_RT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	25, IDM_MAP_CRNT_DN, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	26, IDM_MAP_DEC_RD, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	27, IDM_MAP_DEC_LD, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	28, IDM_MAP_DEC_FLD, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	29, IDM_MAP_DEC_RU, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	30, IDM_MAP_DEC_LU, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	31, IDM_MAP_SPACE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	33, IDM_MAP_TEAMBASE, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	34, IDM_MAP_CHECKPOINT, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
	35, IDM_MAP_FRICTION, TBSTATE_ENABLED, TBSTYLE_CHECKGROUP, 0, 0, 0, 0,
} ;
HWND hwndToolTip;
static HWND hwndTT;
extern char szTbStrings[];


/* Map toolbar button command to string index. */
int CommandToString[] = 
{ IDM_NEW, IDM_OPEN, IDM_SAVE, IDM_EDIT_CUT, IDM_EDIT_COPY, IDM_EDIT_PASTE,
IDM_EDIT_UNDO, IDM_PEN, IDM_ERASE, IDM_LINE, IDM_LINEFILL, IDM_SHAPE, IDM_FILL,
IDM_CLEARMAP, IDM_CROP, IDM_SELECT, IDM_ROUNDMAP, IDM_ZOOMIN, IDM_ZOOMOUT, IDM_ROTATEMAP,
IDM_MIRRORMAPV, IDM_MIRRORMAPH, IDM_CYCLEMAPH, IDM_CYCLEMAPHNEG, IDM_CYCLEMAPV,
IDM_CYCLEMAPVNEG, IDM_NEGATIVEMAP, IDM_PROPERTIES, IDM_MAP_REC_RD, IDM_MAP_REC_LD,
IDM_MAP_FILLED, IDM_MAP_REC_RU, IDM_MAP_REC_LU, IDM_MAP_CAN_LEFT, IDM_MAP_CAN_UP,
IDM_MAP_CAN_DOWN, IDM_MAP_CAN_RIGHT,
IDM_MAP_BASE, IDM_MAP_BASE_ORNT, IDM_MAP_FUEL, IDM_MAP_TARGET, IDM_MAP_TREASURE,IDM_MAP_EMPTYTREASURE,
IDM_MAP_ITEM_CONC, IDM_MAP_ASTEROID_CONC, IDM_MAP_GRAV_POS, IDM_MAP_GRAV_NEG, IDM_MAP_GRAV_ACWISE,
IDM_MAP_GRAV_CWISE, IDM_MAP_WORM_NORMAL, IDM_MAP_WORM_OUT, IDM_MAP_WORM_IN,
IDM_MAP_CRNT_UP, IDM_MAP_CRNT_LT, IDM_MAP_CRNT_RT, IDM_MAP_CRNT_DN,
IDM_MAP_DEC_RD, IDM_MAP_DEC_LD, IDM_MAP_DEC_FLD, IDM_MAP_DEC_RU, IDM_MAP_DEC_LU,
IDM_MAP_SPACE, IDM_MAP_CAN_UNSPEC, IDM_GROWMAP, IDM_CHECKMAP, IDM_MAPSOURCE,
IDM_LAUNCHMAP, IDM_MAP_TEAMBASE, IDM_MAP_CHECKPOINT,IDM_MAP_FRICTION,
IDM_CIRCLEEMPTY, IDM_CIRCLEFILLED, IDM_RECTEMPTY, IDM_RECTFILLED,
-1   
} ;
/***************************************************************************/
/* InitFileToolBar                                                         */
/* Arguments :                                                             */
/*   hwndParent                                                            */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/* Purpose : Initializes the File Toolbar                                  */
/***************************************************************************/
HWND InitFileToolBar (HWND hwndParent)
{
	LPTBBUTTON ptbfile ;
	
	ptbfile = &tbfile[0] ;
	
	
	hwndTBfile = CreateToolbarEx (hwndParent,
		dwToolBarStyles | CCS_TOP | TBSTYLE_FLAT,
		1, 15,
		HINST_COMMCTRL,
		IDB_STD_SMALL_COLOR,
		ptbfile,
		sizeof(tbfile) / sizeof(tbfile[0]),
		0, 0, 15, 15,
		sizeof (TBBUTTON)) ;
	
	return hwndTBfile ;
}
/***************************************************************************/
/* InitMapSymsToolBar                                                      */
/* Arguments :                                                             */
/*   hwndParent                                                            */
/*   hMenu                                                                 */
/*                                                                         */
/*                                                                         */
/* Purpose : Initializes the Map symbols Toolbar                           */
/***************************************************************************/
HWND InitMapSymsToolBar (HWND hwndParent)
{
	LPTBBUTTON ptbmapsyms ;
	
	ptbmapsyms = &tbmapsyms[0] ;
	
	hwndTBmapsyms = CreateToolbarEx (hwndParent,
		dwToolBarStyles | CCS_NORESIZE,
		1, 31,
		hInst,
		IDB_BLOCKSTOOLBAR,
		ptbmapsyms,
		sizeof(tbmapsyms) / sizeof(tbmapsyms[0]),
		0, 0, 16, 16,
		sizeof (TBBUTTON)) ;
	
	SetWindowPos (hwndTBmapsyms, HWND_TOP, 0, 54, TOOLSWIDTH, 290, SWP_NOACTIVATE);
	
	iSelectionMapSyms = IDM_MAP_FILLED;
	SendMessage(hwndTBmapsyms, TB_CHECKBUTTON, iSelectionMapSyms, TRUE);
	
	return hwndTBmapsyms ;
}
/***************************************************************************/
/* InitToolsToolBar                                                        */
/* Arguments :                                                             */
/*   hwndParent                                                            */
/*   hMenu                                                                 */
/*                                                                         */
/*                                                                         */
/* Purpose : Initializes the Tools Toolbar                                 */
/***************************************************************************/
HWND InitToolsToolBar (HWND hwndParent)
{
	LPTBBUTTON ptbtools ;
	
	ptbtools = &tbtools[0] ;
	
	
	
	hwndTBtools = CreateToolbarEx (hwndParent,
		dwToolBarStyles | CCS_NOMOVEY | TBSTYLE_FLAT,
		1, 31,
		hInst,
		IDB_TOOLSTOOLBAR,
		ptbtools,
		sizeof(tbtools) / sizeof(tbtools[0]),
		0, 0, 16, 16,
		sizeof (TBBUTTON)) ;
	
	SetWindowPos (hwndTBtools, HWND_TOP, 0, 25, 0, 0, SWP_NOSIZE);
	
	iSelectionTools = IDM_PEN;
	SendMessage(hwndTBtools, TB_CHECKBUTTON, iSelectionTools, TRUE);
	
	return hwndTBtools ;
}
/***************************************************************************/
/* InitShapeToolBar                                                       */
/* Arguments :                                                             */
/*   hwndParent                                                            */
/*   hMenu                                                                 */
/*                                                                         */
/*                                                                         */
/* Purpose : Initializes the Shape Toolbar                                */
/***************************************************************************/
HWND InitShapeToolBar (HWND hwndParent)
{
	LPTBBUTTON ptbshape ;
	
	ptbshape = &tbshape[0] ;
	
	
	
	hwndTBshape = CreateToolbarEx (hwndParent,
		WS_CHILD | WS_CLIPSIBLINGS |
		TBSTYLE_TOOLTIPS | TBSTYLE_WRAPABLE | CCS_NOMOVEY | CCS_NORESIZE,
		1, 31,
		hInst,
		IDB_SHAPESUBBAR,
		ptbshape,
		sizeof(tbshape) / sizeof(tbshape[0]),
		0, 0, 16, 16,
		sizeof (TBBUTTON)) ;
	
	SetWindowPos (hwndTBshape, HWND_BOTTOM, 0, 350, 64, 64, SWP_NOACTIVATE);
	
	iSelectionShape = IDM_CIRCLEEMPTY;
	SendMessage(hwndTBshape, TB_CHECKBUTTON, iSelectionShape, TRUE);
	
	return hwndTBshape;
}
/***************************************************************************/
/* CopyToolTipText                                                         */
/* Arguments :                                                             */
/*   lpttt                                                                 */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/* Purpose : Copys tool tip text to a toolbar                              */
/***************************************************************************/
void CopyToolTipText (LPTOOLTIPTEXT lpttt)
{
	int i ;
	int iButton = lpttt->hdr.idFrom ;
	int cb ;
	int cMax ;
	LPSTR pString ;
	LPSTR pDest = lpttt->lpszText ;
	
	/* Map command ID to string index */
	for (i = 0 ; CommandToString[i] != -1 ; i++)
	{
		if (CommandToString[i] == iButton)
		{
			iButton = i ;
			break ;
		}
	}
	
	/* To be safe, count number of strings in text */
	pString = szTbStrings ;
	cMax = 0 ;
	while (*pString != '\0')
	{
		cMax++ ;
		cb = lstrlen (pString) ;
		pString += (cb + 1) ;
	}
	
	/* Check for valid parameter */
	if (iButton > cMax)
	{
		pString = "Invalid Button Index" ;
	}
	else
	{
		/* Cycle through to requested string */
		pString = szTbStrings ;
		for (i = 0 ; i < iButton ; i++)
		{
			cb = lstrlen (pString) ;
			pString += (cb + 1) ;
		}
	}
	
	lstrcpy (pDest, pString) ;
}

