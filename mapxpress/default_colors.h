/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#define COLOR_WALL      RGB(0,0,255)
#define COLOR_DECOR     RGB(255,128,0)
#define COLOR_FUEL      RGB(255,0,0)

#define COLOR_TREASURE  RGB(255,0,0)
#define COLOR_TARGET    RGB(255,0,0)
#define COLOR_ITEM_CONC RGB(255,0,0)

#define COLOR_GRAVITY   RGB(0,255,0)
#define COLOR_CURRENT   RGB(0,255,0)
#define COLOR_WORMHOLE  RGB(0,255,0)

#define COLOR_BASE      RGB(255,255,255)
#define COLOR_CANNON    RGB(255,255,255)

#define COLOR_FRICTION  RGB(0,100,100)
