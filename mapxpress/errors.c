/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "mapxpress.h"

/***************************************************************************/
/* char CheckMap                                                           */
/* Arguments :                                                             */
/*   lpMapDocData: Pointer to Map Data to be checked                       */
/*                                                                         */
/*                                                                         */
/* Purpose :                                                               */
/***************************************************************************/
int CheckMap (LPMAPDOCDATA lpMapDocData) 
{
	int count =0, option, letter;
	char *tmp;
	char errorheader[] = "######################################################\
\r\nThe following is a list of errors in the current map, option settings selected \
\r\nto be saved, etc. They should be corrected before the map is finalized. \
\r\n######################################################\r\n\0";
	char maperror1[] = "\"edgeWrap\" and \"extraBorder\" settings are incompatible.\r\n\0";
	char maperror2[] = "doesn't have a value entered.\r\n\0";
	char maperror3[] = "\"edgeBounce\" and \"extraBorder\" settings are incompatible.\r\n\0";
	char maperror4[] = "More than one checkpoint lettered \0";
	char maperror5[] = "\"timing\" is not on, but checkpoints are set.\r\n\0";
	char maperror6[] = "\"timing\" is on, but no checkpoints are set.\r\n\0";
	char maperror7[] = "Map has no bases!\r\n\0";


	if (mapErrors != NULL)
		free(mapErrors);

	/*Add the header*/
	mapErrors = (char *) malloc(strlen(errorheader)+2);
	sprintf(mapErrors, "%s", errorheader);

	/*Are there any bases?*/
	CountBases(lpMapDocData);
	if (lpMapDocData->MapItems.NumBases == 0)
	{
		count++;
		tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror7)+5);
		sprintf(tmp,"%s%d) %s", mapErrors,count,maperror7);
		free(mapErrors);
		mapErrors = tmp;
	}
	
	
	/*Check the edge settings*/
	if (lpMapDocData->MapStruct.edgeWrap && lpMapDocData->MapStruct.extraBorder)
	{
		count++;
		tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror1)+5);
		sprintf(tmp,"%s%d) %s", mapErrors,count,maperror1);
		free(mapErrors);
		mapErrors = tmp;
	}

	if (lpMapDocData->MapStruct.extraBorder && lpMapDocData->MapStruct.edgeBounce)
	{
		count++;
		tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror3)+5);
		sprintf(tmp,"%s%d) %s", mapErrors,count,maperror3);
		free(mapErrors);
		mapErrors = tmp;
	}
	
	/*Now check the checkpoints*/
	CountCheckPoints(lpMapDocData);
	if (lpMapDocData->MapStruct.timing)
	{
		if (lpMapDocData->MapItems.NumChecks == 0)
		{
			count++;
			tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror6)+5);
			sprintf(tmp,"%s%d) %s", mapErrors,count,maperror6);
			free(mapErrors);
			mapErrors = tmp;
		}
		else			
			for (letter = 0; letter <= 25; letter++)
			{
				if (lpMapDocData->MapItems.checkpoints[letter] > 1)
				{
					count++;
					tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror4)+9);
					sprintf(tmp,"%s%d) %s%c.\r\n", mapErrors,count,maperror4, letter+65);
					free(mapErrors);
					mapErrors = tmp;
				}
			}
	}
	else
		if (lpMapDocData->MapItems.NumChecks > 0)
		{
			count++;
			tmp = (char *) malloc(strlen(mapErrors)+strlen(maperror5)+5);
			sprintf(tmp,"%s%d) %s", mapErrors,count,maperror5);
			free(mapErrors);
			mapErrors = tmp;
		}

	/*Now check the option values*/
	/*NOTE: Need to implement more code here to allow checking to insure
	value is not just present, but also is actually valid.
	Example: 
			gravityPoint: 0,0 (acceptable)
			gravityPoint: quijibo (not acceptable)
	*/
	for (option = 0; option <= NUMPREFS; option++)
	{
		switch(lpMapDocData->PrefsArray[option].type)
		{
		case STRING:
		case COORD: /*Is there a value entered?*/
			if(*lpMapDocData->PrefsArray[option].charvar == (int) '\0' && lpMapDocData->PrefsArray[option].output)
			{
						count++;
						tmp = (char *) malloc(strlen(mapErrors)+strlen(lpMapDocData->PrefsArray[option].name)+strlen(maperror2)+8);
						if (tmp == NULL)
						{
							ErrorHandler("Couldn't allocate memory for map errors list!");
							break;
						}
						sprintf(tmp,"%s%d) \"%s\" %s",mapErrors,count, lpMapDocData->PrefsArray[option].name, maperror2);
						free(mapErrors);
						mapErrors = tmp;
			}
			continue;
		case INT:
		case POSINT:
		case FLOAT:
		case POSFLOAT: /*Is there a value entered?*/
		case LISTINT:
			if(*lpMapDocData->PrefsArray[option].charvar == (int) '\0' && lpMapDocData->PrefsArray[option].output)
			{
						count++;
						tmp = (char *) malloc(strlen(mapErrors)+strlen(lpMapDocData->PrefsArray[option].name)+strlen(maperror2)+8);
						if (tmp == NULL)
						{
							ErrorHandler("Couldn't allocate memory for map errors list!");
							break;
						}
						sprintf(tmp,"%s%d) \"%s\" %s",mapErrors,count, lpMapDocData->PrefsArray[option].name, maperror2);
						free(mapErrors);
						mapErrors = tmp;
			}
			continue;

/*		case YESNO:
			if(*lpMapDocData->PrefsArray[option].charvar == (int) '\0' && lpMapDocData->PrefsArray[option].output)
			{
						count++;
						tmp = (char *) malloc(strlen(lpMapDocData->mapErrors)+strlen(maperror2a)+strlen(lpMapDocData->PrefsArray[option].name)+strlen(maperror2b)+4);
						if (tmp == NULL)
						{
							ErrorHandler("Couldn't allocate memory for map errors list!");
							break;
						}
						else
						{
						sprintf(tmp,"%s%d%s%s%s",lpMapDocData->mapErrors,count,maperror2a, lpMapDocData->PrefsArray[option].name, maperror2b);
						free(lpMapDocData->mapErrors);
						lpMapDocData->mapErrors = tmp;
						}

			}
			break;*/


		default:
			break;
		}
	}
	return 0;
}


/***************************************************************************/
/* MapErrorsDlgProc                                                        */
/* Arguments :                                                             */
/*    hDlg                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/* Purpose :   The procedure for the errors dialog box.                    */
/***************************************************************************/
BOOL CALLBACK MapErrorsDlgProc (HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	LPMAPDOCDATA lpMapDocData;
	static HWND hwndLocClient;
	HWND hwndChild;
	
	switch(iMsg)
	{
	case WM_INITDIALOG :
		hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
		SetEditText (hDlg, IDC_MAPERRORSTEXT, mapErrors);
		
		return TRUE;
		
	case WM_COMMAND :
		switch (LOWORD (wParam))
		{
		case IDOK:
		case IDCANCEL :
			EndDialog (hDlg, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}
