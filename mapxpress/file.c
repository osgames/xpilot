/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "mapxpress.h"

static OPENFILENAME ofn ;
char                     oldmap[90] = {
		' ',' ',' ',' ',' ',
		' ',' ',' ',' ',' ',
		'_','+',' ','-',' ',
		' ',' ',' ',' ',' ',
		' ',' ',' ',' ',' ',
		' ',' ',' ','<',' ',
		'>',' ',' ',' ',' ',
		' ',' ',' ','#',' ',
		' ','(',' ',' ',' ',
		' ',' ',')',' ',' ',
		' ',' ',' ',' ',' ',
		'@',' ',' ',' ',' ',
		' ',' ',' ',' ',' ',
		'a',' ','c','d',' ',
		'f',' ',' ',' ',' ',
		' ',' ',' ',' ',' ',
		' ','q','r','s',' ',
		' ',' ','w','x',' ' };
		
/***************************************************************************/
/* XpFileInitialize                                                        */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*                                                                         */
/*                                                                         */
/*                                                                         */
/* Purpose : Initializes the windows common dialog boxes                   */
/***************************************************************************/
void XpFileInitialize (HWND hwnd)
{
			
	ofn.lStructSize       = sizeof (OPENFILENAME) ;
	ofn.hwndOwner         = hwnd ;
	ofn.hInstance         = NULL ;
	ofn.lpstrCustomFilter = NULL ;
	ofn.nMaxCustFilter    = 0 ;
	ofn.nFilterIndex      = 0;
	ofn.lpstrFile         = NULL ;          // Set in Open and Close functions
	ofn.nMaxFile          = _MAX_PATH ;
	ofn.lpstrFileTitle    = NULL ;          // Set in Open and Close functions
	ofn.nMaxFileTitle     = _MAX_FNAME + _MAX_EXT ;
	ofn.lpstrInitialDir   = (LPCTSTR) xpilotmap_dir ;
	ofn.lpstrTitle        = NULL ;
	ofn.Flags             = 0 ;             // Set in Open and Close functions
	ofn.nFileOffset       = 0 ;
	ofn.nFileExtension    = 0 ;
	ofn.lpstrDefExt       = "xp" ;
	ofn.lCustData         = 0L ;
	ofn.lpfnHook          = NULL ;
	ofn.lpTemplateName    = NULL ;
}
/***************************************************************************/
/* XpFileOpenDlg                                                           */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    pstrFileName                                                         */
/*    pstrTitleName                                                        */
/*                                                                         */
/* Purpose : runs the file open common dialogv                             */
/***************************************************************************/
BOOL XpFileOpenDlg (HWND hwnd, PSTR pstrFileName, PSTR pstrTitleName)
{
	static char szOFilter[] = "Xpilot Files (*.xp,*.map)\0*.xp;*.map\0"  \
							"X11 Bitmap files (*.xbm)\0*.xbm\0" \
							"PBMPLUS Portable Bitmaps (*.pbm)\0*.pbm\0" \
							/*"X11 Pixmap Files (*.xpm)\0*.xpm\0" \
							"Jpeg Files (*.jpg)\0*.jpg\0" \
							"Windows Bitmap Files (*.bmp)\0*.bmp\0" \
							"Gif Image Files (*.gif)\0*.gif\0" \
							"Tiff Image Files (*.tif)\0*.tif\0";*/
							"All Files (*.*)\0*.*\0\0" ;
	ofn.lpstrFilter       = szOFilter ;
	ofn.hwndOwner         = hwnd ;
	ofn.lpstrFile         = pstrFileName ;
	ofn.lpstrFileTitle    = pstrTitleName ;
	ofn.Flags             = OFN_HIDEREADONLY | OFN_CREATEPROMPT ;
	
	return GetOpenFileName (&ofn) ;

}
/***************************************************************************/
/* XpFileSaveDlg                                                           */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    pstrFileName                                                         */
/*    pstrTitleName                                                        */
/*                                                                         */
/* Purpose : runs the file save common dialog                              */
/***************************************************************************/
BOOL XpFileSaveDlg (HWND hwnd, PSTR pstrFileName, PSTR pstrTitleName)
{
	int success;

	static char szSFilter[] = "Xpilot Map (*.XP,*.MAP)\0*.xp;*.map\0"  \
							  "X11 Pixmap (*.xpm)\0*.xpm\0" \
							  "X11 Bitmap (*.xbm)\0*.xbm\0" \
							  "PBMPLUS Portable Bitmap  (*.pbm)\0*.pbm\0" \
							  "PBMPLUS Portable Pixmap Ascii (*.ppm)\0*.ppm\0" \
							  "PBMPLUS Portable Pixmap Binary (*.ppm)\0*.ppm\0" \
							  "Windows Bitmap (*.bmp)\0*.bmp\0" \
							/*  "Jpeg Files (*.jpg)\0*.jpg\0" \
							  "Gif Image Files (*.gif)\0*.gif\0" \
							  "Tiff Image Files (*.tif)\0*.tif\0";
   							  "All Files (*.*)\0*.*\0\0"*/
							  ;

	ofn.lpstrFilter       = szSFilter ;
	ofn.hwndOwner         = hwnd ;
	ofn.lpstrFile         = pstrFileName ;
	ofn.lpstrFileTitle    = pstrTitleName ;
	ofn.Flags             = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT ;
	ofn.nFilterIndex	  = 1 ;
	success = GetSaveFileName (&ofn) ;
	zfilterIndex = &ofn.nFilterIndex;
	return success;
}
/***************************************************************************/
/* XpTextSaveDlg                                                           */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    pstrFileName                                                         */
/*    pstrTitleName                                                        */
/*                                                                         */
/* Purpose : runs the file save common dialog                              */
/***************************************************************************/
BOOL XpTextSaveDlg (HWND hwnd, PSTR pstrFileName, PSTR pstrTitleName)
{
	int success;

	static char szSFilter[] = "Text File (*.txt)\0*.txt\0"  \
							  ;

	ofn.lpstrFilter       = szSFilter ;
	ofn.hwndOwner         = hwnd ;
	ofn.lpstrFile         = pstrFileName ;
	ofn.lpstrFileTitle    = pstrTitleName ;
	ofn.Flags             = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT ;
	ofn.nFilterIndex	  = 1 ;
	success = GetSaveFileName (&ofn) ;
	return success;
}
/***************************************************************************/
/* XpOnlySaveDlg                                                           */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    pstrFileName                                                         */
/*    pstrTitleName                                                        */
/*                                                                         */
/* Purpose : runs the file save common dialog                              */
/***************************************************************************/
BOOL XpOnlySaveDlg (HWND hwnd, PSTR pstrFileName, PSTR pstrTitleName)
{
	int success;

	static char szSFilter[] = "Xpilot Map (*.XP)\0*.xp\0"  \
							  ;

	ofn.lpstrFilter       = szSFilter ;
	ofn.hwndOwner         = hwnd ;
	ofn.lpstrFile         = pstrFileName ;
	ofn.lpstrFileTitle    = pstrTitleName ;
	ofn.Flags             = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT ;
	ofn.nFilterIndex	  = 1 ;
	success = GetSaveFileName (&ofn) ;
	return success;
}
/***************************************************************************/
/* SaveMap                                                                 */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   pstrFileName                                                          */
/*   saveData                                                              */
/*   selected                                                              */
/* Purpose :                                                               */
/***************************************************************************/
int SaveMap(LPMAPDOCDATA lpMapDocData, PSTR pstrFileName, BOOL saveData, BOOL selected)
{
	FILE *ofile = NULL;
	int n,i,j;
	time_t tim;
	
	if (NULL == (ofile = fopen (pstrFileName, "wb")))
	{
		ErrorHandler("Couldn't open file *%s* for writing!", pstrFileName);
		return FALSE ;
	}

	time(&tim);
	fprintf(ofile,"#Created by %s on %s\n",szAppName,ctime(&tim));
	fprintf(ofile,"#Options updated for %s\n",szXpilotVersion);

	if (lpMapDocData->MapStruct.comments != NULL)
	{
	fprintf(ofile,"#");
		for (i = 0; (unsigned) i < strlen(lpMapDocData->MapStruct.comments); i++)
			if (lpMapDocData->MapStruct.comments[i] != '\n')
			{
			fprintf(ofile,"%c", lpMapDocData->MapStruct.comments[i]);
			}
			else 
			  fprintf(ofile,"\n#");
	fprintf(ofile,"\n\n");
	}

	if (selected)
		SortSelectionArea(lpMapDocData);

	for ( n=0; n< NUMPREFS; n++ )
	{
		switch (lpMapDocData->PrefsArray[n].type)
		{
			/*if were saving a selected area, only save the width and height of that area.*/
			/*All other prefs are saved with their set values*/
		case MAPWIDTH: if (selected)
					   {
							fprintf(ofile,"%s : %d\n",lpMapDocData->PrefsArray[n].name,lpMapDocData->seldxend-lpMapDocData->seldxbeg);
							break;
					   }						   
		case MAPHEIGHT:if (selected)
					   {
							fprintf(ofile,"%s : %d\n",lpMapDocData->PrefsArray[n].name,lpMapDocData->seldyend-lpMapDocData->seldybeg);
							break;
					   }
		case STRING:
		case INT:
		case POSINT:
		case FLOAT:
		case POSFLOAT:
		case COORD:
		case LISTINT:
			if ( strlen(lpMapDocData->PrefsArray[n].charvar) != (int) NULL )
			  if ((lpMapDocData->PrefsArray[n].output == TRUE) || (SaveAllPrefs == TRUE))
				fprintf(ofile,"%s : %s\n",lpMapDocData->PrefsArray[n].name,lpMapDocData->PrefsArray[n].charvar);
			break;
			
		case MAPDATA:
			/*if we are saving a selected area, only save out that selection*/
			if(selected)
			{
				fprintf(ofile,"\nmapData: \\multiline: EndOfMapdata\n");
				for (i=lpMapDocData->seldybeg;i<lpMapDocData->seldyend;i++)
				{
					for(j=lpMapDocData->seldxbeg;j<lpMapDocData->seldxend;j++)
					{	
						fprintf(ofile,"%c",lpMapDocData->MapStruct.data[j][i].cdata);
					}
					fprintf(ofile,"\n");
				}
				fprintf(ofile,"EndOfMapdata\n");
			}
			else
				if (saveData)
				{
					fprintf(ofile,"\nmapData: \\multiline: EndOfMapdata\n");
					for (i=0;i<lpMapDocData->MapStruct.height;i++)
					{
						for(j=0;j<lpMapDocData->MapStruct.width;j++)
						{	
							fprintf(ofile,"%c",lpMapDocData->MapStruct.data[j][i].cdata);
						}
						fprintf(ofile,"\n");
					}
					fprintf(ofile,"EndOfMapdata\n");
				}
			break;
				
		case YESNO:
			if ((lpMapDocData->PrefsArray[n].output == TRUE) || (SaveAllPrefs == TRUE))
				if ( (*lpMapDocData->PrefsArray[n].intvar) == 0 ) 
					fprintf(ofile,"%s : no\n",lpMapDocData->PrefsArray[n].name);
				else
					fprintf(ofile,"%s : yes\n",lpMapDocData->PrefsArray[n].name);
			break;
		}
	}
	fclose(ofile);

	strcpy(lpMapDocData->MapStruct.mapFileName, pstrFileName);
	return FALSE;
}
/***************************************************************************/
/* LoadMap                                                                 */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   pstrFileName                                                          */
/*   loadData                                                              */
/* Purpose :                                                               */
/***************************************************************************/
int LoadMap(LPMAPDOCDATA lpMapDocData, PSTR pstrFileName, BOOL loadData) 
{
	
	FILE                  *ifile = NULL;
	int                   ich;
	int                   corrupted=0;
	char                  *tmpstr;
	

	if (NULL == (ifile = fopen (pstrFileName, "rb")))
	{
		return FALSE ;
	}

   tmpstr = strrchr(pstrFileName,(int) '.');
   if (tmpstr != NULL) {
      if (strcmp(tmpstr, ".xbm") == 0) {
         fclose(ifile);
         return LoadXbmFile(lpMapDocData, pstrFileName);
      }
	  if (strcmp(tmpstr, ".pbm") == 0) {
         fclose(ifile);
         return LoadPbmFile(lpMapDocData, pstrFileName);
      }
	/*These blocks of code were left here to prompt future development
	in the image import functionality. Below are a few of the image types
	it would be nice to be able to load as xpilot maps*/
	   
	/*  if (strcmp(tmpstr, ".xpm") == 0) {
         fclose(ifile);
		 //Need to add code here to support loading this filetype.
		 return LoadXpmFile(pstrFileName);
      }*/
	 
	/*  if (strcmp(tmpstr, ".bmp") == 0) {
         fclose(ifile);
		 //Need to add code here to support loading this filetype.
		 return FALSE;
      }*/
	/*  if (strcmp(tmpstr, ".jpg") == 0) {
         fclose(ifile);
		 //Need to add code here to support loading this filetype.
		 return FALSE;
      }*/
	/*  if (strcmp(tmpstr, ".gif") == 0) {
         fclose(ifile);
		 //Need to add code here to support loading this filetype.
		 return FALSE;
      }*/
	/*  if (strcmp(tmpstr, ".tif") == 0) {
         fclose(ifile);
		 //Need to add code here to support loading this filetype.
		 return FALSE;
      }*/
   }

		ich = getc(ifile);
		if (ich != EOF)
			ungetc(ich, ifile);
		if (isdigit(ich))
		{
			fclose(ifile);
			return LoadOldMap(lpMapDocData, pstrFileName);
		}
		else
		{
			while (!feof(ifile))
			{
				if (ParseLine(lpMapDocData, ifile, loadData)) corrupted = 1;
			}
		}
		if (ifile) fclose(ifile); 
		if (corrupted)
		{
			ErrorHandler("%s : File is invalid or corrupted!", pstrFileName);
			return FALSE;
		}

		return TRUE;		
}
/***************************************************************************/
/* LoadOldMap                                                              */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   *file                                                                 */
/* Purpose : Load a version 2 map                                          */
/***************************************************************************/
int LoadOldMap(LPMAPDOCDATA lpMapDocData, char *file)
{
	FILE                  *fp;
	max_str_t             line, filenm;
	int                   x,y,shortline, corrupted;
	int                   fchr=32, rule;
	char                  *tmpstr;
	
	strcpy(filenm, file);
	if ((fp = fopen(filenm, "r")) == NULL)
	{
		ErrorHandler("Couldn't open file *%s* for reading!",file);
		return 1;
	}

	/* read in map x and y size */
	fgets(line, sizeof(max_str_t), fp);
	tmpstr = (char *) strstr(line,"x");
	tmpstr++;
	lpMapDocData->MapStruct.height = atoi( tmpstr );
	strncpy(lpMapDocData->MapStruct.height_str,tmpstr, strlen(tmpstr)-1);
	tmpstr = (char *) strstr(line, "x");
	(*tmpstr) = (char) NULL; 
	lpMapDocData->MapStruct.width = atoi( line );
	strcpy(lpMapDocData->MapStruct.width_str,line);
	/* read in map rule */
	fgets(line, sizeof(max_str_t), fp);
	rule = atoi ( line );
	switch (rule)
	{
	case 6:
		lpMapDocData->MapStruct.edgeWrap = 2;
		break;
	}

	/* get map name and author */
	fgets(line, sizeof(max_str_t), fp);
	strncpy(lpMapDocData->MapStruct.mapName, line, strlen(line) - 1);
	lpMapDocData->MapStruct.mapName[strlen(line) - 1] = (char) NULL;
	fgets(line, sizeof(max_str_t), fp);
	strncpy(lpMapDocData->MapStruct.mapAuthor, line, strlen(line) - 1);
	lpMapDocData->MapStruct.mapAuthor[strlen(line) - 1] = (char) NULL;
	
	/* read in map */
	shortline = corrupted =0;
	for (y=0; y<(lpMapDocData->MapStruct.height); y++)
	{
		for (x=0; x<(lpMapDocData->MapStruct.width); x++)
		{
			if (shortline == 0)
				fchr = fgetc(fp);
			if (fchr == '.') fchr = ' ';
			else
				if ((fchr == '\n') || (fchr == EOF))
					shortline = corrupted = 1;
				if (shortline == 0)
					lpMapDocData->MapStruct.data[x][y].cdata = oldmap[fchr - 32];
				else
					lpMapDocData->MapStruct.data[x][y].cdata = ' ';
		}
		if (shortline == 0)
			fgetc(fp);
		shortline = 0;
	}
	
	if (corrupted == 1)
	{
		ErrorHandler("Corrupted map file!");
	}
	fclose(fp);
	return 0;
}
/***************************************************************************/
/* toeol                                                                   */
/* Arguments :                                                             */
/*   ifile                                                                 */
/* Purpose :                                                               */
/***************************************************************************/
void toeol(FILE *ifile)
{
	int                   ich;
	
	while (!feof(ifile))
		if ((ich = getc(ifile)) == '\n')
		{
			return;
		}
}
/***************************************************************************/
/* skipspace                                                               */
/* Arguments :                                                             */
/*   ifile                                                                 */
/* Purpose :                                                               */
/***************************************************************************/
char skipspace(FILE *ifile)
{
	int                   ich;
	
	while (!feof(ifile))
	{
		ich = getc(ifile);
		if (ich == '\n')
		{
			return (char) ich;
		}
		if (!isascii(ich) || !isspace(ich))
			return (char) ich;
	}
	return EOF;
}
/***************************************************************************/
/* char *getMultilineValue                                                 */
/* Arguments :                                                             */
/*   delimiter                                                             */
/*    ifile                                                                */
/* Purpose :                                                               */
/***************************************************************************/
char *getMultilineValue(char *delimiter, FILE *ifile)
{
	char                  *s = (char *) malloc(32768);
	int                   i = 0;
	int                   slen = 32768;
	char                  *bol;
	int                   ich;
	
	bol = s;
	while (1)
	{
		ich = getc(ifile);
		if (ich == EOF)
		{
			s = (char *) realloc(s, i + 1);
			s[i] = '\0';
			return s;
		}
		if (i == slen)
		{
			char            *t = s;
			
			s = (char *) realloc(s, slen += 32768);
			bol += s - t;
		}
		if (ich == '\n')
		{
			s[i] = 0;
			if (delimiter && !strcmp(bol, delimiter))
			{
				char         *t = s;
				
				s = (char *) realloc(s, bol - s + 1);
				s[bol - t] = '\0';
				return s;
			}
			bol = &s[i + 1];
		}
		s[i++] = ich;
	}
}
		
#define EXPAND \
	if (i == slen) {                   \
	s = (char *) realloc(s, slen *= 2);      \
	}
/***************************************************************************/
/* ParseLine                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   ifile                                                                 */
/*   loadData                                                              */
/* Purpose :                                                               */
/***************************************************************************/
int ParseLine(LPMAPDOCDATA lpMapDocData, FILE *ifile, BOOL loadData)
{
	int                   ich;
	char                  *value, *head, *name, *s = (char *)malloc(128);
	char                  *tmp, *commentline;
	int                   slen = 128;
	int                   i=0;
	int                   override = 0;
	int                   multiline = 0;
	
	ich = getc(ifile);
	
	/* Skip blank lines... */
	if (ich == '\n')
	{
		free(s);
		return 0;
	}
	/* Skip leading space... */
	if (isascii(ich) && isspace(ich))
	{
		ich = skipspace(ifile);
		if (ich == '\n')
		{
			free(s);
			return 0;
		}
	}
	/* Skip lines that start with comment character... */
	if (ich == '#')
	{
		commentline = (char *) malloc(2);
		sprintf(commentline,"\000");
		ich = getc(ifile);
		while ( (ich != EOF) && (ich != '\n') )
		{
			tmp = (char *) malloc(strlen(commentline)+2);
			sprintf(tmp,"%s%c",commentline,ich);
			free(commentline);
			commentline = tmp;
			ich = getc(ifile);
		}
		if (ich == '\n')
		{
			tmp = (char *) malloc(strlen(commentline)+2);
			sprintf(tmp,"%s\n",commentline);
			free(commentline);
			commentline = tmp;
		}
		
		/* only add comment lines not created by mapxpress or mapedit*/ 
		if (strstr(commentline,"Created by") == NULL)
		{
			if ( lpMapDocData->MapStruct.comments == NULL )
			{
				lpMapDocData->MapStruct.comments = (char *) malloc(strlen(commentline)+1);
				lpMapDocData->MapStruct.comments = commentline;
			} else
			{
				tmp = (char *) malloc(strlen(lpMapDocData->MapStruct.comments)+strlen(commentline)+1);
				sprintf(tmp, "%s%s",lpMapDocData->MapStruct.comments,commentline);
				free(lpMapDocData->MapStruct.comments);
				free(commentline);
				lpMapDocData->MapStruct.comments = tmp;
			}
		}
		free(s);
		return 0;
	}
	/* Skip lines that start with the end of the file... :') */
	if (ich == EOF)
	{
		return 0;
	}
	/* Start with ascii? */
	if (!isascii(ich) || !isalpha(ich))
	{
		toeol(ifile);
		free(s);
		return 1;
	}
	s[i++] = ich;
	do
	{
		ich = getc(ifile);
		if (ich == '\n' || ich == '#' || ich == EOF)
		{
			if (ich == '#') toeol(ifile);
			free(s);
			return 1;
		}
		if (isascii(ich) && isspace(ich))
			continue;
		if (ich == ':')
			break;
		EXPAND;
		s[i++] = ich;
	}
	while (1);
	
	ich = skipspace(ifile);
	
	EXPAND;
	s[i++] = '\0';
	name = s;
	
	s = (char *) malloc(slen = 128);
	i = 0;
	do
	{
		EXPAND;
		s[i++] = ich;
		ich = getc(ifile);
	}
	while (ich != EOF && ich != '#' && ich != '\n');
	
	if (ich == '#')
		toeol(ifile);
	
	EXPAND;
	s[i++] = 0;
	head = value = s;
	s = value + strlen(value) - 1;
	while (s >= value && isascii(*s) && isspace(*s))
		--s;
	*++s = 0;
	if (!strncmp(value, "\\override:", 10))
	{
		override = 1;
		value += 10;
	}
	while (*value && isascii(*value) && isspace(*value))
		++value;
	if (!strncmp(value, "\\multiline:", 11))
	{
		multiline = 1;
		value += 11;
	}
	while (*value && isascii(*value) && isspace(*value))
		++value;
	if (!*value)
	{
		free(name);
		free(head);
		return 1;
	}
	if (multiline) 
		value = (char *) getMultilineValue(value, ifile);
	i = AddOption(lpMapDocData, name, value, TRUE, loadData);
	
	free(name);
	free(head);
	return i;
}
/***************************************************************************/
/* AddOption                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*    *name                                                                */
/*    *value                                                               */
/*    output                                                               */
/*    loadData                                                             */
/* Purpose :                                                               */
/***************************************************************************/
int AddOption(LPMAPDOCDATA lpMapDocData, char *name, char *value, BOOL output, BOOL loadData)
{
	int                   option, i;
	char                  *tmp;

	for (i=0; i < (int) strlen(name); i++)
	{
		 if (isupper(name[i]))
			name[i] = tolower(name[i]);
	}

	option = FindOption(lpMapDocData, name);

	if ( option >= NUMPREFS )
	{
		if (lpMapDocData->MapStruct.comments == NULL)
		{
			lpMapDocData->MapStruct.comments = (char *) malloc(strlen(name)+strlen(value)+3);
			sprintf(lpMapDocData->MapStruct.comments,"%s:%s \n",name,value);
		}
		else
		{
			tmp = (char *) malloc(strlen(lpMapDocData->MapStruct.comments)+strlen(name)+strlen(value)+3);
			sprintf(tmp,"%s%s:%s \n",lpMapDocData->MapStruct.comments,name,value);
			free(lpMapDocData->MapStruct.comments);
			lpMapDocData->MapStruct.comments = tmp;
		}
		return 0;
	}
	if (output)
		lpMapDocData->PrefsArray[option].output = TRUE;

		switch(lpMapDocData->PrefsArray[option].type)
		{
			
		case MAPDATA:
			if (loadData)
			return (LoadMapData(lpMapDocData, value));
			return 0;
		case MAPWIDTH:
			lpMapDocData->MapStruct.width = atoi(value);
			strncpy(lpMapDocData->MapStruct.width_str,value,3);
			return 0;
			
		case MAPHEIGHT:
			lpMapDocData->MapStruct.height = atoi(value);
			strncpy(lpMapDocData->MapStruct.height_str,value,3);
			return 0;
			
		case STRING:
		case COORD:
			strncpy(lpMapDocData->PrefsArray[option].charvar,value,lpMapDocData->PrefsArray[option].length);
			return 0;
			
		case YESNO:
			(*(lpMapDocData->PrefsArray[option].intvar)) = YesNo(value);
			return 0;
			
		case INT:
		case POSINT:
		case FLOAT:
		case POSFLOAT:
		case LISTINT:
			strcpy(lpMapDocData->PrefsArray[option].charvar, StrToNum(value,lpMapDocData->PrefsArray[option].length,lpMapDocData->PrefsArray[option].type));
			return 0;
		}
		return 1;
}
/***************************************************************************/
/* YesNo                                                                   */
/* Arguments :                                                             */
/*   val                                                                   */
/* Purpose :                                                               */
/***************************************************************************/
int YesNo(char *val)
{
	if ( (tolower(val[0]) == 'y') || (tolower(val[0]) == 't') )
		return 1;

	return 0;
}

/***************************************************************************/
/* char *StrToNum                                                          */
/* Arguments :                                                             */
/*   string                                                                */
/*   len                                                                   */
/*   type                                                                  */
/* Purpose :                                                               */
/***************************************************************************/
char *StrToNum(char *string, int len, int type)
{
	char                  *returnval;
	
	returnval = (char *) malloc(len+1);
	returnval[0] = (char) NULL;
	
	if ( type == FLOAT || type == INT)
	{
		if ( (string[0] == '-') || ((string[0] >= '0') && (string[0] <= '9')) )
			sprintf(returnval,"%s%c",returnval,string[0]);
		
	}
	else if ( (string[0] == '-') || ((string[0] >= '0') &&
        (string[0] <= '9')) || (string[0] == '.') ) 
		sprintf(returnval,"%s%c",returnval,string[0]);
	
	string++;
	while ( (string[0] != (char) NULL) && ((int) strlen(returnval) <= (len-1) ) )
	{
		if ( type == FLOAT || type == POSFLOAT )
		{
            sprintf(returnval,"%s%c",returnval,string[0]);
		}
		else if ((string[0] >= '0') && (string[0] <= '9'))
			sprintf(returnval,"%s%c",returnval,string[0]);
		string++;
	}
	return (char *) returnval;
}

/***************************************************************************/
/* LoadMapData                                                             */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   value                                                                 */
/* Purpose :                                                               */
/***************************************************************************/
int LoadMapData(LPMAPDOCDATA lpMapDocData, char *value)
{
	int                   x=0, y=0; 
	
	while ( *value != '\0' )
	{
		if ( *value == '\n' )
		{
			x = 0;
			y ++;
		}
		else
		{
			lpMapDocData->MapStruct.data[x++][y].cdata = *value;
		}
		value ++;
	}
	CountBases(lpMapDocData);
	CountCheckPoints(lpMapDocData);
	return 0;
}
/***************************************************************************/
/* NewMap                                                                  */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/* Purpose :                                                               */
/***************************************************************************/
int NewMap(LPMAPDOCDATA lpMapDocData)
{
	int                   i,j;
	
	if (lpMapDocData->MapStruct.comments)
		free(lpMapDocData->MapStruct.comments);
	
	lpMapDocData->MapStruct.comments = (char *) NULL;
    lpMapDocData->MapStruct.mapName[0] = lpMapDocData->MapStruct.mapFileName[0] = (char ) NULL;
	lpMapDocData->MapStruct.width = DEFAULT_WIDTH;
	sprintf(lpMapDocData->MapStruct.width_str,"%d",lpMapDocData->MapStruct.width);
	lpMapDocData->MapStruct.height = DEFAULT_HEIGHT;
	sprintf(lpMapDocData->MapStruct.height_str,"%d",lpMapDocData->MapStruct.height);
	lpMapDocData->MapStruct.view_zoom = DEFAULT_MAP_ZOOM;
    lpMapDocData->MapStruct.changed = 0;
	for ( i=0; i<MAX_MAP_SIZE; i++) 
		for ( j=0; j<MAX_MAP_SIZE; j++)
		{
			lpMapDocData->MapStruct.data[i][j].cdata = MAP_SPACE;
			lpMapDocData->MapStruct.data[i][j].backx = VOIDBACK;
			lpMapDocData->MapStruct.data[i][j].backy = VOIDBACK;
		}
	for (i = 4; i < NUMPREFS; i++)
		lpMapDocData->PrefsArray[i].output = FALSE;

	EmptyTempData();
	Setup_default_server_options(lpMapDocData);
	VoidUndo(lpMapDocData);
	return 0;
}
/***************************************************************************/
/* CountBases                                                              */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/* Purpose :                                                               */
/***************************************************************************/
void CountBases(LPMAPDOCDATA lpMapDocData)
{
	int x,y;
	BOOL update  = FALSE;
	
	free(lpMapDocData->MapItems.base);
	lpMapDocData->MapItems.NumBases = 0;


	if ( (lpMapDocData->MapItems.base = (base_t *)
		malloc(sizeof(base_t))) == NULL ) {
			ErrorHandler("Memory Error: Out of memory - bases");
			exit(-1);
	}

	
	//count the number of items
	for (x = 0; x < lpMapDocData->MapStruct.width; x++)
		for (y = 0; y < lpMapDocData->MapStruct.height; y++)
		{
			switch (lpMapDocData->MapStruct.data[x][y].cdata)
			{
			case '_':
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].pos.x = x;
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].pos.y = y;
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].team = 0;
				lpMapDocData->MapItems.NumBases++;
				update = TRUE;
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].pos.x = x;
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].pos.y = y;
				lpMapDocData->MapItems.base[lpMapDocData->MapItems.NumBases].team = lpMapDocData->MapStruct.data[x][y].cdata - '0';
				lpMapDocData->MapItems.NumBases++;
				update = TRUE;
				break;
			}
			//reallocate the memory for the new number of items
			if (update)
			{
				if (lpMapDocData->MapItems.NumBases > 0
					&& (lpMapDocData->MapItems.base = (base_t *)
					realloc(lpMapDocData->MapItems.base, (lpMapDocData->MapItems.NumBases + 1) * sizeof(base_t))) == NULL) {
					ErrorHandler("Memory Error: Out of memory - bases");
					exit(-1);
				}
			}
			
			update = FALSE;
		}

	//Exit Function quietly.

}
/***************************************************************************/
/* CountCheckPoints                                                        */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/* Purpose : Counts the check points.                                      */
/***************************************************************************/
void CountCheckPoints(LPMAPDOCDATA lpMapDocData)
{
	int x,y;

	lpMapDocData->MapItems.NumChecks = 0;
	
	for (x = 0; x <= 25; x++)
		lpMapDocData->MapItems.checkpoints[x] = 0;
	
	for (x = 0; x < lpMapDocData->MapStruct.width; x++)
		for (y = 0; y < lpMapDocData->MapStruct.height; y++)
			if ((lpMapDocData->MapStruct.data[x][y].cdata >= 'A') && (lpMapDocData->MapStruct.data[x][y].cdata <= 'Z'))
			{
				lpMapDocData->MapItems.checkpoints[lpMapDocData->MapStruct.data[x][y].cdata - 65]++;
				lpMapDocData->MapItems.NumChecks++;
			}


}
/***************************************************************************/
/* FindNextCheckPoint                                                      */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/* Purpose : returns the value of the next checkpoint.                     */
/***************************************************************************/
char FindNextCheckPoint(LPMAPDOCDATA lpMapDocData)
{
	int x, next;
	
	next = 0;

	for (x = 1; x <= 25;x++)
		if (lpMapDocData->MapItems.checkpoints[x] < lpMapDocData->MapItems.checkpoints[x-1])
			next = x;


	return (char)(next+65);

}
/***************************************************************************/
/* FindOption                                                              */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*    *name                                                                */
/* Purpose :                                                               */
/***************************************************************************/
int FindOption(LPMAPDOCDATA lpMapDocData, char *name)
{
	int                   option;

	for (option = 0; option < NUMPREFS; option++)
	{
		if(!strcmp(name, lpMapDocData->PrefsArray[option].name))
			break;
		/*If the first name isn't it, check the alternative*/
		if(!strcmp(name, lpMapDocData->PrefsArray[option].altname))
			break;
	}

	return option;
}