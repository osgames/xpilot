/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "mapxpress.h"

/***************************************************************************/
/* AboutDlgProc                                                            */
/* Arguments :                                                             */
/*    hDlg                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/* Purpose :   The procedure for the about dialog box                      */
/***************************************************************************/
BOOL CALLBACK AboutDlgProc (HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
//	FILE *fp;
//	char ch;
//	int count = 0;
//	char *credits, *tmp;

	switch(iMsg)
	{
	case WM_INITDIALOG :
		SetEditText (hDlg, IDC_ABOUTVERSION, szAppName);
		SetEditText (hDlg, IDC_ABOUTXPILOTVERSION, szXpilotVersion);

/*		This block of code, with the commented variables above...allows the
credits text to be read in from a text file....but this would require the about.txt
file to be shipped with MapXpress, and I don't really want to require that. I'd
much rather include the text file at compile time...If I could figure out how.*/
	/*	if (NULL == (fp = fopen ("about.txt", "rb")))
		{
			ErrorHandler("Couldn't open '%s' for reading!", "about.txt");
			return FALSE;
		}

		credits = (char *) malloc(2);

		while ((ch = getc(fp)) != EOF)
		{
			tmp = (char *) malloc(strlen(credits)+2);
			sprintf(tmp, "%s%c", credits, ch);
			free(credits);
			credits = tmp;
		}	
		
		SetEditText(hDlg, IDC_ABOUTTEXT, credits);*/

		SetEditText (hDlg, IDC_ABOUTTEXT, "Map Xpress was created by: Jarrod L. Miller (Daedlaes) \
		\r\nCopyright �2001  - Jarrod L. Miller \
		\r\n \
		\r\nFor any questions, comments, suggestions, mail to: mapedit@xpilot.org \
		\r\n                                               or: jlmiller@ctitech.com\
		\r\n \
		\r\nMap Xpress comes with ABSOLUTELY NO WARRANTY, implied or otherwise.\
		\r\nFor details see the provided License file. \
		\r\n \
		\r\nSome code for Map Xpress was ported from XMapedit on unix, which was\
		\r\nwritten by Aaron Averill, with revisions by Robert Templeman, William Doctor, \
		\r\nand recent updates by Jarrod Miller. \
		\r\n \
		\r\n \
		\r\nMap Xpress also includes sections of code originally contained in the programs: \
		\r\n \
		\r\nwildmap.c - Random Map Generator. \
		\r\nAuthors: \
		\r\n   Bj�rn Stabell        (bjoern@xpilot.org) \
		\r\n   Ken Ronny Schouten   (ken@xpilot.org) \
		\r\n   Bert Gijsbers        (bert@xpilot.org) \
		\r\n \
		\r\npbmtomap.c - Pbm Image File to Map Converter. \
		\r\nAuthor: Greg Renda (greg@ncd.com)\
		\r\nwith an update by: Ben Armstrong (synrg@sanctuary.nslug.ns.ca) \
		\r\n \
		\r\nmaps2image.c - X11 Bitmap and Pbm Export Utility. \
		\r\nAuthor: Andrew W. Scherpbier \
		\r\n \
		\r\nSome of the Windows bitmap export code I used came from the \
		\r\nMicrosoft code samples. \
		\r\n \
		\r\nMuch thanks to Vincent for all of his input and support, both with new \
		\r\nideas and pointing out all the stupid little things I missed. \
		\r\n \
		\r\nMap Xpress may include code from other sources: these are not listed here, \
		\r\nbecause the author couldn't remember where he swiped it from. Sorry to anyone \
		\r\noverlooked. \
		");
		
		return TRUE;
		
	case WM_COMMAND :
		switch (LOWORD (wParam))
		{
		case IDOK:
		case IDCANCEL :
			EndDialog (hDlg, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}
