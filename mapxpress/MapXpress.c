/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"
#include "default_settings.h"

#define UNTITLED "(untitled)"

unsigned char *xpilotmap_dir;
unsigned long *xpilotmap_dir_length;
unsigned char *filled_world;
unsigned long *filled_world_length;
unsigned char *trans_paste;
unsigned long *trans_paste_length;
unsigned char *save_all_prefs;
unsigned long *save_all_prefs_length;
unsigned char *editor_command;
unsigned long *editor_command_length;
unsigned char *mini_map;
unsigned long *mini_map_length;
unsigned char *client_command;
unsigned long *client_command_length;
unsigned char *server_command;
unsigned long *server_command_length;
unsigned char *template_file;
unsigned long *template_file_length;
unsigned char *usetemplate_file;
unsigned long *usetemplate_file_length;

BOOL FilledWorld;
BOOL TransPaste;
BOOL SaveAllPrefs;
BOOL Mini_Map;
BOOL UseTemplateFile = FALSE;

static BOOL fDrawing;
static BOOL fPasting;
static BOOL fDragging;
static BOOL fClipboarded;
static BOOL fSelect;

int iSelectionMapSyms = 0;
int iSelectionTools = 0;
int iSelectionShape = 0;

char lastTeamBase = '0';


int cWidth, cHeight;
map_data_t               clipdata;
map_data_t               ClipBoard;


HWND hwndFrame = NULL;
HWND hwndClient = NULL;
HWND hwndChild 	 = NULL;    /* Handle to currently activated child   */
HWND hwndMiniMap = NULL;
HWND hwndThumb = NULL;

HWND hwndFileToolBar = NULL;
HWND hwndMapSymsToolBar = NULL;
HWND hwndToolsToolBar = NULL;
HWND hwndShapeToolBar = NULL;
HWND hwndStatusBar = NULL;

HMENU hMenuInit, hMenuMap;
HMENU hMenuInitWindow, hMenuMapWindow;
HINSTANCE hInst;
HKEY xpilotkey;

static int wincxClient, wincyClient; 
static int cxClient, cyClient;
static POINT ptCurse;
POINT ptOutbeg, ptOutend;
static POINT ptBeg, ptEnd;

static PSTR szCmdLineMap;

char wmapwidth[4] = "200";
char wmapheight[4] = "200";
char wmapseed[20];
char wmapseed_ratio[10] = "0.19";
char wmapfill_ratio[10] = "0.18";
char wmapnum_bases[4] = "16";
char wmapnum_teams[4] = "3";
char wmapcannon_ratio[10] = "0.0020";
char wmapfuel_ratio[10] = "0.0006";
char wmapgrav_ratio[10] = "0.0006";
char wmapworm_ratio[10] = "0.0002";

unsigned long  *zfilterIndex;

char *mapErrors;

HBRUSH hBrushSolidWall;
HBRUSH hBrushFuel;
HBRUSH hBrushCannon;
HBRUSH hBrushBase;
HBRUSH hBrushDecorWall;
HBRUSH hBrushWormhole;
HBRUSH hBrushTreasure;
HBRUSH hBrushTarget;
HBRUSH hBrushItemConc;
HBRUSH hBrushGravity;
HBRUSH hBrushCurrent;
HBRUSH hBrushFriction;


HPEN hPenSolidWall;
HPEN hPenCannon;
HPEN hPenBase;
HPEN hPenDecorWall;
HPEN hPenWormhole;
HPEN hPenTreasure;
HPEN hPenTarget;
HPEN hPenItemConc;
HPEN hPenGravity;
HPEN hPenCurrent;
HPEN hPenFriction;

/***************************************************************************/
/* WinMain                                                                 */
/* Arguments :                                                             */
/*    hInstance                                                            */
/*   hPrevInstance                                                         */
/*   szCmdLine                                                             */
/*   iCmdShow                                                              */
/* Purpose :   The main windows procedure                                  */
/***************************************************************************/
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow)
{
	HACCEL  hAccel;
	HWND hwndLocFrame;
	HWND hwndLocClient;
	MSG		 msg ;
    WNDCLASSEX	wc;

	hInst = hInstance;
	szCmdLineMap = szCmdLine;
	
	/*create the space for the registry vars, give a default value*/
	/*******************************************************************/
	xpilotmap_dir = (unsigned char *) malloc(_MAX_PATH);
	xpilotmap_dir_length = (unsigned long *) malloc(sizeof(unsigned long));
	filled_world = (unsigned char *) malloc(_MAX_PATH);
	filled_world_length = (unsigned long *) malloc(sizeof(unsigned long));
	trans_paste = (unsigned char *) malloc(_MAX_PATH);
	trans_paste_length = (unsigned long *) malloc(sizeof(unsigned long));
	save_all_prefs = (unsigned char *) malloc(_MAX_PATH);
	save_all_prefs_length = (unsigned long *) malloc(sizeof(unsigned long));
	editor_command = (unsigned char *) malloc(_MAX_PATH);
	editor_command_length = (unsigned long *) malloc(sizeof(unsigned long));
	mini_map = (unsigned char *) malloc(_MAX_PATH);
	mini_map_length = (unsigned long *) malloc(sizeof(unsigned long));
	client_command = (unsigned char *) malloc(_MAX_PATH);
	client_command_length = (unsigned long *) malloc(sizeof(unsigned long));
	server_command = (unsigned char *) malloc(_MAX_PATH);
	server_command_length = (unsigned long *) malloc(sizeof(unsigned long));
	template_file = (unsigned char *) malloc(_MAX_PATH);
	template_file_length = (unsigned long *) malloc(sizeof(unsigned long));
	usetemplate_file = (unsigned char *) malloc(_MAX_PATH);
	usetemplate_file_length = (unsigned long *) malloc(sizeof(unsigned long));
	
	sprintf((char *) xpilotmap_dir, "C:\\Xpilot\\lib\\maps");
	sprintf((char *) filled_world, "0");
	sprintf((char *) trans_paste, "0");
	sprintf((char *) save_all_prefs, "0");
	sprintf((char *) editor_command, "C:\\Progra~1\\Window~1\\Access~1\\WordPad");
	sprintf((char *) mini_map, "0");
	sprintf((char *) client_command, "C:\\Xpilot\\Xpilot");
	sprintf((char *) server_command, "C:\\Xpilot\\XPilotServer");
	sprintf((char *) template_file,  "mapxpress_template.xp");
	sprintf((char *) usetemplate_file, "0");
	
	*xpilotmap_dir_length = _MAX_PATH;
	*filled_world_length = _MAX_PATH;
	*trans_paste_length = _MAX_PATH;
	*save_all_prefs_length = _MAX_PATH;
	*editor_command_length = _MAX_PATH;
	*mini_map_length = _MAX_PATH;
	*client_command_length = _MAX_PATH;
	*server_command_length = _MAX_PATH;
	*template_file_length = _MAX_PATH;
	*usetemplate_file_length = _MAX_PATH;

	hBrushSolidWall = CreateSolidBrush(COLOR_WALL);
	hBrushFuel = CreateSolidBrush (COLOR_FUEL);
	hBrushCannon = CreateSolidBrush (COLOR_CANNON);
	hBrushBase = CreateSolidBrush (COLOR_BASE);
	hBrushDecorWall = CreateSolidBrush (COLOR_DECOR);
	hBrushWormhole = CreateSolidBrush (COLOR_WORMHOLE);
	hBrushTreasure = CreateSolidBrush (COLOR_TREASURE);
	hBrushTarget = CreateSolidBrush (COLOR_TARGET);
	hBrushItemConc = CreateSolidBrush (COLOR_ITEM_CONC);
	hBrushGravity = CreateSolidBrush (COLOR_GRAVITY);
	hBrushCurrent = CreateSolidBrush (COLOR_CURRENT);
	hBrushFriction = CreateSolidBrush (COLOR_FRICTION);

	hPenSolidWall = CreatePen (PS_SOLID,0,COLOR_WALL);
	hPenCannon = CreatePen (PS_SOLID,0,COLOR_CANNON);
	hPenBase = CreatePen (PS_SOLID,0,COLOR_BASE);
	hPenDecorWall = CreatePen (PS_SOLID,0,COLOR_DECOR);
	hPenWormhole = CreatePen (PS_SOLID,0,COLOR_WORMHOLE);
	hPenTreasure = CreatePen (PS_SOLID,0,COLOR_TREASURE);
	hPenTarget = CreatePen (PS_SOLID,0,COLOR_TARGET);
	hPenItemConc = CreatePen (PS_SOLID,0,COLOR_ITEM_CONC);
	hPenGravity = CreatePen (PS_SOLID,0,COLOR_GRAVITY);
	hPenCurrent = CreatePen (PS_SOLID,0,COLOR_CURRENT);
	hPenFriction = CreatePen (PS_SOLID,0,COLOR_FRICTION);

	/******************************************************************/
	if (!hPrevInstance) {
    // Register the frame class
	wc.cbSize        = sizeof (wc) ;
    wc.style	     = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = FrameWndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon (hInst, MAKEINTRESOURCE(IDI_LGICON)) ;
    wc.hCursor	     = LoadCursor(NULL,IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetSysColorBrush (COLOR_ACTIVEBORDER) ; //(HBRUSH) (COLOR_APPWORKSPACE+1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = szAppName ;
	wc.hIconSm = NULL;
	
    if (!RegisterClassEx (&wc) )
		return FALSE;
	
	// Register the MDI child class 
	wc.cbSize        = sizeof (wc) ;
	wc.style	     = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = MapWndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = sizeof (HANDLE);
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon (hInst, MAKEINTRESOURCE(IDI_LGICON)) ; 
    wc.hCursor	     = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH) ;
    wc.lpszMenuName  = NULL; 
    wc.lpszClassName = "ChildMapWin"; 
	wc.hIconSm = NULL;
	
    if (!RegisterClassEx(&wc))
		return FALSE;
    

	// Register the Mini Map class 
	wc.cbSize        = sizeof (wc) ;
	wc.style	     = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = MiniMapWndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = sizeof (HANDLE);
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon (hInst, MAKEINTRESOURCE(IDI_LGICON)) ; 
    wc.hCursor	     = LoadCursor(NULL,IDC_ARROW);
	wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH) ;
    wc.lpszMenuName  = NULL; 
    wc.lpszClassName = "MiniMapWin"; 
	wc.hIconSm = NULL;
	
    if (!RegisterClassEx(&wc))
		return FALSE;
    }
	
    /* Obtain handles to two possible menu's */
	hMenuInit = LoadMenu (hInst, MAKEINTRESOURCE(MDIINITMENU));
	hMenuMap = LoadMenu (hInst, MAKEINTRESOURCE(MDIMAPMENU));
	
	hMenuInitWindow = GetSubMenu (hMenuInit, 0);
	hMenuMapWindow = GetSubMenu (hMenuMap, 5);	
	
    /* Load main menu accelerators */
    hAccel = LoadAccelerators (hInstance, MAKEINTRESOURCE(IDR_ACCELERATOR));
	
    /* Create the frame */
    hwndLocFrame = CreateWindow (szAppName,
		"MapXpress - The Xpilot Map Editor",
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		hMenuInit,
		hInstance,
		NULL);
	
	hwndLocClient = GetWindow(hwndLocFrame, GW_CHILD);

	hwndFrame = hwndLocFrame;
	hwndClient = hwndLocClient;
	
    ShowWindow (hwndLocFrame, iCmdShow);
    UpdateWindow (hwndLocFrame);
	

		
    /* Enter main message loop */
    while (GetMessage (&msg, NULL, 0, 0)){
	/* If a keyboard message is for the MDI , let the MDI client
	* take care of it.  Otherwise, check to see if it's a normal
	* accelerator key (like F3 = Zoom In).  Otherwise, just handle
	* the message as usual.
		*/
		if ( !TranslateMDISysAccel (hwndLocClient, &msg) &&
			!TranslateAccelerator (hwndLocFrame, hAccel, &msg)){
			TranslateMessage (&msg);
			DispatchMessage (&msg);
		}
    }
	
	DestroyMenu (hMenuMap);
	
    return msg.wParam ;
}
/***************************************************************************/
/* DoCaption                                                               */
/* Arguments :                                                             */
/*    lpMapDocData - pointer to document data.                             */
/*    hwnd - the parent window.                                            */
/*   *szCaptionText - the title text to set to                             */
/*                                                                         */
/*                                                                         */
/* Purpose :   Updates the windows caption to the specified text.          */
/***************************************************************************/
void DoCaption (LPMAPDOCDATA lpMapDocData, HWND hwnd, char *szCaptionText)
{
	char szCaption[64 + _MAX_FNAME + _MAX_EXT] ;
	
	wsprintf (szCaption, "** %s **%s",
		szCaptionText[0] ? szCaptionText : "Unnamed Map", lpMapDocData->MapStruct.changed ? "+" : "") ;
	
	SetWindowText (hwnd, szCaption) ;
}
/***************************************************************************/
/* AskAboutSave                                                            */
/* Arguments :                                                             */
/*    hwnd  - the parent window                                            */
/*   *szTitleName - title to display on the query box.                     */
/*                                                                         */
/*                                                                         */
/* Purpose :   Asks whether user wants to save the current file            */
/***************************************************************************/
short AskAboutSave (HWND hwnd, char *szTitleName)
{
	char szBuffer[64 + _MAX_FNAME + _MAX_EXT] ;
	int  iReturn ;
	
	wsprintf (szBuffer, "Save current changes in: %s?",
		szTitleName[0] ? szTitleName : UNTITLED) ;
	
	iReturn = MessageBox (hwnd, szBuffer, szAppName,
		MB_YESNOCANCEL | MB_ICONQUESTION) ;
	
	if (iReturn == IDYES)
		if (!SendMessage (hwndFrame, WM_COMMAND, IDM_SAVE, 0L))
			iReturn = IDCANCEL ;
		
		return iReturn ;
}
/***************************************************************************/
/* FrameWndProc                                                            */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/*                                                                         */
/* Purpose :   The procedure for the main window, contains mdi client win &*/
/* toolbars                                                                */
/***************************************************************************/
LRESULT CALLBACK FrameWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static WNDPROC lpfnAboutDlgProc;
	static BOOL      bNeedSave = FALSE ;
	static HINSTANCE hWndInstance;
	static HWND hwndLocClient;
	LPMAPDOCDATA lpMapDocData;
	CLIENTCREATESTRUCT clientcreate;
	char szTempFileName[_MAX_PATH] ;
	char szTempTitleName[_MAX_FNAME + _MAX_EXT] ;
	FILE *tmpfile = NULL;
	int *lpParts;
	BOOL bRunWildmap=FALSE;
	BOOL bLoadPrefs=FALSE;
	
	switch (iMsg)
	{
	case WM_CREATE:          // Create the client window
		
		clientcreate.hWindowMenu  = hMenuInitWindow ;
		clientcreate.idFirstChild = 100 ;
		
		hwndLocClient = CreateWindow ("MDICLIENT", NULL,
			WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE,
			0, 0, 0, 0, hwnd, (HMENU) 1, hInst,
			(LPSTR) &clientcreate) ;

		hWndInstance = ((LPCREATESTRUCT) lParam)->hInstance;
		
		XpFileInitialize (hwnd) ;
		hwndFileToolBar = InitFileToolBar(hwnd);			
		hwndMapSymsToolBar = InitMapSymsToolBar(hwnd);
		hwndToolsToolBar = InitToolsToolBar(hwnd);
		hwndShapeToolBar = InitShapeToolBar(hwnd);
		
		hwndStatusBar = CreateStatusWindow (WS_CHILD |
			WS_VISIBLE |
			WS_CLIPSIBLINGS |
			SBARS_SIZEGRIP |
			CCS_BOTTOM,
			"Ready",
			hwnd,
			2);		
		
		lpParts = (int *) malloc(sizeof(int) * 4);
		lpParts[0] = 100;
		lpParts[1] = 200;
		lpParts[2] = 300;
		lpParts[3] = 800;
		SendMessage(hwndStatusBar, SB_SETPARTS, (WPARAM) 4,(LPARAM) lpParts);

		//Process all the Registry information
		Process_Registry(hWndInstance, hwnd, hMenuMap);
		EnableClipboard(hwndFileToolBar, hMenuMap, FALSE);
		EnableUndo(hwndFileToolBar, hMenuMap, FALSE);


		if(szCmdLineMap && strlen(szCmdLineMap) )
		{
			if (NULL == (tmpfile = fopen (szCmdLineMap, "rb")))
			{
				SendMessage(hwnd, WM_COMMAND, IDM_NEW, 0);
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			}
			else
			{
				fclose(tmpfile);
				SendMessage(hwnd, WM_COMMAND, IDM_NEW, 0);
				
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);

				//Process any map from the command line or dropped into here
				strcpy(lpMapDocData->MapStruct.mapFileName, szCmdLineMap);
				
				if (lpMapDocData->MapStruct.mapFileName)
				{
					LoadMap(lpMapDocData, lpMapDocData->MapStruct.mapFileName, TRUE);
					InvalidateRect (hwndChild, NULL, TRUE);
					DoCaption(lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName);
				}
			}
		}
		else
		{
				SendMessage(hwnd, WM_COMMAND, IDM_NEW, 0);
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
		}

		
		if (hwndChild != NULL)
		{

			if (Mini_Map)
				CreateMiniMap(lpMapDocData, hwndLocClient);

			SendMessage (hwndLocClient,
			WM_MDIACTIVATE,
			0,
			(LPARAM) hwndChild);
		
		}

		//Done processing command line maps
		return 0;
		
	case WM_SIZE :
		wincxClient = LOWORD (lParam) ;
		wincyClient = HIWORD (lParam) ;
		{
			int x,y,cx,cy;
			RECT rWindow ;
			
			GetWindowRect(hwndStatusBar, &rWindow);
			cy = rWindow.bottom - rWindow.top;
			x = 0;
			y = wincyClient - cy;
			cx = wincxClient;
			MoveWindow (hwndStatusBar, x, y, cx, cy, TRUE);
			MoveWindow (hwndLocClient, TOOLSWIDTH, 54, wincxClient-TOOLSWIDTH, wincyClient - 75, TRUE);
		}
		return 0;
		
	case WM_COMMAND:
		switch (wParam)
		{			
		case IDM_MAP_REC_RD : 
		case IDM_MAP_REC_LD : 
		case IDM_MAP_FILLED :
		case IDM_MAP_REC_RU :
		case IDM_MAP_REC_LU :
		case IDM_MAP_CAN_LEFT :
		case IDM_MAP_CAN_UP :
		case IDM_MAP_CAN_DOWN :
		case IDM_MAP_CAN_RIGHT :
		case IDM_MAP_BASE :
		case IDM_MAP_BASE_ORNT :
		case IDM_MAP_FUEL :
		case IDM_MAP_TARGET :
		case IDM_MAP_TREASURE :
		case IDM_MAP_ITEM_CONC :
		case IDM_MAP_GRAV_POS :
		case IDM_MAP_GRAV_NEG :
		case IDM_MAP_GRAV_ACWISE :
		case IDM_MAP_GRAV_CWISE :
		case IDM_MAP_WORM_NORMAL :
		case IDM_MAP_WORM_OUT :
		case IDM_MAP_WORM_IN :
		case IDM_MAP_CRNT_UP :
		case IDM_MAP_CRNT_LT :
		case IDM_MAP_CRNT_RT :
		case IDM_MAP_CRNT_DN :
		case IDM_MAP_DEC_RD :
		case IDM_MAP_DEC_LD :
		case IDM_MAP_DEC_FLD :
		case IDM_MAP_DEC_RU :
		case IDM_MAP_DEC_LU :
		case IDM_MAP_SPACE :
		case IDM_MAP_CAN_UNSPEC :
		case IDM_MAP_TEAMBASE :
		case IDM_MAP_CHECKPOINT :
		case IDM_MAP_FRICTION:
		case IDM_MAP_EMPTYTREASURE:
		case IDM_MAP_ASTEROID_CONC:
			iSelectionMapSyms = LOWORD (wParam);
			return 0;

		case IDM_CIRCLEEMPTY :
		case IDM_CIRCLEFILLED :
		case IDM_RECTEMPTY :
		case IDM_RECTFILLED :
			iSelectionShape = LOWORD (wParam);
			return 0;

		case IDM_PEN :
		case IDM_ERASE :
		case IDM_SELECT :
		case IDM_FILL :
		case IDM_LINE :
		case IDM_LINEFILL :
		case IDM_SHAPE :
			fSelect = FALSE;
			iSelectionTools = LOWORD (wParam);

			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			if (lpMapDocData)
				if (lpMapDocData->fSelected)
				{
					lpMapDocData->fSelected = FALSE;
					EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);
					DrawBoxOutline (hwndChild, ptBeg, ptEnd) ;
				}

			if (iSelectionTools == IDM_SHAPE)
				ShowWindow(hwndShapeToolBar, SW_SHOW);
			else
				ShowWindow(hwndShapeToolBar, SW_HIDE);

			return 0;
			
		case IDM_WILDMAP :
			if (!DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_WILDPARAMS), hwnd, WildDlgProc))
			{
				return 0;
			}
			else
				bRunWildmap = TRUE;
			//FALL THROUGH
		case IDM_NEW:
			hwndChild = (HWND) AddMapWindow(hwndLocClient);

			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			/*Try to load the override file*/
			if(UseTemplateFile)
				if (LoadMap(lpMapDocData, (char *)template_file, TRUE))
				{
					if(hwndMiniMap && IsWindowVisible(hwndMiniMap))
					{
						SizeSmallMap(lpMapDocData);
						InvalidateRect(hwndMiniMap, NULL, TRUE);
					}
					DoCaption(lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName);
					UpdateStatusBarSettings(lpMapDocData);
				}

			if (bRunWildmap)
			{
				wildmap(lpMapDocData);
				InvalidateRect (hwndChild, NULL, TRUE);
				DoCaption(lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName);
				UpdateStatusBarSettings(lpMapDocData);
			}
			if(hwndMiniMap && IsWindowVisible(hwndMiniMap))
			{
				SizeSmallMap(lpMapDocData);
				InvalidateRect(hwndMiniMap, NULL, TRUE);
			}
			return 0;
			

		case IDM_LOADPREFS :
			bLoadPrefs = TRUE;
			// fall through
		case IDM_OPEN :
			szTempFileName[0]  = '\0' ;
			szTempTitleName[0]  = '\0' ;
			if (XpFileOpenDlg (hwnd, szTempFileName, szTempTitleName))
			{
				if (bLoadPrefs)
					hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
				else
					hwndChild = (HWND) AddMapWindow(hwndLocClient);

				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
				if (!LoadMap(lpMapDocData, szTempFileName, !bLoadPrefs))
				{
					ErrorHandler("Could not read file %s!", szTempTitleName);
					SendMessage (hwndClient, WM_MDIDESTROY, (WPARAM) hwndChild, 0);
					return 1;
				}
				else
					if (bLoadPrefs)
						lpMapDocData->MapStruct.changed = 1;
					else
						strcpy(lpMapDocData->MapStruct.mapFileName, szTempFileName);
			}
			else
				return 1;

			if(hwndMiniMap && IsWindowVisible(hwndMiniMap))
				{
				SizeSmallMap(lpMapDocData);
				InvalidateRect(hwndMiniMap, NULL, TRUE);
			}
			InvalidateRect (hwndChild, NULL, TRUE);
			DoCaption(lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName);
			UpdateStatusBarSettings(lpMapDocData);
			return 0;

		case IDM_SAVE :
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			strcpy(szTempFileName, lpMapDocData->MapStruct.mapFileName);
			if (szTempFileName[0])
			{
				if (!SaveMap(lpMapDocData, szTempFileName, TRUE, FALSE))
				{
					lpMapDocData->MapStruct.changed = FALSE ;
					DoCaption (lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName) ;
					return 1 ;
				}
				else
					ErrorHandler("Could not write file %s!", szTempTitleName);
				break;
			}
			// fall through
		case IDM_SAVEAS :
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			szTempFileName[0]  = '\0' ;
			szTempTitleName[0]  = '\0' ;
			if (XpFileSaveDlg (hwnd, szTempFileName, szTempTitleName))
			{
				BOOL rValue = FALSE; //Only true if error is returned.

				switch (*zfilterIndex) {
				case 1: //Xpilot Map
					if (rValue = SaveMap(lpMapDocData, szTempFileName, TRUE, FALSE))
					{
						lpMapDocData->MapStruct.changed = FALSE ;
						DoCaption (lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName) ;
						return 1 ;
					}
					break;
				case 2: //X11 Pixmap
					rValue = SaveXpmFile(lpMapDocData, szTempFileName);
					break;
				case 3: //X11 Bitmap
					rValue = SaveXbmFile(lpMapDocData, szTempFileName);
					break;			
				case 4: //PBMPLUS Bitmap
					rValue = SavePbmPlusFile(lpMapDocData, szTempFileName, 0);
					break;
				case 5: //PBMPLUS Pixmap
					rValue = SavePbmPlusFile(lpMapDocData, szTempFileName, 1);
					break;
				case 6: //PBMPLUS Pixmap
					rValue = SavePbmPlusFile(lpMapDocData, szTempFileName, 2);
					break;
				case 7: //Windows Bitmap
					{
						HWND hwndSaveWin = (HWND) SendMessage (hwndLocClient, WM_MDIGETACTIVE, 0, 0);
						if (hwndSaveWin == hwndMiniMap)
							rValue = SaveBmpFile(hwndMiniMap, szTempFileName);
						else
							rValue = SaveBmpFile(hwndChild, szTempFileName);
					}
					break;
				}
				if (rValue)
					ErrorHandler("Could not write file %s!", szTempTitleName);

			}
			return 0;

		case IDM_SAVESELECTEDAS :
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			szTempFileName[0]  = '\0' ;
			szTempTitleName[0]  = '\0' ;
			if (XpOnlySaveDlg (hwnd, szTempFileName, szTempTitleName))
			{
					if (!SaveMap(lpMapDocData, szTempFileName, TRUE, TRUE))
					{
						lpMapDocData->MapStruct.changed = FALSE ;
						DoCaption (lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName) ;
						return 1 ;
					}
					else
						ErrorHandler("Could not write file %s!", szTempTitleName);
			}
			InvalidateRect (hwndChild, NULL, TRUE);
			return 0;


		case IDM_SAVEPREFS :
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
			szTempFileName[0]  = '\0' ;
			szTempTitleName[0]  = '\0' ;
			if (XpTextSaveDlg (hwnd, szTempFileName, szTempTitleName))
			{
					if (!SaveMap(lpMapDocData, szTempFileName, FALSE, FALSE))
					{
						lpMapDocData->MapStruct.changed = FALSE ;
						DoCaption (lpMapDocData, hwndChild, lpMapDocData->MapStruct.mapName) ;
						return 1 ;
					}
					else
						ErrorHandler("Could not write file %s!", szTempTitleName);
			}
			return 0;
			
		case IDM_FILLEDWORLD :
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			FilledWorld = !FilledWorld;
			ToggleMenuItem(hMenuMap, LOWORD (wParam), FilledWorld);
			itoa(FilledWorld, (char *) filled_world, 10);
			RegSetValueEx(xpilotkey, (LPTSTR) "filled", 0, REG_SZ, filled_world, strlen((char *) filled_world) );			
			InvalidateRect(hwndChild, NULL, TRUE);
			return 0;	
			
		case IDM_TRANSPASTE :
			TransPaste = !TransPaste;
			ToggleMenuItem(hMenuMap, LOWORD (wParam), TransPaste);
			itoa(TransPaste, (char *) trans_paste, 10);
			RegSetValueEx(xpilotkey, (LPTSTR) "paste", 0, REG_SZ, trans_paste, strlen((char *) trans_paste) );			
			return 0;
			
		case IDM_SAVEALLPREFS :
			SaveAllPrefs = !SaveAllPrefs;
			ToggleMenuItem(hMenuMap, LOWORD (wParam), SaveAllPrefs);
			itoa(SaveAllPrefs, (char *) save_all_prefs, 10);
			RegSetValueEx(xpilotkey, (LPTSTR) "saveallprefs", 0, REG_SZ, save_all_prefs, strlen((char *) save_all_prefs) );			
			return 0;
			
		case IDM_MINIMAP :
			Mini_Map = !Mini_Map;
			ToggleMenuItem(hMenuMap, LOWORD (wParam), Mini_Map);
			if (!Mini_Map)
			{
				SendMessage(hwndLocClient, WM_MDIDESTROY, (WPARAM) hwndMiniMap, 0);
			}
			else
			{
   			    hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndChild, 0);
				CreateMiniMap(lpMapDocData, hwndLocClient);
			}
			itoa(Mini_Map, (char *) mini_map, 10);
			RegSetValueEx(xpilotkey, (LPTSTR) "mini_map", 0, REG_SZ, mini_map, strlen((char *) mini_map) );			
			return 0;
			
		case IDM_MAPDIR :
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPDIR), hwnd, DirDlgProc);
			return 0;
			
		case IDM_CLOSE :          // Close the active window
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);

			if (SendMessage (hwndChild, WM_QUERYENDSESSION, 0, 0))
					SendMessage (hwndLocClient, WM_MDIDESTROY, (WPARAM) hwndChild, 0);
			return 0;
			
		case IDM_EXIT :           // Exit the program
#ifndef USEHTMLHELP
			WinHelp(hwnd, NULL, HELP_QUIT, 0L);
#endif
			SendMessage (hwnd, WM_CLOSE, 0, 0);
			return 0;
			
			// messages for arranging windows
		case IDM_TILE :
			SendMessage (hwndLocClient, WM_MDITILE, 0, 0);
			return 0;
			
		case IDM_CASCADE :
			SendMessage (hwndLocClient, WM_MDICASCADE, 0, 0);
			return 0;
			
		case IDM_ARRANGE :
			SendMessage (hwndLocClient, WM_MDIICONARRANGE, 0, 0);
			return 0;
			
		case IDM_CLOSEALL :       // Attempt to close all children
			
			EnumChildWindows (hwndLocClient, &CloseEnumProc, 0);
			return 0;
			
		case IDM_ABOUT :
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
			return 0;
			
		case IDM_HELP :
#ifdef USEHTMLHELP
			HtmlHelp(hwnd, CHMHELPFILE, HH_DISPLAY_TOPIC, 0);
#else
			WinHelp(hwnd, HELPFILE, HELP_FINDER, 0L);
			WinHelp(hwnd, HELPFILE, HELP_QUIT, 0L);
#endif
			return 0;
			
		default: //Pass to active child...
			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			
			if (IsWindow (hwndChild))
			{
				SendMessage (hwndChild, WM_COMMAND, LOWORD (wParam), lParam);
			}
			break ;        // ...and then to DefFrameProc
		}
		break;

	
		case WM_QUERYENDSESSION :
		case WM_CLOSE :                     // Attempt to close all children
			SendMessage (hwnd, WM_COMMAND, IDM_CLOSEALL, 0);
			
			if (NULL != GetWindow(hwndLocClient, GW_CHILD))
				return 0;

			hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
			SendMessage (hwndLocClient, WM_MDIDESTROY, (WPARAM) hwndChild, 0);

			break ;   // I.e., call DefFrameProc 
			
		case WM_NOTIFY :
			{
				LPNMHDR pnmh = (LPNMHDR) lParam ;
				
				if (pnmh->code == TTN_NEEDTEXT)
				{
					LPTOOLTIPTEXT lpttt = (LPTOOLTIPTEXT) lParam;
					CopyToolTipText (lpttt);
				}
				return 0;
			}
			
		case WM_DESTROY : 
			PostQuitMessage (0) ;
			return 0;
    }
	// Pass unprocessed messages to DefFrameProc (not DefWindowProc)
	return DefFrameProc (hwnd,hwndLocClient,iMsg,wParam,lParam);
	
}
/***************************************************************************/
/* CloseEnumProc                                                           */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    lParam                                                               */
/*                                                                         */
/* Purpose :   Cycle through the MDI child windows and close them.         */
/***************************************************************************/
BOOL CALLBACK CloseEnumProc (HWND hwnd, LPARAM lParam)
{
	if (GetWindow (hwnd, GW_OWNER))         // Check for icon title
		return 1;
	SendMessage (GetParent (hwnd), WM_MDIRESTORE, (WPARAM) hwnd, 0);
	
	if (!SendMessage (hwnd, WM_QUERYENDSESSION, 0, 0))
		return 1;

	SendMessage (GetParent (hwnd), WM_MDIDESTROY, (WPARAM) hwnd, 0);
	return 1;
}
/***************************************************************************/
/* DrawBoxOutline                                                          */
/* Arguments :                                                             */
/*    hwnd - the parent window.                                            */
/*    ptBeg - the point to begin at.                                       */
/*    ptEnd - the point to end at.                                         */
/*                                                                         */
/* Purpose :   Draws a dashed box outline for selecting an area.           */
/***************************************************************************/
void DrawBoxOutline (HWND hwnd, POINT ptBeg, POINT ptEnd)
{
	HDC hdc ;
	
	RECT rect;

	rect.left = min(ptBeg.x, ptEnd.x) ;
	rect.top = min(ptBeg.y, ptEnd.y);
	rect.right = max(ptBeg.x, ptEnd.x) ;
	rect.bottom = max(ptBeg.y, ptEnd.y);
	
	hdc = GetDC (hwnd) ;
	
	DrawFocusRect (hdc, &rect) ;
	
	ReleaseDC (hwnd, hdc) ;
}
/***************************************************************************/
/* DrawMapOutline                                                          */
/* Arguments :                                                             */
/*    hwnd - the parent window.                                            */
/*    ptBeg - the point to begin at.                                       */
/*    ptEnd - the point to end at.                                         */
/*    color - the color to draw the box in                                 */
/*                                                                         */
/* Purpose :  Draws a box outline for the entire map in the specifie color */
/***************************************************************************/
void DrawMapOutline (HWND hwnd, POINT ptBeg, POINT ptEnd, COLORREF color)
{
	HDC hdc ;
	HPEN hPen;
	
	hdc = GetDC (hwnd) ;
	hPen = CreatePen(PS_SOLID, 0, color);
	
	SelectObject (hdc, GetStockObject (NULL_BRUSH)) ;
	SelectObject (hdc, hPen) ;
	Rectangle (hdc, ptBeg.x, ptBeg.y, ptEnd.x, ptEnd.y) ;
	
	DeleteObject (SelectObject(hdc, GetStockObject(BLACK_PEN)));
	ReleaseDC (hwnd, hdc) ;
}
/***************************************************************************/
/* DrawHighlightline                                                       */
/* Arguments :                                                             */
/*    hwnd - the parent window.                                            */
/*    ptBeg - the point to begin at.                                       */
/*    ptEnd - the point to end at.                                         */
/*                                                                         */
/* Purpose :   Draws a ROP2 line between two points.                       */
/***************************************************************************/
void DrawHighlightLine (HWND hwnd, POINT ptBeg, POINT ptEnd)
{
	static POINT pt[1];
	HPEN hPen;
	HDC hdc ;
	
	pt[0].x = ptBeg.x;
	pt[0].y = ptBeg.y;
	pt[1].x = ptEnd.x;
	pt[1].y = ptEnd.y;
	
	
	hPen = CreatePen(PS_SOLID, 0, RGB(255,0,0));
	
	hdc = GetDC (hwnd) ;
	SelectObject (hdc, hPen) ;
	
	SetROP2 (hdc, R2_NOT) ;
	
	Polyline (hdc, pt, 2) ;
	DeleteObject(SelectObject(hdc, (HPEN) BLACK_PEN));
	ReleaseDC (hwnd, hdc) ;
}
/***************************************************************************/
/* DrawHighlightline                                                       */
/* Arguments :                                                             */
/*    hwnd - the parent window.                                            */
/*    ptBeg - the point to begin at.                                       */
/*    ptEnd - the point to end at.                                         */
/*                                                                         */
/* Purpose :   Draws a ROP2 circle with two points as the radius.          */
/***************************************************************************/
void DrawHighlightCircle (HWND hwnd, POINT ptBeg, POINT ptEnd)
{
	int deltax, deltay;
	int rad;
	HPEN hPen;
	HDC hdc ;
	

	deltax = ptEnd.x - ptBeg.x;
	deltay = ptEnd.y - ptBeg.y;

	rad = (int) sqrt(deltax*deltax + deltay*deltay);

	hPen = CreatePen(PS_SOLID, 0, RGB(255,0,0));
	
	hdc = GetDC (hwnd) ;
	SelectObject (hdc, hPen) ;
	MoveToEx(hdc, ptBeg.x, ptBeg.y,  NULL);
	
	SetROP2 (hdc, R2_NOT) ;
	
	Arc(hdc, ptBeg.x - rad, ptBeg.y - rad, ptBeg.x + rad, ptBeg.y + rad,
		ptBeg.x + rad, ptBeg.y + rad, ptBeg.x + rad, ptBeg.y + rad);

	DeleteObject(SelectObject(hdc, (HPEN) BLACK_PEN));
	ReleaseDC (hwnd, hdc) ;
}
/***************************************************************************/
/* WndMapProc                                                              */
/* Arguments :                                                             */
/*    hwnd                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/*                                                                         */
/* Purpose :   The procedure for the child map windows.                    */
/***************************************************************************/
LRESULT CALLBACK MapWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndLocClient, hwndLocFrame;
	HWND hwndTmp;
	HDC         hdc ;
	RECT rect;
	LPMAPDOCDATA	lpMapDocData ;
	PAINTSTRUCT ps ;
	static HINSTANCE hWndInstance;
	char tempc;
	
	char sbtext[100];
	int iVPaintBeg, iVPaintEnd, iHPaintBeg, iHPaintEnd,
		iVscrollInc, iHscrollInc, dx, dy;
	
	switch (iMsg)
	{
	case WM_CREATE: {
		int i;

		hWndInstance = ((LPCREATESTRUCT) lParam)->hInstance;
		//Allocate memory for window private data
		lpMapDocData = (LPMAPDOCDATA) HeapAlloc (GetProcessHeap (), HEAP_ZERO_MEMORY, sizeof (MAPDOCDATA));
		
		/*Create a prefs template, setting the default values, then point the documents
		prefs array at the newly created prefs array.*/
		
		/*format for prefs structure {
		"map option name-lowercase",
		"map option name alternate-lowercase",
		"number of digits",
		"type of option",
		"name of map structure variable-for numbers",
		"&name of map structure variable-for yesno",
		"Is option output by default? (set to true when value is changed, most are false)",
		"Ctrl Id of first Control",
		"Ctrl Id of second Control"};
		*/  
		{
		prefs_t Prefs_Template[NUMPREFS]=
		{
			{ "mapwidth"     		,"mapwidth",3,MAPWIDTH, lpMapDocData->MapStruct.width_str,0, TRUE, IDC_MAPWIDTH},
			{ "mapheight"    		,"mapheight",3,MAPHEIGHT,lpMapDocData->MapStruct.height_str,0, TRUE, IDC_MAPHEIGHT},
			{ "mapname"             ,"mapname",255,STRING,lpMapDocData->MapStruct.mapName,0, TRUE, IDC_MAPNAME},
			{ "mapauthor"    		,"mapauthor",255,STRING,lpMapDocData->MapStruct.mapAuthor,0, TRUE, IDC_MAPAUTHOR},
			{ "limitedlives"		,"limitedlives",0,YESNO,0,&lpMapDocData->MapStruct.limitedLives, FALSE, IDC_LIMITEDLIVES},
			{ "worldlives"   		,"lives",3,POSINT,lpMapDocData->MapStruct.worldLives,0, FALSE, IDC_WORLDLIVES},
			{ "shipmass"     		,"shipmass",6,POSFLOAT,lpMapDocData->MapStruct.shipMass,0, FALSE, IDC_SHIPMASS},
			{ "gravity"      		,"gravity",6,FLOAT,lpMapDocData->MapStruct.gravity,0, FALSE, IDC_GRAVITY},
			{ "gravityangle" 		,"gravityangle",3,POSINT,lpMapDocData->MapStruct.gravityAngle,0, FALSE, IDC_GRAVITYANGLE},
			{ "gravitypoint" 		,"gravitypoint",7,COORD,lpMapDocData->MapStruct.gravityPoint,0, FALSE, IDC_GRAVITYPOINT},
			{ "gravitypointsource"		,"gravitypointsource",0,YESNO,0,&lpMapDocData->MapStruct.gravityPointSource, FALSE, IDC_GRAVITYPOINTSOURCE},
			{ "gravityclockwise"		,"gravityclockwise",0,YESNO,0,&lpMapDocData->MapStruct.gravityClockwise, FALSE, IDC_GRAVITYCLOCKWISE},
			{ "gravityanticlockwise"	,"gravityanticlockwise",0,YESNO,0,&lpMapDocData->MapStruct.gravityAnticlockwise, FALSE, IDC_GRAVITYANTICLOCKWISE},
			{ "shotsgravity" 		,"shotsgravity",0,YESNO,0,&lpMapDocData->MapStruct.shotsGravity, FALSE, IDC_SHOTSGRAVITY},
			{ "shotmass"     		,"shotmass",6,POSFLOAT,lpMapDocData->MapStruct.shotMass,0, FALSE, IDC_SHOTMASS},
			{ "shotspeed"    		,"shotspeed",6,FLOAT,lpMapDocData->MapStruct.shotSpeed,0, FALSE, IDC_SHOTSPEED},
			{ "shotlife"     		,"shotlife",3,POSINT,lpMapDocData->MapStruct.shotLife,0, FALSE, IDC_SHOTLIFE},
			{ "maxplayershots"		,"shots",3,POSINT,lpMapDocData->MapStruct.maxPlayerShots,0, FALSE, IDC_MAXPLAYERSHOTS},
			{ "firerepeatrate"		,"firerepeat",19,POSINT,lpMapDocData->MapStruct.fireRepeatRate,0, FALSE, IDC_FIREREPEATRATE},
			{ "friction"			,"friction",19,POSFLOAT,lpMapDocData->MapStruct.friction,0, FALSE, IDC_FRICTION},
			{ "edgebounce" 			,"edgebounce",0,YESNO,0,&lpMapDocData->MapStruct.edgeBounce, FALSE, IDC_EDGEBOUNCE},
			{ "edgewrap"			,"edgewrap",0,YESNO,0,&lpMapDocData->MapStruct.edgeWrap, FALSE, IDC_EDGEWRAP},
			{ "extraborder"			,"extraborder",0,YESNO,0,&lpMapDocData->MapStruct.extraBorder, FALSE, IDC_EXTRABORDER},
			{ "defaultshipshape"	,"defaultshipshape",255,STRING,lpMapDocData->MapStruct.defaultShipShape, 0, FALSE, IDC_DEFAULTSHIPSHAPE},
			{ "tankshipshape"		,"tankshipshape",255,STRING,lpMapDocData->MapStruct.tankShipShape,0, FALSE, IDC_TANKSHIPSHAPE},
			{ "selfimmunity"		,"selfimmunity",0,YESNO,0,&lpMapDocData->MapStruct.selfImmunity, FALSE, IDC_SELFIMMUNITY},
			{ "turnthrust"			,"turnfuel",0,YESNO,0,&lpMapDocData->MapStruct.turnThrust, FALSE, IDC_TURNTHRUST},
			{ "coriolis"			,"coriolis",19,INT,lpMapDocData->MapStruct.coriolis,0, FALSE, IDC_CORIOLIS},
			{ "blockfriction"		,"blockfriction",19,POSFLOAT,lpMapDocData->MapStruct.blockFriction,0, FALSE, IDC_BLOCKFRICTION},

			
			{ "robotstalk"			,"robotstalk",0,YESNO,0,&lpMapDocData->MapStruct.robotsTalk, FALSE, IDC_ROBOTSTALK},
			{ "robotsleave"			,"robotsleave",0,YESNO,0,&lpMapDocData->MapStruct.robotsLeave, FALSE, IDC_ROBOTSLEAVE},
			{ "robotleavelife"		,"robotleavelife",19,POSINT,lpMapDocData->MapStruct.robotLeaveLife,0, FALSE, IDC_ROBOTLEAVELIFE},
			{ "robotleavescore"		,"robotleavescore",19,INT,lpMapDocData->MapStruct.robotLeaveScore,0, FALSE, IDC_ROBOTLEAVESCORE},
			{ "robotleaveratio"		,"robotleaveratio",19,POSFLOAT,lpMapDocData->MapStruct.robotLeaveRatio,0, FALSE, IDC_ROBOTLEAVERATIO},
			{ "robotteam"			,"robotteam",2,POSINT,lpMapDocData->MapStruct.robotTeam,0, FALSE, IDC_ROBOTTEAM},
			{ "restrictrobots"		,"restrictrobots",0,YESNO,0,&lpMapDocData->MapStruct.restrictRobots, FALSE, IDC_RESTRICTROBOTS},  
			{ "reserverobotteam"		,"reserverobotteam",0,YESNO,0,&lpMapDocData->MapStruct.reserveRobotTeam, FALSE, IDC_RESERVEROBOTTEAM},
			{ "minrobots"			,"minrobots",2,FLOAT,lpMapDocData->MapStruct.minRobots,0, FALSE, IDC_MINROBOTS},
			{ "maxrobots"			,"robots",2,POSINT,lpMapDocData->MapStruct.maxRobots,0, FALSE, IDC_MAXROBOTS},
			{ "playerstartsshielded"	,"playerstartshielded",0,YESNO,0,&lpMapDocData->MapStruct.playerStartsShielded, FALSE, IDC_PLAYERSTARTSSHIELDED},
			{ "shotswallbounce"		,"shotswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.shotsWallBounce, FALSE, IDC_SHOTSWALLBOUNCE},
			{ "ballswallbounce"		,"ballswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.ballsWallBounce, FALSE, IDC_BALLSWALLBOUNCE},
			{ "mineswallbounce"		,"mineswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.minesWallBounce, FALSE, IDC_MINESWALLBOUNCE},
			{ "itemswallbounce"		,"itemswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.itemsWallBounce, FALSE, IDC_ITEMSWALLBOUNCE},
			{ "missileswallbounce"		,"missileswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.missilesWallBounce, FALSE, IDC_MISSILESWALLBOUNCE},
			{ "sparkswallbounce"		,"sparkswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.sparksWallBounce, FALSE, IDC_SPARKSWALLBOUNCE},
			{ "debriswallbounce"		,"debriswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.debrisWallBounce, FALSE, IDC_DEBRISWALLBOUNCE},
			{ "maxobjectwallbouncespeed"	,"maxobjectbouncespeed",19,POSFLOAT,lpMapDocData->MapStruct.maxObjectWallBounceSpeed,0, FALSE, IDC_MAXOBJECTWALLBOUNCESPEED},
			{ "maxshieldedwallbouncespeed"	,"maxshieldedbouncespeed",19,POSFLOAT,lpMapDocData->MapStruct.maxShieldedWallBounceSpeed,0, FALSE, IDC_MAXSHIELDEDWALLBOUNCESPEED},
			{ "maxunshieldedwallbouncespeed","maxunshieldedbouncespeed",19,POSFLOAT,lpMapDocData->MapStruct.maxUnshieldedWallBounceSpeed,0, FALSE, IDC_MAXUNSHIELDEDWALLBOUNCESPEED},
			{ "maxshieldedplayerwallbounceangle","maxshieldedbounceangle",19,POSFLOAT,lpMapDocData->MapStruct.maxShieldedPlayerWallBounceAngle,0, FALSE, IDC_MAXSHIELDEDPLAYERWALLBOUNCEANGLE},
			{ "maxunshieldedplayerwallbounceangle","maxunshieldedbounceangle",19,POSFLOAT,lpMapDocData->MapStruct.maxUnshieldedPlayerWallBounceAngle,0, FALSE, IDC_MAXUNSHIELDEDPLAYERWALLBOUNCEANGLE},
			{ "playerwallbouncebrakefactor"	,"playerwallbrake",19,POSFLOAT,lpMapDocData->MapStruct.playerWallBounceBrakeFactor,0, FALSE, IDC_PLAYERWALLBOUNCEBRAKEFACTOR},
			{ "objectwallbouncebrakefactor"	,"objectwallbrake",19,POSFLOAT,lpMapDocData->MapStruct.objectWallBounceBrakeFactor,0, FALSE, IDC_OBJECTWALLBOUNCEBRAKEFACTOR},
			{ "objectwallbouncelifefactor"	,"objectwallbouncelifefactor",19,POSFLOAT,lpMapDocData->MapStruct.objectWallBounceLifeFactor,0, FALSE, IDC_OBJECTWALLBOUNCELIFEFACTOR},
			{ "wallbouncefueldrainmult"	,"wallbouncedrain",19,POSFLOAT,lpMapDocData->MapStruct.wallBounceFuelDrainMult,0, FALSE, IDC_WALLBOUNCEFUELDRAINMULT},
			{ "wallbouncedestroyitemprob" ,"wallbouncedestroyitemprob",19,POSFLOAT,lpMapDocData->MapStruct.wallBounceDestroyItemProb,0, FALSE, IDC_WALLBOUNCEDESTROYITEMPROB},
			{ "loseitemdestroys" ,"loseitemdestroys",0,YESNO,0,&lpMapDocData->MapStruct.loseItemDestroys, FALSE, IDC_LOSEITEMDESTROYS},
			{ "robotrealname"		,"robotrealname",255,STRING,lpMapDocData->MapStruct.robotRealName,0, FALSE, IDC_ROBOTREALNAME},
			{ "robothostname"		,"robothostname",255,STRING,lpMapDocData->MapStruct.robotHostName,0, FALSE, IDC_ROBOTHOSTNAME},
			{ "tankrealname"		,"tankrealname",255,STRING,lpMapDocData->MapStruct.tankRealName,0, FALSE, IDC_TANKREALNAME},
			{ "tankhostname"		,"tankhostname",255,STRING,lpMapDocData->MapStruct.tankHostName,0, FALSE, IDC_TANKHOSTNAME},
			{ "asteroidswallbounce"		,"asteroidswallbounce",0,YESNO,0,&lpMapDocData->MapStruct.asteroidsWallBounce, FALSE, IDC_ASTEROIDSWALLBOUNCE},

			
			{ "limitedvisibility"		,"limitedvisibility",0,YESNO,0,&lpMapDocData->MapStruct.limitedVisibility, FALSE, IDC_LIMITEDVISIBILITY},
			{ "minvisibilitydistance"	,"minvisibility",19,POSFLOAT,lpMapDocData->MapStruct.minVisibilityDistance,0, FALSE, IDC_MINVISIBILITYDISTANCE},
			{ "maxvisibilitydistance"	,"maxvisibility",19,POSFLOAT,lpMapDocData->MapStruct.maxVisibilityDistance,0, FALSE, IDC_MAXVISIBILITYDISTANCE},
			{ "teamplay"			,"teams",0,YESNO,0,&lpMapDocData->MapStruct.teamPlay, FALSE, IDC_TEAMPLAY},
			{ "teamassign"			,"teamplay",0,YESNO,0,&lpMapDocData->MapStruct.teamAssign, FALSE, IDC_TEAMASSIGN},
			{ "teamimmunity"		,"teamimmunity",0,YESNO,0,&lpMapDocData->MapStruct.teamImmunity, FALSE, IDC_TEAMIMMUNITY},
			{ "teamcannons"			,"teamcannons",0,YESNO,0,&lpMapDocData->MapStruct.teamCannons, FALSE, IDC_TEAMCANNONS},
			{ "teamfuel"			,"teamfuel",0,YESNO,0,&lpMapDocData->MapStruct.teamFuel, FALSE, IDC_TEAMFUEL},
			{ "targetkillteam"		,"targetkillteam",0,YESNO,0,&lpMapDocData->MapStruct.targetKillTeam, FALSE, IDC_TARGETKILLTEAM},
			{ "targetteamcollision"		,"targetcollision",0,YESNO,0,&lpMapDocData->MapStruct.targetTeamCollision, FALSE, IDC_TARGETTEAMCOLLISION},
			{ "targetsync"			,"targetsync",0,YESNO,0,&lpMapDocData->MapStruct.targetSync, FALSE, IDC_TARGETSYNC},
			{ "treasurekillteam"		,"treasurekillteam",0,YESNO,0,&lpMapDocData->MapStruct.treasureKillTeam, FALSE, IDC_TREASUREKILLTEAM},
			{ "treasurecollisiondestroys"	,"treasurecollisiondestroys",0,YESNO,0,&lpMapDocData->MapStruct.treasureCollisionDestroys, FALSE, IDC_TREASURECOLLISIONDESTROYS},
			{ "treasurecollisionmaykill"	,"treasureunshieldedcollisionkills",0,YESNO,0,&lpMapDocData->MapStruct.treasureCollisionMayKill, FALSE, IDC_TREASURECOLLISIONMAYKILL},
			{ "wreckagecollisionmaykill"	,"wreckageunshieldedcollisionkills",0,YESNO,0,&lpMapDocData->MapStruct.wreckageCollisionMayKill, FALSE, IDC_WRECKAGECOLLISIONMAYKILL},  
			{ "distinguishmissiles"		,"distinguishmissiles",0,YESNO,0,&lpMapDocData->MapStruct.distinguishMissiles, FALSE, IDC_DISTINGUISHMISSILES},
			{ "keepshots"			,"keepshots",0,YESNO,0,&lpMapDocData->MapStruct.keepShots, FALSE, IDC_KEEPSHOTS},
			{ "identifymines"		,"identifymines",0,YESNO,0,&lpMapDocData->MapStruct.identifyMines, FALSE, IDC_IDENTIFYMINES},
			{ "playersonradar"		,"playersradar",0,YESNO,0,&lpMapDocData->MapStruct.playersOnRadar, FALSE, IDC_PLAYERSONRADAR},
			{ "missilesonradar"		,"missilesradar",0,YESNO,0,&lpMapDocData->MapStruct.missilesOnRadar, FALSE, IDC_MISSILESONRADAR},
			{ "minesonradar"		,"minesradar",0,YESNO,0,&lpMapDocData->MapStruct.minesOnRadar, FALSE, IDC_MINESONRADAR},
			{ "nukesonradar"		,"nukesradar",0,YESNO,0,&lpMapDocData->MapStruct.nukesOnRadar, FALSE, IDC_NUKESONRADAR},
			{ "treasuresonradar"		,"treasuresradar",0,YESNO,0,&lpMapDocData->MapStruct.treasuresOnRadar, FALSE, IDC_TREASURESONRADAR},
			{ "shieldeditempickup"		,"shielditem",0,YESNO,0,&lpMapDocData->MapStruct.shieldedItemPickup, FALSE, IDC_SHIELDEDITEMPICKUP},
			{ "shieldedmining"		,"shieldmine",0,YESNO,0,&lpMapDocData->MapStruct.shieldedMining, FALSE, IDC_SHIELDEDMINING},
			{ "cloakedexhaust"		,"cloakedexhaust",0,YESNO,0,&lpMapDocData->MapStruct.cloakedExhaust, FALSE, IDC_CLOAKEDEXHAUST},  
			{ "cloakedshield"		,"cloakedshield",0,YESNO,0,&lpMapDocData->MapStruct.cloakedShield, FALSE, IDC_CLOAKEDSHIELD},
			{ "capturetheflag"		,"ctf",0,YESNO,0,&lpMapDocData->MapStruct.captureTheFlag, FALSE, IDC_CAPTURETHEFLAG},
			{ "targetdeadtime"		,"targetdeadtime",19,POSINT,lpMapDocData->MapStruct.targetDeadTime,0, FALSE, IDC_TARGETDEADTIME},
			{ "ballconnectorlength"	,"ballconnectorlengt",19,POSFLOAT,lpMapDocData->MapStruct.ballConnectorLength,0, FALSE, IDC_BALLCONNECTORLENGTH},
			{ "maxballconnectorratio"	,"maxballconnectorratio",19,POSFLOAT,lpMapDocData->MapStruct.maxBallConnectorRatio,0, FALSE, IDC_MAXBALLCONNECTORRATIO},
			{ "ballconnectordamping"	,"ballconnectordamping",19,POSFLOAT,lpMapDocData->MapStruct.ballConnectorDamping,0, FALSE, IDC_BALLCONNECTORDAMPING},
			{ "ballconnectorspringconstant"	,"ballconnectorspringconstant",19,POSFLOAT,lpMapDocData->MapStruct.ballConnectorSpringConstant,0, FALSE, IDC_BALLCONNECTORSPRINGCONSTANT},
			{ "ballmass"	,"ballmass",19,POSFLOAT,lpMapDocData->MapStruct.ballMass,0, FALSE, IDC_BALLMASS},
			
			{ "cannonsuseitems"		,"cannonspickupitems",0,YESNO,0,&lpMapDocData->MapStruct.cannonsUseItems, FALSE, IDC_CANNONSUSEITEMS},   
			{ "cannonsmartness"		,"cannonsmartness",1,LISTINT,lpMapDocData->MapStruct.cannonSmartness,0, FALSE, IDC_CANNONSMARTNESSLIST},
			{ "ecmsreprogramrobots"		,"ecmsreprogramrobots",0,YESNO,0,&lpMapDocData->MapStruct.ecmsReprogramRobots, FALSE, IDC_ECMSREPROGRAMROBOTS},
			{ "ecmsreprogrammines"		,"ecmsreprogrammines",0,YESNO,0,&lpMapDocData->MapStruct.ecmsReprogramMines, FALSE, IDC_ECMSREPROGRAMMINES},
			{ "gravityvisible"		,"gravityvisible",0,YESNO,0,&lpMapDocData->MapStruct.gravityVisible, FALSE, IDC_GRAVITYVISIBLE},
			{ "wormholevisible"		,"wormholevisible",0,YESNO,0,&lpMapDocData->MapStruct.wormholeVisible, FALSE, IDC_WORMHOLEVISIBLE},   
			{ "wormtime"			,"wormtime",19,POSINT,lpMapDocData->MapStruct.wormTime,0, FALSE, IDC_WORMTIME},
			{ "itemconcentratorvisible"	,"itemconcentratorvisible",0,YESNO,0,&lpMapDocData->MapStruct.itemConcentratorVisible, FALSE, IDC_ITEMCONCENTRATORVISIBLE},
			{ "allowsmartmissiles"		,"allowsmarts",0,YESNO,0,&lpMapDocData->MapStruct.allowSmartMissiles, FALSE, IDC_ALLOWSMARTMISSILES},   
			{ "allowheatseekers"		,"allowheats",0,YESNO,0,&lpMapDocData->MapStruct.allowHeatSeekers, FALSE, IDC_ALLOWHEATSEEKERS},
			{ "allowtorpedoes"		,"allowtorps",0,YESNO,0,&lpMapDocData->MapStruct.allowTorpedoes, FALSE, IDC_ALLOWTORPEDOES},
			{ "allowplayercrashes"		,"allowplayercrashes",0,YESNO,0,&lpMapDocData->MapStruct.allowPlayerCrashes, FALSE, IDC_ALLOWPLAYERCRASHES},
			{ "allowplayerbounces"		,"allowplayerbounces",0,YESNO,0,&lpMapDocData->MapStruct.allowPlayerBounces, FALSE, IDC_ALLOWPLAYERBOUNCES},
			{ "allowplayerkilling"		,"killings",0,YESNO,0,&lpMapDocData->MapStruct.allowPlayerKilling, FALSE, IDC_ALLOWPLAYERKILLING},
			{ "allowshields"		,"shields",0,YESNO,0,&lpMapDocData->MapStruct.allowShields, FALSE, IDC_ALLOWSHIELDS},
			{ "allownukes"			,"nukes",0,YESNO,0,&lpMapDocData->MapStruct.allowNukes, FALSE, IDC_ALLOWNUKES},
			{ "allowclusters"		,"clusters",0,YESNO,0,&lpMapDocData->MapStruct.allowClusters, FALSE, IDC_ALLOWCLUSTERS},
			{ "allowmodifiers"		,"modifiers",0,YESNO,0,&lpMapDocData->MapStruct.allowModifiers, FALSE, IDC_ALLOWMODIFIERS},
			{ "allowlasermodifiers"		,"lasermodifiers",0,YESNO,0,&lpMapDocData->MapStruct.allowLaserModifiers, FALSE, IDC_ALLOWLASERMODIFIERS},
			{ "allowshipshapes"		,"shipshapes",0,YESNO,0,&lpMapDocData->MapStruct.allowShipShapes, FALSE, IDC_ALLOWSHIPSHAPES},
			{ "maxmissilesperpack"		,"maxmissilesperpack",6,INT,lpMapDocData->MapStruct.maxMissilesPerPack,0, FALSE, IDC_MAXMISSILESPERPACK},
			{ "missilelife"			,"missilelife",19,POSINT,lpMapDocData->MapStruct.missileLife,0, FALSE, IDC_MISSILELIFE},
			{ "rogueheatprob"		,"rogueheatprob",19,POSFLOAT,lpMapDocData->MapStruct.rogueHeatProb,0, FALSE, IDC_ROGUEHEATPROB},
			{ "maxminesperpack"		,"maxminesperpack",6,INT,lpMapDocData->MapStruct.maxMinesPerPack,0, FALSE, IDC_MAXMINESPERPACK},
			{ "minelife"			,"minelife",19,POSINT,lpMapDocData->MapStruct.mineLife,0, FALSE, IDC_MINELIFE},
			{ "minefusetime"		,"minefusetime",19,POSFLOAT,lpMapDocData->MapStruct.mineFuseTime,0, FALSE, IDC_MINEFUSETIME},
			{ "baseminerange"		,"baseminerange",19,POSINT,lpMapDocData->MapStruct.baseMineRange,0, FALSE, IDC_BASEMINERANGE},
			{ "roguemineprob"		,"roguemineprob",19,POSFLOAT,lpMapDocData->MapStruct.rogueMineProb,0, FALSE, IDC_ROGUEMINEPROB},
			{ "minminespeed"	,"minminespeed",19,POSFLOAT,lpMapDocData->MapStruct.minMineSpeed,0, FALSE, IDC_MINMINESPEED},
			{ "blockfrictionvisible"		,"blockfrictionvisible",0,YESNO,0,&lpMapDocData->MapStruct.blockFrictionVisible, FALSE, IDC_BLOCKFRICTIONVISIBLE},   
			{ "cannondeadtime"		,"cannondeadtime",19,POSINT,lpMapDocData->MapStruct.cannonDeadTime,0, FALSE, IDC_CANNONDEADTIME},
			{ "asteroidcollisionmaykill"	,"asteroidunshieldedcollisionkills",0,YESNO,0,&lpMapDocData->MapStruct.asteroidCollisionMayKill, FALSE, IDC_ASTEROIDCOLLISIONMAYKILL},  
			{ "asteroidsonradar"		,"asteroidsradar",0,YESNO,0,&lpMapDocData->MapStruct.asteroidsOnRadar, FALSE, IDC_ASTEROIDSONRADAR},
			{ "maxasteroiddensity"		,"maxasteroiddensity",19,POSFLOAT,lpMapDocData->MapStruct.maxAsteroidDensity,0, FALSE, IDC_MAXASTEROIDDENSITY},
			
			{ "timing"			,"race",0,YESNO,0,&lpMapDocData->MapStruct.timing, FALSE, IDC_TIMING},
			{ "maxroundtime"		,"maxroundtime",6,POSINT,lpMapDocData->MapStruct.maxRoundTime,0, FALSE, IDC_MAXROUNDTIME},
			{ "roundstoplay"		,"roundstoplay",6,POSINT,lpMapDocData->MapStruct.roundsToPlay,0, FALSE, IDC_ROUNDSTOPLAY},
			{ "gameduration"		,"time",6,POSFLOAT,lpMapDocData->MapStruct.gameDuration,0, FALSE, IDC_GAMEDURATION},
			{ "rounddelay"			,"rounddelay",6,POSINT,lpMapDocData->MapStruct.roundDelay,0, FALSE, IDC_ROUNDDELAY},
			{ "laserisstungun"		,"stungun",0,YESNO,0,&lpMapDocData->MapStruct.laserIsStunGun, FALSE, IDC_LASERISSTUNGUN},
			{ "nukeminsmarts"		,"nukeminsmarts",6,POSINT,lpMapDocData->MapStruct.nukeMinSmarts,0, FALSE, IDC_NUKEMINSMARTS},
			{ "nukeminmines"		,"nukeminmines",6,POSINT,lpMapDocData->MapStruct.nukeMinMines,0, FALSE, IDC_NUKEMINMINES},
			{ "nukeclusterdamage"		,"nukeclusterdamage",19,POSFLOAT,lpMapDocData->MapStruct.nukeClusterDamage,0 , FALSE, IDC_NUKECLUSTERDAMAGE},
			{ "itemconcentratorradius"	,"itemconcentratorrange",19,POSFLOAT,lpMapDocData->MapStruct.itemConcentratorRadius,0 , FALSE, IDC_ITEMCONCENTRATORRADIUS},
			{ "maxitemdensity"		,"maxitemdensity",19,POSFLOAT,lpMapDocData->MapStruct.maxItemDensity,0, FALSE, IDC_MAXITEMDENSITY},
			{ "checkpointradius"		,"checkpointradius",19,POSFLOAT,lpMapDocData->MapStruct.checkpointRadius,0, FALSE, IDC_CHECKPOINTRADIUS},
			{ "racelaps"			,"racelaps",6,POSINT,lpMapDocData->MapStruct.raceLaps,0, FALSE, IDC_RACELAPS},
			{ "resetonhuman" 		,"humanreset",0,POSINT,lpMapDocData->MapStruct.resetOnHuman,0, FALSE, IDC_RESETONHUMAN},
			{ "lockotherteam"		,"lockotherteam",0,YESNO,0,&lpMapDocData->MapStruct.lockOtherTeam, FALSE, IDC_LOCKOTHERTEAM},
			{ "allowviewing"		,"allowviewing",0,YESNO,0,&lpMapDocData->MapStruct.allowViewing, FALSE, IDC_ALLOWVIEWING},
			{ "framespersecond"		,"fps",6,POSINT,lpMapDocData->MapStruct.framesPerSecond,0, FALSE, IDC_FRAMESPERSECOND},
			{ "usewreckage"			,"usewreckage",0,YESNO,0,&lpMapDocData->MapStruct.useWreckage, FALSE, IDC_USEWRECKAGE},
			{ "reset"			,"reset",0,YESNO,0,&lpMapDocData->MapStruct.reset, FALSE, IDC_RESET},
			
			{ "initialfuel"			,"initialfuel",19,POSFLOAT,lpMapDocData->MapStruct.initialFuel,0, FALSE, IDC_INITIALFUEL},
			{ "initialtanks"		,"initialtanks",19,POSFLOAT,lpMapDocData->MapStruct.initialTanks,0, FALSE, IDC_INITIALTANKS},
			{ "initialecms"			,"initialecms",19,POSFLOAT, lpMapDocData->MapStruct.initialECMs,0, FALSE, IDC_INITIALECMS},
			{ "initialmines"		,"initialmines",19,POSFLOAT,lpMapDocData->MapStruct.initialMines,0, FALSE, IDC_INITIALMINES},
			{ "initialmissiles"		,"initialmissiles",19,POSFLOAT,lpMapDocData->MapStruct.initialMissiles,0, FALSE, IDC_INITIALMISSILES},
			{ "initialcloaks"		,"initialcloaks",19,POSFLOAT,lpMapDocData->MapStruct.initialCloaks,0, FALSE, IDC_INITIALCLOAKS},
			{ "initialsensors"		,"initialsensors",19,POSFLOAT,lpMapDocData->MapStruct.initialSensors,0, FALSE, IDC_INITIALSENSORS},
			{ "initialwideangles"		,"initialwideangles",19,POSFLOAT,lpMapDocData->MapStruct.initialWideangles,0, FALSE, IDC_INITIALWIDEANGLES},
			{ "initialrearshots"		,"initialrearshots",19,POSFLOAT,lpMapDocData->MapStruct.initialRearshots,0, FALSE, IDC_INITIALREARSHOTS},
			{ "initialafterburners"		,"initialafterburners",19,POSFLOAT,lpMapDocData->MapStruct.initialAfterburners,0, FALSE, IDC_INITIALAFTERBURNERS},
			{ "initialtransporters"		,"initialtransporters",19,POSFLOAT,lpMapDocData->MapStruct.initialTransporters,0, FALSE, IDC_INITIALTRANSPORTERS},
			{ "initialdeflectors"		,"initialdeflectors",19,POSFLOAT,lpMapDocData->MapStruct.initialDeflectors,0, FALSE, IDC_INITIALDEFLECTORS},
			{ "initialphasings"		,"initialphasings",19,POSFLOAT,lpMapDocData->MapStruct.initialPhasings,0, FALSE, IDC_INITIALPHASINGS},
			{ "initialhyperjumps"		,"initialhyperjumps",19,POSFLOAT,lpMapDocData->MapStruct.initialHyperJumps,0, FALSE, IDC_INITIALHYPERJUMPS},
			{ "initialemergencythrusts"	,"initialemergencythrusts",19,POSFLOAT,lpMapDocData->MapStruct.initialEmergencyThrusts,0, FALSE, IDC_INITIALEMERGENCYTHRUSTS},
			{ "initiallasers"		,"initiallasers",19,POSFLOAT,lpMapDocData->MapStruct.initialLasers,0, FALSE, IDC_INITIALLASERS},
			{ "initialtractorbeams"		,"initialtractorbeams",19,POSFLOAT,lpMapDocData->MapStruct.initialTractorBeams,0, FALSE, IDC_INITIALTRACTORBEAMS},
			{ "initialautopilots"		,"initialautopilots",19,POSFLOAT,lpMapDocData->MapStruct.initialAutopilots,0, FALSE, IDC_INITIALAUTOPILOTS},
			{ "initialemergencyshields"	,"initialemergencyshields",19,POSFLOAT,lpMapDocData->MapStruct.initialEmergencyShields,0, FALSE, IDC_INITIALEMERGENCYSHIELDS},
			{ "initialmirrors"		,"initialmirrors",19,POSFLOAT,lpMapDocData->MapStruct.initialMirrors,0, FALSE, IDC_INITIALMIRRORS},
			{ "initialarmor"		,"initialarmors",19,POSFLOAT,lpMapDocData->MapStruct.initialArmor,0, FALSE, IDC_INITIALARMOR},
			
			{ "maxoffensiveitems"		,"maxoffensiveitems",19,POSINT,lpMapDocData->MapStruct.maxOffensiveItems,0, FALSE, IDC_MAXOFFENSIVEITEMS},
			{ "maxdefensiveitems"		,"maxdefensiveitems",19,POSINT,lpMapDocData->MapStruct.maxDefensiveItems,0, FALSE, IDC_MAXDEFENSIVEITEMS},
			{ "maxfuel"			,"maxfuel",19,POSINT,lpMapDocData->MapStruct.maxFuel,0, FALSE, IDC_MAXFUEL},
			{ "maxtanks"			,"maxtanks",19,POSINT,lpMapDocData->MapStruct.maxTanks,0, FALSE, IDC_MAXTANKS},
			{ "maxecms"			,"maxecms",19,POSINT,lpMapDocData->MapStruct.maxECMs,0, FALSE, IDC_MAXECMS},
			{ "maxmines"			,"maxmines",19,POSINT,lpMapDocData->MapStruct.maxMines,0, FALSE, IDC_MAXMINES},
			{ "maxmissiles"			,"maxmissiles",19,POSINT,lpMapDocData->MapStruct.maxMissiles,0, FALSE, IDC_MAXMISSILES},
			{ "maxcloaks"			,"maxcloaks",19,POSINT,lpMapDocData->MapStruct.maxCloaks,0, FALSE, IDC_MAXCLOAKS},
			{ "maxsensors"			,"maxsensors",19,POSINT,lpMapDocData->MapStruct.maxSensors,0, FALSE, IDC_MAXSENSORS},
			{ "maxwideangles"		,"maxwideangles",19,POSINT,lpMapDocData->MapStruct.maxWideangles,0, FALSE, IDC_MAXWIDEANGLES},
			{ "maxrearshots"		,"maxrearshots",19,POSINT,lpMapDocData->MapStruct.maxRearshots,0, FALSE, IDC_MAXREARSHOTS},
			{ "maxafterburners"		,"maxafterburners",19,POSINT,lpMapDocData->MapStruct.maxAfterburners,0, FALSE, IDC_MAXAFTERBURNERS},
			{ "maxtransporters"		,"maxtransporters",19,POSINT,lpMapDocData->MapStruct.maxTransporters,0, FALSE, IDC_MAXTRANSPORTERS},
			{ "maxdeflectors"		,"maxdeflectors",19,POSINT,lpMapDocData->MapStruct.maxDeflectors,0, FALSE, IDC_MAXDEFLECTORS},
			{ "maxphasings"			,"maxphasings",19,POSINT,lpMapDocData->MapStruct.maxPhasings,0, FALSE, IDC_MAXPHASINGS},
			{ "maxhyperjumps"		,"maxhyperjumps",19,POSINT,lpMapDocData->MapStruct.maxHyperJumps,0, FALSE, IDC_MAXHYPERJUMPS},
			{ "maxemergencythrusts"		,"maxemergencythrusts",19,POSINT,lpMapDocData->MapStruct.maxEmergencyThrusts,0, FALSE, IDC_MAXEMERGENCYTHRUSTS},
			{ "maxlasers"			,"maxlasers",19,POSINT,lpMapDocData->MapStruct.maxLasers,0, FALSE, IDC_MAXLASERS},
			{ "maxtractorbeams"		,"maxtractorbeams",19,POSINT,lpMapDocData->MapStruct.maxTractorBeams,0, FALSE, IDC_MAXTRACTORBEAMS},
			{ "maxautopilots"		,"maxautopilots",19,POSINT,lpMapDocData->MapStruct.maxAutopilots,0, FALSE, IDC_MAXAUTOPILOTS},
			{ "maxemergencyshields"		,"maxemergencyshields",19,POSINT,lpMapDocData->MapStruct.maxEmergencyShields,0, FALSE, IDC_MAXEMERGENCYSHIELDS},
			{ "maxmirrors"			,"maxmirrors",19,POSINT,lpMapDocData->MapStruct.maxMirrors,0, FALSE, IDC_MAXMIRRORS},
			{ "maxarmor"			,"maxarmors",19,POSINT,lpMapDocData->MapStruct.maxArmor,0, FALSE, IDC_MAXARMOR},
			
			{ "itemenergypackprob"		,"itemenergypackprob",19,POSFLOAT,lpMapDocData->MapStruct.itemEnergyPackProb,0, FALSE, IDC_ITEMENERGYPACKPROB},
			{ "itemtankprob"		,"itemtankprob",19,POSFLOAT,lpMapDocData->MapStruct.itemTankProb,0, FALSE, IDC_ITEMTANKPROB},
			{ "itemecmprob"			,"itemecmprob",19,POSFLOAT,lpMapDocData->MapStruct.itemECMProb,0, FALSE, IDC_ITEMECMPROB},
			{ "itemmineprob"		,"itemmineprob",19,POSFLOAT,lpMapDocData->MapStruct.itemMineProb,0, FALSE, IDC_ITEMMINEPROB},
			{ "itemmissileprob"		,"itemmissileprob",19,POSFLOAT,lpMapDocData->MapStruct.itemMissileProb,0, FALSE, IDC_ITEMMISSILEPROB},
			{ "itemcloakprob"		,"itemcloakprob",19,POSFLOAT,lpMapDocData->MapStruct.itemCloakProb,0, FALSE, IDC_ITEMCLOAKPROB},
			{ "itemsensorprob"		,"itemsensorprob",19,POSFLOAT,lpMapDocData->MapStruct.itemSensorProb,0, FALSE, IDC_ITEMSENSORPROB},
			{ "itemwideangleprob"		,"itemwideangleprob",19,POSFLOAT,lpMapDocData->MapStruct.itemWideangleProb,0, FALSE, IDC_ITEMWIDEANGLEPROB},
			{ "itemrearshotprob"		,"itemrearshotprob",19,POSFLOAT,lpMapDocData->MapStruct.itemRearshotProb,0, FALSE, IDC_ITEMREARSHOTPROB},
			{ "itemafterburnerprob"		,"itemafterburnerprob",19,POSFLOAT,lpMapDocData->MapStruct.itemAfterburnerProb,0, FALSE, IDC_ITEMAFTERBURNERPROB},
			{ "itemtransporterprob"		,"itemtransporterprob",19,POSFLOAT,lpMapDocData->MapStruct.itemTransporterProb,0, FALSE, IDC_ITEMTRANSPORTERPROB},
			{ "itemlaserprob"		,"itemlaserprob",19,POSFLOAT,lpMapDocData->MapStruct.itemLaserProb,0, FALSE, IDC_ITEMLASERPROB},
			{ "itememergencythrustprob"	,"itememergencythrustprob",19,POSFLOAT,lpMapDocData->MapStruct.itemEmergencyThrustProb,0, FALSE, IDC_ITEMEMERGENCYTHRUSTPROB},
			{ "itemtractorbeamprob"		,"itemtractorbeamprob",19,POSFLOAT,lpMapDocData->MapStruct.itemTractorBeamProb,0, FALSE, IDC_ITEMTRACTORBEAMPROB},
			{ "itemautopilotprob"		,"itemautopilotprob",19,POSFLOAT,lpMapDocData->MapStruct.itemAutopilotProb,0, FALSE, IDC_ITEMAUTOPILOTPROB},
			{ "itememergencyshieldprob"	,"itememergencyshieldprob",19,POSFLOAT,lpMapDocData->MapStruct.itemEmergencyShieldProb,0, FALSE, IDC_ITEMEMERGENCYSHIELDPROB},
			{ "itemdeflectorprob"		,"itemdeflectorprob",19,POSFLOAT,lpMapDocData->MapStruct.itemDeflectorProb,0, FALSE, IDC_ITEMDEFLECTORPROB},
			{ "itemhyperjumpprob"		,"itemhyperjumpprob",19,POSFLOAT,lpMapDocData->MapStruct.itemHyperJumpProb,0, FALSE, IDC_ITEMHYPERJUMPPROB},
			{ "itemphasingprob"		,"itemphasingprob",19,POSFLOAT,lpMapDocData->MapStruct.itemPhasingProb,0, FALSE, IDC_ITEMPHASINGPROB},
			{ "itemmirrorprob"		,"itemmirrorprob",19,POSFLOAT,lpMapDocData->MapStruct.itemMirrorProb,0, FALSE, IDC_ITEMMIRRORPROB},
			{ "itemarmorprob"		,"itemarmorprob",19,POSFLOAT,lpMapDocData->MapStruct.itemArmorProb,0, FALSE, IDC_ITEMARMORPROB},
			{ "movingitemprob"		,"movingitemprob",19,POSFLOAT,lpMapDocData->MapStruct.movingItemProb,0, FALSE, IDC_MOVINGITEMPROB},
			{ "dropitemonkillprob"		,"dropitemonkillprob",19,POSFLOAT,lpMapDocData->MapStruct.dropItemOnKillProb,0, FALSE, IDC_DROPITEMONKILLPROB},
			{ "detonateitemonkillprob"	,"detonateitemonkillprob",19,POSFLOAT,lpMapDocData->MapStruct.detonateItemOnKillProb,0, FALSE, IDC_DETONATEITEMONKILLPROB},
			{ "destroyitemincollisionprob"	,"destroyitemincollisionprob",19,POSFLOAT,lpMapDocData->MapStruct.destroyItemInCollisionProb,0, FALSE, IDC_DESTROYITEMINCOLLISIONPROB},
			{ "itemconcentratorprob"	,"itemconcentratorprob",19,POSFLOAT,lpMapDocData->MapStruct.itemConcentratorProb,0, FALSE, IDC_ITEMCONCENTRATORPROB},
			{ "itemprobmult"		,"itemprobfact",19,POSFLOAT,lpMapDocData->MapStruct.itemProbMult,0, FALSE, IDC_ITEMPROBMULT},
			{ "cannonitemprobmult"		,"cannonitemprobmult",19,POSFLOAT,lpMapDocData->MapStruct.cannonItemProbMult,0, FALSE, IDC_CANNONITEMPROBMULT},
			{ "randomitemprob"	,"randomitemprob",19,POSFLOAT,lpMapDocData->MapStruct.randomItemProb,0, FALSE, IDC_RANDOMITEMPROB},
			{ "asteroidprob"	,"asteroidprob",19,POSFLOAT,lpMapDocData->MapStruct.asteroidProb,0, FALSE, IDC_ASTEROIDPROB},
			
			{ "shotkillscoremult"		,"shotkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.shotKillScoreMult,0, FALSE, IDC_SHOTKILLSCOREMULT},
			{ "torpedokillscoremult"	,"torpedokillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.torpedoKillScoreMult,0, FALSE, IDC_TORPEDOKILLSCOREMULT},
			{ "smartkillscoremult"		,"smartkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.smartKillScoreMult,0, FALSE, IDC_SMARTKILLSCOREMULT},
			{ "heatkillscoremult"		,"heatkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.heatKillScoreMult,0, FALSE, IDC_HEATKILLSCOREMULT},
			{ "clusterkillscoremult"	,"clusterkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.clusterKillScoreMult,0, FALSE, IDC_CLUSTERKILLSCOREMULT},
			{ "laserkillscoremult"		,"laserkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.laserKillScoreMult,0, FALSE, IDC_LASERKILLSCOREMULT},
			{ "tankkillscoremult"		,"tankkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.tankKillScoreMult,0, FALSE, IDC_TANKKILLSCOREMULT},
			{ "runoverkillscoremult"	,"runoverkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.runoverKillScoreMult,0, FALSE, IDC_RUNOVERKILLSCOREMULT},
			{ "ballkillscoremult"		,"ballkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.ballKillScoreMult,0, FALSE, IDC_BALLKILLSCOREMULT},
			{ "explosionkillscoremult"	,"explosionkillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.explosionKillScoreMult,0, FALSE, IDC_EXPLOSIONKILLSCOREMULT},
			{ "shovekillscoremult"		,"shovekillscoremult",19,POSFLOAT,lpMapDocData->MapStruct.shoveKillScoreMult,0, FALSE, IDC_SHOVEKILLSCOREMULT},
			{ "crashscoremult"		,"crashscoremult",19,POSFLOAT,lpMapDocData->MapStruct.crashScoreMult,0, FALSE, IDC_CRASHKILLSCOREMULT},
			{ "minescoremult"		,"minescoremult",19,POSFLOAT,lpMapDocData->MapStruct.mineScoreMult,0, FALSE, IDC_MINESCOREMULT},
			{ "tankscoredecrement"		,"tankscoredecrement",19,POSFLOAT,lpMapDocData->MapStruct.tankScoreDecrement,0, FALSE, IDC_TANKSCOREDECREMENT},

			{ "cannonflak","cannonaaa",0,YESNO,0,&lpMapDocData->MapStruct.cannonFlak, FALSE, IDC_CANNONFLAK},
			{ "connectorisstring","connectorIsString",0,YESNO,0,&lpMapDocData->MapStruct.connectorIsString, FALSE, IDC_CONNECTORISSTRING},
			{ "ballcollisions","ballcollisions",0,YESNO,0,&lpMapDocData->MapStruct.ballCollisions, FALSE, IDC_BALLCOLLISIONS},
			{ "ballsparkcollisions","ballsparkcollisions",0,YESNO,0,&lpMapDocData->MapStruct.ballSparkCollisions, FALSE, IDC_BALLSPARKCOLLISIONS},
			{ "asteroiditemprob","asteroiditemprob",19,POSFLOAT,lpMapDocData->MapStruct.asteroidItemProb,0, FALSE, IDC_ASTEROIDITEMPROB},
			{ "asteroidmaxitems","asteroidmaxitems",19,POSINT,lpMapDocData->MapStruct.asteroidMaxItems,0, FALSE, IDC_ASTEROIDMAXITEMS},
			{ "teamsharescore","teamsharescore",0,YESNO,0,&lpMapDocData->MapStruct.teamShareScore, FALSE, IDC_TEAMSHARESCORE},
			{ "allowalliances","alliances",0,YESNO,0,&lpMapDocData->MapStruct.allowAlliances, FALSE, IDC_ALLOWALLIANCES},
			{ "announcealliances ","announcealliances",0,YESNO,0,&lpMapDocData->MapStruct.announceAlliances, FALSE, IDC_ANNOUNCEALLIANCES},
			{ "maxpausetime","maxpausetime",19,POSINT,lpMapDocData->MapStruct.maxPauseTime,0, FALSE, IDC_MAXPAUSETIME},
			{ "asteroidconcentratorvisible","asteroidconcentratorvisible",0,YESNO,0,&lpMapDocData->MapStruct.asteroidConcentratorVisible, FALSE, IDC_ASTEROIDCONCENTRATORVISIBLE},
			{ "asteroidconcentratorradius","asteroidconcentratorrange",19,POSINT,lpMapDocData->MapStruct.asteroidConcentratorRadius,0, FALSE, IDC_ASTEROIDCONCENTRATORRADIUS},
			{ "asteroidconcentratorprob","asteroidconcentratorprob",19,POSFLOAT,lpMapDocData->MapStruct.asteroidConcentratorProb,0, FALSE, IDC_ASTEROIDCONCENTRATORPROB},
			{ "ballrace","ballrace",0,YESNO,0,&lpMapDocData->MapStruct.ballrace, FALSE, IDC_BALLRACE},
			{ "ballraceconnected","ballraceconnected",0,YESNO,0,&lpMapDocData->MapStruct.ballraceConnected, FALSE, IDC_BALLRACECONNECTED},
			{ "mineshotdetonatedistance","mineshotdetonatedistance",19,POSINT,lpMapDocData->MapStruct.mineShotDetonateDistance,0, FALSE, IDC_MINESHOTDETONATEDISTANCE},
			{ "cannonpoints","cannonpoints",19,POSFLOAT,lpMapDocData->MapStruct.cannonPoints,0, FALSE, IDC_CANNONPOINTS},
			{ "cannonmaxscore","cannonmaxscore",19,POSFLOAT,lpMapDocData->MapStruct.cannonMaxScore,0, FALSE, IDC_CANNONMAXSCORE},
			{ "cannonsdefend","cannonsdefend",0,YESNO,0,&lpMapDocData->MapStruct.cannonsDefend, FALSE, IDC_CANNONSDEFEND},
			{ "asteroidpoints","asteroidpoints",19,POSFLOAT,lpMapDocData->MapStruct.asteroidPoints,0, FALSE, IDC_ASTEROIDPOINTS},
			{ "asteroidmaxscore","asteroidmaxscore",19,POSFLOAT,lpMapDocData->MapStruct.asteroidMaxScore,0, FALSE, IDC_ASTEROIDMAXSCORE},

			{ "mapdata"			,"mapdata",0,MAPDATA ,NULL,0, TRUE, 0},
			};			
			
			for (i = 0; i < NUMPREFS; i++)
				lpMapDocData->PrefsArray[i] = Prefs_Template[i];
			
			NewMap(lpMapDocData);
			
			VoidUndo(lpMapDocData);
			lpMapDocData->fSelected=FALSE;
			
			CountBases(lpMapDocData);
			SetWindowLong(hwnd, 0, (long) lpMapDocData);
			
			hwndLocClient = GetParent(hwnd);
			hwndLocFrame = GetParent (hwndLocClient);
			
			
			return 0;
	}
	}
	
	case WM_COMMAND:
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		switch (wParam)
		{
		case IDM_PROPERTIES :
			CreatePropertySheet(hwnd) ;
			if(hwndMiniMap && IsWindowVisible(hwndMiniMap))
				{
					SizeSmallMap(lpMapDocData);
					InvalidateRect(hwndMiniMap, NULL, TRUE);
				}
			UpdateStatusBarSettings(lpMapDocData);
			break;
			
		case IDM_ZOOMIN :
			if (lpMapDocData->MapStruct.view_zoom < 100)
			{
				ptBeg.x = ptBeg.y = ptEnd.x = ptEnd.y = 0;
				lpMapDocData->ptTempBeg.x = lpMapDocData->ptTempBeg.y = lpMapDocData->ptTempEnd.x = lpMapDocData->ptTempEnd.y = 0;
				lpMapDocData->MapStruct.view_zoom += 5;
				InvalidateRect (hwnd, NULL, TRUE);
			}
			break;
			
		case IDM_ZOOMOUT :
			if (lpMapDocData->MapStruct.view_zoom <= 6)
				lpMapDocData->MapStruct.view_zoom = 2;
			else
				lpMapDocData->MapStruct.view_zoom -= 5;

			ptBeg.x = ptBeg.y = ptEnd.x = ptEnd.y = 0;
			lpMapDocData->ptTempBeg.x = lpMapDocData->ptTempBeg.y = lpMapDocData->ptTempEnd.x = lpMapDocData->ptTempEnd.y = 0;
			InvalidateRect (hwnd, NULL, TRUE);
			break;
			
			
		case IDM_EDIT_UNDO :
			Undo(lpMapDocData);
			lpMapDocData->fSelected = FALSE;
			EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);                                                            					      
			if (lpMapDocData->undolist == NULL)
				EnableUndo(hwndFileToolBar, hMenuMap, FALSE);                                                            
			break;
			
		case IDM_ROTATEMAP :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE);

			/*We have to repaint the selected area here if one is selected, cause it will
			be filled with temporary empty blocks. The PAINT message is placed in the stack
			and processed after the rotation. This insures that our original area is displayed
			appropriately.
			*/
			if (lpMapDocData->fSelected)
			{
				rect.left = (lpMapDocData->seldxbeg - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
				rect.top = (lpMapDocData->seldybeg - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
				rect.right = (lpMapDocData->seldxend - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
				rect.bottom = (lpMapDocData->seldyend - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
				InvalidateRect(hwnd, &rect, TRUE);
			}
			RotateMap(lpMapDocData, lpMapDocData->fSelected);
			break;
			
		case IDM_MIRRORMAPV :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			//MirrorMapVertical(lpMapDocData, lpMapDocData->fSelected);
			MirrorMap(lpMapDocData, lpMapDocData->fSelected, FALSE);
			break;
			
		case IDM_MIRRORMAPH :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
//			MirrorMapHorizontal(lpMapDocData, lpMapDocData->fSelected);
			MirrorMap(lpMapDocData, lpMapDocData->fSelected, TRUE);
			break;
			
		case IDM_CYCLEMAPH :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
			CycleMapHorizontal(lpMapDocData, TRUE, lpMapDocData->fSelected);
			break;
			
		case IDM_CYCLEMAPV :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			CycleMapVertical(lpMapDocData, TRUE, lpMapDocData->fSelected);
			break;
			
		case IDM_CYCLEMAPHNEG :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			CycleMapHorizontal(lpMapDocData, FALSE, lpMapDocData->fSelected);
			break;
			
		case IDM_CYCLEMAPVNEG :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			CycleMapVertical(lpMapDocData, FALSE, lpMapDocData->fSelected);
			break;
			
		case IDM_NEGATIVEMAP :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			NegativeMapArea(lpMapDocData, lpMapDocData->fSelected);
			break;
			
		case IDM_CLEARMAP :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
			
			ClearMap(lpMapDocData, lpMapDocData->fSelected, FALSE);

			if (lpMapDocData->fSelected)
			{
				//lpMapDocData->fSelected = FALSE;
				EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);
			}
			break;
			
		case IDM_EDIT_CUT :
			if (lpMapDocData->fSelected)
			{
				ClearUndo(lpMapDocData);
				EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
				FillClipData(lpMapDocData, TRUE);
				fClipboarded = TRUE;
				EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);
			}
			break;
		case IDM_EDIT_COPY :
			if (lpMapDocData->fSelected)
			{
				FillClipData(lpMapDocData, FALSE);
				fClipboarded = TRUE;
				EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);
			}
			break;
			
		case IDM_EDIT_PASTE :
			fPasting = TRUE;
			ptBeg.x = ptBeg.y = ptEnd.x = ptEnd.y = 0;
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			break;
			
		case IDM_CROP :
			if (lpMapDocData->fSelected)
			{
				ClearUndo(lpMapDocData);
				EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
				ClearMap(lpMapDocData, lpMapDocData->fSelected, TRUE);
				lpMapDocData->fSelected = FALSE;
			}
			break;
			
		case IDM_ROUNDMAP :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			RoundMapArea(lpMapDocData, lpMapDocData->fSelected);
			break;
			
		case IDM_GROWMAP :
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE); 
			{
				int i;
				for (i = 0; i < 60; i++)
					GrowMapArea(lpMapDocData, lpMapDocData->fSelected);
			}
			break;


		case IDM_CHECKMAP :
			if (lpMapDocData)
			{
				CheckMap(lpMapDocData);
				DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPERRORS), hwnd, MapErrorsDlgProc);
			}
			return 0;


		case IDM_MAPSOURCE :
			if (lpMapDocData)
			{
				SaveMap(lpMapDocData, MXPTEMPFILE, TRUE, FALSE);
				spawnl(_P_NOWAIT, (char *) editor_command, (char *) editor_command, MXPTEMPFILE, NULL );
			}
			break;

		case IDM_LAUNCHMAP :
			if (lpMapDocData)
			{
				SaveMap(lpMapDocData, MXPTEMPFILE, TRUE, FALSE);
				spawnl(_P_NOWAIT, (char *) server_command, (char *) server_command, "-map", MXPTEMPFILE, NULL );
				spawnl(_P_NOWAIT, (char *) client_command, (char *) client_command, NULL );
			}
			break;

		case IDM_MINIMAP:
			SendMessage(hwndFrame, WM_COMMAND, (WPARAM) IDM_MINIMAP, (LPARAM) 0);
			break;

		}

		if (lpMapDocData->fSelected)
		{
			rect.left = (lpMapDocData->seldxbeg - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
			rect.top = (lpMapDocData->seldybeg - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
			rect.right = (lpMapDocData->seldxend - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
			rect.bottom = (lpMapDocData->seldyend - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
			InvalidateRect(hwnd, &rect, TRUE);
		}
		else
		  InvalidateRect (hwnd, NULL, TRUE);

		DoCaption (lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName) ;
		return 0;

	case WM_PAINT :
		{
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
			hdc = BeginPaint (hwnd, &ps) ;
			/*Recalculate the scrollbars, incase map has been zoomed*/
			GetClientRect (hwnd, &rect);
			cxClient = rect.right;
			cyClient = rect.bottom;
			
			lpMapDocData->iVscrollMax = max (0, lpMapDocData->MapStruct.height - cyClient / lpMapDocData->MapStruct.view_zoom + 1) ;
			lpMapDocData->iVscrollPos = min (lpMapDocData->iVscrollPos, lpMapDocData->iVscrollMax) ;
			
			SetScrollRange (hwnd, SB_VERT, 0, lpMapDocData->iVscrollMax, FALSE) ;
			SetScrollPos   (hwnd, SB_VERT, lpMapDocData->iVscrollPos, TRUE) ;
			
			lpMapDocData->iHscrollMax = max (0, lpMapDocData->MapStruct.width - cxClient / lpMapDocData->MapStruct.view_zoom + 1) ;
			lpMapDocData->iHscrollPos = min (lpMapDocData->iHscrollPos, lpMapDocData->iHscrollMax) ;
			
			SetScrollRange (hwnd, SB_HORZ, 0, lpMapDocData->iHscrollMax, FALSE) ;
			SetScrollPos   (hwnd, SB_HORZ, lpMapDocData->iHscrollPos, TRUE) ;
			
			ps.rcPaint.top = ps.rcPaint.top / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
			ps.rcPaint.bottom = ps.rcPaint.bottom / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
			ps.rcPaint.left = ps.rcPaint.left / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
			ps.rcPaint.right = ps.rcPaint.right / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
			
			
			/*Determine the area to paint*/
			iVPaintBeg = max (0, lpMapDocData->iVscrollPos + ps.rcPaint.top / lpMapDocData->MapStruct.view_zoom) ;
			iVPaintEnd = min (lpMapDocData->MapStruct.height, lpMapDocData->iVscrollPos + ps.rcPaint.bottom / lpMapDocData->MapStruct.view_zoom) ;
			iHPaintBeg = max (0, lpMapDocData->iHscrollPos + ps.rcPaint.left / lpMapDocData->MapStruct.view_zoom) ;
			iHPaintEnd = min (lpMapDocData->MapStruct.width, lpMapDocData->iHscrollPos + ps.rcPaint.right / lpMapDocData->MapStruct.view_zoom) ;
			
			/*Draw the selected area of the map*/
			DrawMapSection(hdc, lpMapDocData, iHPaintBeg, iVPaintBeg, iHPaintEnd, iVPaintEnd, ps.rcPaint.left, ps.rcPaint.top);
			
			/*If theres an area selected, redraw the selection box*/
			if (lpMapDocData->fSelected)
			{
				ptBeg.x = (lpMapDocData->seldxbeg - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
				ptBeg.y = (lpMapDocData->seldybeg - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
				ptEnd.x = (lpMapDocData->seldxend - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
				ptEnd.y = (lpMapDocData->seldyend - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
				DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
			}
			
			/*The Border Box*/
			ptOutbeg.x = (0 - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom -1;
			ptOutbeg.y = (0 - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom -1;
			ptOutend.x = (lpMapDocData->MapStruct.width - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom +1; 
			ptOutend.y = (lpMapDocData->MapStruct.height - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom +1;
			if (lpMapDocData->MapStruct.extraBorder)
				DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(0,0,255));
			else
			{
				if (lpMapDocData->MapStruct.edgeWrap)
					DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(0,255,0));
				else
					DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(255,0,0));
			}
			EndPaint (hwnd, &ps) ;
			return 0 ;
		}	
/*Here we process the mouse notifications. We grab the mouse location and figure
the relative map position, taking into account the scroll bars. Remember that the
map array starts from [0][0], not [1][1], so the bottom right block of a 50x50 map is
[49][49].*/
	case WM_LBUTTONDOWN:
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		
		SetCapture(hwnd);
		GetCursorPos(&ptCurse);
		ScreenToClient (hwnd, &ptCurse);
		dx = ptCurse.x / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iHscrollPos;
		dy = ptCurse.y / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iVscrollPos;
		
		//Way out of map limits, don't bother going any further
		if ((dx >= lpMapDocData->MapStruct.width + 1) || (dy >= lpMapDocData->MapStruct.height + 1))
		{
			sprintf(sbtext, "Outside of Map Limits!");
			SendMessage(hwndStatusBar, SB_SETTEXT, (WPARAM) NULL, (LPARAM) (LPSTR) sbtext);
			return 0;
		}
		
		if ((dx >= lpMapDocData->seldxbeg) && (dx < lpMapDocData->seldxend) &&
			(dy >= lpMapDocData->seldybeg) && (dy < lpMapDocData->seldyend) && lpMapDocData->fSelected && !fPasting)
		{
			fDragging = TRUE;
			lpMapDocData->strdx = dx;
			lpMapDocData->strdy = dy;
			dragArea(lpMapDocData, 0, 0, FALSE);
			return 0;
		}
		
		if ((dx <= lpMapDocData->MapStruct.width) && (dy <= lpMapDocData->MapStruct.height)
			&& (dx >= 0) && (dy >= 0) && !fPasting)
		{
			//We're drawing here, so insert a break into the undo struct.
			lpMapDocData->MapStruct.changed = TRUE;
			fDrawing = TRUE;
			ClearUndo(lpMapDocData);
			
			switch (iSelectionTools)
			{
			case IDM_PEN :
				if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
				{
					SelectGrid(lpMapDocData, hwnd, dx, dy, (int)lastTeamBase, TRUE);
				}
				else if (iSelectionMapSyms == IDM_MAP_CHECKPOINT)
				{
					tempc = FindNextCheckPoint(lpMapDocData);
					SelectGrid(lpMapDocData, hwnd, dx, dy, (int) tempc, TRUE);
				}
				else
					SelectGrid(lpMapDocData, hwnd, dx, dy, iSelectionMapSyms, TRUE);
				EnableClipboard(hwndFileToolBar, hMenuMap, FALSE); 
				break;
			case IDM_ERASE :
				SelectGrid(lpMapDocData, hwnd, dx, dy, IDM_MAP_SPACE, TRUE);
				EnableClipboard(hwndFileToolBar, hMenuMap, FALSE); 
				break;
			case IDM_FILL :
				if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
					FillArea(lpMapDocData, hwnd, dx, dy, (int)lastTeamBase); 						 
				else
					FillArea(lpMapDocData, hwnd, dx, dy, iSelectionMapSyms); 						 
				EnableClipboard(hwndFileToolBar, hMenuMap, FALSE); 
				InvalidateRect (hwnd, NULL, TRUE) ;
				//InvalidateRect (hwndMiniMap, NULL, TRUE);
				break;
			case IDM_LINE :
				ptBeg.x = ptEnd.x = LOWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
				ptBeg.y = ptEnd.y = HIWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
				DrawHighlightLine (hwnd, ptBeg, ptEnd) ;
				break;
			case IDM_LINEFILL :
				ptBeg.x = ptEnd.x = LOWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
				ptBeg.y = ptEnd.y = HIWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
				break;
			case IDM_SHAPE :
				switch (iSelectionShape)
				{
					case IDM_CIRCLEEMPTY:
					case IDM_CIRCLEFILLED:						
						ptBeg.x = ptEnd.x = LOWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
						ptBeg.y = ptEnd.y = HIWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
						DrawHighlightCircle (hwnd, ptBeg, ptEnd) ;
						break;
					case IDM_RECTEMPTY:
					case IDM_RECTFILLED:
						ptBeg.x = ptEnd.x = LOWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
						ptBeg.y = ptEnd.y = HIWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
						DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
						break;
				}
				break;
			case IDM_SELECT :
				InvalidateRect (hwnd, NULL, TRUE) ;
				
				fDrawing = TRUE;
				ptBeg.x = ptEnd.x = LOWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
				ptBeg.y = ptEnd.y = HIWORD (lParam) / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
				lpMapDocData->seldxend = lpMapDocData->seldxbeg = dx;
				lpMapDocData->seldyend = lpMapDocData->seldybeg = dy;
				lpMapDocData->fSelected = TRUE;
				
				lpMapDocData->ptTempBeg.x = 0;
				lpMapDocData->ptTempBeg.y = 0;
				lpMapDocData->ptTempEnd.x = 0;
				lpMapDocData->ptTempEnd.y = 0;
				
				break;
			}
		}
		if (fPasting)
		{	
			lpMapDocData->MapStruct.changed = TRUE;
			fPasting = FALSE;
			PasteData(lpMapDocData, dx, dy);
			InvalidateRect(hwnd, NULL, TRUE);
		}
		DoCaption (lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName) ;
		return 0;
		
	case WM_MOUSEMOVE:
		/*Verify that we should be processing this message. If this isnt the active window, then
		we have no need to process this message for this window*/
		hwndTmp = (HWND) SendMessage (hwndLocClient, WM_MDIGETACTIVE, 0, 0);
		if (hwndTmp != hwnd)
			return 0;

		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		GetCursorPos(&ptCurse);
		ScreenToClient (hwnd, &ptCurse);
		dx = ptCurse.x / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iHscrollPos;
		dy = ptCurse.y / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iVscrollPos;

		if (dx > lpMapDocData->MapStruct.width)
			dx = lpMapDocData->MapStruct.width;
		if (dy > lpMapDocData->MapStruct.height)
			dy = lpMapDocData->MapStruct.height;
		if (dx < 0)
			dx = 0;
		if (dy < 0)
			dy = 0;
		
		if (lpMapDocData->fSelected)
		{
			if ((dx >= lpMapDocData->seldxbeg) && (dx < lpMapDocData->seldxend)
				&& (dy >= lpMapDocData->seldybeg) && (dy < lpMapDocData->seldyend))
				SetCursor (LoadCursor (NULL, IDC_SIZEALL)) ;
		}
		if ((dx < lpMapDocData->MapStruct.width) && (dy < lpMapDocData->MapStruct.height))
		{
			if (fDragging)
				sprintf(sbtext, "X:%d Y:%d | MoveX:%d MoveY:%d",
				dx+1,dy+1,dx-lpMapDocData->strdx,dy-lpMapDocData->strdy);
			else
				sprintf(sbtext, "X:%d Y:%d", dx+1,dy+1);
			
			SendMessage(hwndStatusBar, SB_SETTEXT, (WPARAM) NULL, (LPARAM) (LPSTR) sbtext);
		}
		
		if ((dx <= lpMapDocData->MapStruct.width) &&
			(dy <= lpMapDocData->MapStruct.height) && (dx >= 0) && (dy >= 0))
		{
			if (fDrawing)
			{
				switch (iSelectionTools)
				{
				case IDM_PEN :
					if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
					{
						SelectGrid(lpMapDocData, hwnd, dx, dy, (int)lastTeamBase, TRUE);
					}
					else if (iSelectionMapSyms == IDM_MAP_CHECKPOINT)
					{
						if	( (lpMapDocData->MapStruct.data[dx][dy].cdata < 'A') ||
							(lpMapDocData->MapStruct.data[dx][dy].cdata > 'Z') )
						{
							tempc = FindNextCheckPoint(lpMapDocData);
							SelectGrid(lpMapDocData, hwnd, dx, dy, (int) tempc, TRUE);
						}
					}
					else
						SelectGrid(lpMapDocData, hwnd, dx, dy, iSelectionMapSyms, TRUE);
					break;
				case IDM_ERASE :  SelectGrid(lpMapDocData, hwnd, dx, dy, IDM_MAP_SPACE, TRUE);
					break;
				case IDM_LINE :
					SetCursor (LoadCursor (NULL, IDC_CROSS)) ;
					DrawHighlightLine (hwnd, ptBeg, ptEnd) ;
					ptEnd.x = (dx - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
					ptEnd.y = (dy - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
					DrawHighlightLine (hwnd, ptBeg, ptEnd) ;
					break;
				case IDM_LINEFILL :
					ptEnd.x = (dx - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
					ptEnd.y = (dy - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
					if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
					{
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, (int)lastTeamBase);
					}
					else
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, iSelectionMapSyms);
					break;
				case IDM_SHAPE :
					SetCursor (LoadCursor (NULL, IDC_CROSS)) ;
					switch (iSelectionShape)
					{
					case IDM_CIRCLEEMPTY:
					case IDM_CIRCLEFILLED:
						DrawHighlightCircle (hwnd, ptBeg, ptEnd) ;
						ptEnd.x = (dx - lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
						ptEnd.y = (dy - lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom + (lpMapDocData->MapStruct.view_zoom / 2);
						DrawHighlightCircle (hwnd, ptBeg, ptEnd) ;
						break;
					case IDM_RECTEMPTY:
					case IDM_RECTFILLED:
						DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
						ptEnd.x = (dx-lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
						ptEnd.y = (dy-lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
						DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
						break;
					}
					break;
				case IDM_SELECT :
					SetCursor (LoadCursor (NULL, IDC_CROSS)) ;
					DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
					ptEnd.x = (dx-lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
					ptEnd.y = (dy-lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
					lpMapDocData->seldxend = dx;
					lpMapDocData->seldyend = dy;
					DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
					lpMapDocData->fSelected = TRUE;
					break;
				}
			}
			if (fPasting)
			{
				DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
				SetCursor (LoadCursor (NULL, IDC_CROSS)) ;
				ptBeg.x = (dx-lpMapDocData->iHscrollPos) * lpMapDocData->MapStruct.view_zoom;
				ptBeg.y = (dy-lpMapDocData->iVscrollPos) * lpMapDocData->MapStruct.view_zoom;
				ptEnd.x = (dx-lpMapDocData->iHscrollPos + cWidth) * lpMapDocData->MapStruct.view_zoom;
				ptEnd.y = (dy-lpMapDocData->iVscrollPos + cHeight) * lpMapDocData->MapStruct.view_zoom;
				DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
			}
			if (fDragging)
			{
				DrawBoxOutline(hwnd, lpMapDocData->ptTempBeg, lpMapDocData->ptTempEnd);
				lpMapDocData->ptTempBeg.x = ptBeg.x+((dx-lpMapDocData->strdx)* lpMapDocData->MapStruct.view_zoom);
				lpMapDocData->ptTempBeg.y = ptBeg.y+((dy-lpMapDocData->strdy)* lpMapDocData->MapStruct.view_zoom);
				lpMapDocData->ptTempEnd.x = ptEnd.x+((dx-lpMapDocData->strdx)* lpMapDocData->MapStruct.view_zoom);
				lpMapDocData->ptTempEnd.y = ptEnd.y+((dy-lpMapDocData->strdy)* lpMapDocData->MapStruct.view_zoom);
				DrawBoxOutline(hwnd, lpMapDocData->ptTempBeg, lpMapDocData->ptTempEnd);
			}
		}
		return 0;
		
	case WM_LBUTTONUP:
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		ReleaseCapture();
		GetCursorPos(&ptCurse);
		ScreenToClient (hwnd, &ptCurse);
		dx = ptCurse.x / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iHscrollPos;
		dy = ptCurse.y / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iVscrollPos;

		if (dx > lpMapDocData->MapStruct.width)
			dx = lpMapDocData->MapStruct.width;
		if (dy > lpMapDocData->MapStruct.height)
			dy = lpMapDocData->MapStruct.height;
		if (dx < 0)
			dx = 0;
		if (dy < 0)
			dy = 0;
		
		if (fDragging)
		{
			fDragging = FALSE;
			ClearUndo(lpMapDocData);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
			
			dragArea(lpMapDocData, dx-lpMapDocData->strdx, dy-lpMapDocData->strdy, TRUE);
			lpMapDocData->strdx = 0;
			lpMapDocData->strdy = 0;
			SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
			DoCaption (lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName) ;
			InvalidateRect(hwnd, NULL, TRUE);
			return 0;
		}
		
		if ((dx <= lpMapDocData->MapStruct.width) && (dy <= lpMapDocData->MapStruct.height)
			&& (dx >= 0) && (dy >= 0))
		{
			if (fDrawing)
				switch (iSelectionTools)
			{
				case IDM_PEN :
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
					break;
				case IDM_ERASE :
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
					break;
				case IDM_FILL :
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
					break;
				case IDM_LINE : 
					SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
					DrawHighlightLine (hwnd, ptBeg, ptEnd) ;
					if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
					{
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, (int)lastTeamBase);
					}
					else
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, iSelectionMapSyms);
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
					break;
				case IDM_LINEFILL : 
					SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
					if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
					{
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, (int)lastTeamBase);
					}
					else
						DrawBlockLine(lpMapDocData, hwnd, ptBeg, ptEnd, iSelectionMapSyms);
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
					break;
				case IDM_SHAPE : 
					switch (iSelectionShape)
					{
					case IDM_CIRCLEEMPTY:
					case IDM_CIRCLEFILLED:
						SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
						DrawHighlightCircle (hwnd, ptBeg, ptEnd) ;
						if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
						{
							DrawBlockCircle(lpMapDocData, hwnd, ptBeg, ptEnd, (int)lastTeamBase);
						}
						else
							DrawBlockCircle(lpMapDocData, hwnd, ptBeg, ptEnd, iSelectionMapSyms);
						EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
						break;
					case IDM_RECTEMPTY:
					case IDM_RECTFILLED:
						SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
						DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
						if (iSelectionMapSyms == IDM_MAP_TEAMBASE)
						{
							DrawBlockRectangle(lpMapDocData, hwnd, ptBeg, ptEnd, (int)lastTeamBase);
						}
						else
							DrawBlockRectangle(lpMapDocData, hwnd, ptBeg, ptEnd, iSelectionMapSyms);
						EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
						break;

					}
					break;
				case IDM_SELECT :
					SetCursor (LoadCursor (NULL, IDC_ARROW)) ;
					EnableClipboard(hwndFileToolBar,hMenuMap, TRUE);
					SortSelectionArea(lpMapDocData);
					break;
			}
		}
		fDrawing = FALSE;
		return 0;	
		
	case WM_RBUTTONDOWN:
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		GetCursorPos(&ptCurse);
		ScreenToClient (hwnd, &ptCurse);
		dx = ptCurse.x / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iHscrollPos;
		dy = ptCurse.y / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iVscrollPos;
		lpMapDocData->xcoord = ptCurse.x / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
		lpMapDocData->ycoord = ptCurse.y / lpMapDocData->MapStruct.view_zoom * lpMapDocData->MapStruct.view_zoom;
		if ((dx < lpMapDocData->MapStruct.width) && (dy < lpMapDocData->MapStruct.height) && (dx >= 0) && (dy >= 0))
		{
			ClearUndo(lpMapDocData);
			IncrementMapBlock(lpMapDocData, hwnd, lpMapDocData->xcoord, lpMapDocData->ycoord, dx, dy);
			EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
			DoCaption (lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName) ;
		}
		return 0;
		
		
		
	case WM_VSCROLL :
		lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
		fDrawing = FALSE;
		switch (LOWORD (wParam))
		{
		case SB_TOP :
			iVscrollInc = -lpMapDocData->iVscrollPos ;
			break ;
			
		case SB_BOTTOM :
			iVscrollInc = lpMapDocData->iVscrollMax - lpMapDocData->iVscrollPos ;
			break ;
			
		case SB_LINEUP :
			iVscrollInc = -1 ;
			break ;
			
		case SB_LINEDOWN :
			iVscrollInc = 1 ;
			break ;
			
		case SB_PAGEUP :
			iVscrollInc = min (-1, -cyClient / lpMapDocData->MapStruct.view_zoom) ;
			break ;
			
		case SB_PAGEDOWN :
			iVscrollInc = max (1, cyClient / lpMapDocData->MapStruct.view_zoom) ;
			break ;
			
		case SB_THUMBTRACK :
			iVscrollInc = HIWORD (wParam) - lpMapDocData->iVscrollPos ;
			break ;
			
		default :
			iVscrollInc = 0 ;
		}
		iVscrollInc = max (-lpMapDocData->iVscrollPos,
			min (iVscrollInc, lpMapDocData->iVscrollMax - lpMapDocData->iVscrollPos)) ;
		
		if (iVscrollInc != 0)
		{
			lpMapDocData->iVscrollPos += iVscrollInc ;
			if (lpMapDocData->fSelected)
				DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
			ScrollWindow (hwnd, 0, -lpMapDocData->MapStruct.view_zoom * iVscrollInc, NULL, NULL) ;
			SetScrollPos (hwnd, SB_VERT, lpMapDocData->iVscrollPos, TRUE) ;
			UpdateWindow(hwnd);
		}
		return 0 ;
		
		
		case WM_HSCROLL :
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
			fDrawing = FALSE;
			switch (LOWORD (wParam))
			{
			case SB_TOP :
				iHscrollInc = -lpMapDocData->iHscrollPos ;
				break ;
				
			case SB_BOTTOM :
				iHscrollInc = lpMapDocData->iHscrollMax - lpMapDocData->iHscrollPos ;
				break ;
				
			case SB_LINEUP :
				iHscrollInc = -1 ;
				break ;
				
			case SB_LINEDOWN :
				iHscrollInc = 1 ;
				break ;
				
			case SB_PAGEUP :
				iHscrollInc = min (-1, -cxClient / lpMapDocData->MapStruct.view_zoom) ;
				break ;
				
			case SB_PAGEDOWN :
				iHscrollInc = max (1, cxClient / lpMapDocData->MapStruct.view_zoom) ;
				break ;
				
			case SB_THUMBTRACK :
				iHscrollInc = HIWORD (wParam) - lpMapDocData->iHscrollPos ;
				break ;
				
			default :
				iHscrollInc = 0 ;
			}               
			iHscrollInc = max (-lpMapDocData->iHscrollPos,
				min (iHscrollInc, lpMapDocData->iHscrollMax - lpMapDocData->iHscrollPos)) ;
			
			if (iHscrollInc != 0)
			{
				lpMapDocData->iHscrollPos += iHscrollInc ;
				if (lpMapDocData->fSelected)
					DrawBoxOutline (hwnd, ptBeg, ptEnd) ;
				ScrollWindow (hwnd, -lpMapDocData->MapStruct.view_zoom * iHscrollInc, 0, NULL, NULL) ;
				SetScrollPos (hwnd, SB_HORZ, lpMapDocData->iHscrollPos, TRUE) ;
				UpdateWindow(hwnd);
			}
			return 0 ;

	case WM_KEYDOWN :
		switch (wParam)
		{
		case VK_HOME :
			SendMessage (hwnd, WM_VSCROLL, SB_TOP, 0L) ;
			break ;
		case VK_END :
			SendMessage (hwnd, WM_VSCROLL, SB_BOTTOM, 0L) ;
			break ;
		case VK_PRIOR :
			SendMessage (hwnd, WM_VSCROLL, SB_PAGEUP, 0L) ;
			break ;
		case VK_NEXT :
			SendMessage (hwnd, WM_VSCROLL, SB_PAGEDOWN, 0L) ;
			break ;			
		case VK_UP :
			SendMessage (hwnd, WM_VSCROLL, SB_LINEUP, 0L) ;
			break ;			
		case VK_DOWN :
			SendMessage (hwnd, WM_VSCROLL, SB_LINEDOWN, 0L) ;
			break ;			
		case VK_LEFT :
			SendMessage (hwnd, WM_HSCROLL, SB_LINEUP, 0L) ;
			break ;			
		case VK_RIGHT :
			SendMessage (hwnd, WM_HSCROLL, SB_LINEDOWN, 0L) ;
			break ;
		}
		return 0;

		case WM_CHAR :
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
				GetCursorPos(&ptCurse);
				ScreenToClient (hwnd, &ptCurse);
				dx = ptCurse.x / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iHscrollPos;
				dy = ptCurse.y / lpMapDocData->MapStruct.view_zoom+lpMapDocData->iVscrollPos;
				if ((dx < lpMapDocData->MapStruct.width) && (dy < lpMapDocData->MapStruct.height)
					&& (dx >= 0) & (dy >=0))
				{
					ClearUndo(lpMapDocData);
					if ((toupper((char)wParam) >= 'A') && (toupper((char)wParam) <= 'Z') || ((char)wParam >= '0') && ((char)wParam <= '9'))
					{
						switch (iSelectionTools)
						{
						case IDM_FILL :
							if ( ((char)wParam >= '0') && ((char)wParam <= '9'))
							{
								FillArea(lpMapDocData, hwnd, dx, dy, (int)wParam);
								break;
							}
						case IDM_SELECT :
						case IDM_LINE :
						case IDM_LINEFILL :
						case IDM_ERASE : 
						case IDM_PEN :
						case IDM_SHAPE :
							SelectGrid(lpMapDocData, hwnd, dx, dy, (int)wParam, TRUE);
							break;
						}
						if (((char)wParam >= '0') && ((char)wParam <= '9'))
						{
							CountBases(lpMapDocData);
							lastTeamBase = (char)wParam;
							InvalidateRect(hwnd, NULL, TRUE);
							UpdateStatusBarSettings(lpMapDocData);
						}
						if (((char)wParam >= 'A') && ((char)wParam <= 'Z'))
						{
							CountCheckPoints(lpMapDocData);
						}
						EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
						DoCaption (lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName) ;
					}				
				}
				return 0;
		case WM_MDIACTIVATE:
			fPasting = FALSE;
			if(lParam == (LPARAM) hwnd)
			{
				/*Make sure that the right menu and toolbars are visible, including the appropriate
				buttons on the toolbars*/
				SendMessage (hwndLocClient, WM_MDISETMENU, (WPARAM) hMenuMap,
					(LPARAM) hMenuMapWindow);
				ShowWindow(hwndMapSymsToolBar, SW_SHOW);
				ShowWindow(hwndToolsToolBar, SW_SHOW);
				SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_SAVE, (LPARAM) MAKELONG(TRUE, 0));
				if (iSelectionTools == IDM_SHAPE)
					ShowWindow(hwndShapeToolBar, SW_SHOW);
				
				hwndChild = (HWND) GetCurrentMapWindow(hwndLocClient);
				
				lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
				
				/*Set the undo menu appropriately*/
				if (lpMapDocData->undolist != NULL)
					EnableUndo(hwndFileToolBar, hMenuMap, TRUE);
				
				/*Set the clipboard appropriately*/
				if (lpMapDocData->fSelected)
					if (!fSelect)
						lpMapDocData->fSelected = FALSE;

				EnableClipboard(hwndFileToolBar, hMenuMap, lpMapDocData->fSelected);

				DoCaption(lpMapDocData, hwnd, lpMapDocData->MapStruct.mapName);
				UpdateStatusBarSettings(lpMapDocData);

				InvalidateRect(hwnd, NULL, TRUE);
				
				if(hwndMiniMap)
				{
					SizeSmallMap(lpMapDocData);
					SetWindowLong(hwndMiniMap, 0, (long) hwndChild);
					InvalidateRect(hwndMiniMap, NULL, TRUE);
				}
				
			} else if (lParam == (LPARAM) hwndMiniMap)
			{
				hwndChild = GetCurrentMapWindow(hwndLocClient);
				SetWindowLong(hwndMiniMap, 0, (long) hwndChild);
			}
			else
			{
				SendMessage (hwndLocClient, WM_MDISETMENU, (WPARAM) hMenuInit, (LPARAM) hMenuInitWindow);
				ShowWindow(hwndMapSymsToolBar, SW_HIDE);
				ShowWindow(hwndToolsToolBar, SW_HIDE);
				if (iSelectionTools == IDM_SHAPE)
					ShowWindow(hwndShapeToolBar, SW_HIDE);
				SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_SAVE, (LPARAM) MAKELONG(FALSE, 0));
				EnableClipboard(hwndFileToolBar, hMenuMap, FALSE);
				SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_PASTE, (LPARAM) MAKELONG(FALSE, 0));

				if (hwndMiniMap && (hwndChild == NULL || hwndChild == hwndMiniMap))
				{
					SendMessage(hwndMiniMap, WM_CLOSE, (WPARAM) NULL, (LPARAM) NULL);
					hwndChild = NULL;
				}
			}


			/*Redisplay the menubar*/
			DrawMenuBar (hwndLocFrame);
			return 0;
			
		case WM_QUERYENDSESSION :
		case WM_CLOSE :
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);

			if (lpMapDocData->MapStruct.changed && IDCANCEL == AskAboutSave (hwnd, lpMapDocData->MapStruct.mapName))
			{
				return 0 ;
			}

//			EnableUndo(hwndFileToolBar, hMenuMap, FALSE);
//			hwndChild = NULL;
			break;
		case WM_DESTROY :
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwnd, 0);
			if (lpMapDocData != NULL)
				HeapFree (GetProcessHeap (), 0, lpMapDocData);
			return 0;
	}
	//Pass unprocessed message to DefMDIChildProc
    return DefMDIChildProc (hwnd, iMsg, wParam, lParam);
	
}
/***************************************************************************/
/* WildDlgProc                                                             */
/* Arguments :                                                             */
/*    hDlg                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/* Purpose :   The procedure for the Wild Map prefs                        */
/***************************************************************************/
BOOL CALLBACK DirDlgProc (HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static char *tempdir, *tempedit, *tempclient, *tempserver, *temptemplate;
	HWND hwndTemplateCheck = GetDlgItem (hDlg, IDC_USETEMPLATE) ;
	
	tempdir = (char *) malloc(_MAX_PATH*sizeof(char));
	tempedit = (char *) malloc(_MAX_PATH*sizeof(char));
	tempclient = (char *) malloc(_MAX_PATH*sizeof(char));
	tempserver = (char *) malloc(_MAX_PATH*sizeof(char));
	temptemplate = (char *) malloc(_MAX_PATH*sizeof(char));
	
	
	switch(iMsg)
	{
	case WM_INITDIALOG :
		SetEditText (hDlg, IDC_MAPDIR, (LPCTSTR) xpilotmap_dir);	 
		SetEditText (hDlg, IDC_EDITORCOMMAND, (LPCTSTR) editor_command);	 
		SetEditText (hDlg, IDC_CLIENTCOMMAND, (LPCTSTR) client_command);	 
		SetEditText (hDlg, IDC_SERVERCOMMAND, (LPCTSTR) server_command);	 
		SetEditText (hDlg, IDC_TEMPLATE, (LPCTSTR) template_file);
		
		if (UseTemplateFile)
			Button_SetCheck(hwndTemplateCheck, BST_CHECKED);
		else
			Button_SetCheck(hwndTemplateCheck, BST_UNCHECKED);
		
		return TRUE;
		
	case WM_COMMAND :
		switch (LOWORD (wParam))
		{
		case IDOK:
			UpdateMapPrefText(hDlg, IDC_MAPDIR, (LPSTR) tempdir);
			UpdateMapPrefText(hDlg, IDC_EDITORCOMMAND, (LPSTR) tempedit);
			UpdateMapPrefText(hDlg, IDC_CLIENTCOMMAND, (LPSTR) tempclient);
			UpdateMapPrefText(hDlg, IDC_SERVERCOMMAND, (LPSTR) tempserver);
			UpdateMapPrefText(hDlg, IDC_TEMPLATE, (LPSTR) temptemplate);

			sprintf((char *)xpilotmap_dir, "%s", tempdir);
			RegSetValueEx(xpilotkey, (LPTSTR) "mapdir", 0, REG_SZ, xpilotmap_dir, *xpilotmap_dir_length);			

			sprintf((char *)editor_command, "%s", tempedit);
			RegSetValueEx(xpilotkey, (LPTSTR) "editcomm", 0, REG_SZ, editor_command, strlen((char *)editor_command));			

			sprintf((char *)client_command, "%s", tempclient);
			RegSetValueEx(xpilotkey, (LPTSTR) "clientcomm", 0, REG_SZ, client_command, strlen((char *)client_command));			

			sprintf((char *)server_command, "%s", tempserver);
			RegSetValueEx(xpilotkey, (LPTSTR) "servercomm", 0, REG_SZ, server_command, strlen((char *)server_command));			

			sprintf((char *)template_file, "%s", temptemplate);
			RegSetValueEx(xpilotkey, (LPTSTR) "template_file", 0, REG_SZ, template_file, strlen((char *)template_file));			

			UseTemplateFile = Button_GetCheck (hwndTemplateCheck);
			itoa(UseTemplateFile, (char *) usetemplate_file, 10);
			RegSetValueEx(xpilotkey, (LPTSTR) "use_template", 0, REG_SZ, usetemplate_file, strlen((char *) usetemplate_file) );			

			free(tempdir);
			free(tempedit);
			free(tempclient);
			free(tempserver);
			free(temptemplate);

			EndDialog (hDlg, 1);
			return TRUE;
		case IDCANCEL :
			free(tempdir);
			free(tempedit);
			free(tempclient);
			free(tempserver);
			free(temptemplate);
			EndDialog (hDlg, 0);
			return FALSE;
		}
		break;
	}
	return FALSE;
}
/***************************************************************************/
/* Setup_default_server_options                                            */
/* Arguments :                                                             */
/*    lpMapDocData - pointer to map document.                              */
/* Purpose :   Begin filling default options                               */
/***************************************************************************/
void   Setup_default_server_options(LPMAPDOCDATA lpMapDocData)
{
	int i;
	
	
	for(i=0;i<num_default_settings;i++)
		AddOption(lpMapDocData, default_settings[i].name, default_settings[i].value, FALSE, TRUE);
	
	return;
}
/***************************************************************************/
/* EnableClipboard                                                         */
/* Arguments :                                                             */
/*    hwndFileToolBar = handle to toolbar to enable or disable clipboard in*/
/*    hMenu = handle to menu to enable or disable clipboard in             */
/*    state = enable or disable                                            */
/*                                                                         */
/* Purpose :   Disable the clipboard menu and buttons                      */
/***************************************************************************/
void EnableClipboard(HWND hwndFileToolBar, HMENU hMenu, BOOL state)
{
	if (state)
	{
		EnableMenuItem(hMenu, IDM_EDIT_CUT, MF_ENABLED);
		EnableMenuItem(hMenu, IDM_EDIT_COPY, MF_ENABLED);
		EnableMenuItem(hMenu, IDM_CROP, MF_ENABLED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_CUT, (LPARAM) MAKELONG(TRUE, 0));
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_COPY, (LPARAM) MAKELONG(TRUE, 0));
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_CROP, (LPARAM) MAKELONG(TRUE, 0));
		EnableMenuItem(hMenu, IDM_SAVESELECTEDAS, MF_ENABLED);
	}
	else
	{
		EnableMenuItem(hMenu, IDM_EDIT_CUT, MF_GRAYED);
		EnableMenuItem(hMenu, IDM_EDIT_COPY, MF_GRAYED);
		EnableMenuItem(hMenu, IDM_CROP, MF_GRAYED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_CUT, (LPARAM) MAKELONG(FALSE, 0));
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_COPY, (LPARAM) MAKELONG(FALSE, 0));
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_CROP, (LPARAM) MAKELONG(FALSE, 0));
		EnableMenuItem(hMenu, IDM_SAVESELECTEDAS, MF_GRAYED);
	}
	if (fClipboarded)
	{
		EnableMenuItem(hMenu, IDM_EDIT_PASTE, MF_ENABLED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_PASTE, (LPARAM) MAKELONG(TRUE, 0));
	}
	else
	{
		EnableMenuItem(hMenu, IDM_EDIT_PASTE, MF_GRAYED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_PASTE, (LPARAM) MAKELONG(FALSE, 0));
	}
}
/***************************************************************************/
/* EnableUndo                                                              */
/* Arguments :                                                             */
/*    hwndFileToolBar = handle to toolbar to enable or disable undo in	   */
/*    hMenu = handle to menu to enable or disable undo in                  */
/*    state = enable or disable                                            */
/*                                                                         */
/* Purpose :   Disable the undo menu and buttons                           */
/***************************************************************************/
void EnableUndo(HWND hwndFileToolBar, HMENU hMenu, BOOL state)
{
	if (state)
	{
		EnableMenuItem(hMenu, IDM_EDIT_UNDO, MF_ENABLED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_UNDO, (LPARAM) MAKELONG(TRUE, 0));
	}
	else
	{
		EnableMenuItem(hMenu, IDM_EDIT_UNDO, MF_GRAYED);
		SendMessage(hwndFileToolBar, TB_ENABLEBUTTON, IDM_EDIT_UNDO, (LPARAM) MAKELONG(FALSE, 0));
	}
	
}
/***************************************************************************/
/* Process_Registry                                                        */
/* Arguments :                                                             */
/*   hWndInstance - instance of the program                                */
/*   hwnd - handle to parent window                                        */
/*   hMenu - handle to main menu                                           */
/* Purpose :   Process all the windows registry entries                    */
/***************************************************************************/
void Process_Registry(HINSTANCE hWndInstance, HWND hwnd, HMENU hMenu)
{
	if ( !RegCreateKeyEx(HKEY_LOCAL_MACHINE, "SOFTWARE\\Xpilot\\MapXpress", 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &xpilotkey, NULL) )
	{
		if ( RegQueryValueEx(xpilotkey, (LPTSTR) "mapdir", NULL, NULL, xpilotmap_dir, xpilotmap_dir_length) )
		{
			OkMessage(NULL, "Map directory not set!\nYou will be prompted for the default directory.", "Default not set");
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPDIR), hwnd, DirDlgProc);
		}
		if ( RegQueryValueEx(xpilotkey, (LPTSTR) "editcomm", NULL, NULL, editor_command, editor_command_length) )
		{
			OkMessage(NULL, "Editor command not set!\nYou will be prompted for the default value.", "Default not set");
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPDIR), hwnd, DirDlgProc);
		}
		if ( RegQueryValueEx(xpilotkey, (LPTSTR) "clientcomm", NULL, NULL, client_command, client_command_length) )
		{
			OkMessage(NULL, "Xpilot Client command not set!\nYou will be prompted for the default value.", "Default not set");
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPDIR), hwnd, DirDlgProc);
		}
		if ( RegQueryValueEx(xpilotkey, (LPTSTR) "servercomm", NULL, NULL, server_command, server_command_length) )
		{
			OkMessage(NULL, "Xpilot Server command not set!\nYou will be prompted for the default value.", "Default not set");
			DialogBox (hWndInstance, MAKEINTRESOURCE(IDD_MAPDIR), hwnd, DirDlgProc);
		}

		RegQueryValueEx(xpilotkey, (LPTSTR) "template_file", NULL, NULL, template_file, template_file_length);

		if ( !RegQueryValueEx(xpilotkey, (LPTSTR) "filled", NULL, NULL, filled_world, filled_world_length) )
		{
			FilledWorld = (int) atoi((char *) filled_world);
			if (FilledWorld)
				CheckMenuItem (hMenu, IDM_FILLEDWORLD, MF_CHECKED) ;
		}
		if ( !RegQueryValueEx(xpilotkey, (LPTSTR) "paste", NULL, NULL, trans_paste, trans_paste_length) )
		{
			TransPaste = (int) atoi((char *) trans_paste);
			if (TransPaste)
				CheckMenuItem (hMenu, IDM_TRANSPASTE, MF_CHECKED) ;
		}
		if ( !RegQueryValueEx(xpilotkey, (LPTSTR) "saveallprefs", NULL, NULL, save_all_prefs, save_all_prefs_length) )
		{
			SaveAllPrefs = (int) atoi((char *) save_all_prefs);
			if (SaveAllPrefs)
				CheckMenuItem (hMenu, IDM_SAVEALLPREFS, MF_CHECKED) ;
		}
		if ( !RegQueryValueEx(xpilotkey, (LPTSTR) "mini_map", NULL, NULL, mini_map, mini_map_length) )
		{
			Mini_Map = (int) atoi((char *) mini_map);
			if (Mini_Map)
				CheckMenuItem (hMenu, IDM_MINIMAP, MF_CHECKED) ;
		}
		if ( !RegQueryValueEx(xpilotkey, (LPTSTR) "use_template", NULL, NULL, usetemplate_file, usetemplate_file_length) )
		{
			UseTemplateFile = (int) atoi((char *) usetemplate_file);
		}
	
	}
}
/***************************************************************************/
/* OkMessage                                                               */
/* Arguments :                                                             */
/*    hwnd - the parent window.                                            */
/*   *szMessage - the message to display.                                  */
/*    *szTitleName - the title to display.                                 */
/*                                                                         */
/* Purpose :   Prints the specified message in an OK message box           */
/***************************************************************************/
void OkMessage (HWND hwnd, char *szMessage, char *szTitleName)
{
	char szBuffer[64 + _MAX_FNAME + _MAX_EXT] ;
	
	wsprintf (szBuffer, szMessage, szTitleName[0] ? szTitleName : UNTITLED) ;
	
	MessageBox (hwnd, szBuffer, szAppName, MB_OK | MB_ICONEXCLAMATION) ;
}
/***************************************************************************/
/* ErrorHandler                                                            */
/* Arguments :                                                             */
/*   szMessage - error messge to be output                                 */
/*   ... - variable number of arguments, subsituted for %s, %d, etc.       */
/* Purpose : Display an Error notification box with the specified message  */
/***************************************************************************/
void ErrorHandler(LPSTR szMessage, ...)
{
	va_list marker; 
	char szBuffer[512];
	
	// Figure through the extra arguments.
	va_start(marker, szMessage);
	vsprintf(szBuffer, szMessage, marker);
	va_end(marker);              
	
	// Display the error message.
	MessageBox(NULL, szBuffer, "MapXpress Error", MB_ICONEXCLAMATION | MB_OK);
} 
/***************************************************************************/
/* UpdateStatusBarSettings                                                 */
/* Arguments :                                                             */
/*   szMessage - error messge to be output                                 */
/*   ... - variable number of arguments, subsituted for %s, %d, etc.       */
/* Purpose : Display an Error notification box with the specified message  */
/***************************************************************************/
void UpdateStatusBarSettings(LPMAPDOCDATA lpMapDocData)
{
	char sbtext[100];

	if (lpMapDocData->MapStruct.edgeWrap)
		sprintf(sbtext, "edgeWrap:On");
	else
		sprintf(sbtext, "edgeWrap:Off");
	SendMessage(hwndStatusBar, SB_SETTEXT, (WPARAM) 1 | 0, (LPARAM) (LPSTR) sbtext); 

	sprintf(sbtext, "Current Team: %c", lastTeamBase);
	SendMessage(hwndStatusBar, SB_SETTEXT, (WPARAM) 2 | 0, (LPARAM) (LPSTR) sbtext); 

	sprintf(sbtext, "Author: %s", lpMapDocData->MapStruct.mapAuthor);
	SendMessage(hwndStatusBar, SB_SETTEXT, (WPARAM) 3 | 0, (LPARAM) (LPSTR) sbtext); 

}
/***************************************************************************/
/* GetCurrentMapWindow                                                     */
/* Arguments :                                                             */
/*   hwndLocClient - handle to client window                               */
/* Purpose : Return the handle to the current selected map window.         */
/***************************************************************************/
HWND GetCurrentMapWindow(HWND hwndLocClient)
{
	HWND hwndMap = NULL;

	hwndMap = (HWND) SendMessage (hwndLocClient, WM_MDIGETACTIVE, 0, 0);

	if (hwndMap == hwndMiniMap)
	{
		if ( !(hwndMap = (HWND) GetWindowLong (hwndMiniMap, 0)) )
			hwndMap = GetNextWindow(hwndMiniMap, GW_HWNDPREV);
		if (!hwndMap)
			hwndMap = GetNextWindow(hwndMiniMap, GW_HWNDNEXT);
	}

	return hwndMap;
}
/***************************************************************************/
/* AddMapWindow                                                            */
/* Arguments :                                                             */
/*   hwndLocClient - handle to client window                               */
/* Purpose : Return the handle to a new map window.                        */
/***************************************************************************/
HWND AddMapWindow(HWND hwndLocClient)
{
    MDICREATESTRUCT mdicreate;
	HWND hwndNewMap;

	mdicreate.szClass = "ChildMapWin";
	mdicreate.szTitle = "New Map";
	mdicreate.hOwner	= hInst;
	mdicreate.x = CW_USEDEFAULT;
	mdicreate.cx = DEFAULT_WIDTH;
	mdicreate.y = CW_USEDEFAULT;
	mdicreate.cy = DEFAULT_HEIGHT;
	mdicreate.style = WS_VSCROLL | WS_HSCROLL;
	mdicreate.lParam = 0;
	
	hwndNewMap = (HWND)SendMessage (hwndLocClient,
		WM_MDICREATE,
		0,
		(LONG)(LPMDICREATESTRUCT)&mdicreate);

	return hwndNewMap;
}