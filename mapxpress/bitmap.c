/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"

/***************************************************************************/
/* SaveBmpFile                                                             */
/* Arguments :                                                             */
/*   hwnd - Window whose contents should be saved.                         */
/*   szTempFileName - the file name to save too.                           */
/* Purpose :                                                               */
/***************************************************************************/
int SaveBmpFile(HWND hwnd, PSTR szTempFileName)
{
	HDC hdcWindow;
	HDC hdcCompatible;
	HBITMAP hbmWindow;
	PBITMAPINFO pbmi;
	RECT rect;


	// Create a normal DC and a memory DC for the entire screen. The 
	// normal DC provides a "snapshot" of the screen contents. The 
	// memory DC keeps a copy of this "snapshot" in the associated 
	// bitmap. 
	hdcWindow = GetDC (hwnd) ;
	//hdcWindow = CreateDC("DISPLAY", NULL, NULL, NULL); 
	hdcCompatible = CreateCompatibleDC(hdcWindow); 
	
	GetClientRect (hwnd, &rect); 
	// Create a compatible bitmap for hdcScreen. 
	
	hbmWindow = CreateCompatibleBitmap(hdcWindow, 
		rect.right - rect.left, 
		rect.bottom - rect.top); 
	
	if (hbmWindow == 0) 
		ErrorHandler("hbmWindow"); 
	
	// Select the bitmaps into the compatible DC. 
	
	if (!SelectObject(hdcCompatible, hbmWindow)) 
		ErrorHandler("Compatible Bitmap Selection"); 
	
	UpdateWindow(hwnd); 
	
	
	//Copy color data for the entire display into a 
	//bitmap that is selected into a compatible DC. 
	
	if (!BitBlt(hdcCompatible, 
		0,0, 
		rect.right - rect.left, 
		rect.bottom - rect.top,
		hdcWindow,
		0,0, 
		SRCCOPY)) 
		
        ErrorHandler("Window to Compat Blt Failed"); 
	
	pbmi = CreateBitmapInfoStruct(hwnd, hbmWindow);
	CreateBMPFile(hwnd, szTempFileName, pbmi, hbmWindow, hdcCompatible) ; 

	return FALSE;
}
PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{ 
    BITMAP bmp; 
    PBITMAPINFO pbmi; 
    WORD    cClrBits; 
	
    // Retrieve the bitmap's color format, width, and height. 
    if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp)) 
        ErrorHandler("GetObject"); 
	
    // Convert the color format to a count of bits. 
    cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
    if (cClrBits == 1) 
        cClrBits = 1; 
    else if (cClrBits <= 4) 
        cClrBits = 4; 
    else if (cClrBits <= 8) 
        cClrBits = 8; 
    else if (cClrBits <= 16) 
        cClrBits = 16; 
    else if (cClrBits <= 24) 
        cClrBits = 24; 
    else cClrBits = 32; 
	
    // Allocate memory for the BITMAPINFO structure. (This structure 
    // contains a BITMAPINFOHEADER structure and an array of RGBQUAD 
    // data structures.) 
	
	if (cClrBits != 24) 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
		sizeof(BITMAPINFOHEADER) + 
		sizeof(RGBQUAD) * (1<< cClrBits)); 
	
	// There is no RGBQUAD array for the 24-bit-per-pixel format. 
	
	else 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
		sizeof(BITMAPINFOHEADER)); 
	
    // Initialize the fields in the BITMAPINFO structure. 
	
    pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    pbmi->bmiHeader.biWidth = bmp.bmWidth; 
    pbmi->bmiHeader.biHeight = bmp.bmHeight; 
    pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
    pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
    if (cClrBits < 24) 
        pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 
	
    // If the bitmap is not compressed, set the BI_RGB flag. 
    pbmi->bmiHeader.biCompression = BI_RGB; 
	
    // Compute the number of bytes in the array of color 
    // indices and store the result in biSizeImage. 
    pbmi->bmiHeader.biSizeImage = (pbmi->bmiHeader.biWidth + 7) /8 * pbmi->bmiHeader.biHeight * cClrBits; 
	// Set biClrImportant to 0, indicating that all of the 
    // device colors are important. 
	pbmi->bmiHeader.biClrImportant = 0; 
	return pbmi; 
} 
void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, 
				   HBITMAP hBMP, HDC hDC) 
{ 
	HANDLE hf;                 // file handle 
    BITMAPFILEHEADER hdr;       // bitmap file-header 
    PBITMAPINFOHEADER pbih;     // bitmap info-header 
    LPBYTE lpBits;              // memory pointer 
    DWORD dwTotal;              // total count of bytes 
    DWORD cb;                   // incremental count of bytes 
    BYTE *hp;                   // byte pointer 
    DWORD dwTmp; 
	
    pbih = (PBITMAPINFOHEADER) pbi; 
    lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);
	
    if (!lpBits) 
		ErrorHandler("GlobalAlloc"); 
	
    // Retrieve the color table (RGBQUAD array) and the bits 
    // (array of palette indices) from the DIB. 
    if (!GetDIBits(hDC, hBMP, 0, (WORD) pbih->biHeight, lpBits, pbi, 
        DIB_RGB_COLORS)) 
    {
        ErrorHandler("GetDIBits"); 
    }
	
    // Create the .BMP file. 
    hf = CreateFile(pszFile, 
		GENERIC_READ | GENERIC_WRITE, 
		(DWORD) 0, 
		NULL, 
		CREATE_ALWAYS, 
		FILE_ATTRIBUTE_NORMAL, 
		(HANDLE) NULL); 
    if (hf == INVALID_HANDLE_VALUE) 
        ErrorHandler("CreateFile"); 
    hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M" 
    // Compute the size of the entire file. 
    hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
		pbih->biSize + pbih->biClrUsed 
		* sizeof(RGBQUAD) + pbih->biSizeImage); 
    hdr.bfReserved1 = 0; 
    hdr.bfReserved2 = 0; 
	
    // Compute the offset to the array of color indices. 
    hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
		pbih->biSize + pbih->biClrUsed 
		* sizeof (RGBQUAD); 
	
    // Copy the BITMAPFILEHEADER into the .BMP file. 
    if (!WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), 
        (LPDWORD) &dwTmp,  NULL)) 
    {
		ErrorHandler("WriteFile"); 
    }
	
    // Copy the BITMAPINFOHEADER and RGBQUAD array into the file. 
    if (!WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) 
		+ pbih->biClrUsed * sizeof (RGBQUAD), (LPDWORD) &dwTmp, ( NULL)) ) 
        ErrorHandler("WriteFile"); 
	
    // Copy the array of color indices into the .BMP file. 
    dwTotal = cb = pbih->biSizeImage; 
    hp = lpBits; 
    if (!WriteFile(hf, (LPSTR) hp, (int) cb, (LPDWORD) &dwTmp,NULL)) 
		ErrorHandler("WriteFile"); 
	
    // Close the .BMP file. 
	if (!CloseHandle(hf)) 
		ErrorHandler("CloseHandle"); 
	
    // Free memory. 
    GlobalFree((HGLOBAL)lpBits);
}

