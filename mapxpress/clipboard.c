/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"

extern BOOL TransPaste;

/***************************************************************************/
/* EmptyClipData                                                           */
/* Arguments :                                                             */
/*   none                                                                  */
/*                                                                         */
/*                                                                         */
/* Purpose : Empty out the Clipboard array.                                */
/***************************************************************************/
void EmptyClipData()
{
	int x,y;
	
	for(x = 0; x < MAX_MAP_SIZE; x++)
		for(y = 0; y < MAX_MAP_SIZE; y++)
			ClipBoard[x][y] = MAP_SPACE;
}
/***************************************************************************/
/* FillClipData                                                            */
/* Arguments :                                                             */
/*   lpMapDocData: Pointer to map document.                                */
/*   cut: Is this a cut or a copy?                                         */
/*                                                                         */
/* Purpose :  Fill the Clipboard Array.                                    */
/***************************************************************************/
void FillClipData(LPMAPDOCDATA lpMapDocData, BOOL cut)
{
	
	int x,y,i,j,xbegin,ybegin,xend,yend;
	BOOL base = FALSE;

	EmptyClipData();      

	
		SortSelectionArea(lpMapDocData);
		xbegin = lpMapDocData->seldxbeg;
		xend = lpMapDocData->seldxend;
		ybegin = lpMapDocData->seldybeg;
		yend = lpMapDocData->seldyend;

	
	cWidth = xend - xbegin;
	cHeight = yend - ybegin;
	
	for (x=0, i=xbegin; i < xend; x++, i++)
		for(y=0, j=ybegin; j < yend; y++, j++)
		{
				if (!InsideMap(lpMapDocData, i, j))
					continue;

			if ( (lpMapDocData->MapStruct.data[i][j].cdata >= '0') && (lpMapDocData->MapStruct.data [i][j].cdata <= '9') )
			   base = TRUE;

			ClipBoard[x][y] = lpMapDocData->MapStruct.data[i][j].cdata;
			if (cut)
				ChangeMapData(lpMapDocData,i,j, MAP_SPACE, 1);
				
		}

		
		if (cut)
		{
			lpMapDocData->MapStruct.changed = TRUE;
			if (base)
				CountBases(lpMapDocData);
		}
		
}
/***************************************************************************/
/* PasteData                                                               */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to the map document.                            */
/*   xbeg: pasting start x coord.                                          */
/*   ybeg: pasting start y coord.                                          */
/* Purpose : Put the Clipboarded data into the map starting at xbeg,ybeg.  */
/***************************************************************************/
void PasteData(LPMAPDOCDATA lpMapDocData, int xbeg, int ybeg)
{
	int x,y,i,j;
	BOOL base = FALSE;

	lpMapDocData->MapStruct.changed = 1;
	for (x=xbeg, i = 0; i < cWidth; x++, i++)
		for (y = ybeg, j = 0; j < cHeight; y++, j++)
			{
				if (!InsideMap(lpMapDocData, i, j))
					continue;
		        if ( (lpMapDocData->MapStruct.data[i][j].cdata >= '0') && (lpMapDocData->MapStruct.data[i][j].cdata <= '9') )
			   base = TRUE;
			
		        if ( (ClipBoard[i][j] >= '0') && (ClipBoard[i][j] <= '9') )
			   base = TRUE;

			switch (TransPaste)
			 {
			  case FALSE :  ChangeMapData(lpMapDocData,x,y,ClipBoard[i][j],1);
					break;
			  case TRUE  :	if (ClipBoard[i][j] == MAP_SPACE)
					  break;
					else
					  {
					    ChangeMapData(lpMapDocData,x,y,ClipBoard[i][j],1);
					    break;
					  }
			 }
			}
		lpMapDocData->seldxbeg = xbeg;
		lpMapDocData->seldxend = xbeg+cWidth;
		lpMapDocData->seldybeg = ybeg;
		lpMapDocData->seldyend = ybeg+cHeight;

	if (base)
		CountBases(lpMapDocData);
}
