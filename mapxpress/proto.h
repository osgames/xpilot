/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "xprTypes.h"

/*prototypes for mapxpress.c*/
LRESULT CALLBACK FrameWndProc (HWND, UINT, WPARAM, LPARAM) ;
void DoCaption (LPMAPDOCDATA, HWND, char *);
short AskAboutSave (HWND, char *);
BOOL CALLBACK CloseEnumProc (HWND, LPARAM);
void DrawBoxOutline (HWND, POINT, POINT);
void DrawMapOutline (HWND, POINT, POINT, COLORREF);
void DrawHighlightLine (HWND, POINT, POINT);
void DrawHighlightCircle (HWND, POINT, POINT);
LRESULT CALLBACK MapWndProc (HWND, UINT, WPARAM, LPARAM) ;
BOOL CALLBACK DirDlgProc (HWND, UINT, WPARAM, LPARAM) ;
void Setup_default_server_options(LPMAPDOCDATA);
void EnableClipboard(HWND, HMENU, BOOL);
void EnableUndo(HWND, HMENU, BOOL);
void Process_Registry(HINSTANCE, HWND, HMENU);
void OkMessage (HWND, char *, char *);
void ErrorHandler(LPSTR, ... );
void UpdateStatusBarSettings(LPMAPDOCDATA);
HWND GetCurrentMapWindow(HWND);
HWND AddMapWindow(HWND);

/*prototypes for expose.c*/
void DrawSmallBlock (HDC, LPMAPDOCDATA, int, int, HBRUSH);
void DrawMapSection(HDC, LPMAPDOCDATA, int, int, int, int, int, int);
void DrawMapPic(HDC, LPMAPDOCDATA, int, int, int, char, int, int, int);
u_short Find_closest_team(LPMAPDOCDATA, int, int);
double Wrap_length(LPMAPDOCDATA, double, double);
HFONT GetZoomFont(HDC, LPMAPDOCDATA);
void DeleteZoomFont(HDC, HFONT);

/*Functions in Toolbar.C*/
HWND InitFileToolBar (HWND);
HWND InitMapSymsToolBar (HWND);
HWND InitToolsToolBar (HWND);
HWND InitShapeToolBar (HWND);
void CopyToolTipText (LPTOOLTIPTEXT);

/*Functions in prefs.c*/
BOOL CreatePropertySheet (HWND);
int CALLBACK PropSheetProc (HWND, UINT, LPARAM);
BOOL CALLBACK PrefsDefaultDlgProc (HWND, UINT, WPARAM, LPARAM);
UINT CALLBACK PrefsPageProc (HWND, UINT, LPPROPSHEETPAGE);
BOOL CALLBACK PrefsCommentsDlgProc (HWND, UINT, WPARAM, LPARAM);

/*Functions in Helper.C*/
void SetSimpleCheck (HWND, int, int);
void SetEditText (HWND, int, LPCSTR);
void UpdateMapPrefText (HWND, int, LPTSTR);
int UpdateMapPrefCheck (HWND, int);
void InitListBox (HWND, int, LPCSTR );
void UpdateMapPrefList (HWND, int, LPTSTR);
void ToggleMenuItem(HMENU, int, BOOL);

/*Functions in tools.c*/
void DrawBlockLine(LPMAPDOCDATA, HWND, POINT, POINT, int);
void DrawBlockCircle(LPMAPDOCDATA, HWND, POINT, POINT, int);
void DrawBlockRectangle(LPMAPDOCDATA, HWND, POINT, POINT, int);
void FillArea(LPMAPDOCDATA, HWND, int, int, int);
void SelectGrid(LPMAPDOCDATA, HWND, int, int, int, BOOL);
void ChangeMapData(LPMAPDOCDATA, int, int, char, int);
void IncrementMapBlock(LPMAPDOCDATA, HWND, int, int, int, int);
void RotateMap(LPMAPDOCDATA, BOOL);
void MirrorMap(LPMAPDOCDATA, BOOL, BOOL);
void CycleMapHorizontal(LPMAPDOCDATA, BOOL, BOOL);
void CycleMapVertical(LPMAPDOCDATA, BOOL, BOOL);
void NegativeMapArea(LPMAPDOCDATA, BOOL);
int SaveUndoIcon(LPMAPDOCDATA,int, int, char, BOOL);
int Undo(LPMAPDOCDATA);
void ClearUndo(LPMAPDOCDATA);
void VoidUndo(LPMAPDOCDATA);
void SwapMapSize(LPMAPDOCDATA);
void SortSelectionArea(LPMAPDOCDATA);
void EmptyTempData();
void dragArea(LPMAPDOCDATA, int, int, BOOL);
void ClearMap(LPMAPDOCDATA, BOOL, BOOL);
char MapData(LPMAPDOCDATA, int, int);
BOOL InsideMap(LPMAPDOCDATA, int, int);

/*Functions in round.c*/
int RoundMapArea(LPMAPDOCDATA, BOOL);

/*Functions in clipboard.c*/
void EmptyClipData();
void FillClipData(LPMAPDOCDATA, BOOL);
void PasteData(LPMAPDOCDATA, int, int);

/*Functions in wildmap.c*/
//int wildmap(PSTR);
int wildmap(LPMAPDOCDATA);
BOOL CALLBACK WildDlgProc (HWND, UINT, WPARAM, LPARAM) ;

/*Functions in FILE.C*/
void	XpFileInitialize (HWND) ;
BOOL	XpFileOpenDlg    (HWND, PSTR, PSTR) ;
BOOL	XpFileSaveDlg    (HWND, PSTR, PSTR) ;
BOOL	XpTextSaveDlg    (HWND, PSTR, PSTR) ;
BOOL	XpOnlySaveDlg    (HWND, PSTR, PSTR) ;
int	SaveMap(LPMAPDOCDATA, PSTR, BOOL, BOOL);
int	LoadMap(LPMAPDOCDATA, PSTR, BOOL);
int	LoadOldMap(LPMAPDOCDATA, char *);
void	toeol(FILE *);
char	skipspace(FILE *);
char	*getMultilineValue(char *, FILE *);
int	ParseLine(LPMAPDOCDATA, FILE *, BOOL);
int	AddOption(LPMAPDOCDATA, char *, char *, BOOL, BOOL);
int	YesNo(char *);
char	*StrToNum(char *, int, int);
int	LoadMapData(LPMAPDOCDATA, char *);
char	*getMultilineValue();
int NewMap(LPMAPDOCDATA);
void	CountBases(LPMAPDOCDATA);
void CountCheckPoints(LPMAPDOCDATA);
char FindNextCheckPoint(LPMAPDOCDATA);
int FindOption(LPMAPDOCDATA, char *);


/*Functions in imexport.c*/
int LoadXbmFile(LPMAPDOCDATA, char *);
int LoadPbmFile(LPMAPDOCDATA, char *);
int SaveXbmFile(LPMAPDOCDATA, PSTR);
int	SaveXpmFile(LPMAPDOCDATA, PSTR);
int SavePbmPlusFile(LPMAPDOCDATA, PSTR, int);
static void build_image(LPMAPDOCDATA, char *);
static char *convert_map(LPMAPDOCDATA);


/*Functions in grow.c*/
int GrowMapArea(LPMAPDOCDATA, BOOL);

/*Functions in bitmap.c*/
int SaveBmpFile(HWND, PSTR);
PBITMAPINFO CreateBitmapInfoStruct(HWND, HBITMAP);
void CreateBMPFile(HWND, LPTSTR, PBITMAPINFO, HBITMAP, HDC);

/*Functions in about.c*/ 
BOOL CALLBACK AboutDlgProc (HWND, UINT, WPARAM, LPARAM) ;

/*Functions in errors.c*/
int CheckMap(LPMAPDOCDATA);
BOOL CALLBACK MapErrorsDlgProc (HWND, UINT, WPARAM, LPARAM) ;

/*Functions in minimap.c*/
LRESULT CALLBACK MiniMapWndProc (HWND, UINT, WPARAM, LPARAM);
void DrawSmallMap(LPMAPDOCDATA);
int ShowMiniMap(void);
void SizeSmallMap(LPMAPDOCDATA);
void UpdateSmallMap(LPMAPDOCDATA, int,int);
BOOL CreateMiniMap (LPMAPDOCDATA, HWND);
