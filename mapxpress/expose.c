/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"

/*The minimum size to draw detailed blocks, any zoom smaller
than this is drawn as a simple rect.*/
#define MIN_NORMAL_VIEW 4

segment_t         mapicon_seg[35] = {
	{MAP_FILLED,  5, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01}, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.99} }, /*  0:x MAP_WALL */
	{MAP_REC_RD, 4, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.99,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00} }, /*  1:q */
	{MAP_REC_LD, 4, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00} }, /*  2:w */
	{MAP_REC_RU, 4, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00} }, /*  3:a */
	{MAP_REC_LU, 4, {(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00} }, /*  4:s */
	
	{MAP_FUEL, 5, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01}, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.99} }, /*  5:# MAP_FUEL */
	
	{MAP_CAN_LEFT, 4, {(float) 0.70,(float) 0.99,(float) 0.99,(float) 0.70,(float) 0.00}, {(float) 0.50,(float) 0.99,(float) 0.01,(float) 0.50,(float) 0.00} }, /*  6:d MAP_CANNON*/
	{MAP_CAN_UP,  4, {(float) 0.01,(float) 0.99,(float) 0.50,(float) 0.01,(float) 0.00}, {(float) 0.99,(float) 0.99,(float) 0.70,(float) 0.99,(float) 0.00} }, /*  7:r MAP_CANNON*/
	{MAP_CAN_DOWN, 4, {(float) 0.50,(float) 0.01,(float) 0.99,(float) 0.50,(float) 0.00}, {(float) 0.30,(float) 0.01,(float) 0.01,(float) 0.30,(float) 0.00} }, /*  8:c MAP_CANNON*/
	{MAP_CAN_RIGHT, 4, {(float) 0.30,(float) 0.01,(float) 0.01,(float) 0.30,(float) 0.00}, {(float) 0.50,(float) 0.99,(float) 0.01,(float) 0.50,(float) 0.00} }, /*  9:f MAP_CANNON*/
	{MAP_BASE, 2, {(float) 0.01,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.99,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 10:_ MAP_BASE (UP)*/ 
	
	
	{MAP_GRAV_POS, 5, {(float) 0.50,(float) 0.50,(float) 0.50,(float) 0.30,(float) 0.70}, {(float) 0.30,(float) 0.70,(float) 0.50,(float) 0.50,(float) 0.50} }, /* 11:+ MAP_GRAV_POS*/
	{MAP_GRAV_NEG, 2, {(float) 0.30,(float) 0.70,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.50,(float) 0.50,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 12:- MAP_GRAV_NET*/
	{MAP_WORM_NORMAL, 0, {(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 13:@ MAP_WORM_NORMAL */
	{MAP_WORM_OUT, 5, {(float) 0.10,(float) 0.75,(float) 0.30,(float) 0.75,(float) 0.75}, {(float) 0.10,(float) 0.75,(float) 0.75,(float) 0.75,(float) 0.30} }, /* 14:) MAP_WORM_OUT */
	{MAP_WORM_IN, 5, {(float) 0.75,(float) 0.10,(float) 0.55,(float) 0.10,(float) 0.10}, {(float) 0.75,(float) 0.10,(float) 0.10,(float) 0.10,(float) 0.55} }, /* 15:( MAP_WORM_IN */
	{MAP_TREASURE, 4, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.00}, {(float) 0.49,(float) 0.99,(float) 0.99,(float) 0.49,(float) 0.00} }, /* 16:* MAP_TREASURE*/
	{MAP_GRAV_ACWISE, 3, {(float) 0.60,(float) 0.50,(float) 0.60,(float) 0.00,(float) 0.00}, {(float) 0.05,(float) 0.15,(float) 0.30,(float) 0.00,(float) 0.00} }, /* 17:< COUNTER CLOCKWISE GRAVITY*/
	{MAP_GRAV_CWISE, 3, {(float) 0.40,(float) 0.50,(float) 0.40,(float) 0.00,(float) 0.00}, {(float) 0.05,(float) 0.15,(float) 0.30,(float) 0.00,(float) 0.00} }, /* 18:> CLOCKWISE GRAVITY*/
	{MAP_TARGET, 5, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01}, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.99} }, /* 19:! MAP_TARGET */
	
	{MAP_SPACE, 0, {(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 20:  MAP_SPACE */
	
	{MAP_DEC_FLD, 5, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01}, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.99} }, /* 21:b MAP_DECORATION */
	{MAP_DEC_RD, 4, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.99,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00} }, /* 22:t */
	{MAP_DEC_LD, 4, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00} }, /* 23:y */
	{MAP_DEC_RU, 4, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00} }, /* 24:g */
	{MAP_DEC_LU, 4, {(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.00}, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.01,(float) 0.00} }, /* 25:h */
	
	{MAP_BASE_ORNT, 4, {(float) 0.25,(float) 0.75,(float) 0.50,(float) 0.25,(float) 0.00}, {(float) 0.15,(float) 0.15,(float) 0.80,(float) 0.15,(float) 0.00} }, /* 26:$ BASE_ORIENT*/
	{MAP_ITEM_CONC, 4, {(float) 0.50,(float) 0.01,(float) 0.99,(float) 0.50,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.00} }, /* 27:% ITEM CONCENTRATOR*/
	{MAP_CRNT_UP, 5, {(float) 0.50,(float) 0.50,(float) 0.75,(float) 0.50,(float) 0.25}, {(float) 0.99,(float) 0.01,(float) 0.50,(float) 0.01,(float) 0.50} }, /* 28:i CURRENT UP*/
	{MAP_CRNT_LT, 5, {(float) 0.99,(float) 0.01,(float) 0.50,(float) 0.01,(float) 0.50}, {(float) 0.50,(float) 0.50,(float) 0.75,(float) 0.50,(float) 0.25} }, /* 29:j CURRENT LEFT*/
	{MAP_CRNT_RT, 5, {(float) 0.01,(float) 0.99,(float) 0.50,(float) 0.99,(float) 0.50}, {(float) 0.50,(float) 0.50,(float) 0.75,(float) 0.50,(float) 0.25} }, /* 30:k CURRENT RIGHT*/
	{MAP_CRNT_DN, 5, {(float) 0.50,(float) 0.50,(float) 0.75,(float) 0.50,(float) 0.25}, {(float) 0.01,(float) 0.99,(float) 0.50,(float) 0.99,(float) 0.50} }, /* 31:m CURRENT DOWN*/
	
	{MAP_EMPTYTREASURE, 4, {(float) 0.01,(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.00}, {(float) 0.49,(float) 0.99,(float) 0.99,(float) 0.49,(float) 0.00} }, /* 32:^ MAP_EMPTYTREASURE*/
	{MAP_FRICTION, 5, {(float) 0.01,(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01}, {(float) 0.99,(float) 0.99,(float) 0.01,(float) 0.01,(float) 0.99} }, /* 33:z MAP_FRICTION */
	{MAP_ASTEROID_CONC, 5, {(float) 0.10,(float) 0.90,(float) 0.90,(float) 0.10,(float) 0.10}, {(float) 0.90,(float) 0.90,(float) 0.10,(float) 0.10,(float) 0.90} }, /* 34:& MAP_ASTEROID_CONC */
};
#define NUMDETS 9
segment_t         mapicondet_seg[NUMDETS] = {
	{MAP_TARGET, 5, {(float) 0.25,(float) 0.75,(float) 0.75,(float) 0.25,(float) 0.25}, {(float) 0.75,(float) 0.75,(float) 0.25,(float) 0.25,(float) 0.75} }, /* 0:! TARGET (DETAIL)*/
	{MAP_ITEM_CONC, 4, {(float) 0.55,(float) 0.25,(float) 0.90,(float) 0.55,(float) 0.00}, {(float) 0.15,(float) 0.85,(float) 0.75,(float) 0.15,(float) 0.00} }, /* 1:% ITEM CONCENTRATOR (DETAIL)*/
	{MAP_ITEM_CONC, 4, {(float) 0.65,(float) 0.45,(float) 0.80,(float) 0.65,(float) 0.00}, {(float) 0.30,(float) 0.65,(float) 0.55,(float) 0.30,(float) 0.00} }, /* 2:% ITEM CONCENTRATOR (DETAIL)*/
	{MAP_BASE, 2, {(float) 0.01,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.01,(float) 0.01,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 3:_ MAP_BASE_TEAM (DOWN)*/ 
	{MAP_BASE, 2, {(float) 0.01,(float) 0.01,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 4:_ MAP_BASE_TEAM (RIGHT)*/ 
	{MAP_BASE, 2, {(float) 0.99,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00}, {(float) 0.01,(float) 0.99,(float) 0.00,(float) 0.00,(float) 0.00} }, /* 5:_ MAP_BASE_TEAM (LEFT)*/ 
	{MAP_BASE_ORNT, 5, {(float) 0.50,(float) 0.50,(float) 0.50,(float) 0.40,(float) 0.60}, {(float) 0.30,(float) 0.50,(float) 0.40,(float) 0.40,(float) 0.40} }, /* 6:$ BASE_ORIENT (DETAIL)*/
	{MAP_ASTEROID_CONC, 5, {(float) 0.25,(float) 0.75,(float) 0.75,(float) 0.25,(float) 0.25}, {(float) 0.75,(float) 0.75,(float) 0.25,(float) 0.25,(float) 0.75} }, /* 34:& MAP_ASTEROID_CONC */
	{MAP_ASTEROID_CONC, 5, {(float) 0.40,(float) 0.60,(float) 0.605,(float) 0.40,(float) 0.40}, {(float) 0.60,(float) 0.60,(float) 0.40,(float) 0.40,(float) 0.60} }, /* 34:& MAP_ASTEROID_CONC */
};

/* ascii char -32 */
int mapicon_ptr[91] = {	20, 19,  0,  5, 26,
						27, 34,  0, 15, 14,
						16, 11,  0, 12,  0,
						 0, 51, 51, 51, 51,
						51, 51, 51, 51, 51,
						51,  0,  0, 17,  0,
						18,  0, 13, 50, 50,
						50, 50, 50, 50, 50,
						50, 50, 50, 50, 50,
						50, 50, 50, 50, 50,
						50, 50, 50, 50, 50,
						50, 50, 50, 50,  0,
						 0,  0, 32, 10,  0,
						 3, 21,  8,  6,  0,
						 9, 24, 25, 28, 29,
						30,  0, 31,  0,  0,
						 0,  1,  7,  4, 22,
						 0,  0,  2,  0, 23,
						33};
/***************************************************************************/
/* DrawSmallBlock                                                          */
/* Arguments :                                                             */
/*   hdc                                                                   */
/*   lpMapDocData: pointer to map document.                                */
/*   x:xcoord to start at.                                                 */
/*   y:ycoord to start at.                                                 */
/*   hNewBrush: brush draw block in.                                       */
/*                                                                         */
/*                                                                         */
/* Purpose : Draw a simplified block as a filled in rectangle using a brush*/
/***************************************************************************/
void DrawSmallBlock (HDC hdc, LPMAPDOCDATA lpMapDocData, int x, int y, HBRUSH hNewBrush)
{
	RECT rect;

	rect.left = x;
	rect.top = y;
	rect.right = x + lpMapDocData->MapStruct.view_zoom;
	rect.bottom = y + lpMapDocData->MapStruct.view_zoom;

	FillRect(hdc, &rect, hNewBrush);

}

/***************************************************************************/
/* DrawMapSection                                                          */
/* Arguments :                                                             */
/*   hdc: handle to device context.                                        */
/*   lpMapDocData: pointer to map document.                                */
/*   x : the xcoord in map coordinates to start at.                        */
/*   y : the ycoord in map coordinates to start at.                        */
/*   width : the width of the section to draw.                             */
/*   height : the height of the section to draw.                           */
/*   xpos : x coord in pixels to start at.                                 */
/*   ypos : y coord in pixels to start at.                                 */
/* Purpose : Draw a section of the map, one block at a time.               */
/***************************************************************************/
void DrawMapSection(HDC hdc, LPMAPDOCDATA lpMapDocData, int x, int y,
					int width, int height, int xcrd, int ycrd)
{
	int                   i,j,w,h,data;
	int xpos, ypos;
	HFONT storeFont;
	
	w = width+x;
	h = height+y;
	
	/*Set the font so that later, when we draw checkpoints and bases, the text
	will look decent.*/
	storeFont = GetZoomFont(hdc, lpMapDocData);
	
	for (i=x, xpos = xcrd;i<=w;i++, xpos+=lpMapDocData->MapStruct.view_zoom)
		for (j=y, ypos = ycrd;j<=h;j++, ypos+=lpMapDocData->MapStruct.view_zoom)
			if ( (i<lpMapDocData->MapStruct.width) && (j<lpMapDocData->MapStruct.height) )
			{
				data = lpMapDocData->MapStruct.data[i][j].cdata;
				DrawMapPic(hdc,lpMapDocData, xpos,ypos, mapicon_ptr[data-32], (char) data,lpMapDocData->MapStruct.view_zoom, i, j);
			}
			
	DeleteZoomFont(hdc, storeFont);
}
/***************************************************************************/
/* DrawMapPic                                                              */
/* Arguments :                                                             */
/*   hdc: handle to device context                                         */
/*   lpMapDocData: pointer to map document.                                */
/*   x: xcoord to draw at in pixels.                                       */
/*   y: ycoord to draw at in pixels.                                       */
/*   picnum: which block to draw.                                          */
/*   data: optional character to draw in for text.                         */
/*   zoom: zoom level(or width in pixels of blocks.                        */
/*   mapx: x location in map coordinates.                                  */
/*   mapy: y location in map coordinates.                                  */
/* Purpose :  draw the specified block at the place & size given.          */
/***************************************************************************/
void DrawMapPic(HDC hdc, LPMAPDOCDATA lpMapDocData, int x, int y, int picnum,
				char data, int zoom, int mapx, int mapy)
{
	HBRUSH hBrush;
	RECT rect;
	HFONT smFont;
	HFONT holderFont;
	POINT	points[5]; /*Win point structure, accepts only (long) dims*/
	int i,j,arc,xo=0,yo=0;
	char strng[2];
	char tteam[2];
	int t;
	
	
	/*Find the team base closest the current grid space, if we need to.
	We only need to if teamplay is on; if this is a fuel or cannon, we only need to if
	the teamFuel or teamCannon options are on.*/
	if (lpMapDocData->MapStruct.teamPlay &&
		((picnum == IDM_MAP_FUEL && lpMapDocData->MapStruct.teamFuel) || 
		(lpMapDocData->MapStruct.teamCannons && (picnum == IDM_MAP_CAN_LEFT || picnum == IDM_MAP_CAN_UP || picnum == IDM_MAP_CAN_DOWN || picnum == IDM_MAP_CAN_RIGHT)) ||
		picnum == IDM_MAP_BASE || picnum == IDM_MAP_TREASURE || picnum == IDM_MAP_TARGET || picnum == IDM_MAP_EMPTYTREASURE) )
	{
		t=Find_closest_team(lpMapDocData, mapx, mapy);
        itoa(t, tteam, 10);
		if (data == MAP_BASE)
		{
			data = '0';
			picnum = IDM_MAP_TEAMBASE;
		}
	}
	
	if (picnum == IDM_MAP_TEAMBASE || picnum == IDM_MAP_CHECKPOINT)
	{/* TEAMBASES CHECKPOINTS OUTPUT TEXT*/
		strng[0] = data;
		strng[1] = '\0';
		if (lpMapDocData->MapStruct.view_zoom >= 5)
		{
			TextOut( hdc, x+(int)(.3*lpMapDocData->MapStruct.view_zoom), y, strng, strlen(strng));
		}
	}

	if (picnum == IDM_MAP_CHECKPOINT)
	{/* CHECKPOINTS ARE DONE*/
		return;
	}
	if (picnum == IDM_MAP_TEAMBASE)
	{/* TEAMBASES SHOULD GO ON AS NORMAL BASES*/
		picnum = IDM_MAP_BASE;
	}
	
	/*----------------FILL THE POINTS ARRAY WITH THE SIZES FROM THE ICON SEGMENT-------*/
    for (i=0;i<mapicon_seg[picnum].num_points; i++)
	{
		points[i].x = (long) (mapicon_seg[picnum].x[i] * zoom + x + xo);
		points[i].y = (long) (mapicon_seg[picnum].y[i] * zoom + y + yo);
	}
	
	/*------------------------------------------------------------*/
	if ((picnum >= IDM_MAP_FILLED) && (picnum <= IDM_MAP_REC_LU))
	{/* Walls */
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenSolidWall);
			hBrush = (HBRUSH) SelectObject(hdc, hBrushSolidWall);
			if (FilledWorld)
				Polygon (hdc, points, mapicon_seg[picnum].num_points);
			else
				Polyline(hdc, points, mapicon_seg[picnum].num_points);
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			SelectObject(hdc, hBrush);
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushSolidWall);
		
		return;
	}
	else if (picnum == IDM_MAP_FUEL)
	{/* Fuels */
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenSolidWall);
			
			smFont = CreateFont((int)(lpMapDocData->MapStruct.view_zoom*.5), (int)(lpMapDocData->MapStruct.view_zoom*.3), 0, 0, 600, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, NULL);
			holderFont = (HFONT) SelectObject(hdc, smFont);	
			
			
			rect.left = x+1;
			rect.top = (y+1);
			rect.right = (x+1)+(zoom-3);
			rect.bottom = (y+1)+(zoom-2);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			FillRect(hdc, &rect, hBrushFuel);
			if ((lpMapDocData->MapStruct.teamPlay) && (lpMapDocData->MapStruct.teamFuel))
			{
				TextOut( hdc, x+(int)(.33*zoom), y+(int)(.26*zoom), tteam, strlen(tteam));
			}
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			DeleteObject (SelectObject(hdc, holderFont));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushFuel);
		
		return;
	}
	else if ((picnum >= IDM_MAP_CAN_LEFT) && (picnum <= IDM_MAP_CAN_RIGHT))
	{/* Cannons */
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenCannon);
			
			hBrush = (HBRUSH) SelectObject(hdc, hBrushCannon);
			
			smFont = CreateFont((int)(lpMapDocData->MapStruct.view_zoom*.5), (int)(lpMapDocData->MapStruct.view_zoom*.3), 0, 0, 600, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, NULL);
			holderFont = (HFONT) SelectObject(hdc, smFont);	
			
			if (FilledWorld)
				Polygon (hdc, points, mapicon_seg[picnum].num_points);
			else
				Polyline(hdc, points, mapicon_seg[picnum].num_points);
			if ((lpMapDocData->MapStruct.teamPlay) && (lpMapDocData->MapStruct.teamCannons))
			{
				switch (picnum)
				{
				case 6:
					TextOut( hdc, x+(int)(.25*zoom), y+(int)(.32*zoom), tteam, strlen(tteam));
					break;
				case 7:
					TextOut( hdc, x+(int)(.4*zoom), y+(int)(.28*zoom), tteam, strlen(tteam));
					break;
				case 8:
					TextOut( hdc, x+(int)(.4*zoom), y+(int)(.32*zoom), tteam, strlen(tteam));
					break;
				case 9:
					TextOut( hdc, x+(int)(.4*zoom), y+(int)(.32*zoom), tteam, strlen(tteam));
					break;
				}
			}
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			DeleteObject (SelectObject(hdc, holderFont));
			SelectObject (hdc, hBrush);
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushCannon);
		
		return;
	}
	else if (picnum == IDM_MAP_BASE)
	{/* Bases */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			int numpoints = mapicon_seg[picnum].num_points;
			SelectObject(hdc, hPenBase);		
			/*Orient Upwards - make sure we don't flip some other direction even though
			we should be forced upwards*/
			if (mapy > 0 && lpMapDocData->MapStruct.data[mapx][mapy-1].cdata == MAP_BASE_ORNT)
			{
				;
			}
			else if (mapy == 0 && lpMapDocData->MapStruct.data[mapx][lpMapDocData->MapStruct.height-1].cdata == MAP_BASE_ORNT && lpMapDocData->MapStruct.edgeWrap) /*wrap*/
			{
				;
			}
			/*Orient Downwards*/
			else if (mapy < lpMapDocData->MapStruct.height - 1 && lpMapDocData->MapStruct.data[mapx][mapy+1].cdata == MAP_BASE_ORNT)
			{
				picnum=3;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			else if (mapy == lpMapDocData->MapStruct.height-1 && lpMapDocData->MapStruct.data[mapx][0].cdata == MAP_BASE_ORNT && lpMapDocData->MapStruct.edgeWrap) /*wrap*/
			{
				picnum=3;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			/*Orient Rightwards*/
			else if (mapx < lpMapDocData->MapStruct.width - 1 && lpMapDocData->MapStruct.data[mapx+1][mapy].cdata == MAP_BASE_ORNT)
			{
				picnum=4;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			else if (mapx == lpMapDocData->MapStruct.width - 1 && lpMapDocData->MapStruct.data[0][mapy].cdata == MAP_BASE_ORNT && lpMapDocData->MapStruct.edgeWrap) /*wrap*/
			{
				picnum=4;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			/*Orient Leftwards*/
			else if (mapx > 0 && lpMapDocData->MapStruct.data[mapx-1][mapy].cdata == MAP_BASE_ORNT)
			{
				picnum=5;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			else if (mapx == 0 && lpMapDocData->MapStruct.data[lpMapDocData->MapStruct.width-1][mapy].cdata == MAP_BASE_ORNT && lpMapDocData->MapStruct.edgeWrap) /*wrap*/
			{
				picnum=5;
				for (i=0;i<mapicondet_seg[picnum].num_points; i++)
				{
					points[i].x = (long) (mapicondet_seg[picnum].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[picnum].y[i] * zoom + y + yo);
				}
			}
			Polyline(hdc, points, numpoints);
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushBase);
		
		return;
	}
	else if ((picnum >=IDM_MAP_GRAV_POS) && (picnum<=IDM_MAP_GRAV_NEG))
	{/* +/- gravity*/
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{	
			SelectObject(hdc, hPenGravity);
            Polyline(hdc, points, mapicon_seg[picnum].num_points);
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushGravity);
		
		/*Go on and draw the arcs too!*/		
	}
	else if ((picnum >=IDM_MAP_WORM_NORMAL) && (picnum<=IDM_MAP_WORM_IN))
	{/* wormholes */
		
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{	
			SelectObject(hdc, hPenWormhole);
            Polyline(hdc, points, mapicon_seg[picnum].num_points);
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushWormhole);
		/*Go on and draw the arcs too!*/		
	}
	else if ((picnum ==IDM_MAP_TREASURE) || (picnum==IDM_MAP_EMPTYTREASURE))
	{/* Treasure */
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{	
			SelectObject(hdc, hPenTreasure);
			
			smFont = CreateFont((int)(lpMapDocData->MapStruct.view_zoom*.5), (int)(lpMapDocData->MapStruct.view_zoom*.3), 0, 0, 600, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, NULL);
			holderFont = (HFONT) SelectObject(hdc, smFont);	
			
            Polyline(hdc, points, mapicon_seg[picnum].num_points);
			if (lpMapDocData->MapStruct.teamPlay)
			{
				TextOut( hdc, x+(int)(.5*zoom), y+(int)(.1*zoom), tteam, strlen(tteam));
			}
			DeleteObject (SelectObject(hdc, holderFont));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushTreasure);
		
		/*Go on and draw the arcs too!*/		
	}
	else if ((picnum >=IDM_MAP_GRAV_ACWISE) && (picnum<=IDM_MAP_GRAV_CWISE))
	{/* clockwise and anti clockwise gravity */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenGravity);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushGravity);
		
		/*Go on and draw the arcs too!*/		
	}
	else if (picnum ==IDM_MAP_TARGET)
	{/* Target */
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{			
			SelectObject(hdc, hPenTarget);
			
			smFont = CreateFont((int)(lpMapDocData->MapStruct.view_zoom*.5), (int)(lpMapDocData->MapStruct.view_zoom*.3), 0, 0, 600, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, NULL);
			holderFont = (HFONT) SelectObject(hdc, smFont);	
			Polyline(hdc, points, mapicon_seg[picnum].num_points);

			//Draw the details
			for (j = 0; j<NUMDETS; j++)
			{
				if (mapicondet_seg[j].char_val == MAP_TARGET)
				{
					for (i=0;i<mapicondet_seg[j].num_points; i++)
					{
					points[i].x = (long) (mapicondet_seg[j].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[j].y[i] * zoom + y + yo);
					}
					Polyline(hdc, points, mapicondet_seg[j].num_points);
				}
			}
			
			if (lpMapDocData->MapStruct.teamPlay)
			{
				TextOut( hdc, x+(int)(.33*zoom), y+(int)(.26*zoom), tteam, strlen(tteam));
			}
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			DeleteObject (SelectObject(hdc, holderFont));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushTarget);
		
		return;
	}
	else if (picnum ==IDM_MAP_SPACE)
	{/* Space */
		return;
	}
	else if ((picnum >= IDM_MAP_DEC_FLD) && (picnum <= IDM_MAP_DEC_LU) || (picnum == IDM_MAP_FRICTION))
	{/* Decorations */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			if (picnum == IDM_MAP_FRICTION)
			{
				SelectObject(hdc, hPenFriction);
				hBrush = (HBRUSH) SelectObject(hdc, hBrushFriction);
			}
			else
			{
				SelectObject(hdc, hPenDecorWall);
				hBrush = (HBRUSH) SelectObject(hdc, hBrushDecorWall);
			}
			if (FilledWorld)
				Polygon (hdc, points, mapicon_seg[picnum].num_points);
			else
				Polyline(hdc, points, mapicon_seg[picnum].num_points);
			SelectObject(hdc, GetStockObject(BLACK_PEN));
			SelectObject(hdc, hBrush);
		}
		else
			if (picnum == IDM_MAP_FRICTION)
			{
				DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushFriction);
			}
			else
				DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushDecorWall);
		
		return;
	}
	else if (picnum ==IDM_MAP_BASE_ORNT)
	{/* Base Orient */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenBase);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			//Draw the details
			for (j = 0; j<NUMDETS; j++)
			{
				if (mapicondet_seg[j].char_val == MAP_BASE_ORNT)
				{
					for (i=0;i<mapicondet_seg[j].num_points; i++)
					{
					points[i].x = (long) (mapicondet_seg[j].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[j].y[i] * zoom + y + yo);
					}
					Polyline(hdc, points, mapicondet_seg[j].num_points);
				}
			}
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		
		return;		
	}
	else if (picnum ==IDM_MAP_ITEM_CONC)
	{/* Item Concentrator */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{	
			SelectObject(hdc, hPenItemConc);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			//Draw the details
			for (j = 0; j<NUMDETS; j++)
			{
				if (mapicondet_seg[j].char_val == MAP_ITEM_CONC)
				{
					for (i=0;i<mapicondet_seg[j].num_points; i++)
					{
					points[i].x = (long) (mapicondet_seg[j].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[j].y[i] * zoom + y + yo);
					}
					Polyline(hdc, points, mapicondet_seg[j].num_points);
				}
			}
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushItemConc);
		
		return;
	}
	else if ((picnum >=IDM_MAP_CRNT_UP) && (picnum <= IDM_MAP_CRNT_DN))
	{/* Gravity Currents */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{
			SelectObject(hdc, hPenCurrent);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushCurrent);
		
		return;
	}
	else if (picnum ==IDM_MAP_ASTEROID_CONC)
	{/* Asteroid Concentrator */
		
		if (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW)
		{	
			SelectObject(hdc, hPenItemConc);
			Polyline(hdc, points, mapicon_seg[picnum].num_points);
			//Draw the details	
			
			for (j = 0; j<NUMDETS; j++)
			{
				if (mapicondet_seg[j].char_val == MAP_ASTEROID_CONC)
				{
					for (i=0;i<mapicondet_seg[j].num_points; i++)
					{
					points[i].x = (long) (mapicondet_seg[j].x[i] * zoom + x + xo);
					points[i].y = (long) (mapicondet_seg[j].y[i] * zoom + y + yo);
					}
					Polyline(hdc, points, mapicondet_seg[j].num_points);
				}
			}
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		else
			DrawSmallBlock(hdc, lpMapDocData, x, y, hBrushItemConc);
		
		return;
	}	
	/*---------------------------------------------------------------*/
	arc = (int)(.7*zoom);
	if ( ((zoom-arc)/2)*2 != (zoom-arc) ) 
		arc--;
	if ((picnum >= IDM_MAP_GRAV_POS) && (picnum <= IDM_MAP_GRAV_NEG) && (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW))
	{/* +/- Gravity */
		Arc(hdc, (int)(x+(zoom-arc)/2),(int)(y+(zoom-arc)/2),x+(int)(zoom-(zoom-arc)/2),
			y+(int)(zoom-(zoom-arc)/2),x+(zoom/2),y+(zoom/2),x+(zoom/2),y+(zoom/2));
		SelectObject(hdc, GetStockObject(BLACK_PEN));
		return;
	}
	else if ((picnum >= IDM_MAP_WORM_NORMAL) && (picnum <= IDM_MAP_WORM_IN) && (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW))
	{ /* Wormhole */
		Arc(hdc, (int)(x+(zoom-arc)/2),(int)(y+(zoom-arc)/2),x+(int)(zoom-(zoom-arc)/2),
			y+(int)(zoom-(zoom-arc)/2),x+(zoom/2),y+(zoom/2),x+(zoom/2),y+(zoom/2));
		Arc(hdc, (int)(x+.15*zoom),(int)(y+.15*zoom),(int)(x+.15*zoom)+(int)(.4*zoom),
			(int)(y+.15*zoom)+(int)(.4*zoom),(int)(x+.15*zoom),(int)(y+.15*zoom),(int)(x+.15*zoom),(int)(y+.15*zoom));
		Arc(hdc, (int)(x+.16*zoom),(int)(y+.16*zoom),(int)(x+.16*zoom)+(int)(.55*zoom),
			(int)(y+.16*zoom)+(int)(.55*zoom),(int)(x+.15*zoom),(int)(y+.15*zoom),(int)(x+.15*zoom),(int)(y+.15*zoom));
		SelectObject(hdc, GetStockObject(BLACK_PEN));
		return;
	}
	else if (((picnum == IDM_MAP_TREASURE) || (picnum==IDM_MAP_EMPTYTREASURE)) && (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW))
	{/* Treasure */
		
		Arc(hdc, x,y,x+zoom,y+zoom,x+zoom,y+(zoom/2),x,y+(zoom/2));

		if (picnum != IDM_MAP_EMPTYTREASURE)
		{		
			SelectObject(hdc, hPenBase);
			
			Arc(hdc, (int)(x+(.15*zoom)),(int)(y+(.6*zoom)),x+(int)(.55*zoom),
				y+zoom,x+(zoom/2),y+(zoom/2),x+(zoom/2),y+(zoom/2));
			SelectObject(hdc, GetStockObject(BLACK_PEN));
		}
		return;
	}
	else if (picnum == IDM_MAP_GRAV_ACWISE && (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW))
	{/* Gravity */
		
		Arc(hdc, (int)(x+.15*zoom),(int)(y+.15*zoom),x+zoom-(int)(.15*zoom),y+zoom-(int)(.15*zoom),
			x+(zoom/2)-(int)(.24*zoom),y+(int)(.08*zoom),x+(zoom/2)+(int)(.01*zoom),y+(int)(.08*zoom));
		SelectObject(hdc, GetStockObject(BLACK_PEN));
		return;
	}
	else if (picnum == IDM_MAP_GRAV_CWISE && (lpMapDocData->MapStruct.view_zoom > MIN_NORMAL_VIEW))
	{/* Gravity */
		
		Arc(hdc, (int)(x+.15*zoom),(int)(y+.15*zoom),x+zoom-(int)(.15*zoom),y+zoom-(int)(.15*zoom),
			x+(zoom/2)-(int)(.01*zoom),y+(int)(.08*zoom),x+(zoom/2)+(int)(.24*zoom),y+(int)(.08*zoom));
		
		SelectObject(hdc, GetStockObject(BLACK_PEN));
		return;
	} 
}
/***************************************************************************/
/* Find_closest_team                                                       */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to the map document.                            */
/*   posx: x coord to start at.                                            */
/*   posy: y coord to start at.                                            */
/*                                                                         */
/* Purpose : Find the closest team base from the start location.           */
/***************************************************************************/
u_short Find_closest_team(LPMAPDOCDATA lpMapDocData, int posx, int posy)
{

	u_short team = 0; /*If no team bases are found..targets, cannons, treasures etc..
	will belong to team "0" */
	int i;
	double closest = FLT_MAX, l;

	for (i=0; i<lpMapDocData->MapItems.NumBases; i++) 
	{
		l = Wrap_length(lpMapDocData, (posx - lpMapDocData->MapItems.base[i].pos.x),(posy - lpMapDocData->MapItems.base[i].pos.y));
		
		if (l < closest)
		{
			team = lpMapDocData->MapItems.base[i].team;
			closest = l;
		}
	}
	return team;
}
/***************************************************************************/
/* Wrap_length	                                                           */
/* Arguments :                                                             */
/*      lpMapDocData: pointer to map document.                             */
/*		dx: x coord to start at.                                           */
/*		dy: y coord to start at.                                           */
/*                                                                         */
/* Purpose : find the shortest distance to a point.                        */
/***************************************************************************/
double Wrap_length(LPMAPDOCDATA lpMapDocData, double dx, double dy)
{
	dx = WRAP_DX(dx);
	dy = WRAP_DY(dy);
	return LENGTH(dx, dy);
}
/***************************************************************************/
/* GetZoomFont                                                             */
/* Arguments :                                                             */
/*   hdc: handle to device context.                                        */
/*   lpMapDocData: pointer to map document.                                */
/* Purpose : Creates a font of the selected size, return the handle to the */
/* previous font.                                                          */
/***************************************************************************/
HFONT GetZoomFont(HDC hdc, LPMAPDOCDATA lpMapDocData)
{
	HFONT blockFont;
	
	
	blockFont = CreateFont(lpMapDocData->MapStruct.view_zoom, (int)(lpMapDocData->MapStruct.view_zoom*.3), 0, 0, 600, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, NULL);//"Arial\0");
	
	
    SetTextColor(hdc, RGB(255,255,255));
	SetBkMode(hdc, TRANSPARENT);
	return((HFONT) SelectObject(hdc, blockFont));	
}
/***************************************************************************/
/* DeleteZoomFont                                                          */
/* Arguments :                                                             */
/*   hdc: handle to device context.                                        */
/*   gotoFont: handle to font.                                             */
/* Purpose : Deletes a font of returning to the selected font. This fuction*/
/* is here to simplify code.                                               */
/***************************************************************************/
void DeleteZoomFont(HDC hdc, HFONT gotoFont)
{

	SetTextColor(hdc, RGB(0,0,0));
	SetBkMode(hdc, OPAQUE);
	DeleteObject(SelectObject(hdc, gotoFont));
}


