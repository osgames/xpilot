/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/
#include "mapxpress.h"

static int               smlmap_width, smlmap_height;
static float             smlmap_xscale = 1;
static float             smlmap_yscale = 1;
static float             smlmap_mainscale = 1;

/***************************************************************************/
/* MiniMapWndProc                                                          */
/* Arguments :                                                             */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/*                                                                         */
/* Purpose :   The procedure for the main window, contains map & toolbars  */
/***************************************************************************/
LRESULT CALLBACK MiniMapWndProc (HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static HWND hwndLocClient;
	LPMAPDOCDATA lpMapDocData;
	PAINTSTRUCT ps ;
	HDC         hdc ;
	HMENU    hSystemMenu;    // the MDI child's system menu
	HWND hwndActive = NULL;
	RECT rect;

	
	switch (iMsg)
	{
	case WM_CREATE :
		hwndLocClient = GetParent(hwnd);
		return 0 ;
	
    case WM_INITMENU:
        // Disable and gray the Maximize, Minimize, and Size items
        // on the MDI child's system menu
        hSystemMenu = GetSystemMenu(hwnd, FALSE);
        EnableMenuItem(hSystemMenu, SC_MAXIMIZE, MF_GRAYED |
             MF_BYCOMMAND);
        EnableMenuItem(hSystemMenu, SC_MINIMIZE, MF_GRAYED |
             MF_BYCOMMAND);
        EnableMenuItem(hSystemMenu, SC_SIZE, MF_GRAYED |
             MF_BYCOMMAND);
        break;

	case WM_COMMAND:
		hwndActive = (HWND) GetWindowLong (hwnd, 0);
		switch (wParam)
		{
		case IDM_PROPERTIES :
		case IDM_ZOOMIN :
		case IDM_ZOOMOUT :
		case IDM_EDIT_UNDO :
		case IDM_ROTATEMAP :
		case IDM_MIRRORMAPV :
		case IDM_MIRRORMAPH :
		case IDM_CYCLEMAPH :
		case IDM_CYCLEMAPV :
		case IDM_CYCLEMAPHNEG :
		case IDM_CYCLEMAPVNEG :
		case IDM_NEGATIVEMAP :
		case IDM_CLEARMAP :
		case IDM_EDIT_CUT :
		case IDM_EDIT_COPY :
		case IDM_EDIT_PASTE :
		case IDM_CROP :
		case IDM_ROUNDMAP :
		case IDM_GROWMAP :
		case IDM_CHECKMAP :
		case IDM_MAPSOURCE :
		case IDM_LAUNCHMAP :
		case IDM_MINIMAP:
			hwndChild = hwndActive;
			SendMessage (hwndActive, WM_COMMAND, LOWORD (wParam), lParam);
		}
		return 0;
	case WM_PAINT :
		hdc = BeginPaint (hwnd, &ps) ;
		hwndActive = (HWND) GetWindowLong (hwnd, 0);

		if (hwndActive)
		{
			if(lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndActive, 0))
			{
				//Draw the selected area of the map
				DrawSmallMap(lpMapDocData);
				
				ptOutbeg.x = 0;
				ptOutbeg.y = 0;
				ptOutend.x = (long) (lpMapDocData->MapStruct.width/smlmap_xscale)+1; 
				ptOutend.y = (long) (lpMapDocData->MapStruct.height/smlmap_yscale)+1;
				if (lpMapDocData->MapStruct.extraBorder)
					DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(0,0,255));
				else
				{
					if (lpMapDocData->MapStruct.edgeWrap)
						DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(0,255,0));
					else
						DrawMapOutline (hwnd, ptOutbeg, ptOutend, RGB(255,0,0));
				}
			}
		}
		else
		{
			GetClientRect(hwnd, &rect);
			FillRect(hdc, &rect, (HBRUSH) GetStockObject (BLACK_BRUSH));
		}

		EndPaint (hwnd, &ps) ;
		return 0 ;
	case WM_SIZE :
		hwndActive = (HWND) GetWindowLong (hwnd, 0);
		if (hwndActive)
		{
			lpMapDocData = (LPMAPDOCDATA) GetWindowLong (hwndActive, 0);
			if (hwndMiniMap && lpMapDocData)
			{
				SizeSmallMap(lpMapDocData);
				InvalidateRect(hwndMiniMap, NULL, TRUE);
			}
		}
		return 0;

	case WM_DESTROY :
		CheckMenuItem (hMenuMap, LOWORD (IDM_MINIMAP), MF_UNCHECKED) ;
		Mini_Map = FALSE;
		hwndMiniMap = NULL;
		return 0;
    }
	return DefMDIChildProc (hwnd, iMsg, wParam, lParam);
}
/*PLEASE NOTE!!! THE FOLLOWING TWO FUNCTIONS COULD BE SIMPLIFIED CODE WISE
BY PLACING THE SWITCH IN A SINGLE SEPARATE FUNCTION. BUT ITS INCREDIBLY FASTER
TO NOT HAVE TO CALL ANOTHER FUNCTION 900x900 TIMES. IF THERES AN EASY ENOUGH METHOD
TO COMBINE THESE TWO ROUTINES, THAT WOULD BE VERY HELPFULL.*/
/***************************************************************************/
/* DrawSmallMap                                                            */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/***************************************************************************/
void DrawSmallMap(LPMAPDOCDATA lpMapDocData)
{
	int                   i,j;
	HDC hdc;
	
	hdc = GetDC (hwndMiniMap) ;
	
	for(j = 0; j < lpMapDocData->MapStruct.height/smlmap_yscale; j++)
		for (i = 0; i < lpMapDocData->MapStruct.width/smlmap_xscale; i++)
		{
			switch (mapicon_ptr[lpMapDocData->MapStruct.data[(int)(i*smlmap_xscale)][(int)(j*smlmap_yscale)].cdata - 32])
			{
			case 0: /*  0:x MAP_WALL */
			case 1: /*  1:q */
			case 2: /*  2:w */
			case 3: /*  3:a */
			case 4: /*  4:s */
				SetPixelV(hdc, i, j, COLOR_WALL);
				break;
			case 5: /*  5:# MAP_FUEL */
				SetPixelV(hdc, i, j, COLOR_FUEL);
				break;
			case 6: /*  6:d MAP_CANNON*/
			case 7: /*  7:r MAP_CANNON*/
			case 8: /*  8:c MAP_CANNON*/
			case 9: /*  9:f MAP_CANNON*/
				SetPixelV(hdc, i, j, COLOR_CANNON);
				break;
			case 10: /* 10:_ MAP_BASE (UP)*/ 
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			case 11: /* 11:+ MAP_GRAV_POS*/
			case 12: /* 12:- MAP_GRAV_NET*/
				SetPixelV(hdc, i, j, COLOR_GRAVITY);
				break;
			case 13: /* 13:@ MAP_WORM_NORMAL */
			case 14: /* 14:) MAP_WORM_OUT */
			case 15: /* 15:( MAP_WORM_IN */
				SetPixelV(hdc, i, j, COLOR_WORMHOLE);
				break;
			case 16: /* 16:* MAP_TREASURE*/
				SetPixelV(hdc, i, j, COLOR_TREASURE);
				break;
			case 17: /* 17:< COUNTER CLOCKWISE GRAVITY*/
			case 18: /* 18:> CLOCKWISE GRAVITY*/
				SetPixelV(hdc, i, j, COLOR_GRAVITY);
				break;
			case 19: /* 19:! MAP_TARGET */
				SetPixelV(hdc, i, j, COLOR_TARGET);
				break;
			case 21: /* 21:b MAP_DECORATION */
			case 22: /* 22:t */
			case 23: /* 23:y */
			case 24: /* 24:g */ 
			case 25: /* 25:h */
				SetPixelV(hdc, i, j, COLOR_DECOR);
				break;
			case 26: /* 26:$ BASE_ORIENT*/
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			case 27: /* 27:% ITEM CONCENTRATOR*/
				SetPixelV(hdc, i, j, COLOR_ITEM_CONC);
				break;
			case 28: /* 28:i CURRENT UP*/
			case 29: /* 29:j CURRENT LEFT*/
			case 30: /* 30:k CURRENT RIGHT*/
			case 31: /* 31:m CURRENT DOWN*/
				SetPixelV(hdc, i, j, COLOR_CURRENT);
				break;
				
			case 32: /* 32:^ MAP_EMPTYTREASURE*/
				SetPixelV(hdc, i, j, COLOR_TREASURE);
				break;
			case 33: /* 33:z MAP_FRICTION */
				SetPixelV(hdc, i, j, COLOR_FRICTION);
				break;
				
			case 50: /* 40: CHECKPOINTS*/
			case 51: /* 35:_ MAP_BASE_TEAM (DOWN)*/ 
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			default:
				SetPixelV(hdc, i, j, PALETTERGB(0,0,0));
				break;
			}
		}
		ReleaseDC(hwndMiniMap, hdc);
		
}
/***************************************************************************/
/* UpdateSmallMap                                                          */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   x                                                                     */
/*   y                                                                     */
/***************************************************************************/
void UpdateSmallMap(LPMAPDOCDATA lpMapDocData, int x,int y)
{
	int                   x2,y2,i,j,xs,ys;
	HDC hdc;
	
	hdc = GetDC (hwndMiniMap) ;
	
	x2 = (int) (x/smlmap_xscale);
	y2 = (int) (y/smlmap_yscale);
	
	if ( (smlmap_xscale<1) || (smlmap_yscale<1) )
	{
		xs = (int) ((1+x)/smlmap_xscale+1.5);
		ys = (int) ((1+y)/smlmap_yscale+1.5);
	}
	else
	{
		xs = x2+2;
		ys = y2+2;
	}
	
	for (i=x2;i<xs;i++)
		for (j=y2;j<ys;j++)
		{

			switch (mapicon_ptr[lpMapDocData->MapStruct.data[(int)(i*smlmap_xscale)][(int)(j*smlmap_yscale)].cdata - 32])
			{
			case 0: /*  0:x MAP_WALL */
			case 1: /*  1:q */
			case 2: /*  2:w */
			case 3: /*  3:a */
			case 4: /*  4:s */
				SetPixelV(hdc, i, j, COLOR_WALL);
				break;
			case 5: /*  5:# MAP_FUEL */
				SetPixelV(hdc, i, j, COLOR_FUEL);
				break;
			case 6: /*  6:d MAP_CANNON*/
			case 7: /*  7:r MAP_CANNON*/
			case 8: /*  8:c MAP_CANNON*/
			case 9: /*  9:f MAP_CANNON*/
				SetPixelV(hdc, i, j, COLOR_CANNON);
				break;
			case 10: /* 10:_ MAP_BASE (UP)*/ 
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			case 11: /* 11:+ MAP_GRAV_POS*/
			case 12: /* 12:- MAP_GRAV_NET*/
				SetPixelV(hdc, i, j, COLOR_GRAVITY);
				break;
			case 13: /* 13:@ MAP_WORM_NORMAL */
			case 14: /* 14:) MAP_WORM_OUT */
			case 15: /* 15:( MAP_WORM_IN */
				SetPixelV(hdc, i, j, COLOR_WORMHOLE);
				break;
			case 16: /* 16:* MAP_TREASURE*/
				SetPixelV(hdc, i, j, COLOR_TREASURE);
				break;
			case 17: /* 17:< COUNTER CLOCKWISE GRAVITY*/
			case 18: /* 18:> CLOCKWISE GRAVITY*/
				SetPixelV(hdc, i, j, COLOR_GRAVITY);
				break;
			case 19: /* 19:! MAP_TARGET */
				SetPixelV(hdc, i, j, COLOR_TARGET);
				break;
			case 21: /* 21:b MAP_DECORATION */
			case 22: /* 22:t */
			case 23: /* 23:y */
			case 24: /* 24:g */ 
			case 25: /* 25:h */
				SetPixelV(hdc, i, j, COLOR_DECOR);
				break;
			case 26: /* 26:$ BASE_ORIENT*/
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			case 27: /* 27:% ITEM CONCENTRATOR*/
				SetPixelV(hdc, i, j, COLOR_ITEM_CONC);
				break;
			case 28: /* 28:i CURRENT UP*/
			case 29: /* 29:j CURRENT LEFT*/
			case 30: /* 30:k CURRENT RIGHT*/
			case 31: /* 31:m CURRENT DOWN*/
				SetPixelV(hdc, i, j, COLOR_CURRENT);
				break;
				
			case 32: /* 32:^ MAP_EMPTYTREASURE*/
				SetPixelV(hdc, i, j, COLOR_TREASURE);
				break;
			case 33: /* 33:z MAP_FRICTION */
				SetPixelV(hdc, i, j, COLOR_FRICTION);
				break;
				
			case 50: /* 40: CHECKPOINTS*/
			case 51: /* 35:_ MAP_BASE_TEAM (DOWN)*/ 
				SetPixelV(hdc, i, j, COLOR_BASE);
				break;
			default:
				SetPixelV(hdc, i, j, PALETTERGB(0,0,0));
				break;
			}
		}
		ReleaseDC(hwndMiniMap, hdc);
}
/***************************************************************************/
/* ShowMiniMapMap                                                          */
/***************************************************************************/
int ShowMiniMap(void)
{

	if (Mini_Map)
		ShowWindow(hwndMiniMap, SW_SHOW);
	else
		ShowWindow(hwndMiniMap, SW_HIDE);

	return TRUE;
}
/***************************************************************************/
/* SizeSmallMap                                                            */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/***************************************************************************/
void SizeSmallMap(LPMAPDOCDATA lpMapDocData)
{
	RECT rect;
	
	GetClientRect(hwndMiniMap, &rect);
	
	smlmap_width = rect.right - rect.left-1;
	smlmap_height = rect.bottom - rect.top-1;
	smlmap_xscale = (float)(lpMapDocData->MapStruct.width)/(float)(smlmap_width);
	smlmap_yscale = (float)(lpMapDocData->MapStruct.height)/(float)(smlmap_height);
	smlmap_mainscale = max(smlmap_xscale, smlmap_yscale);
	if ( lpMapDocData->MapStruct.width > lpMapDocData->MapStruct.height ) {
		smlmap_height = (int)(lpMapDocData->MapStruct.height/smlmap_mainscale);
	} else if ( lpMapDocData->MapStruct.height > lpMapDocData->MapStruct.width ) {
		smlmap_width = (int)(lpMapDocData->MapStruct.width/smlmap_mainscale);
	}
	
	smlmap_xscale = smlmap_yscale = smlmap_mainscale;
	
}
/***************************************************************************/
/* CreateMiniMap                                                           */
/* Arguments :                                                             */
/*   lpMapDocData: pointer to document data                                */
/*   hwndLocClient: handle to MDI client window.                           */
/* Purpose :   The procedure for the Wild Map prefs                        */
/***************************************************************************/
BOOL CreateMiniMap (LPMAPDOCDATA lpMapDocData, HWND hwndLocClient)
{
	HWND hwndActive = (HWND) SendMessage (hwndLocClient, WM_MDIGETACTIVE, 0, 0);
    MDICREATESTRUCT mdicreate;
	
	//Add a mini map
	mdicreate.szClass = "MiniMapWin";
	mdicreate.szTitle = "Thumnail View";
	mdicreate.hOwner	= hInst;
	mdicreate.x = DEFAULT_WIDTH;
	mdicreate.cx = 200;
	mdicreate.y = 200;
	mdicreate.cy = 200;
	mdicreate.style = 0;
	mdicreate.lParam = 0;

	hwndMiniMap = (HWND)SendMessage (hwndLocClient,
		WM_MDICREATE,
		0,
		(LONG)(LPMDICREATESTRUCT)&mdicreate);

	SendMessage(hwndLocClient, WM_MDIACTIVATE, (WPARAM) hwndActive, 0);

	SizeSmallMap(lpMapDocData);
	return TRUE;
}
