# Microsoft Developer Studio Generated NMAKE File, Based on MapXpress.dsp
!IF "$(CFG)" == ""
CFG=MapXpress - Win32 Release
!MESSAGE No configuration specified. Defaulting to MapXpress - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "MapXpress - Win32 Release" && "$(CFG)" != "MapXpress - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MapXpress.mak" CFG="MapXpress - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MapXpress - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "MapXpress - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MapXpress - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\MapXpress.exe"


CLEAN :
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\bitmap.obj"
	-@erase "$(INTDIR)\clipboard.obj"
	-@erase "$(INTDIR)\errors.obj"
	-@erase "$(INTDIR)\expose.obj"
	-@erase "$(INTDIR)\file.obj"
	-@erase "$(INTDIR)\grow.obj"
	-@erase "$(INTDIR)\helper.obj"
	-@erase "$(INTDIR)\imexport.obj"
	-@erase "$(INTDIR)\mapxpress.obj"
	-@erase "$(INTDIR)\mapxpress.res"
	-@erase "$(INTDIR)\minimap.obj"
	-@erase "$(INTDIR)\prefs.obj"
	-@erase "$(INTDIR)\round.obj"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\tools.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\wildmap.obj"
	-@erase "$(OUTDIR)\MapXpress.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /ML /W3 /GX /O2 /I "C:\PROGRA~1\HTMLHE~1\INCLUDE" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "USEHTMLHELP" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\mapxpress.res" /d "NDEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\MapXpress.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib C:\Progra~1\HTMLHE~1\lib\hhctrl.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\MapXpress.pdb" /machine:I386 /out:"$(OUTDIR)\MapXpress.exe" 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\bitmap.obj" \
	"$(INTDIR)\clipboard.obj" \
	"$(INTDIR)\errors.obj" \
	"$(INTDIR)\expose.obj" \
	"$(INTDIR)\file.obj" \
	"$(INTDIR)\grow.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\imexport.obj" \
	"$(INTDIR)\mapxpress.obj" \
	"$(INTDIR)\minimap.obj" \
	"$(INTDIR)\prefs.obj" \
	"$(INTDIR)\round.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\tools.obj" \
	"$(INTDIR)\wildmap.obj" \
	"$(INTDIR)\mapxpress.res"

"$(OUTDIR)\MapXpress.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\MapXpress.exe" "$(OUTDIR)\MapXpress.bsc"


CLEAN :
	-@erase "$(INTDIR)\about.obj"
	-@erase "$(INTDIR)\about.sbr"
	-@erase "$(INTDIR)\bitmap.obj"
	-@erase "$(INTDIR)\bitmap.sbr"
	-@erase "$(INTDIR)\clipboard.obj"
	-@erase "$(INTDIR)\clipboard.sbr"
	-@erase "$(INTDIR)\errors.obj"
	-@erase "$(INTDIR)\errors.sbr"
	-@erase "$(INTDIR)\expose.obj"
	-@erase "$(INTDIR)\expose.sbr"
	-@erase "$(INTDIR)\file.obj"
	-@erase "$(INTDIR)\file.sbr"
	-@erase "$(INTDIR)\grow.obj"
	-@erase "$(INTDIR)\grow.sbr"
	-@erase "$(INTDIR)\helper.obj"
	-@erase "$(INTDIR)\helper.sbr"
	-@erase "$(INTDIR)\imexport.obj"
	-@erase "$(INTDIR)\imexport.sbr"
	-@erase "$(INTDIR)\mapxpress.obj"
	-@erase "$(INTDIR)\mapxpress.res"
	-@erase "$(INTDIR)\mapxpress.sbr"
	-@erase "$(INTDIR)\minimap.obj"
	-@erase "$(INTDIR)\minimap.sbr"
	-@erase "$(INTDIR)\prefs.obj"
	-@erase "$(INTDIR)\prefs.sbr"
	-@erase "$(INTDIR)\round.obj"
	-@erase "$(INTDIR)\round.sbr"
	-@erase "$(INTDIR)\toolbar.obj"
	-@erase "$(INTDIR)\toolbar.sbr"
	-@erase "$(INTDIR)\tools.obj"
	-@erase "$(INTDIR)\tools.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wildmap.obj"
	-@erase "$(INTDIR)\wildmap.sbr"
	-@erase "$(OUTDIR)\MapXpress.bsc"
	-@erase "$(OUTDIR)\MapXpress.exe"
	-@erase "$(OUTDIR)\MapXpress.ilk"
	-@erase "$(OUTDIR)\MapXpress.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MLd /W3 /Gm /GX /ZI /Od /I "C:\PROGRA~1\HTMLHE~1\INCLUDE" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "USEHTMLHELP" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /GZ /c 
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /win32 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\mapxpress.res" /d "_DEBUG" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\MapXpress.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\about.sbr" \
	"$(INTDIR)\bitmap.sbr" \
	"$(INTDIR)\clipboard.sbr" \
	"$(INTDIR)\errors.sbr" \
	"$(INTDIR)\expose.sbr" \
	"$(INTDIR)\file.sbr" \
	"$(INTDIR)\grow.sbr" \
	"$(INTDIR)\helper.sbr" \
	"$(INTDIR)\imexport.sbr" \
	"$(INTDIR)\mapxpress.sbr" \
	"$(INTDIR)\minimap.sbr" \
	"$(INTDIR)\prefs.sbr" \
	"$(INTDIR)\round.sbr" \
	"$(INTDIR)\toolbar.sbr" \
	"$(INTDIR)\tools.sbr" \
	"$(INTDIR)\wildmap.sbr"

"$(OUTDIR)\MapXpress.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib C:\Progra~1\HTMLHE~1\lib\hhctrl.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\MapXpress.pdb" /debug /machine:I386 /out:"$(OUTDIR)\MapXpress.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\about.obj" \
	"$(INTDIR)\bitmap.obj" \
	"$(INTDIR)\clipboard.obj" \
	"$(INTDIR)\errors.obj" \
	"$(INTDIR)\expose.obj" \
	"$(INTDIR)\file.obj" \
	"$(INTDIR)\grow.obj" \
	"$(INTDIR)\helper.obj" \
	"$(INTDIR)\imexport.obj" \
	"$(INTDIR)\mapxpress.obj" \
	"$(INTDIR)\minimap.obj" \
	"$(INTDIR)\prefs.obj" \
	"$(INTDIR)\round.obj" \
	"$(INTDIR)\toolbar.obj" \
	"$(INTDIR)\tools.obj" \
	"$(INTDIR)\wildmap.obj" \
	"$(INTDIR)\mapxpress.res"

"$(OUTDIR)\MapXpress.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("MapXpress.dep")
!INCLUDE "MapXpress.dep"
!ELSE 
!MESSAGE Warning: cannot find "MapXpress.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "MapXpress - Win32 Release" || "$(CFG)" == "MapXpress - Win32 Debug"
SOURCE=.\about.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\about.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\about.obj"	"$(INTDIR)\about.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\bitmap.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\bitmap.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\bitmap.obj"	"$(INTDIR)\bitmap.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\clipboard.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\clipboard.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\clipboard.obj"	"$(INTDIR)\clipboard.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\errors.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\errors.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\errors.obj"	"$(INTDIR)\errors.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\expose.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\expose.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\expose.obj"	"$(INTDIR)\expose.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\file.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\file.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\file.obj"	"$(INTDIR)\file.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\grow.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\grow.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\grow.obj"	"$(INTDIR)\grow.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\helper.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\helper.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\helper.obj"	"$(INTDIR)\helper.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\imexport.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\imexport.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\imexport.obj"	"$(INTDIR)\imexport.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\mapxpress.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\mapxpress.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\mapxpress.obj"	"$(INTDIR)\mapxpress.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\minimap.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\minimap.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\minimap.obj"	"$(INTDIR)\minimap.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\prefs.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\prefs.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\prefs.obj"	"$(INTDIR)\prefs.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\round.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\round.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\round.obj"	"$(INTDIR)\round.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\toolbar.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\toolbar.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\toolbar.obj"	"$(INTDIR)\toolbar.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\tools.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\tools.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\tools.obj"	"$(INTDIR)\tools.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\wildmap.c

!IF  "$(CFG)" == "MapXpress - Win32 Release"


"$(INTDIR)\wildmap.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "MapXpress - Win32 Debug"


"$(INTDIR)\wildmap.obj"	"$(INTDIR)\wildmap.sbr" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=.\mapxpress.rc

"$(INTDIR)\mapxpress.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)



!ENDIF 

