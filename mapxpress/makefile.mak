#
# Map Xpress, the XPilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000 by
#
#      Jarrod L. Miller           <jlmiller@ctitech.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Modifications to makefile.mak
# 1999:
#      Jarrod L. Miller        <jlmiller@ctitech>
# 03/06/2000:
#      Jarrod L. Miller        <jlmiller@ctitech>
# 07/20/2000: JLM - Update for MDI and HTMLHelp
# 08/30/2000: JLM - Fixed Make clean.
#

#The Compiler
CC=cl

#Which help system to use?
#To use the new html system, and have Microsoft HTML Help Workshop installed,
#uncomment & edit the following two lines so that they point to the
#correct locations for the lib and include files & folders.
HTMLLIBS=C:\Progra~1\HTMLHE~1\lib\hhctrl.lib
HTMLFLAGS=/IC:\PROGRA~1\HTMLHE~1\INCLUDE /DUSEHTMLHELP

CFLAGS=-c -DSTRICT -G3 -W3 -Zp -Zi -Tp
LINKER=link
GUIFLAGS=-SUBSYSTEM:windows
GUILIBS=-DEFAULTLIB:user32.lib gdi32.lib winmm.lib comdlg32.lib \
        comctl32.lib shell32.lib advapi32.lib
RC=rc
RCVARS=-r -DWIN32

EXE = MapXpress.exe
INCLUDES = mapxpress.h defaults.h proto.h map.h mapxpmnu.h expose.h \
	   default_colors.h grow.h xprtypes.h default_settings.h
OBJS = mapxpress.obj expose.obj file.obj toolbar.obj prefs.obj helper.obj \
       tools.obj round.obj clipboard.obj wildmap.obj imexport.obj grow.obj \
       bitmap.obj about.obj errors.obj minimap.obj
GUIRES = mapxpress.res
RES = mapxpresslg.ico toolsbar.bmp blocksbar.bmp shapesubbar.bmp

$(EXE) : $(OBJS) $(GUIRES) $(RES)
     $(LINKER) $(GUIFLAGS) -OUT:$(EXE) $(OBJS) $(GUIRES) $(GUILIBS) $(HTMLLIBS)

$(OBJS): $*.c $(INCLUDES)
     $(CC) $(HTMLFLAGS) $(CFLAGS) $*.c

$(GUIRES) : $*.rc $(RES)
     $(RC) $(RCVARS) $*.rc

clean :
	del *.obj
	del *.res
	del *.pdb
	del MapXpress.exe