/*
* MapXpress, the Xpilot Map Editor for Windows 95/98/NT.  Copyright (C) 1999, 2000, 2001
* by
*
*      Jarrod L. Miller           <jlmiller@ctitech.com>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file COPYRIGHT.TXT for current copyright information.
*
*/

#include "mapxpress.h"

#define NELEM(a)	(sizeof(a) / sizeof(a[0]))

#define FUZZ_MASK	0xFFFFFFFFU
#define MAX_FUZZ	FUZZ_MASK

#define MAPOFFLEFT(i)	(((i) >= 1) ? (i) - 1 : map2.datasize - 1)
#define MAPOFFRIGHT(i)	(((i) + 1 < map2.datasize) ? (i) + 1 : 0)
#define MAPOFFUP(i)	(((i) >= map2.linewidth) \
				? (i) - map2.linewidth \
				: (i) - map2.linewidth + map2.datasize)
#define MAPOFFDOWN(i)	(((i) + map2.linewidth < map2.datasize) \
				? (i) + map2.linewidth \
				: (i) + map2.linewidth - map2.datasize)
#define MAPLEFT(i)	map2.data[MAPOFFLEFT(i)]
#define MAPRIGHT(i)	map2.data[MAPOFFRIGHT(i)]
#define MAPUP(i)	map2.data[MAPOFFUP(i)]
#define MAPDOWN(i)	map2.data[MAPOFFDOWN(i)]

extern char wmapwidth[4];
extern char wmapheight[4];
extern char wmapseed[20];
extern char wmapseed_ratio[10];
extern char wmapfill_ratio[10];
extern char wmapnum_bases[4];
extern char wmapnum_teams[4];
extern char wmapcannon_ratio[10];
extern char wmapfuel_ratio[10];
extern char wmapgrav_ratio[10];
extern char wmapworm_ratio[10];


struct map2 {
    unsigned		seed,
			fuzz_word;
    char		*data;
    double		fill_ratio,
			seed_ratio,
			cannon_ratio,
			fuel_ratio,
			grav_ratio,
			worm_ratio;
    int			width,
			height,
			linewidth,
			datasize,
			num_bases,
			num_teams,
			flood_marker,
			flood_trigger,
			flood_count[256];
};

static struct map2	map2;

static unsigned fuzz(void)
{
    /*
     * Implement a private pseudo-random generator to be able
     * to regenerate the same map from the same seed on
     * different architectures.
     * It assumes an unsigned int with at least 32 bits (as I do always).
     * The magic constant used below is from Sedgewick's Algorithms.
     * Note that the low order digits are not random at all :(.
     */
    map2.fuzz_word = (map2.fuzz_word * 31415821 + 1) & FUZZ_MASK;

    return map2.fuzz_word;
}

static unsigned fuzz_bound(unsigned max)
{
    /*
     * Return a pseudo-random unsigned integer
     * in the range: [0-max] inclusive.
     */
    return (unsigned)(((double) fuzz() * (double) max) / (double) MAX_FUZZ);
}

static void Default_map(void)
{
    map2.width = 200;
    map2.height = 200;
    map2.seed = (unsigned)getpid() ^ (unsigned)time(NULL) * (unsigned)getpid();
    map2.seed_ratio = 0.19;
    map2.fill_ratio = 0.18;
    map2.num_bases = 16;
    map2.num_teams = 3;
    map2.cannon_ratio = 0.0020;
    map2.fuel_ratio   = 0.0006;
    map2.grav_ratio   = 0.0006;
    map2.worm_ratio   = 0.0002;
}


static void Option_map()
{
    struct map_opt {
	char		*name;
	int		*intp;
	unsigned	*unsp;
	double		*dblp;
	double		min;
	double		max;
    };
    static struct map_opt mapopts[] = {
	{ "mapWidth", &map2.width, 0, 0, 90, 936 },
	{ "mapHeight", &map2.height, 0, 0, 90, 936 },
	{ "seed", 0, &map2.seed, 0, 0, MAX_FUZZ },
	{ "seedRatio", 0, 0, &map2.seed_ratio, 0.02, 0.22 },
	{ "fillRatio", 0, 0, &map2.fill_ratio, 0.02, 0.80 },
	{ "numBases", &map2.num_bases, 0, 0, 2, 50 },
	{ "numTeams", &map2.num_teams, 0, 0, 3, 10 },
	{ "cannonRatio", 0, 0, &map2.cannon_ratio, 0.0, 0.2 },
	{ "fuelRatio", 0, 0, &map2.fuel_ratio, 0.0, 0.1 },
	{ "gravRatio", 0, 0, &map2.grav_ratio, 0.0, 0.1 },
	{ "wormRatio", 0, 0, &map2.worm_ratio, 0.0, 0.1 }
    };
    
   if (atoi(wmapwidth) >= 90 && atoi(wmapwidth) <= 936)
	map2.width = atoi(wmapwidth);
   if (atoi(wmapheight) >= 90 && atoi(wmapheight) <= 936)
	map2.height = atoi(wmapheight);
   if (atoi(wmapseed) >= 0 && atoi(wmapseed) <= MAX_FUZZ)
	map2.seed = atoi(wmapseed);
   if (atof(wmapseed_ratio) >= 0.02 && atof(wmapseed_ratio) <= 0.22)
	map2.seed_ratio = atof(wmapseed_ratio);
   if (atof(wmapfill_ratio) >= 0.02 && atof(wmapfill_ratio) <= 0.22)
	map2.fill_ratio = atof(wmapfill_ratio);
   if (atoi(wmapnum_bases) > 2 && atoi(wmapnum_bases) <= 50)
	map2.num_bases = atoi(wmapnum_bases);
   if (atoi(wmapnum_teams) > 3 && atoi(wmapnum_teams) <= 10)
	map2.num_teams = atoi(wmapnum_teams);

   if (atof(wmapcannon_ratio) >= 0.0 && atof(wmapcannon_ratio) <= 0.2)
	map2.cannon_ratio = atof(wmapcannon_ratio);
   if (atof(wmapfuel_ratio) >= 0.0 && atof(wmapfuel_ratio) <= 0.1)
	map2.fuel_ratio = atof(wmapfuel_ratio);
   if (atof(wmapgrav_ratio) >= 0.0 && atof(wmapgrav_ratio) <= 0.1)
	map2.grav_ratio = atof(wmapgrav_ratio);
   if (atof(wmapworm_ratio) >= 0.0 && atof(wmapworm_ratio) <= 0.1)
	map2.worm_ratio = atof(wmapworm_ratio);


}

static void Alloc_map(void)
{
    /*
     * Allocate the map data.
     * The (0, 0) coordinate is in the top left corner.
     * An extra column is appended at the far right, which
     * will later be turned into newlines for easy printing.
     */
    map2.fuzz_word = map2.seed;
    map2.linewidth = map2.width + 1;
    map2.datasize = map2.linewidth * map2.height;
	if (map2.data)
	{
		free(map2.data);
		map2.data = NULL;
	}
    map2.data = (char *) malloc(map2.datasize + 1);
    if (!map2.data) {
	printf("no mem\n");
	exit(1);
    }
}

static void Flood_map(int i)
{
    /*
     * Connect all adjacent (up, down, left, right) map objects,
     * which have the same type as map.flood_trigger.
     * The map objects will be marked with value map.flood_marker,
     * to distinguish them from map objects which already have
     * been processed.
     */

#define INTARR_SIZE	1024

    struct int_arr {
	struct int_arr	*next;
	int		n;
	int		arr[INTARR_SIZE];
    };

    struct int_arr	intarr, *putp = &intarr, *getp, *tmpp;
    int			k, j;

    if (map2.data[i] != map2.flood_trigger) {
	return;
    }
    map2.data[i] = map2.flood_marker;
    putp->next = NULL;
    putp->n = 1;
    putp->arr[0] = i;

    for (getp = &intarr; getp != NULL; getp = tmpp) {

	while (getp->n > 0) {
	    k = getp->arr[--getp->n];
	    if (putp->n + 4 > INTARR_SIZE) {
		if ((putp->next = (struct int_arr *)
				  malloc(sizeof(struct int_arr))) == NULL) {
		    fprintf(stderr, "No mem\n");
		    exit(1);
		}
		putp = putp->next;
		putp->next = NULL;
		putp->n = 0;
	    }
	    j = MAPOFFUP(k);
	    if (map2.data[j] == map2.flood_trigger) {
		map2.data[j] = map2.flood_marker;
		putp->arr[putp->n++] = j;
	    }
	    j = MAPOFFLEFT(k);
	    if (map2.data[j] == map2.flood_trigger) {
		map2.data[j] = map2.flood_marker;
		putp->arr[putp->n++] = j;
	    }
	    j = MAPOFFDOWN(k);
	    if (map2.data[j] == map2.flood_trigger) {
		map2.data[j] = map2.flood_marker;
		putp->arr[putp->n++] = j;
	    }
	    j = MAPOFFRIGHT(k);
	    if (map2.data[j] == map2.flood_trigger) {
		map2.data[j] = map2.flood_marker;
		putp->arr[putp->n++] = j;
	    }
	}
	tmpp = getp->next;
	if (getp != &intarr) {
	    free(getp);
	}
    }
}

static void Generate_map(void)
{
    /* 
     * Initialize the map with noise.
     * Later the noise is either removed or connected,
     * depending upon the outcome of a randomizer.
     */

    int			todo, i;
    unsigned		edge;

    map2.flood_trigger = 1;
    map2.flood_marker = 2;

    edge = (unsigned) (MAX_FUZZ * map2.seed_ratio);
    for (todo = map2.datasize; todo--; ) {
	map2.data[todo] = (fuzz() < edge);
    }

    edge = (MAX_FUZZ / map2.datasize);
    for (todo = map2.datasize; todo--; ) {
	i = fuzz_bound(map2.datasize - 1);
	if (map2.data[i] == 0) {
	    if (MAPUP(i) == 1) MAPUP(i) = 0;
	    if (MAPLEFT(i) == 1) MAPLEFT(i) = 0;
	    if (MAPDOWN(i) == 1) MAPDOWN(i) = 0;
	    if (MAPRIGHT(i) == 1) MAPRIGHT(i) = 0;
	}
	else {
	    if (map2.data[i] == 0) {
		map2.data[i] = 1;
		Flood_map(i);
		map2.data[i] = 1;
	    }
	    else if (map2.data[i] == 1) {
		Flood_map(i);
		if (MAPUP(i) == 0) {
		    MAPUP(i) = 1;
		    Flood_map(MAPOFFUP(i));
		    MAPUP(i) = 1;
		}
		if (MAPLEFT(i) == 0) {
		    MAPLEFT(i) = 1;
		    Flood_map(MAPOFFLEFT(i));
		    MAPLEFT(i) = 1;
		}
		if (MAPDOWN(i) == 0) {
		    MAPDOWN(i) = 1;
		    Flood_map(MAPOFFDOWN(i));
		    MAPDOWN(i) = 1;
		}
		if (MAPRIGHT(i) == 0) {
		    MAPRIGHT(i) = 1;
		    Flood_map(MAPOFFRIGHT(i));
		    MAPRIGHT(i) = 1;
		}
	    }
	}
    }
}

static void Connect_map(void)
{
    /*
     * Connect small constructs to another if they are very close
     * to one another (1 or only 2 blocks apart).
     */

    char		*p0, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *p9, *pa;
    char		*maxp;
    int			n;

    maxp = &map2.data[map2.datasize];

    do {
	n = 0;

	p0 = &map2.data[map2.datasize];
	p1 = p0 - 1;
	p2 = p0 - 2;
	p3 = p0 - 3;
	p4 = p0 - map2.linewidth;
	p5 = p4 - 1;
	p6 = p4 - 2;
	p7 = p4 - map2.linewidth;
	p8 = p7 - 1;
	p9 = p7 - 2;
	pa = p7 - map2.linewidth;
	p0 = map2.data;

	while (p0 < maxp) {
	    if (*p0) {
		if (!*p1) {
		    if (*p2) {
			*p1 = *p0;
			n++;
		    }
		    else if (*p3) {
			*p2 = *p1 = *p0;
			n++;
		    }
		}
		if (!*p4) {
		    if (*p7) {
			*p4 = *p0;
			n++;
		    }
		    else if (*pa) {
			*p7 = *p4 = *p0;
			n++;
		    }
		}
		if (*p5) {
		    if (!*p1 && !*p4) {
			*p4 = *p1 = *p0;
			n++;
		    }
		}
	    } else {
		if (!*p5 && *p1 && *p4) {
		    *p0 = *p5 = *p1;
		    n++;
		}
	    }
	    if (!*p5) {
		if ((*p0 || (*p1 && *p4)) && *p9) {
		    *p5 = *p0;
		    n++;
		}
		else if (*p6 && *p8 && *p0) {
		    *p5 = *p0;
		    n++;
		}
		else if (*p2 && (*p7 || (*p4 && *p8))) {
		    *p5 = *p2;
		    n++;
		}
		else if (*p7 && *p1 && *p6) {
		    *p5 = *p7;
		    n++;
		}
		else if (!*p0 && !*p1 && *p2
			&& !*p4 && *p6
			&& *p7 && *p8 && *p9) {
		    *p5 = *p9;
		    n++;
		}
		else if (*p0 && !*p1 && !*p2
			&& *p4 && !*p6
			&& *p7 && *p8 && *p9) {
		    *p5 = *p7;
		    n++;
		}
		else if (*p0 && *p1 && *p2
			&& *p4 && !*p6
			&& *p7 && !*p8 && !*p9) {
		    *p5 = *p0;
		    n++;
		}
		else if (*p0 && *p1 && *p2
			&& !*p4 && *p6
			&& !*p7 && !*p8 && *p9) {
		    *p5 = *p0;
		    n++;
		}
		else if (*p0 && !*p1 && !*p2
			&& !*p4 && !*p6
			&& !*p7 && *p8 && !*p9) {
		    *p5 = *p8;
		    n++;
		}
		else if (!*p0 && !*p1 && *p2
			&& !*p4 && !*p6
			&& !*p7 && *p8 && !*p9) {
		    *p5 = *p8;
		    n++;
		}
		else if (*p0 && !*p1 && !*p2
			&& !*p4 && *p6
			&& !*p7 && !*p8 && !*p9) {
		    *p5 = *p6;
		    n++;
		}
		else if (!*p0 && !*p1 && !*p2
			&& !*p4 && *p6
			&& *p7 && !*p8 && !*p9) {
		    *p5 = *p6;
		    n++;
		}
		else if (!*p0 && *p1 && !*p2
			&& !*p4 && !*p6
			&& !*p7 && !*p8 && *p9) {
		    *p5 = *p1;
		    n++;
		}
		else if (!*p0 && *p1 && !*p2
			&& !*p4 && !*p6
			&& *p7 && !*p8 && !*p9) {
		    *p5 = *p1;
		    n++;
		}
		else if (!*p0 && !*p1 && !*p2
			&& *p4 && !*p6
			&& !*p7 && !*p8 && *p9) {
		    *p5 = *p4;
		    n++;
		}
		else if (!*p0 && !*p1 && *p2
			&& *p4 && !*p6
			&& !*p7 && !*p8 && !*p9) {
		    *p5 = *p4;
		    n++;
		}
	    }
	    p3 = p2;
	    p2 = p1;
	    p1 = p0;
	    p0++;
	    p6 = p5;
	    p5 = p4;
	    if (++p4 >= maxp) {
		p4 = map2.data;
	    }
	    p9 = p8;
	    p8 = p7;
	    if (++p7 >= maxp) {
		p7 = map2.data;
	    }
	    if (++pa >= maxp) {
		pa = map2.data;
	    }
	}

    } while (n > 0);
}

static void Count_labels(void)
{
    /*
     * For each possible map value count the number of occurences.
     */

    int			todo;

    memset(map2.flood_count, 0, sizeof map2.flood_count);

    for (todo = map2.datasize; todo--; ) {
	map2.flood_count[(unsigned char) map2.data[todo]]++;
    }
}

static void Sort_labels(int tbl[256])
{
    /*
     * Sort the map to have big map constructs with low map values
     * close to zero and small map constructs with high map values.
     */

    int			i, j, n;

    Count_labels();

    memset(tbl, 0, sizeof(int *) * 256);

    tbl[0] = 0;
    tbl[1] = 1;
    n = 2;
    for (i = 2; i < 256; i++) {
	for (j = n; j > 2; j--) {
	    if (map2.flood_count[i] > map2.flood_count[tbl[j - 1]]) {
		tbl[j] = tbl[j - 1];
	    } else {
		break;
	    }
	}
	tbl[j] = i;
	n++;
    }
}

static void Label_map(int label)
{
    /*
     * Give each map construct an unique value depending on how
     * big it is.  It is OK to delete small constructs if we are
     * running out of label values.
     */

    int			todo,
			i,
			tbl[256],
			del[256];

    memset(tbl, 0, sizeof tbl);
    tbl[label] = 1;
    for (todo = map2.datasize; todo--; ) {
	map2.data[todo] = tbl[(unsigned char)map2.data[todo]];
    }

    map2.flood_trigger = 1;
    map2.flood_marker = 2;

    for (todo = map2.datasize; todo--; ) {
	if (map2.data[todo] == map2.flood_trigger) {
	    if (map2.flood_marker >= 256) {
		Sort_labels(tbl);
		for (i = 0; i < 224; i++) {
		    del[tbl[i]] = i;
		}
		for (i = 224; i < 256; i++) {
		    del[tbl[i]] = 0;
		}
		for (i = map2.datasize; i--; ) {
		    map2.data[i] = del[(unsigned char)map2.data[i]];
		}
		map2.flood_marker = 224;
	    }
	    Flood_map(todo);
	    map2.flood_marker++;
	}
    }
}

static void Partition_map(void)
{
    /*
     * Remove all the inaccessible holes from the map.
     * And remove enough small map constructs to meet our fillRatio.
     */

    int			todo,
			i,
			j,
			marker,
			count,
			excess,
			tbl[256],
			del[256];

    Label_map(0);
    Count_labels();
    marker = 0;
    for (i = 2; i < 256; i++) {
	if (map2.flood_count[i] > map2.flood_count[marker]) {
	    marker = i;
	}
    }
    memset(tbl, 0, sizeof tbl);
    tbl[marker] = 1;
    for (todo = map2.datasize; todo--; ) {
	map2.data[todo] = tbl[(unsigned char)map2.data[todo]];
    }

    Label_map(0);
    Sort_labels(tbl);
    for (i = 0; i < 256; i++) {
	del[tbl[i]] = tbl[i];
    }
    count = 0;
    excess = map2.datasize - map2.flood_count[0]
	- (int)(map2.fill_ratio * map2.datasize + 0.5);
    for (i = 256; i-- > 2; ) {
	if (excess < count) {
	    break;
	}
	count += map2.flood_count[tbl[i]];
    }
    if (++i < 256) {
	for (j = i + 1; j < 256; j++) {
	    if (count - map2.flood_count[tbl[j]] > excess) {
		if (2 * map2.flood_count[tbl[j]] >= map2.flood_count[tbl[i]]) {
		    continue;
		}
	    }
	    del[tbl[j]] = 0;
	}
	del[tbl[i]] = 0;
	for (todo = map2.datasize; todo--; ) {
	    map2.data[todo] = del[(unsigned char)map2.data[todo]];
	}
    }
}

static void Smooth_map(void)
{
    /*
     * Round all sharp edges and corners.
     */

    char		*p0, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8;
    char		*maxp;
    int			todo, n;

    for (todo = map2.datasize; todo--; ) {
	map2.data[todo] = (map2.data[todo] ? MAP_FILLED : MAP_SPACE);
    }

    maxp = &map2.data[map2.datasize];

    do {
	n = 0;

	p0 = &map2.data[map2.datasize];
	p1 = p0 - 1;
	p2 = p0 - 2;
	p3 = p0 - map2.linewidth;
	p4 = p3 - 1;
	p5 = p4 - 2;
	p6 = p3 - map2.linewidth;
	p7 = p6 - 1;
	p8 = p7 - 2;
	p0 = map2.data;

	while (p0 < maxp) {

	    if (*p4 == MAP_SPACE || *p4 == MAP_FILLED) {
		if (*p0 == MAP_FILLED && *p1 == MAP_FILLED
		    && *p3 == MAP_FILLED && *p5 == MAP_SPACE
		    && *p7 == MAP_SPACE && *p8 == MAP_SPACE) {
		    *p4 = MAP_REC_RD;
		    n++;
		}
		else if (*p1 == MAP_FILLED && *p2 == MAP_FILLED
		    && *p3 == MAP_SPACE && *p5 == MAP_FILLED
		    && *p6 == MAP_SPACE && *p7 == MAP_SPACE) {
		    *p4 = MAP_REC_LD;
		    n++;
		}
		else if (*p0 == MAP_SPACE && *p1 == MAP_SPACE
		    && *p3 == MAP_SPACE && *p5 == MAP_FILLED
		    && *p7 == MAP_FILLED && *p8 == MAP_FILLED) {
		    *p4 = MAP_REC_LU;
		    n++;
		}
		else if (*p1 == MAP_SPACE && *p2 == MAP_SPACE
		    && *p3 == MAP_FILLED && *p5 == MAP_SPACE
		    && *p6 == MAP_FILLED && *p7 == MAP_FILLED) {
		    *p4 = MAP_REC_RU;
		    n++;
		}
		else if (*p4 == MAP_FILLED) {
		    if (*p5 == MAP_SPACE && *p8 == MAP_SPACE
			&& *p7 == MAP_SPACE
			&& *p6 == MAP_SPACE && *p3 == MAP_SPACE) {
			if (*p2 != MAP_FILLED && *p0 != MAP_FILLED) {
			    *p4 = MAP_SPACE;
			    n++;
			}
		    }
		    else if (*p7 == MAP_SPACE && *p6 == MAP_SPACE
			&& *p3 == MAP_SPACE
			&& *p0 == MAP_SPACE && *p1 == MAP_SPACE) {
			if (*p8 != MAP_FILLED && *p2 != MAP_FILLED) {
			    *p4 = MAP_SPACE;
			    n++;
			}
		    }
		    else if (*p3 == MAP_SPACE && *p0 == MAP_SPACE
			&& *p1 == MAP_SPACE
			&& *p2 == MAP_SPACE && *p5 == MAP_SPACE) {
			if (*p6 != MAP_FILLED && *p8 != MAP_FILLED) {
			    *p4 = MAP_SPACE;
			    n++;
			}
		    }
		    else if (*p1 == MAP_SPACE && *p2 == MAP_SPACE
			&& *p5 == MAP_SPACE
			&& *p8 == MAP_SPACE && *p7 == MAP_SPACE) {
			if (*p0 != MAP_FILLED && *p6 != MAP_FILLED) {
			    *p4 = MAP_SPACE;
			    n++;
			}
		    }
		}
	    }

	    p2 = p1;
	    p1 = p0;
	    p0++;
	    p5 = p4;
	    p4 = p3;
	    if (++p3 >= maxp) {
		p3 = map2.data;
	    }
	    p8 = p7;
	    p7 = p6;
	    if (++p6 >= maxp) {
		p6 = map2.data;
	    }
	}

    } while (n > 0);
}

static void Decorate_map(void)
{
    /*
     * Add the cannons, fuelstations, homebases,
     * wormholes and gravity objects.
     * A homebase should be free from influence from gravity objects.
     */

    struct xy {
	int		x,
			y;
    };

    int			margin,
			h,
			i,
			hori,
			vert,
			off,
			x,
			y,
			base,
			cannon,
			num_cannons,
			grav,
			num_gravs,
			fuel,
			num_fuels,
			worm,
			num_worms,
			type,
			wall_offset,
			team,
			tries;
    struct xy		*home;
    unsigned		size;

    size = map2.num_bases * sizeof(struct xy);
    if ((home = (struct xy *) malloc(size)) == NULL) {
	fprintf(stderr, "No mem\n");
	exit(1);
    }
    margin = 2;
    if (map2.num_teams > map2.num_bases) {
	map2.num_teams = map2.num_bases;
    }
    team = map2.num_teams;
    for (i = 0; i < map2.num_bases; i++) {
	for (tries = map2.datasize; tries; tries--) {
	    x = margin + fuzz_bound(map2.width - 2*margin - 1);
	    y = margin + fuzz_bound(map2.height - 2*margin - 1);
	    base = x + y * map2.linewidth;
	    if (map2.data[base] != MAP_SPACE) {
		continue;
	    }
	    if (map2.data[base + map2.linewidth] != MAP_FILLED) {
		continue;
	    }
	    if (map2.data[base - map2.linewidth] != MAP_SPACE) {
		continue;
	    }
	    if (map2.data[base - 2 * map2.linewidth] != MAP_SPACE) {
		continue;
	    }
	    if (map2.data[base - 1] != MAP_SPACE) {
		continue;
	    }
	    if (map2.data[base + 1] != MAP_SPACE) {
		continue;
	    }
	    if (--team < 0) {
		team = map2.num_teams - 1;
	    }
	    map2.data[base] = '0' + team;
	    home[i].x = x;
	    home[i].y = y;
	    break;
	}
	if (tries == 0) {
	    break;
	}
    }

    num_cannons = (int) (map2.cannon_ratio * map2.datasize);
    margin = 1;
    for (i = 0; i < num_cannons; i++) {
	switch (fuzz_bound(3)) {
	case 0:
	    type = MAP_CAN_LEFT;
	    wall_offset = 1;
	    break;
	case 1:
	    type = MAP_CAN_UP;
	    wall_offset = map2.linewidth;
	    break;
	case 2:
	    type = MAP_CAN_RIGHT;
	    wall_offset = -1;
	    break;
	default:
	    type = MAP_CAN_DOWN;
	    wall_offset = -map2.linewidth;
	    break;
	}
	for (tries = map2.datasize; tries; tries--) {
	    x = margin + fuzz_bound(map2.width - 2*margin - 1);
	    y = margin + fuzz_bound(map2.height - 2*margin - 1);
	    cannon = x + y * map2.linewidth;
	    if (map2.data[cannon] != MAP_SPACE) {
		continue;
	    }
	    if (map2.data[cannon + wall_offset] == MAP_SPACE) {
		continue;
	    }
	    if (map2.data[cannon + wall_offset] != MAP_FILLED) {
		switch (type) {
		case MAP_CAN_LEFT:
		    if (map2.data[cannon + wall_offset] != MAP_REC_LU &&
			map2.data[cannon + wall_offset] != MAP_REC_LD) {
			continue;
		    }
		    break;
		case MAP_CAN_UP:
		    if (map2.data[cannon + wall_offset] != MAP_REC_LU &&
			map2.data[cannon + wall_offset] != MAP_REC_RU) {
			continue;
		    }
		    break;
		case MAP_CAN_RIGHT:
		    if (map2.data[cannon + wall_offset] != MAP_REC_RU &&
			map2.data[cannon + wall_offset] != MAP_REC_RD) {
			continue;
		    }
		    break;
		default:
		    if (map2.data[cannon + wall_offset] != MAP_REC_LD &&
			map2.data[cannon + wall_offset] != MAP_REC_LD) {
			continue;
		    }
		    break;
		}
	    }
	    for (h = 0; h < map2.num_bases; h++) {
		if (((x < home[h].x)
			? (x + margin < home[h].x)
			: (x - margin > home[h].x))
		    && ((y < home[h].y)
			? (y + margin < home[h].y)
			: (y - margin > home[h].y))) {
		    continue;
		}
		break;
	    }
	    if (h < map2.num_bases) {
		continue;
	    }
	    map2.data[cannon] = type;
	    break;
	}
	if (tries == 0) {
	    break;
	}
    }

    num_fuels = (int) (map2.fuel_ratio * map2.datasize);
    margin = 1;
    for (i = 0; i < num_fuels; i++) {
	for (tries = map2.datasize; tries; tries--) {
	    x = margin + fuzz_bound(map2.width - 2*margin - 1);
	    y = margin + fuzz_bound(map2.height - 2*margin - 1);
	    fuel = x + y * map2.linewidth;
	    if (map2.data[fuel] != MAP_FILLED) {
		continue;
	    }
	    if (map2.data[fuel + 1] != MAP_SPACE &&
		map2.data[fuel - 1] != MAP_SPACE &&
		map2.data[fuel + map2.linewidth] != MAP_SPACE &&
		map2.data[fuel - map2.linewidth] != MAP_SPACE) {
		continue;
	    }
	    map2.data[fuel] = MAP_FUEL;
	    break;
	}
	if (tries == 0) {
	    break;
	}
    }

    margin = 11;
    num_gravs = (int) (map2.grav_ratio * map2.datasize);
    for (i = 0; i < num_gravs; i++) {
	switch (fuzz_bound(3)) {
	case 0:
	    type = MAP_GRAV_POS;
	    break;
	case 1:
	    type = MAP_GRAV_NEG;
	    break;
	case 2:
	    type = MAP_GRAV_CWISE;
	    break;
	default:
	    type = MAP_GRAV_ACWISE	;
	    break;
	}
	for (tries = map2.datasize; tries; tries--) {
	    x = margin + fuzz_bound(map2.width - 2*margin - 1);
	    y = margin + fuzz_bound(map2.height - 2*margin - 1);
	    grav = x + y * map2.linewidth;
	    if (map2.data[grav] != MAP_SPACE ||
		map2.data[grav + 1] != MAP_SPACE ||
		map2.data[grav - 1] != MAP_SPACE ||
		map2.data[grav + map2.linewidth] != MAP_SPACE ||
		map2.data[grav - map2.linewidth] != MAP_SPACE ||
		map2.data[grav + map2.linewidth + 1] != MAP_SPACE ||
		map2.data[grav - map2.linewidth + 1] != MAP_SPACE ||
		map2.data[grav + map2.linewidth - 1] != MAP_SPACE ||
		map2.data[grav - map2.linewidth - 1] != MAP_SPACE) {
		continue;
	    }

	    for (h = 0; h < map2.num_bases; h++) {
		if ((x < home[h].x)
			? (x + margin < home[h].x)
			: (x - margin > home[h].x)) {
		    continue;
		}
		if ((y < home[h].y)
			? (y + margin < home[h].y)
			: (y - margin > home[h].y)) {
		    continue;
		}
		break;
	    }
	    if (h < map2.num_bases) {
		continue;
	    }
	    map2.data[grav] = type;
	    break;
	}
	if (tries == 0) {
	    break;
	}
    }

    margin = 3;
    num_worms = (int) (map2.worm_ratio * map2.datasize);
    for (i = 0; i < num_worms; i++) {
	switch (fuzz_bound(3)) {
	case 0:
	    type = MAP_WORM_IN;
	    break;
	case 1:
	    type = MAP_WORM_OUT;
	    break;
	default:
	    type = MAP_WORM_NORMAL;
	    break;
	}
	for (tries = map2.datasize; tries; tries--) {
	    x = margin + fuzz_bound(map2.width - 2*margin - 1);
	    y = margin + fuzz_bound(map2.height - 2*margin - 1);
	    worm = x + y * map2.linewidth;
	    if (map2.data[worm] != MAP_SPACE) {
		continue;
	    }

	    for (vert = -margin; vert <= margin; vert++) {
		off = x - margin + (y + vert) * map2.linewidth;
		for (hori = -margin; hori <= margin; hori++, off++) {
		    switch (map2.data[off]) {
		    case MAP_SPACE:
		    case MAP_GRAV_POS:
		    case MAP_GRAV_NEG:
		    case MAP_GRAV_CWISE:
		    case MAP_GRAV_ACWISE	:
		    case MAP_WORM_NORMAL:
		    case MAP_WORM_IN:
		    case MAP_WORM_OUT:
			continue;
		    default:
			break;
		    }
		    break;
		}
		if (hori <= margin) {
		    break;
		}
	    }
	    if (vert <= margin) {
		continue;
	    }

	    for (h = 0; h < map2.num_bases; h++) {
		if ((x < home[h].x)
			? (x + margin < home[h].x)
			: (x - margin > home[h].x)) {
		    continue;
		}
		if ((y < home[h].y)
			? (y + margin < home[h].y)
			: (y - margin > home[h].y)) {
		    continue;
		}
		break;
	    }
	    if (h < map2.num_bases) {
		continue;
	    }
	    map2.data[worm] = type;
	    break;
	}
	if (tries == 0) {
	    break;
	}
    }

    free(home);
}

static void Print_map(LPMAPDOCDATA lpMapDocData)
{
    /*
     * Output the map in XPilot 2.0 map format.
     */

    int			i, x, y, ch;
    char		*left, *middle, *right;
	char	*header, *tmp;

    left = &map2.data[map2.width - 1];
    middle = left + 1;
    right = &map2.data[0];
    for (i = 0; i < map2.height; i++) {
	*middle = '\n';
	if (*right == MAP_FILLED) {
	    if (*left == MAP_REC_LD || *left == MAP_REC_LU) {
		*left = MAP_FILLED;
	    }
	}
	if (*right == MAP_SPACE
	    || *right == MAP_REC_RD
	    || *right == MAP_REC_RU) {
	    if (*left == MAP_REC_RD || *left == MAP_REC_RU) {
		*left = MAP_SPACE;
	    }
	}
	if (*left == MAP_FILLED) {
	    if (*right == MAP_REC_RD || *right == MAP_REC_RU) {
		*right = MAP_FILLED;
	    }
	}
	if (*left == MAP_SPACE
	    || *left == MAP_REC_LU
	    || *left == MAP_REC_LD) {
	    if (*right == MAP_REC_LD || *right == MAP_REC_LU) {
		*right = MAP_SPACE;
	    }
	}
	left += map2.linewidth;
	middle += map2.linewidth;
	right += map2.linewidth;
    }
    map2.data[map2.datasize] = '\0';

	
	header = (char *) malloc ( \
		strlen("Generated by MapXpress modified 'Wildmap' map generator\n") + \
		strlen("Generation Values used:\n") + \
		strlen("Width:          ") + 4 + \
		strlen("Height:         ") + 4 + \
		strlen("Seed:           ") + 20 + \
		strlen("Seed Ratio:     ") + 10 + \
		strlen("Fill Ratio:     ") + 10 + \
		strlen("Bases:          ") + 4 + \
		strlen("Teams:          ") + 4 + \
		strlen("Cannon Ratio:   ") + 10 + \
		strlen("Fuel Ratio:     ") + 10 + \
		strlen("Gravity Ratio:  ") + 10 + \
		strlen("Wormhole Ratio: ") + 10);

    sprintf(header, "Generated by MapXpress modified 'Wildmap' map generator\n\
Generation Values used:\n");

    sprintf(header, "%sWidth:          %d\n", header, map2.width);
    sprintf(header, "%sHeight:         %d\n", header, map2.height);
    sprintf(header, "%sSeed:           %d\n", header, map2.seed);
    sprintf(header, "%sSeed Ratio:     %f\n", header, map2.seed_ratio);
    sprintf(header, "%sFill Ratio:     %f\n", header, map2.fill_ratio);
    sprintf(header, "%sBases:          %d\n", header, map2.num_bases);
    sprintf(header, "%sTeams:          %d\n", header, map2.num_teams);
    sprintf(header, "%sCannon Ratio:   %f\n", header, map2.cannon_ratio);
    sprintf(header, "%sFuel Ratio:     %f\n", header, map2.fuel_ratio);
    sprintf(header, "%sGravity Ratio:  %f\n", header, map2.grav_ratio);
    sprintf(header, "%sWormhole Ratio: %f\n", header, map2.worm_ratio);

	if (lpMapDocData->MapStruct.comments == NULL)
		{
			lpMapDocData->MapStruct.comments = (char *) malloc(strlen(header));
			sprintf(lpMapDocData->MapStruct.comments,"%s\n",header);
		}
	else
		{
			tmp = (char *) malloc(strlen(lpMapDocData->MapStruct.comments)+strlen(header));
			sprintf(tmp,"%s%s\n",lpMapDocData->MapStruct.comments,header);
			free(lpMapDocData->MapStruct.comments);
			lpMapDocData->MapStruct.comments = tmp;
		}

	lpMapDocData->MapStruct.width = map2.width;
	sprintf(lpMapDocData->MapStruct.width_str,"%d",map2.width);
	lpMapDocData->MapStruct.height = map2.height;
	sprintf(lpMapDocData->MapStruct.height_str,"%d",map2.height);
	sprintf(lpMapDocData->MapStruct.mapName, "Wild Map %u", map2.seed);
	sprintf(lpMapDocData->MapStruct.mapAuthor, "The Wild Map Generator");
	lpMapDocData->MapStruct.edgeWrap = TRUE;
	lpMapDocData->MapStruct.teamPlay = TRUE;



	/*This block of code translates the map2.data...which is a single
	character string...into a widthxheight array..dropping the newlines.
	Do it this way because we don't really want to do a lot of editing
	to the main code of Wildmap,*/

	ch = x = y = 0;


	for (i = 0, ch = 0; i < map2.datasize; i++, ch++)
	{
		if (ch == map2.linewidth-1) /*If were at a newline*/
		{
			i++; /*move past the newline*/
			y++; /*move to next row of array*/
			x=0; /*move to first column of array*/
			ch = 0; /*reset our line width counter*/
		}

		lpMapDocData->MapStruct.data[x][y].cdata = map2.data[i];
		x++;
	}		
}

int wildmap(LPMAPDOCDATA lpMapDocData)
{
    Default_map();
    Option_map();
    Alloc_map();
    Generate_map();
    Connect_map();
    Partition_map();
    Smooth_map();
    Decorate_map();
    Print_map(lpMapDocData);
 
    return 0;
}
/***************************************************************************/
/* WildDlgProc                                                             */
/* Arguments :                                                             */
/*    hDlg                                                                 */
/*    iMsg                                                                 */
/*    wParam                                                               */
/*    lParam                                                               */
/* Purpose :   The procedure for the Wild Map prefs                        */
/***************************************************************************/
BOOL CALLBACK WildDlgProc (HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	char	temp[20];
	switch(iMsg)
	{
	case WM_INITDIALOG :
		SetEditText (hDlg, IDC_EDIT1, wmapwidth);	 
		SetEditText (hDlg, IDC_EDIT2, wmapheight);
		sprintf (temp, "%d", (unsigned)rand() ^ time(NULL) );
		SetEditText (hDlg, IDC_EDIT3, temp);	 
		SetEditText (hDlg, IDC_EDIT4, wmapseed_ratio);
		SetEditText (hDlg, IDC_EDIT5, wmapfill_ratio);	 
		SetEditText (hDlg, IDC_EDIT6, wmapnum_bases);
		SetEditText (hDlg, IDC_EDIT7, wmapnum_teams);	 
		SetEditText (hDlg, IDC_EDIT8, wmapcannon_ratio);
		SetEditText (hDlg, IDC_EDIT9, wmapfuel_ratio);	 
		SetEditText (hDlg, IDC_EDIT10, wmapgrav_ratio);
		SetEditText (hDlg, IDC_EDIT11, wmapworm_ratio);	 
		
		return TRUE;
		
	case WM_COMMAND :
		switch (LOWORD (wParam))
		{
		case IDOK: 
			UpdateMapPrefText(hDlg, IDC_EDIT1, wmapwidth);
			if (atoi(wmapwidth) > MAX_MAP_SIZE)
				itoa(MAX_MAP_SIZE, wmapwidth, 10);
			if (atoi(wmapwidth) < 1)
				itoa(0, wmapwidth, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT2, wmapheight);
			if (atoi(wmapheight) > MAX_MAP_SIZE)
				itoa(MAX_MAP_SIZE, wmapheight, 10);
			if (atoi(wmapheight) < 1)
				itoa(0, wmapheight, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT3, wmapseed);
			
			UpdateMapPrefText(hDlg, IDC_EDIT4, wmapseed_ratio);
			if (atof(wmapseed_ratio) > 0.22)
				sprintf(wmapseed_ratio, "%f", 0.22); //ftoa(0.22, wmapseed_ratio, 10);
			if (atof(wmapseed_ratio) < 0.02)
				sprintf(wmapseed_ratio, "%f", 0.02); //ftoa(0.02, wmapseed_ratio, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT5, wmapfill_ratio);
			if (atof(wmapfill_ratio) > 0.22)
				sprintf(wmapfill_ratio, "%f", 0.80); //ftoa(0.80, wmapfill_ratio, 10);
			if (atof(wmapfill_ratio) < 0.02)
				sprintf(wmapfill_ratio, "%f", 0.02); //ftoa(0.02, wmapfill_ratio, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT6, wmapnum_bases);
			if (atoi(wmapnum_bases) > 50)
				itoa(50, wmapnum_bases, 10);
			if (atoi(wmapnum_bases) < 2)
				itoa(2, wmapwidth, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT7, wmapnum_teams);
			if (atoi(wmapnum_teams) > 10)
				itoa(10, wmapnum_teams, 10);
			if (atoi(wmapnum_teams) < 3)
				itoa(3, wmapnum_teams, 10);
			
			UpdateMapPrefText(hDlg, IDC_EDIT8, wmapcannon_ratio);
			if (atof(wmapcannon_ratio) > 0.2)
				sprintf(wmapcannon_ratio, "%f", 0.2); 
			if (atof(wmapcannon_ratio) < 0.0)
				sprintf(wmapcannon_ratio, "%f", 0.0); 
			
			UpdateMapPrefText(hDlg, IDC_EDIT9, wmapfuel_ratio);
			if (atof(wmapfuel_ratio) > 0.1)
				sprintf(wmapfuel_ratio, "%f", 0.1);
			if (atof(wmapfuel_ratio) < 0.0)
				sprintf(wmapfuel_ratio, "%f", 0.0);
			
			UpdateMapPrefText(hDlg, IDC_EDIT10, wmapgrav_ratio);
			if (atof(wmapgrav_ratio) > 0.1)
				sprintf(wmapgrav_ratio, "%f", 0.1);
			if (atof(wmapgrav_ratio) < 0.0)
				sprintf(wmapgrav_ratio, "%f", 0.0);
			
			UpdateMapPrefText(hDlg, IDC_EDIT11, wmapworm_ratio);
			if (atof(wmapworm_ratio) > 0.1)
				sprintf(wmapworm_ratio, "%f", 0.1);
			if (atof(wmapworm_ratio) < 0.0)
				sprintf(wmapworm_ratio, "%f", 0.0);
			
			EndDialog (hDlg, 1);
			return TRUE;
		case IDCANCEL :
			EndDialog (hDlg, 0);
			return FALSE;
		}
		break;
	}
	return FALSE;
}


