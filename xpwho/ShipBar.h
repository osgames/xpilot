/***************************************************************************\
*  ShipBar.h : Toolbar with all of the ships on it                          *
*  $Id$  					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#ifndef	_SHIPBAR_H_
#define	_SHIPBAR_H_

class  ShipBar;

/////////////////////////////////////////////////////////////////////////////
// ShipList window
class ShipList : public CStatic
{
// Construction
public:
	ShipList();

// Attributes
public:
	ShipBar*	parent;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShipList)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~ShipList();

	// Generated message map functions
protected:
	//{{AFX_MSG(ShipList)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// ShipScrollBar window

class ShipScrollBar : public CScrollBar
{
// Construction
public:
	ShipScrollBar();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShipScrollBar)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~ShipScrollBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(ShipScrollBar)
	afx_msg void HScroll(UINT nSBCode, UINT nPos);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// ShipBar dialog
class ShipBar : public CDialogBar
{
// Construction
public:
	ShipBar(CWnd* pParent = NULL);   // standard constructor
	~ShipBar();

// Dialog Data
	//{{AFX_DATA(ShipBar)
	enum { IDD = IDD_SHIPBAR };
	ShipList	m_shiplist;
	ShipScrollBar	m_scrollbar;
	//}}AFX_DATA

	CEdit		m_name;				// displayed name
	CPtrArray	m_ships;
	int			nDefaultShip;		// index into m_ships
	void*		wDefaultShip;		// really a wireobj
	int			shipsOnScreen;		// # of ships on the screen
	CString		nick;				// current nick name
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ShipBar)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void	LoadShips(char* file, char* name);

	int		GetShipCount() { return(m_ships.GetSize()); }
	void*	GetShipAt(int _i) { return(m_ships.GetAt(_i)); }

	int		PaintShip(CPaintDC* dc, void* w, int dir, int c);
	void	SetDefaultShip(int i);
	int		GetDefaultShip() { return(nDefaultShip); }

	void	SetNick(CString	nick);
	void	SetNick(char* nick);
	CString	GetNick();
	void	SaveNickName();

	void	AnimateOn(int ship);
	void	AnimateOff();
	HCURSOR	oldCursor;

#define		SHIPTIMER	5
	int		aniShip;
	void*	aniWire;
	int		aniCol;
	CRect	aniRect;
	int		aniDir;
	int		aniInc;
	UINT	aniTimer;
	bool	aniOn;
protected:
	// Generated message map functions
	//{{AFX_MSG(ShipBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	DECLARE_MESSAGE_MAP()

};


/////////////////////////////////////////////////////////////////////////////
#endif
