/***************************************************************************\
*  XPwhoView.cpp : The View for XPwho                                       *
*  $Id$   				*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.6  2002/05/24 17:28:14  dik
 *  Make persistent the user's display of the statusbar and toolbar.
 *
 *  Revision 1.5  2002/05/13 18:16:54  dik
 *  Bugfix: If you changed your nick (or ship) and then clicked to join a
 *  specific team, the ini file wasn't saved first.
 *
 *  Revision 1.4  2001/07/02 15:32:34  dik
 *  Watches and dings now support different port numbers for the same server.
 *
 *  Revision 1.3  2001/05/17 19:23:43  dik
 *  Use shipobj instead of wireobj
 *
 *  Revision 1.2  2001/05/07 08:10:56  dik
 *  Honor the "primary meta" setting.  Display which meta we are talking to in
 *  the status bar.
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:12  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.7  2001/04/03 12:42:13  dick
 *  From Jarno:
 *  Here is a (temporary) fix to join server which run other than the
 *  default port and use IP instead of servername to join.
 *
 *  XPwho now "sees" the differences between two servers running under
 *  different ports
 *
 *  XPwho now adjusts the xpilot.ini file and sets the port there. (not a
 *  very nice way to do it, but the simplest).
 *  Making XPilot itself accept the -port option should be a much more
 *  graceful way.
 *
 *  XPwho now uses IP-addresses to join servers, rather than their
 *  servername. You don't have to use "Start --> Run..." anymore to do that.
 *
 *  Revision 1.6  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

#include "stdafx.h"
#include "XPwhoApp.h"

#include "ServerSite.h"
#include "Config.h"
#include "UserName.h"
#include "XPwhoDoc.h"
#include "XPwhoView.h"
#include "SysInfo.h"
#include "xp-version.h"		/* yowsa! crossing project boundaries!!! */
#include "xp-const.h"
#include "xp-draw.h"

#include <io.h>
#include <mmsystem.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// XPwhoView

IMPLEMENT_DYNCREATE(XPwhoView, XPwhoViewSUPERCLASS)

BEGIN_MESSAGE_MAP(XPwhoView, XPwhoViewSUPERCLASS)
	//{{AFX_MSG_MAP(XPwhoView)
	ON_WM_DESTROY()
	ON_WM_CREATE()
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_UPDATE_COMMAND_UI(ID_VIEW_USERNAME, OnUpdateViewUserName)
	ON_UPDATE_COMMAND_UI(ID_VIEW_CONFIG, OnUpdateViewConfig)
	ON_COMMAND(ID_VIEW_USERNAME, OnViewUserName)
	ON_COMMAND(ID_VIEW_CONFIG, OnViewConfig)
	ON_WM_TIMER()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_SETCURSOR()
	ON_UPDATE_COMMAND_UI(ID_VIEW_PLAYERS, OnUpdateViewPlayers)
	ON_COMMAND(ID_VIEW_PLAYERS, OnViewPlayers)
	ON_UPDATE_COMMAND_UI(ID_VIEW_WATCH, OnUpdateViewWatch)
	ON_COMMAND(ID_VIEW_WATCH, OnViewWatch)
	ON_WM_SIZE()
	ON_UPDATE_COMMAND_UI(ID_SERVER, OnUpdateServer)
	ON_COMMAND(ID_SERVER, OnServer)
	ON_UPDATE_COMMAND_UI(ID_VIEW_DING, OnUpdateViewDing)
	ON_COMMAND(ID_VIEW_DING, OnViewDing)
	ON_UPDATE_COMMAND_UI(ID_JOINBUTT, OnUpdateJoinbutt)
	ON_COMMAND(ID_JOINBUTT, OnJoinbutt)
	ON_UPDATE_COMMAND_UI(ID_JOINTEAM, OnUpdateJointeam)
	ON_COMMAND(ID_JOINTEAM, OnJointeam)
	ON_UPDATE_COMMAND_UI(ID_VIEW_COLUMNS, OnUpdateViewColumns)
	ON_COMMAND(ID_VIEW_COLUMNS, OnViewColumns)
	ON_UPDATE_COMMAND_UI(ID_TEAM_1, OnUpdateTeam1)
	ON_COMMAND(ID_TEAM_1, OnTeam1)
	ON_UPDATE_COMMAND_UI(ID_TEAM_2, OnUpdateTeam2)
	ON_COMMAND(ID_TEAM_2, OnTeam2)
	ON_UPDATE_COMMAND_UI(ID_TEAM_3, OnUpdateTeam3)
	ON_COMMAND(ID_TEAM_3, OnTeam3)
	ON_UPDATE_COMMAND_UI(ID_TEAM_4, OnUpdateTeam4)
	ON_COMMAND(ID_TEAM_4, OnTeam4)
	ON_UPDATE_COMMAND_UI(ID_TEAM_5, OnUpdateTeam5)
	ON_COMMAND(ID_TEAM_5, OnTeam5)
	ON_UPDATE_COMMAND_UI(ID_TEAM_6, OnUpdateTeam6)
	ON_COMMAND(ID_TEAM_6, OnTeam6)
	ON_UPDATE_COMMAND_UI(ID_TEAM_7, OnUpdateTeam7)
	ON_COMMAND(ID_TEAM_7, OnTeam7)
	ON_UPDATE_COMMAND_UI(ID_TEAM_8, OnUpdateTeam8)
	ON_COMMAND(ID_TEAM_8, OnTeam8)
	ON_UPDATE_COMMAND_UI(ID_TEAM_9, OnUpdateTeam9)
	ON_COMMAND(ID_TEAM_9, OnTeam9)
	ON_WM_LBUTTONDBLCLK()
	ON_WM_CHAR()
	ON_WM_KEYDOWN()
	ON_UPDATE_COMMAND_UI(ID_VIEW_INI, OnUpdateViewIni)
	ON_COMMAND(ID_VIEW_INI, OnViewIni)
	ON_COMMAND(ID_APP_HOME, OnAppHome)
	ON_COMMAND(ID_APP_BUCKOHOME, OnAppBuckohome)
	ON_COMMAND(ID_VIEW_SHIPLIST, OnViewShiplist)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHIPLIST, OnUpdateViewShiplist)
	ON_UPDATE_COMMAND_UI(ID_CONFIGURE_SYSINFO, OnUpdateConfigureSysinfo)
	ON_COMMAND(ID_CONFIGURE_SYSINFO, OnConfigureSysinfo)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, XPwhoViewSUPERCLASS::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, XPwhoViewSUPERCLASS::OnFilePrintPreview)

#if _USESOCK
	ON_MESSAGE(WSA_EVENT, OnWSA_EVENT)
	ON_MESSAGE(WSA_RECV, OnWSA_RECV)
	ON_MESSAGE(WSA_CONNECT, OnWSA_CONNECT)
	ON_MESSAGE(WSA_RESOLVEHOST, OnWSA_RESOLVEHOST)
#endif

END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// XPwhoView construction/destruction

XPwhoView::XPwhoView()
{
	// add construction code here
	namebuf = NULL;
	recvbuf = NULL;
	activeMeta = 0;
#if	_USESOCK
	mysocket = NULL;
#endif
	selection = NULL;
	resize_grabbed = FALSE;
	resize_cursor_on = FALSE;
	resize_cursor = ::LoadCursor(NULL, IDC_SIZEWE);
	lineheight = 20;
}

XPwhoView::~XPwhoView()
{
#if	_USESOCK
	if (mysocket)
	{
		closesocket(mysocket);
	}
#endif
	if (namebuf)
		delete namebuf;
	if (recvbuf)
		delete recvbuf;
}

/////////////////////////////////////////////////////////////////////////////
// XPwhoView diagnostics

#ifdef _DEBUG
void XPwhoView::AssertValid() const
{
	XPwhoViewSUPERCLASS::AssertValid();
}

void XPwhoView::Dump(CDumpContext& dc) const
{
	XPwhoViewSUPERCLASS::Dump(dc);
	dc << "View";
}

XPwhoDoc* XPwhoView::GetDoc() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(XPwhoDoc)));
	return (XPwhoDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////

char	s_Settings[] = "Settings";
char	s_Columns[] = "Columns";
char	s_WindowMet[] = "Window Metrics";
char	s_Watches[] = "Watches";
char	s_Inis[] = "Server Ini";
char	s_Dings[] = "Dings";
char	s_DingWave[] = "Ding Wave";
char	s_ShowShips[] = "Show Ships";
char	s_ShowTools[] = "Show Tools";
char	s_ShowStatus[] = "Show Status";
char	s_DT_Ding[] = "DT_Ding";
char	s_DT_Focus[] = "DT_Focus";
char	s_DT_MessageBox[] = "DT_MessageBox";

char	s_UpdateMinutes[] = "UpdateMinutes";
char	s_MetaHost[] = "MetaHost";
char	s_MetaHost2[] = "MetaHost2";
char	s_Primary[] = "Primary Meta";
char	s_SocksHostName[] = "Socks Host";
char	s_Update[] = "Update";
char	s_Sort[] = "Sort";
char	s_Distance[] = "Distance";
char	s_Players[] = "Show Players";
char	s_Watchtop[] = "Watches on Top";
char	s_SocksSock[] = "Socks Socket";
char	s_XPilotNTApp[] = "XPilot App";
char	s_userUser[] = "User Name";
char	s_userHost[] = "User Host";

char	s_bases[] = "Bases";
char	s_fps[] = "FPS";
char	s_ip[] = "IP";
char	s_mapauthor[] = "Map Author";
char	s_mapname[] = "Map Name";
char	s_mapsize[] = "Map Size";
char	s_servername[] = "Server Name";
char	s_playercount[] = "Playercount";
char	s_sound[] = "Sound";
char	s_teams[] = "Teams";
char	s_timing[] = "Timing";
char	s_uptime[] = "Uptime";
char	s_version[] = "Version";
char	s_port[] = "Port";

char	s_L[] = "Left";
char	s_T[] = "Top";
char	s_R[] = "Right";
char	s_B[] = "Bottom";
char	s_Count[] = "TheCount";
char	s_Unknown[] = "Unknown";


///////////////////////////////////////////////////////////////////////////////
// XPwhoView message handlers

///////////////////////////////////////////////////////////////////////////////
int XPwhoView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (XPwhoViewSUPERCLASS::OnCreate(lpCreateStruct) == -1)
		return -1;
	XPwhoDoc* pDoc = GetDoc();
	
	MainFrame*	f = (MainFrame*)GetParent();

	// Add your specialized creation code here
	int		watchcount;

	f->shipbarOnScreen = theApp.GetProfileInt(s_Settings, s_ShowShips, TRUE);
	f->toolbarOnScreen = theApp.GetProfileInt(s_Settings, s_ShowTools, TRUE);
	f->statusbarOnScreen = theApp.GetProfileInt(s_Settings, s_ShowStatus, TRUE);

	// default column values
	char	s[20];
//	int		w[MAXCOLUMNS] = { 220, 200, 20, 20, 20 };
	int		w[MAXCOLUMNS] = { 0, 220, 420, 526, 581, 601, 623, 656, 676, 692, 712, 732, 752, 800 };
	int		i;
	for (i=0; i<MAXCOLUMNS; i++)
	{
//		columnwidths[i] = w[i];
//		columnwidths[i] = w[i];
		columnx[i] = theApp.GetProfileInt(s_Columns, itoa(i, s, 10), w[i]);
	}
	pDoc->ccol.m_servername = theApp.GetProfileInt(s_Columns, s_servername, TRUE);
	pDoc->ccol.m_mapname = theApp.GetProfileInt(s_Columns, s_mapname, TRUE);
	pDoc->ccol.m_mapauthor = theApp.GetProfileInt(s_Columns, s_mapauthor, TRUE);
	pDoc->ccol.m_mapsize = theApp.GetProfileInt(s_Columns, s_mapsize, TRUE);
	pDoc->ccol.m_playercount = theApp.GetProfileInt(s_Columns, s_playercount, TRUE);
	pDoc->ccol.m_bases = theApp.GetProfileInt(s_Columns, s_bases, FALSE);
	pDoc->ccol.m_teams = theApp.GetProfileInt(s_Columns, s_teams, FALSE);
	pDoc->ccol.m_fps = theApp.GetProfileInt(s_Columns, s_fps, TRUE);
	pDoc->ccol.m_version = theApp.GetProfileInt(s_Columns, s_version, TRUE);
	pDoc->ccol.m_sound = theApp.GetProfileInt(s_Columns, s_sound, FALSE);
	pDoc->ccol.m_uptime = theApp.GetProfileInt(s_Columns, s_uptime, FALSE);
	pDoc->ccol.m_ip = theApp.GetProfileInt(s_Columns, s_ip, FALSE);
	pDoc->ccol.m_timing = theApp.GetProfileInt(s_Columns, s_timing, TRUE);
	pDoc->ccol.m_port = theApp.GetProfileInt(s_Columns, s_port, FALSE);

	// default configuration values
	pDoc->viewfont.lf.lfHeight = 16;
	pDoc->viewfont.lf.lfPitchAndFamily = FF_SWISS;

	pDoc->ccfg.m_update = theApp.GetProfileInt(s_Settings, s_Update, TRUE);
	pDoc->ccfg.m_minutes = theApp.GetProfileInt(s_Settings, s_UpdateMinutes, 5);
//	pDoc->ccfg.m_sort = theApp.GetProfileInt(s_Settings, s_Sort, 1);
//	pDoc->ccfg.m_distance = theApp.GetProfileInt(s_Settings, s_Distance, 0);
//	pDoc->ccfg.m_watchtop = theApp.GetProfileInt(s_Settings, s_Watchtop, TRUE);
	pDoc->ccfg.m_hostname = theApp.GetProfileString(s_Settings, s_MetaHost, "meta.xpilot.org");
	pDoc->ccfg.m_hostname2 = theApp.GetProfileString(s_Settings, s_MetaHost2, "meta2.xpilot.org");
	pDoc->ccfg.m_primary = theApp.GetProfileInt(s_Settings, s_Primary, 0);
	activeMeta = pDoc->ccfg.m_primary ? true : false;
	pDoc->ccfg.m_players = theApp.GetProfileInt(s_Settings, s_Players, 1);
//	pDoc->ccfg.m_sockshostname = theApp.GetProfileString(s_Settings, s_SocksHostName, "");
//	pDoc->ccfg.m_sockssock = theApp.GetProfileInt(s_Settings, s_SocksSock, 1080);
	pDoc->ccfg.m_xpilotntapp = theApp.GetProfileString(s_Settings, s_XPilotNTApp, "C:\\XPilot\\XPilot.exe");
	pDoc->ccfg.m_ding_wave = theApp.GetProfileString(s_Settings, s_DingWave, "");
	pDoc->ccfg.m_dt_ding = theApp.GetProfileInt(s_Settings, s_DT_Ding, FALSE);
	pDoc->ccfg.m_dt_focus = theApp.GetProfileInt(s_Settings, s_DT_Focus, FALSE);
	pDoc->ccfg.m_dt_messagebox = theApp.GetProfileInt(s_Settings, s_DT_MessageBox, FALSE);

	pDoc->userUser = theApp.GetProfileString(s_Settings, s_userUser);
	pDoc->userHost = theApp.GetProfileString(s_Settings, s_userHost);

	CWnd* pWnd = GetParent();
	CRect rect;
	rect.left = theApp.GetProfileInt(s_WindowMet, s_L, 0);
	rect.top = theApp.GetProfileInt(s_WindowMet, s_T, 0);
	rect.right = theApp.GetProfileInt(s_WindowMet, s_R, 0);
	rect.bottom = theApp.GetProfileInt(s_WindowMet, s_B, 0);
	if (rect.left != rect.right)	// only move window to valid coordinates
		pWnd->MoveWindow(rect);

	watchcount = theApp.GetProfileInt(s_Watches, s_Count, 0);
	for (i=0; i<watchcount; i++)
	{
		Watch*	cw;
		cw = new Watch;
		cw->servername = theApp.GetProfileString(s_Watches, itoa(i, s, 10), s_Unknown);
		strcat(s, "p");
		cw->serverport = theApp.GetProfileInt(s_Watches, s, 15345);
		pDoc->watchlist.AddTail(cw);

	}
	watchcount = theApp.GetProfileInt(s_Inis, s_Count, 0);
	for (i=0; i<watchcount; i++)
	{
		Watch*	cw;
		cw = new Watch;
		cw->servername = theApp.GetProfileString(s_Inis, itoa(i, s, 10), s_Unknown);
		strcat(s, "p");
		cw->serverport = theApp.GetProfileInt(s_Inis, s, 15345);
		pDoc->inilist.AddTail(cw);

	}
	watchcount = theApp.GetProfileInt(s_Dings, s_Count, 0);
	for (i=0; i<watchcount; i++)
	{
		Watch*	cw;
		cw = new Watch;
		cw->servername = theApp.GetProfileString(s_Dings, itoa(i, s, 10), s_Unknown);
		strcat(s, "p");
		cw->serverport = theApp.GetProfileInt(s_Dings, s, 15345);
		pDoc->dinglist.AddTail(cw);

	}

	// find the shipshape file
	CString	inifile = theApp.GetProfileString(s_Settings, s_XPilotNTApp, "C:\\XPilot\\XPilot.exe");;
	inifile = inifile.Left(inifile.ReverseFind('\\'));
	inifile += "\\XPilot.ini";
	char	shipfile[128];
	char	shipname[64];
	GetPrivateProfileString("Settings", "shipShapeFile", "", shipfile, 127, (const char*)inifile);
	GetPrivateProfileString("Settings", "shipShape", "", shipname, 63, (const char*)inifile);

	((MainFrame*)GetParent())->m_shipBar.LoadShips(shipfile, shipname);
	GetPrivateProfileString("Settings", "name", "", shipname, 63, (const char*)inifile);
	((MainFrame*)GetParent())->m_shipBar.SetNick(shipname);

	SetStatusActiveServer(GetHost());
	SetState(WS_INIT);
	virgin = TRUE;
//	SetTimer(WS_TIMER, WST_INIT, NULL);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnDestroy() 
{
	
	// Add your message handler code here
	char	s[20];

	XPwhoDoc* pDoc = GetDoc();

	UpdateXPilotIni();
	theApp.WriteProfileInt(s_Columns, s_bases, pDoc->ccol.m_bases);
	theApp.WriteProfileInt(s_Columns, s_fps, pDoc->ccol.m_fps);
	theApp.WriteProfileInt(s_Columns, s_ip, pDoc->ccol.m_ip);
	theApp.WriteProfileInt(s_Columns, s_mapauthor, pDoc->ccol.m_mapauthor);
	theApp.WriteProfileInt(s_Columns, s_mapname, pDoc->ccol.m_mapname);
	theApp.WriteProfileInt(s_Columns, s_mapsize, pDoc->ccol.m_mapsize);
	theApp.WriteProfileInt(s_Columns, s_servername, pDoc->ccol.m_servername);
	theApp.WriteProfileInt(s_Columns, s_playercount, pDoc->ccol.m_playercount);
	theApp.WriteProfileInt(s_Columns, s_sound, pDoc->ccol.m_sound);
	theApp.WriteProfileInt(s_Columns, s_teams, pDoc->ccol.m_teams);
	theApp.WriteProfileInt(s_Columns, s_timing, pDoc->ccol.m_timing);
	theApp.WriteProfileInt(s_Columns, s_uptime, pDoc->ccol.m_uptime);
	theApp.WriteProfileInt(s_Columns, s_version, pDoc->ccol.m_version);
	theApp.WriteProfileInt(s_Columns, s_port, pDoc->ccol.m_port);

	theApp.WriteProfileInt(s_Settings, s_Update, pDoc->ccfg.m_update);
	theApp.WriteProfileInt(s_Settings, s_UpdateMinutes, pDoc->ccfg.m_minutes);
//	theApp.WriteProfileInt(s_Settings, s_Sort, pDoc->ccfg.m_sort);
//	theApp.WriteProfileInt(s_Settings, s_Distance, pDoc->ccfg.m_distance);
//	theApp.WriteProfileInt(s_Settings, s_Watchtop, pDoc->ccfg.m_watchtop);
	theApp.WriteProfileString(s_Settings, s_MetaHost, (LPCTSTR)pDoc->ccfg.m_hostname);
	theApp.WriteProfileString(s_Settings, s_MetaHost2, (LPCTSTR)pDoc->ccfg.m_hostname2);
	theApp.WriteProfileInt(s_Settings, s_Primary, pDoc->ccfg.m_primary);
	theApp.WriteProfileInt(s_Settings, s_Players, pDoc->ccfg.m_players);
//	theApp.WriteProfileString(s_Settings, s_SocksHostName, (LPCTSTR)pDoc->ccfg.m_sockshostname);
//	theApp.WriteProfileInt(s_Settings, s_SocksSock, pDoc->ccfg.m_sockssock);
	theApp.WriteProfileString(s_Settings, s_XPilotNTApp, pDoc->ccfg.m_xpilotntapp);
	theApp.WriteProfileString(s_Settings, s_DingWave, pDoc->ccfg.m_ding_wave);
	theApp.WriteProfileInt(s_Settings, s_DT_Ding, pDoc->ccfg.m_dt_ding);
	theApp.WriteProfileInt(s_Settings, s_DT_Focus, pDoc->ccfg.m_dt_focus);
	theApp.WriteProfileInt(s_Settings, s_DT_MessageBox, pDoc->ccfg.m_dt_messagebox);

	theApp.WriteProfileString(s_Settings, s_userUser, (LPCTSTR)pDoc->userUser);
	theApp.WriteProfileString(s_Settings, s_userHost, (LPCTSTR)pDoc->userHost);


	MainFrame*	f = (MainFrame*)GetParent();

	theApp.WriteProfileInt(s_Settings, s_ShowShips, f->shipbarOnScreen);
	theApp.WriteProfileInt(s_Settings, s_ShowTools, f->toolbarOnScreen);
	theApp.WriteProfileInt(s_Settings, s_ShowStatus, f->statusbarOnScreen);

	for (int i=0; i<MAXCOLUMNS; i++)
		theApp.WriteProfileInt(s_Columns, itoa(i, s, 10), columnx[i]);	

	CWnd* pWnd = GetParent();
	CRect rect;
	pWnd->GetWindowRect(rect);
	WINDOWPLACEMENT	wp;
	pWnd->GetWindowPlacement(&wp);
	if (wp.showCmd != SW_SHOWMINIMIZED)
	{
		theApp.WriteProfileInt(s_WindowMet, s_L, rect.left);
		theApp.WriteProfileInt(s_WindowMet, s_T, rect.top);
		theApp.WriteProfileInt(s_WindowMet, s_R, rect.right);
		theApp.WriteProfileInt(s_WindowMet, s_B, rect.bottom);
	}

	theApp.WriteProfileInt(s_Watches, s_Count, pDoc->watchlist.GetCount());
	i = 0;
	while (!pDoc->watchlist.IsEmpty())
	{
		Watch* cw = (Watch*)pDoc->watchlist.RemoveHead();
		theApp.WriteProfileString(s_Watches, itoa(i, s, 10), cw->servername);
		strcat(s, "p");
		theApp.WriteProfileInt(s_Watches, s,cw->serverport);
		delete cw;
		i++;
	}

	theApp.WriteProfileInt(s_Inis, s_Count, pDoc->inilist.GetCount());
	i = 0;
	while (!pDoc->inilist.IsEmpty())
	{
		Watch* cw = (Watch*)pDoc->inilist.RemoveHead();
		theApp.WriteProfileString(s_Inis, itoa(i, s, 10), cw->servername);
		strcat(s, "p");
		theApp.WriteProfileInt(s_Inis, s,cw->serverport);
		delete cw;
		i++;
	}

	theApp.WriteProfileInt(s_Dings, s_Count, pDoc->dinglist.GetCount());
	i = 0;
	while (!pDoc->dinglist.IsEmpty())
	{
		Watch* cw = (Watch*)pDoc->dinglist.RemoveHead();
		theApp.WriteProfileString(s_Dings, itoa(i, s, 10), cw->servername);
		strcat(s, "p");
		theApp.WriteProfileInt(s_Dings, s,cw->serverport);
		delete cw;
		i++;
	}

	XPwhoViewSUPERCLASS::OnDestroy();
}


///////////////////////////////////////////////////////////////////////////////
// XPwhoView drawing

void XPwhoView::OnSize(UINT nType, int cx, int cy) 
{
	XPwhoViewSUPERCLASS::OnSize(nType, cx, cy);
	
	// Add your message handler code here
	UpdateScrollSizes();	
	
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	TRACE("XPwhoView::OnUpdate\n");
	UpdateScrollSizes();
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::UpdateScrollSizes()
{
	ServerSite*	css;
	CRect			rect;
	CSize			size;
	int				entries = 0;
	POSITION		pos;
	XPwhoDoc* pDoc = GetDoc();

	GetClientRect(rect);
	size.cx = rect.right;

	for (pos = pDoc->serverlist.GetHeadPosition(); pos != NULL; entries++)
	{
		css = (ServerSite*)pDoc->serverlist.GetNext(pos);
		if (pDoc->ccfg.m_players)
			entries += css->playerlist.GetCount();
	}
//	entries = pDoc->serverlist.GetCount() +1;
	size.cy = lineheight*entries;
	SetScrollSizes(MM_TEXT, size);
	TRACE("UpdateScrollSizes:: to %d/%d\n", size.cx, size.cy);
}

///////////////////////////////////////////////////////////////////////////////
static	const char*	coltitles[] = {
	"server",
	"map",
	"author",
	"size",
	"#",
	"b",
	"t",
	"fps",
	"ver",
	"snd",
	"uptime",
	"IP",
	"race",
	"port"
};

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnDraw(CDC* pDC)
{
	XPwhoDoc* pDoc = GetDoc();
	ASSERT_VALID(pDoc);

	// add draw code for native data here
	ServerSite*	css;
	Player*		cp;
	POSITION		pos;
	POSITION		ppos;		// for walking the player list
//	CString			cs;
	int				line;
	int				lineheight = 20;
	int				i, j;
	CFont			font;
	CFont*			oldfont;
	CBrush			gbrush;
	CBrush			yelbrush;
	CBrush*			oldbrush;
	CBrush*			abrush;
	CPen			blkpen;
	CPen			grnpen;
	CPen			redpen;
	CPen			chgpen;		// yellow or purple, depending on my mood
	CPen*			oldpen;
	COLORREF		oldclr;
	CRect			rect;
	CRect			crect;		// clipping rect
	CRect			elrect;		// ellipse rect
	char			s[128];

	BOOL			cols[] = {
		pDoc->ccol.m_servername,
		pDoc->ccol.m_mapname,
		pDoc->ccol.m_mapauthor,
		pDoc->ccol.m_mapsize,
		pDoc->ccol.m_playercount,
		pDoc->ccol.m_bases,
		pDoc->ccol.m_teams,
		pDoc->ccol.m_fps,
		pDoc->ccol.m_version,
		pDoc->ccol.m_sound,
		pDoc->ccol.m_uptime,
		pDoc->ccol.m_ip,
		pDoc->ccol.m_timing,
		pDoc->ccol.m_port
	};


	MainFrame*	f = (MainFrame*)GetParent();
	if (f->virgin)
	{
		if (!f->shipbarOnScreen)
		{
			f->m_shipBar.ShowWindow(SW_HIDE);
			f->RecalcLayout();
		}
		if (!f->toolbarOnScreen)
		{
			f->m_toolBar.ShowWindow(SW_HIDE);
			f->RecalcLayout();
		}
		if (!f->statusbarOnScreen)
		{
			f->m_statusBar.ShowWindow(SW_HIDE);
			f->RecalcLayout();
		}

		f->virgin = FALSE;
	}

#ifdef	WIN32
	GetParentFrame()->SetWindowText("XPwho");
#else
	GetParentFrame()->SetWindowText("xpwho");
#endif
	GetClientRect(rect);

	gbrush.CreateSolidBrush(RGB(192,192,192));
	oldbrush = pDC->SelectObject(&gbrush);

	font.CreateFontIndirect(&pDoc->viewfont.lf);
	oldfont = pDC->SelectObject(&font);

	yelbrush.CreateSolidBrush(RGB(255,255,0));
	blkpen.CreatePen(PS_SOLID, 1, RGB(0,0,0));
	grnpen.CreatePen(PS_SOLID, 1, RGB(0,128,0));
	redpen.CreatePen(PS_SOLID, 1, RGB(255,0,0));
	chgpen.CreatePen(PS_SOLID, 1, RGB(255,0,255));
	oldpen = pDC->SelectObject(&blkpen);
	oldclr = pDC->SetTextColor(RGB(0,0,0));

	CRect r(0, 0, rect.right, pDoc->viewfont.lf.lfHeight);
	pDC->FillRect(&r, &gbrush);
	pDC->SetBkColor(RGB(192, 192, 192));
	for (i=0, j=0; i<MAXCOLUMNS; i++)
	{
		if (cols[i])
		{
			pDC->TextOut(columnx[j]+1, 0, coltitles[i], strlen(coltitles[i]));
			j++;
		}
	}

	// create the ding draw object
	CDC dingDC;
	dingDC.CreateCompatibleDC(pDC);
	CBitmap* oBMP;
	CBitmap dingBMP;
	dingBMP.LoadBitmap(IDB_DING);
	oBMP = dingDC.SelectObject(&dingBMP);

	pDC->SetBkColor(RGB(255,255,255));
	for (line=1, pos = pDoc->serverlist.GetHeadPosition(); pos != NULL; line++)
	{
		COLORREF	tcolor;
		CBrush		bkbrush;

		css = (ServerSite*)pDoc->serverlist.GetNext(pos);
//		TRACE("Draw:: server <%s>\n", (const char*)css->servername);
		crect.top = line*lineheight;
		crect.bottom = (line+1)*lineheight;


		if (css == GetSelection())
		{
			pDC->SetBkColor(GetSysColor(COLOR_HIGHLIGHT));
			pDC->SetTextColor(GetSysColor(COLOR_HIGHLIGHTTEXT));
			bkbrush.CreateSolidBrush(GetSysColor(COLOR_HIGHLIGHT));
		}
		else
		{
			if (css->GetNew())
				tcolor = RGB(0,128,0);
			else if (css->GetDel())
				tcolor = RGB(255,0,0);
			else if (css->map_changed)
				tcolor = RGB(255,0,255);
			else
				tcolor = GetSysColor(COLOR_WINDOWTEXT);
			pDC->SetBkColor(GetSysColor(COLOR_WINDOW));
			pDC->SetTextColor(tcolor);
			bkbrush.CreateSolidBrush(GetSysColor(COLOR_WINDOW));
		}

		crect.left = columnx[0];
		crect.right = columnx[MAXCOLUMNS-1];

		pDC->FillRect(&crect, &bkbrush);

		for (i=0, j=0; i<MAXCOLUMNS; i++)
		{
			if (cols[i])
			{
				CString	cs;
				crect.left = columnx[j];
				crect.right = columnx[j+1];
				j++;
				switch (i)
				{
				case COL_NAME:
					cs = css->servername;
					break;
				case COL_MAP:
					cs = css->mapname;
					break;
				case COL_AUTHOR:
					cs = css->author;
					break;
				case COL_SIZE:
					cs = css->mapsize;
					break;
				case COL_PCOUNT:
					cs = ltoa(css->playercount, s, 10);
					break;
				case COL_BASES:
					cs = ltoa(css->bases, s, 10);
					break;
				case COL_TEAMS:
					cs = ltoa(css->teams, s, 10);
					break;
				case COL_FPS:
					cs = ltoa(css->fps, s, 10);
					break;
				case COL_VERSION:
					cs = css->version;
					break;
				case COL_SOUND:
					cs = css->sound;
					break;
				case COL_IP:
					cs = css->serverip;
					break;
				case COL_TIMING:
					cs = css->timing ? "Y" : "N";
					break;
				case COL_UPTIME:
//					cs = ltoa(css->uptime, s, 10);
					int	secs, mins, hrs, days;
					secs = css->uptime % 60;
					mins = css->uptime / 60;
					mins = mins % 60;
					hrs = css->uptime / 3600;
					days = hrs / 24;
					hrs  = hrs % 24;
					cs.Format("%d:%02d:%02d.%02d", days, hrs, mins, secs);
					break;
				case COL_PORT:
					cs = ltoa(css->serverport, s, 10);
					break;
				}
				pDC->ExtTextOut(crect.left+2, line*lineheight, ETO_CLIPPED, crect,
					cs, NULL);
			}
		}

		// draw the watch circle if need be.
		for (POSITION wpos=pDoc->watchlist.GetHeadPosition(); wpos != NULL;)
		{
			Watch* cw = (Watch*)pDoc->watchlist.GetNext(wpos);
			if (*cw == *css)
			{
				elrect.top = crect.top+3;
				elrect.bottom = crect.bottom-3;
				elrect.right = columnx[2]-3;
				elrect.left = elrect.right - (elrect.bottom - elrect.top);
				abrush = pDC->SelectObject(&yelbrush);
				pDC->Ellipse(elrect);
				pDC->SelectObject(abrush);
				break;
			}
		}

		// draw the ding note if need be.
		for (wpos=pDoc->dinglist.GetHeadPosition(); wpos != NULL;)
		{
			Watch* cw = (Watch*)pDoc->dinglist.GetNext(wpos);
			if (*cw == *css)
			{
				int	oldMode;
//				COLORREF oldColor;
				oldMode = pDC->SetBkMode(NEWTRANSPARENT);
//				oldColor = pDC->SetBkColor(RGB(0xff,0xff,0xff));
				elrect.top = crect.top+3;
				elrect.bottom = crect.bottom-3;
				elrect.right = columnx[2]-3;		// calc where watch would go.
				elrect.left = elrect.right - (elrect.bottom - elrect.top);
				elrect.right = elrect.left - 2;		// and move left from there
				elrect.left = elrect.right - 16;
				pDC->BitBlt(elrect.left, elrect.top, 16, 15, &dingDC, 0,0,SRCCOPY);
//				pDC->SetBkColor(oldColor);
				pDC->SetBkMode(oldMode);
				break;
			}
		}


		// draw the little hash marks between columns
		// (j has how many columns we drew)
		for (i=0; i<=j; i++)
		{
			pDC->MoveTo(columnx[i], line*lineheight);
			pDC->LineTo(columnx[i], (line+1)*lineheight);
		}
		pDC->SetBkColor(GetSysColor(COLOR_WINDOW));

		// draw the players
		if (pDoc->ccfg.m_players)
		{
			for (ppos=css->playerlist.GetHeadPosition(); ppos != NULL;)
			{
				line++;
				cp = (Player*)css->playerlist.GetNext(ppos);
//				if (tcolor != RGB(0,0,0))
				if (cp->GetNew())
					pDC->SetTextColor(RGB(0,128,0));
				else if (cp->GetDel())
					pDC->SetTextColor(RGB(255,0,0));
				else
					pDC->SetTextColor(GetSysColor(COLOR_WINDOWTEXT));
				pDC->TextOut(30, line*lineheight, cp->name);
				pDC->SetTextColor(RGB(0,0,0));
			}
		}

		// draw the line at the bottom
		pDC->MoveTo(rect.left, (line+1)*lineheight-1);
		pDC->LineTo(rect.right, (line+1)*lineheight-1);
	}
	
	pDC->SetTextColor(oldclr);
	pDC->SelectObject(oldpen);
	pDC->SelectObject(oldfont);
	pDC->SelectObject(oldbrush);
	dingDC.SelectObject(oBMP);
	DeleteObject(dingBMP);

}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateServer(CCmdUI* pCmdUI) 
{
	// Add your command update UI handler code here
	pCmdUI->Enable();
}

/////////////////////////////////////////////////////////////////////////////
// XPwhoView printing

BOOL XPwhoView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void XPwhoView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void XPwhoView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

///////////////////////////////////////////////////////////////////////////////
// load our config values
void XPwhoView::OnFileOpen() 
{
	XPwhoDoc* pDoc = GetDoc();
	pDoc->viewfont.lf.lfHeight = 8;
	
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewUserName(CCmdUI* pCmdUI) 
{
	//pCmdUI->Enable();
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewConfig(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewPlayers(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
	pCmdUI->SetCheck(GetDoc()->ccfg.m_players);
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewWatch(CCmdUI* pCmdUI) 
{
	XPwhoDoc* pDoc = GetDoc();
	bool	match = FALSE;

	if (selection)
	{
		pCmdUI->Enable();

		for (POSITION wpos=pDoc->watchlist.GetHeadPosition(); wpos != NULL;)
		{
			Watch* cw = (Watch*)pDoc->watchlist.GetNext(wpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		pCmdUI->SetCheck(match);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewIni(CCmdUI* pCmdUI) 
{
	XPwhoDoc* pDoc = GetDoc();
	bool	match = FALSE;

	if (selection)
	{
		pCmdUI->Enable();

		for (POSITION wpos=pDoc->inilist.GetHeadPosition(); wpos != NULL;)
		{
			Watch* cw = (Watch*)pDoc->inilist.GetNext(wpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		pCmdUI->SetCheck(match);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
	
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewDing(CCmdUI* pCmdUI) 
{
	XPwhoDoc* pDoc = GetDoc();
	bool	match = FALSE;

	if (selection)
	{
		pCmdUI->Enable();

		for (POSITION dpos=pDoc->dinglist.GetHeadPosition(); dpos != NULL;)
		{
			Watch* cw = (Watch*)pDoc->watchlist.GetNext(dpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		pCmdUI->SetCheck(match);
	}
	else
	{
		pCmdUI->Enable(FALSE);
	}
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewDing() 
{
	XPwhoDoc*	pDoc = GetDoc();
	bool		match = FALSE;
	Watch*		cw;

	if (selection)
	{
		for (POSITION wpos=pDoc->dinglist.GetHeadPosition(); wpos != NULL;)
		{
			cw = (Watch*)pDoc->dinglist.GetNext(wpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		if (match)
		{		// remove him from the watch list
			if (wpos)
				pDoc->dinglist.GetPrev(wpos);
			else
				wpos = pDoc->dinglist.GetTailPosition();
			pDoc->dinglist.RemoveAt(wpos);
			delete cw;
		}
		else
		{		// add him to the watch list
			cw = new Watch(selection);
			pDoc->dinglist.AddTail(cw);
		}
		SortList();
		RedrawWindow();
	}
	
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewUserName() 
{
	XPwhoDoc*	pDoc = GetDoc();
	int			ret;
	UserName	un;
	un.SetDoc(pDoc);
	ret = un.DoModal();
	if (ret == IDOK)
	{
		pDoc->userNick=un.sNick;
		pDoc->userUser=un.sUser;
		pDoc->userHost=un.sHost;
	}
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewConfig() 
{
	int		ret;
	XPwhoDoc* pDoc = GetDoc();
	Config	cfg;
	cfg = pDoc->ccfg;
//	ret = pDoc->ccfg.DoModal();
	ret = cfg.DoModal();
	if (ret == IDOK)
	{
		pDoc->ccfg = cfg;
		activeMeta = pDoc->ccfg.m_primary;
		SetStatusActiveServer(GetHost());
	}
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewPlayers() 
{
	// Add your command handler code here
	XPwhoDoc* pDoc = GetDoc();
	pDoc->ccfg.m_players ^= 1;
	pDoc->UpdateAllViews(NULL);		// redraw
	RedrawWindow();

}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewWatch() 
{
	// Add your command handler code here
	XPwhoDoc*	pDoc = GetDoc();
	bool		match = FALSE;
	Watch*		cw;

	if (selection)
	{
		for (POSITION wpos=pDoc->watchlist.GetHeadPosition(); wpos != NULL;)
		{
			cw = (Watch*)pDoc->watchlist.GetNext(wpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		if (match)
		{		// remove him from the watch list
			if (wpos)
				pDoc->watchlist.GetPrev(wpos);
			else
				wpos = pDoc->watchlist.GetTailPosition();
			pDoc->watchlist.RemoveAt(wpos);
			delete cw;
		}
		else
		{		// add him to the watch list
			cw = new Watch(selection);
			pDoc->watchlist.AddTail(cw);
		}
		SortList();
		RedrawWindow();
	}
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnViewIni() 
{
	// TODO: Add your command handler code here
	XPwhoDoc*	pDoc = GetDoc();
	bool		match = FALSE;
	Watch*		cw;

	if (selection)
	{
		for (POSITION wpos=pDoc->inilist.GetHeadPosition(); wpos != NULL;)
		{
			cw = (Watch*)pDoc->inilist.GetNext(wpos);
			if (*cw == *selection)
			{
				match = TRUE;
				break;
			}
		}
		if (match)
		{		// remove him from the watch list
			if (wpos)
				pDoc->inilist.GetPrev(wpos);
			else
				wpos = pDoc->inilist.GetTailPosition();
			pDoc->inilist.RemoveAt(wpos);
			delete cw;
		}
		else
		{		// add him to the watch list
			cw = new Watch(selection);
			pDoc->inilist.AddTail(cw);
		}
		SortList();
		RedrawWindow();
	}
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::DeleteList()
{
	XPwhoDoc* pDoc = GetDoc();
	pDoc->DeleteContents();
	Invalidate();
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::ProcessRecvData(char* text)
{
	char*			ns;
	long			index;
	ServerSite*	css;
	CString			cs;
	XPwhoDoc* pDoc = GetDoc();

	if (recvbuf)
	{
		ns = new char[strlen(recvbuf)+strlen(text)+2];
		strcpy(ns, recvbuf);
		strcat(ns, text);
		delete recvbuf;
		recvbuf = ns;
		recvbuflen = strlen(recvbuf);
	}
	else
	{
		recvbuf = new char[strlen(text)+1];
		strcpy(recvbuf, text);
		recvbuflen = strlen(recvbuf);

	}

	for(index=0;;)
	{
		long	start = index;
		char*	eol;
		eol = strchr(&recvbuf[start], '\n');
		if (!eol)
		{
			if (index == recvbuflen)
			{
				delete recvbuf;
				recvbuf = NULL;
				break;
			}
			ns = new char[recvbuflen-index+1];
			strcpy(ns, &recvbuf[index]);
			delete recvbuf;
			recvbuf = ns;
			break;
		}
		else
		{
			*eol = '\0';
#ifdef	WIN32
			cs = (const unsigned char*)&recvbuf[index];
#else
			cs = &recvbuf[index];
#endif
			index = eol - recvbuf;
			index++;
			css = new ServerSite;
			css->ParseLine(&cs);
#ifdef	_DEBUG
			css->TRACEdump();
#endif
			pDoc->recvlist.AddTail(css);
		}
	}
	SortList();
	pDoc->UpdateAllViews(NULL);		// redraw
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::SortList()
{
	XPwhoDoc* 		pDoc = GetDoc();
	CObList*		rlist = &pDoc->recvlist;		// shortcut from the doc
	CObList*		slist = &pDoc->serverlist;	// shortcut from the doc
	CObList*		wlist = &pDoc->watchlist;
	ServerSite*	csi;
	ServerSite*	csix;
//	int				sorttype = pDoc->ccfg.m_sort;
	int				sorttype = SORT_PLAYERCOUNT;
	POSITION		pos;
	POSITION		wpos;
	bool			wflag;

	if (sorttype == SORT_NONE)
		return;

	if (rlist->GetCount())
	{
		while(!rlist->IsEmpty())
		{
			BOOL	ins_flag = FALSE;
			csi = (ServerSite*)rlist->RemoveHead();
//			TRACE("SortList: doing <%s> pcount=%d\n", 
//				(const char*)csi->servername, csi->playercount);

// See if we can find it already in the list
			POSITION	rpos;
			bool		match = FALSE;
			if (!slist->IsEmpty())
			{
				for (rpos=slist->GetHeadPosition(); rpos != NULL;)
				{
					csix = (ServerSite*)slist->GetNext(rpos);
					if (csi->servername == csix->servername && csi->serverport == csix->serverport)
					{
						// we have a match
						match = TRUE;
						csix->uptime = csi->uptime;
						break;
					}
				}
			}
			if (!match)
			{
				if (!virgin)
					csi->SetNew();
			}
			else
			{
				// csix = already in list
				// csi = new one
				POSITION		ppos;
				POSITION		qpos;
				Player*			acp;
				Player*			ncp;
				bool			pmatch;

				// check the map attributes for change
				csix->map_changed = FALSE;
				if (csix->version		!= csi->version		 || 
					csix->mapname		!= csi->mapname		 ||
					csix->mapsize		!= csi->mapsize		 ||
					csix->author		!= csi->author		 ||
					csix->serverstatus	!= csi->serverstatus ||
					csix->bases			!= csi->bases		 ||
					csix->teams			!= csi->teams		 ||
					csix->fps			!= csi->fps			 ||
					csix->timing		!= csi->timing)
				{
					csix->map_changed = TRUE;
					csix->version		= csi->version;
					csix->mapname		= csi->mapname;
					csix->mapsize		= csi->mapsize;
					csix->author		= csi->author;
					csix->serverstatus	= csi->serverstatus;
					csix->bases			= csi->bases;
					csix->teams			= csi->teams;
					csix->fps			= csi->fps;
					csix->timing		= csi->timing;
					for (int i=0; i<MAX_TEAMS; i++)
						csix->freebases[i]		= csi->freebases[i];
				}

				csix->playercount_changed = FALSE;
				csix->SetNormal();
				if (csix->playercount != csi->playercount)
				{
					// check each player for a new one.
					for (ppos=csi->playerlist.GetHeadPosition(); ppos != NULL;)
					{
						ncp = (Player*)csi->playerlist.GetNext(ppos);
						pmatch = FALSE;
						for (qpos=csix->playerlist.GetHeadPosition(); qpos != NULL;)
						{
							acp = (Player*)csix->playerlist.GetNext(qpos);
							if (acp->name == ncp->name)
							{
								pmatch = TRUE;
								break;
							}
						}
						if (!pmatch)	// he's a newbie
						{
							Player* xcp = new Player(*ncp);
							csix->playerlist.AddTail(xcp);
							if (pDoc->IsDing(csi))
								pDoc->AddDings(csi, xcp);
						}
					}
					// check each player for departures
					for (ppos=csix->playerlist.GetHeadPosition(); ppos != NULL;)
					{
						acp = (Player*)csix->playerlist.GetNext(ppos);
						pmatch = FALSE;
						for (qpos=csi->playerlist.GetHeadPosition(); qpos != NULL;)
						{
							ncp = (Player*)csi->playerlist.GetNext(qpos);
							if (acp->name == ncp->name)
							{
								pmatch = TRUE;
								break;
							}
						}
						if (!pmatch)	// he left us
						{
							acp->SetDel();
						}
					}
					if (csix->playercount < csi->playercount)
						csix->playercount_changed = CHANGEDLESS;
					else
						csix->playercount_changed = CHANGEDMORE;
					csix->playercount = csi->playercount;
					// remove this guy from the list so we can resort him.
					if (rpos)
						slist->GetPrev(rpos);		// rewind
					else
						rpos=slist->GetTailPosition();
					slist->RemoveAt(rpos);
					delete csi;
					csi = csix;
				}
				else
				{
					csix->refreshed = TRUE;
//					TRACE("Sortlist: refreshed A <%s>\n", (const char*)csix->servername);
					delete csi;	// we don't need him anymore
					continue;
				}
			}
			csi->refreshed = TRUE;
			TRACE("Sortlist: refreshed B <%s>\n", (const char*)csi->servername);

			switch (sorttype)
			{
			case SORT_PLAYERCOUNT:
				if (slist->IsEmpty())
				{
					slist->AddTail(csi);
					ins_flag = TRUE;
//					TRACE("SortList: IsEmpty(), addtail()\n");
					break;
				}
				wflag = FALSE;
				bool swflag;
				for (wpos=wlist->GetHeadPosition(); wpos != NULL;)
				{
					Watch* csw= (Watch*)wlist->GetNext(wpos);
					if (*csw == *csi)
					{
						wflag = TRUE;
						break;
					}
				}
				for (pos=slist->GetHeadPosition(); pos != NULL;)
				{
					csix = (ServerSite*)slist->GetNext(pos);
					swflag = FALSE;
					for (wpos=wlist->GetHeadPosition(); wpos != NULL;)
					{
						Watch* csw= (Watch*)wlist->GetNext(wpos);
						if (*csw == *csix)
						{
							swflag = TRUE;
							break;
						}
					}
					if (swflag && !wflag)
						continue;
//					TRACE("SortList: compare to <%s> pcount=%d\n",
//						(const char*)csix->servername, csix->playercount);
					if ((wflag && !swflag) ||
						(csi->playercount >= csix->playercount))
					{
						if (pos)
							slist->GetPrev(pos);		// rewind
						else
							pos=slist->GetTailPosition();
						slist->InsertBefore(pos, csi);
						ins_flag = TRUE;
//						TRACE("SortList: insert before\n");
						break;
					}
				}
				break;
			default:
				break;
			}
			if (!ins_flag)
			{
				slist->AddTail(csi);
//				TRACE("SortList: AddTail\n");
			}
		}
	}
	RedrawWindow();
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::Prelude()
{
	XPwhoDoc* 		pDoc = GetDoc();
	POSITION		pos;
	POSITION		ppos;
	CObList*		slist= &pDoc->serverlist;	// shortcut from the doc
	ServerSite*	csi;
	Player*		cp;

	// now go through the list again clear refresh flags
	for (pos=slist->GetHeadPosition(); pos != NULL;)
	{
		csi = (ServerSite*)slist->GetNext(pos);
		csi->refreshed = FALSE;
		if (csi->GetNew())
			csi->SetNormal();
		for (ppos=csi->playerlist.GetHeadPosition(); ppos != NULL;)
		{
			cp = (Player*)csi->playerlist.GetNext(ppos);
			if (cp->GetDel())
			{
				if (ppos)
					csi->playerlist.GetPrev(ppos);		// rewind
				else
					ppos=csi->playerlist.GetTailPosition();
				csi->playerlist.RemoveAt(ppos);
				TRACE("Prelude: deleting player <%s>\n", (const char*)cp->name);
				delete cp;
				ppos=csi->playerlist.GetHeadPosition();	// start over
			}
			else
			{
				cp->SetNormal();
			}
		}
	}
	pDoc->ClearDings();
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::PostMortem()
{
	XPwhoDoc* 		pDoc = GetDoc();
	POSITION		pos;
	CObList*		slist= &pDoc->serverlist;	// shortcut from the doc
	ServerSite*	csi;
	bool			changed = FALSE;
	// delete anybody who wants to get deleted	
	for (pos=slist->GetHeadPosition(); pos != NULL;)
	{
		csi = (ServerSite*)slist->GetNext(pos);
		if (!csi->refreshed)
		{
			if (csi->GetDel())
			{
				if (pos)
					slist->GetPrev(pos);		// rewind
				else
					pos=slist->GetTailPosition();
				slist->RemoveAt(pos);
				pos=slist->GetHeadPosition();
				if (selection == csi)
					selection = NULL;
				TRACE("Postmortem: deleting server <%s>\n", (const char*)csi->servername);
				delete csi;
				changed = TRUE;
			}
		}
	}
	// now go through the list again check for deletion
	// and count the total number of players
	int	playercount = 0;
	for (pos=slist->GetHeadPosition(); pos != NULL;)
	{
		csi = (ServerSite*)slist->GetNext(pos);
		playercount += csi->playercount;
		if (!csi->refreshed)
		{
			TRACE("Prelude: SetDel() server <%s>\n", (const char*)csi->servername);
			csi->SetDel();
			changed = TRUE;
		}
	}
	if (changed)
	{
		RedrawWindow();
	}
	char	s[50];
	sprintf(s, "Servers: %d", slist->GetCount());
	SetStatusServerCount(s);
	sprintf(s, "Players: %d", playercount);
	SetStatusPlayerCount(s);
	virgin = FALSE;		// can't be a virgin no more.
	if (pDoc->HasDings())
		ProcessDings();
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::OnMouseMove(UINT nFlags, CPoint point) 
{
	// Add your message handler code here and/or call default

	if (resize_grabbed)
	{
		if (point.x > columnx[resize_column-1])
		{
			for (int i=resize_column+1; i<MAXCOLUMNS; i++)
				columnx[i] += (point.x - columnx[resize_column]);
			columnx[resize_column] = point.x;
			RedrawWindow();
		}
	}

	bool	found = FALSE;
//	TRACE("OnMouseMove: x/y = %d/%d\n", point.x, point.y);
	for (int i=1; i<MAXCOLUMNS; i++)
	{
		if (point.x >= columnx[i]-1 && point.x <= columnx[i]-1)
		{
			found = TRUE;
//			TRACE("OnMouseMove: found\n");
			break;
		}
	}
	if (found)
	{
		if (!resize_cursor_on)
		{
			resize_cursor_on = TRUE;
			SetCursor(resize_cursor);
			TRACE("OnMouseMove: SetCursor(resize_cursor)\n");
		}
	}
	else
	{
		if (resize_cursor_on)
		{
			resize_cursor_on = FALSE;
			SetCursor(NULL);
			TRACE("OnMouseMove: SetCursor(NULL)\n");
		}
	}

	XPwhoViewSUPERCLASS::OnMouseMove(nFlags, point);
}

///////////////////////////////////////////////////////////////////////
void XPwhoView::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// Add your message handler code here and/or call default
	CPoint	pnt;
	XPwhoDoc* 		pDoc = GetDoc();
	POSITION		pos;
	CObList*		slist= &pDoc->serverlist;	// shortcut from the doc
	ServerSite*	csi = NULL;
	
	if (!resize_grabbed)
	{
		bool	found = FALSE;
		for (int i=0; i<MAXCOLUMNS; i++)
		{
			if (point.x >= columnx[i]-1 && point.x <= columnx[i]-1)
			{
				found = TRUE;
				resize_column = i;
				TRACE("OnLButtonDown: found\n");
				break;
			}
		}
		if (found)
			resize_grabbed = TRUE;
		// found out which line we are on.
		pnt = GetScrollPosition();
		int	y = point.y + pnt.y;
		int	line = y / lineheight;
		int	wline = 0;
		for (pos=slist->GetHeadPosition(); pos != NULL;)
		{
			csi = (ServerSite*)slist->GetNext(pos);
			wline = wline + 1 + 
				((pDoc->ccfg.m_players) ? csi->playerlist.GetCount() : 0);
			if (wline >= line)
				break;
		}
		TRACE("OnLButtonDown: y=%d line=%d server=<%s>\n", y, line, 
			csi ? (const char*)csi->servername : "NULL");
		SetSelection(csi);
	}
	XPwhoViewSUPERCLASS::OnLButtonDown(nFlags, point);
	MainFrame*	f = (MainFrame*)GetParent();

	f->toolbarOnScreen = f->m_toolBar.m_hWnd && f->m_toolBar.IsWindowVisible();
	f->shipbarOnScreen = f->m_shipBar.m_hWnd && f->m_shipBar.IsWindowVisible();
	f->statusbarOnScreen = f->m_statusBar && f->m_statusBar.IsWindowVisible();
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	resize_grabbed = FALSE;
	XPwhoViewSUPERCLASS::OnLButtonUp(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
BOOL XPwhoView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	// Add your message handler code here and/or call default

	if (resize_cursor_on)
		return(TRUE);
	return XPwhoViewSUPERCLASS::OnSetCursor(pWnd, nHitTest, message);
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::SetSelection(ServerSite* csi)
{
	bool	changed = FALSE;
	
	if (selection != csi)
		changed = TRUE;
	selection = csi;

	if (changed)
		RedrawWindow();
}



///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateJoinbutt(CCmdUI* pCmdUI) 
{
	// Add your command update UI handler code here
	pCmdUI->Enable((BOOL)GetSelection());

}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::UpdateXPilotIni()
{
	XPwhoDoc* pDoc = GetDoc();
	ServerSite* si = GetSelection();
	char selectedPort[12];

	// find the correct XPilot.ini (based on dir to start XPilotNT)
	CString	inifile;
	inifile = pDoc->ccfg.m_xpilotntapp;
	inifile = inifile.Left(inifile.ReverseFind('\\'));
	inifile += "\\XPilot.ini";
	shipobj* w = (shipobj*)((MainFrame*)GetParent())->m_shipBar.wDefaultShip;
	CString	n;
	((MainFrame*)GetParent())->m_shipBar.m_name.GetWindowText(n);
	//const char*	 n = (const char*)((MainFrame*)GetParent())->m_shipbar.m_name->GetWindowText();
	WritePrivateProfileString("Settings", "shipShape", w->name, (const char*)inifile);
	WritePrivateProfileString("Settings", "name", (const char*)n, (const char*)inifile);
	if (si)
	{
		sprintf(selectedPort, "%d", si->serverport);
		WritePrivateProfileString("Settings", "port", selectedPort, (const char*)inifile);
	}
}

///////////////////////////////////////////////////////////////////////////////
CString XPwhoView::GetExe()
{
	CString cs;
	cs = GetDoc()->ccfg.m_xpilotntapp;
	if (cs.Find(".exe") == -1)
		cs += ".exe";
	if (_access((const char*)cs, 0) == -1)
	{
		CString ce;
		ce.Format("Can't access the XPilot executable\nMake sure that you have "
				  "configured the location of XPilot correctly (under the Green "
				  "\"C\")\n\n%s not found", (const char*)cs);
		AfxMessageBox(ce);
		cs = "";
	}
	return cs;
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnJoinbutt() 
{
	XPwhoDoc* pDoc = GetDoc();
	ServerSite* si = GetSelection();
	CString		cs;

	UpdateXPilotIni();
	cs = GetExe();
	if (!cs.GetLength())
		return;
	cs += " ";
	cs += si->serverip;
	if (si->teams)
	{
		MainFrame* mf = (MainFrame*)GetParentFrame();
		CDialogBar* jb = &mf->m_joinbar;
		if (jb->m_hWnd)
		{
			LRESULT	index = jb->SendDlgItemMessage(ID_JOINTEAM, CB_GETCURSEL);
			CString js;
			if (index)
			{
				js.Format(" -team %d", index);
				cs += js;
			}
		}
	}
	// What the hell was this supposed to do?
	/*
	for (POSITION ipos=pDoc->inilist.GetHeadPosition(); ipos != NULL;)
	{
		Watch* cw = (Watch*)pDoc->inilist.GetNext(ipos);
		if (*cw == *selection)
		{
			cs += " -ServerIni";
			break;
		}
	}
	*/
	CString en;
	if (pDoc->userUser.GetLength())
	{
		en = "XPILOTUSER=" + pDoc->userUser;
		putenv(en);
	}
	if (pDoc->userHost.GetLength())
	{
		en = "XPILOTHOST=" + pDoc->userHost;
		putenv(en);
	}
	WinExec((const char*)cs, SW_SHOWDEFAULT);
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateJointeam(CCmdUI* pCmdUI) 
{
	BOOL	f = FALSE;
	ServerSite* si = GetSelection();
	if (si && si->teams)
		f = TRUE;
	pCmdUI->Enable(f);
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateJointeamCommon(CCmdUI* pCmdUI, int t) 
{
	BOOL	f = FALSE;
	ServerSite* si = GetSelection();
	if (si && si->teams)
		f = TRUE;
	if (si && si->version >= "4.0" && !si->freebases[t])
		f = FALSE;
	pCmdUI->Enable(f);
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnJointeam() 
{
	// TODO: Add your command handler code here
	// GetDialogItem
	ASSERT(true);
	TRACE("OnJointeam\n");
}

///////////////////////////////////////////////////////////////////////////////
void XPwhoView::OnUpdateViewColumns(CCmdUI* pCmdUI) 
{
	// TODO: Add your command update UI handler code here
	
}

void XPwhoView::OnViewColumns() 
{
	// TODO: Add your command handler code here
	int		ret;
	XPwhoDoc* pDoc = GetDoc();
	Columns	ccol;
	ret = pDoc->ccol.DoModal();
	if (ret == IDOK)
	{
		CRect	rect;
		GetClientRect(&rect);
		InvalidateRect(rect);
	}
}

// join a game based on the team # button pushed
void XPwhoView::OnJoinWithTeam(int team) 
{
	ServerSite* si = GetSelection();
	CString		cs;

	UpdateXPilotIni();
	cs = GetDoc()->ccfg.m_xpilotntapp;
	cs += " ";
	cs += si->servername;
	if (si->teams)
	{
		CString js;
		js.Format(" -team %d", team);
		cs += js;
	}
	WinExec((const char*)cs, SW_SHOWDEFAULT);
}

void XPwhoView::OnUpdateTeam1(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 1); }
void XPwhoView::OnTeam1() { OnJoinWithTeam(1); }
void XPwhoView::OnUpdateTeam2(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 2); }
void XPwhoView::OnTeam2() { OnJoinWithTeam(2); }
void XPwhoView::OnUpdateTeam3(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 3); }
void XPwhoView::OnTeam3() { OnJoinWithTeam(3); }
void XPwhoView::OnUpdateTeam4(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 4); }
void XPwhoView::OnTeam4() { OnJoinWithTeam(4); }
void XPwhoView::OnUpdateTeam5(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 5); }
void XPwhoView::OnTeam5() { OnJoinWithTeam(5); }
void XPwhoView::OnUpdateTeam6(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 6); }
void XPwhoView::OnTeam6() { OnJoinWithTeam(6); }
void XPwhoView::OnUpdateTeam7(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 7); }
void XPwhoView::OnTeam7() { OnJoinWithTeam(7); }
void XPwhoView::OnUpdateTeam8(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 8); }
void XPwhoView::OnTeam8() { OnJoinWithTeam(8); }
void XPwhoView::OnUpdateTeam9(CCmdUI* pCmdUI) { OnUpdateJointeamCommon(pCmdUI, 9); }
void XPwhoView::OnTeam9() { OnJoinWithTeam(9); }

void XPwhoView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	XPwhoViewSUPERCLASS::OnLButtonDblClk(nFlags, point);
	OnJoinbutt();
}

void XPwhoView::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	XPwhoViewSUPERCLASS::OnChar(nChar, nRepCnt, nFlags);
}

void XPwhoView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	XPwhoDoc*		pDoc = GetDoc();
	ServerSite*	csi;
	ServerSite*	ocsi = NULL;
	POSITION		spos;
	switch (nChar)
	{
	case VK_DOWN:
		if (!GetSelection())
		{
			SetSelection((ServerSite*)pDoc->serverlist.GetHead());
			break;
		}
		for (spos=pDoc->serverlist.GetHeadPosition(); spos != NULL;)
		{
			csi  = (ServerSite*)pDoc->serverlist.GetNext(spos);
			if (csi == GetSelection())
			{
				csi  = (ServerSite*)pDoc->serverlist.GetNext(spos);
				if (csi)
					SetSelection(csi);
				break;
			}
		}
		break;
	case VK_UP:
		csi = GetSelection();
		if (!csi)
			break;
		if (csi == (ServerSite*)pDoc->serverlist.GetHead())
			break;
		for (spos=pDoc->serverlist.GetHeadPosition(); spos != NULL;)
		{
			csi  = (ServerSite*)pDoc->serverlist.GetNext(spos);
			if (csi == GetSelection())
			{
				//csi  = (ServerSite*)pDoc->serverlist.GetPrev(spos);
				if (ocsi)
					SetSelection(ocsi);
				break;
			}
			ocsi = csi;
		}
		break;
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	case '0':
		OnJoinWithTeam(nChar&0x0F);
		break;
	}
	MainFrame*	f = (MainFrame*)GetParent();

	f->toolbarOnScreen = f->m_toolBar.m_hWnd && f->m_toolBar.IsWindowVisible();
	f->shipbarOnScreen = f->m_toolBar.m_hWnd && f->m_shipBar.IsWindowVisible();
	f->statusbarOnScreen = f->m_statusBar && f->m_statusBar.IsWindowVisible();

	XPwhoViewSUPERCLASS::OnKeyDown(nChar, nRepCnt, nFlags);
}


void XPwhoView::OnAppHome() 
{
	ShellExecute(NULL, "open", "http://www.xpilot.org/", NULL, NULL, SW_SHOWDEFAULT);
}

void XPwhoView::OnAppBuckohome() 
{
	CString	cs = "http://www.buckosoft.com/xpilot/0.";
	cs += VERSION_WINDOWS;
	cs += ".html";
	ShellExecute(NULL, "open", cs, NULL, NULL, SW_SHOWDEFAULT);
	
}

void XPwhoView::OnUpdateViewShiplist(CCmdUI* pCmdUI) 
{
	MainFrame*	f = (MainFrame*)GetParent();
	pCmdUI->Enable();
	pCmdUI->SetCheck(f->shipbarOnScreen);
	
}

void XPwhoView::OnViewShiplist() 
{
	//XPwhoDoc*	pDoc = GetDoc();
	MainFrame*	f = (MainFrame*)GetParent();
	if (f->shipbarOnScreen)
	{
		f->shipbarOnScreen = FALSE;
		f->m_shipBar.ShowWindow(SW_HIDE);
		f->RecalcLayout();
	}
	else
	{
		f->shipbarOnScreen = TRUE;
		f->m_shipBar.ShowWindow(SW_SHOW);
		f->RecalcLayout();
	}
	
}


void XPwhoView::OnUpdateConfigureSysinfo(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable();
}

void XPwhoView::OnConfigureSysinfo() 
{
	// TODO: Add your command handler code here
	SysInfo	csi;
	csi.DoModal();
	
}


void XPwhoView::ProcessDings()
{
	ServerSite* csi;
	XPwhoDoc*	pDoc = GetDoc();
	for (POSITION dpos=pDoc->curdings.GetHeadPosition(); dpos != NULL;)
	{
		Watch* cw = (Watch*)pDoc->curdings.GetNext(dpos);
		TRACE("ProcessDings: cw=%s\n", (const char*)cw->servername);
		for (POSITION spos=pDoc->serverlist.GetHeadPosition(); spos != NULL;)
		{
			csi  = (ServerSite*)pDoc->serverlist.GetNext(spos);
			if (*cw == *csi)
			{
				TRACE("ProcessDings: SetSelection=%s\n", (const char*)csi->servername);
				SetSelection(csi);
				dpos = NULL;			// terminate outer loop
				break;					// terminate inner loop
			}
		}
	}

	if (pDoc->ccfg.m_dt_ding)
		PlaySound(pDoc->ccfg.m_ding_wave, NULL, SND_FILENAME);
	if (pDoc->ccfg.m_dt_focus)
	{
		if (GetParent()->IsIconic())
			GetParent()->ShowWindow(SW_RESTORE);
		else
			GetParent()->ShowWindow(SW_SHOW);
	}
	if (pDoc->ccfg.m_dt_messagebox)
	{
		CString		csplayers;
		CString		cs;
		Player*	cp;
		if (pDoc->ccfg.m_players)
		{
			int		count = 0;
			for (POSITION ppos=csi->playerlist.GetHeadPosition(); ppos != NULL;)
			{
				cp = (Player*)csi->playerlist.GetNext(ppos);
				if (cp->GetNew())
				{
					count++;
					cs += cp->name;
					cs += '\n';
				}
			}
			cs += count > 1 ? "has" : "have";
			cs += " joined\n";
			cs += csi->servername;
			int ret = AfxMessageBox((const char*)cs, MB_OKCANCEL | MB_ICONINFORMATION |MB_DEFBUTTON2);
			if (ret == IDOK)
			{
				if (GetParent()->IsIconic())
					GetParent()->ShowWindow(SW_RESTORE);
				else
					GetParent()->ShowWindow(SW_SHOW);
			}
		}
		else
		{
			AfxMessageBox("dinged server contains no players!", MB_OK|MB_ICONEXCLAMATION);
		}
	}
}

void XPwhoView::OnShowWindow(BOOL bShow, UINT nStatus) 
{

	XPwhoViewSUPERCLASS::OnShowWindow(bShow, nStatus);
	
	
}
