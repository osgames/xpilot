/***************************************************************************\
*  ShipBar.cpp : Toolbar with all of the ships on it.						*
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/05/17 19:23:43  dik
 *  Use shipobj instead of wireobj
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:03  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.6  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.5  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 *  Revision 1.4  2001/04/01 19:59:43  dick
 *  Delete pens when done
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "ShipBar.h"
#include "MainFrame.h"
#include "XPwhoDoc.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern "C" {
	void	_Trace(char* lpszFormat, ...);
	// #include "../xpilot/src/math.c"
	#include "xp-shipshape.c"
}

#define	SHIPSPACING	32
#define	SPINSPACING	40

///////////////////////////////////////////////////////////////////////////////
// ShipBar message handlers

ShipBar::ShipBar(CWnd* pParent /*=NULL*/)
	: CDialogBar()
//	: CDialogBar(ShipBar::IDD, pParent)
{
	//{{AFX_DATA_INIT(ShipBar)
	//}}AFX_DATA_INIT
	nDefaultShip = 0;
	Make_table();
	aniOn = FALSE;
}

///////////////////////////////////////////////////////////////////////////////
ShipBar::~ShipBar()
{
	for (int i=1; i<GetShipCount(); i++)
		Free_ship_shape((shipobj*)m_ships.GetAt(i));
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::DoDataExchange(CDataExchange* pDX)
{
	CDialogBar::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ShipBar)
	DDX_Control(pDX, IDC_SHIPSHAPES, m_shiplist);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_scrollbar);
	//}}AFX_DATA_MAP
}


///////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(ShipBar, CDialogBar)
	//{{AFX_MSG_MAP(ShipBar)
	ON_WM_CREATE()
	ON_WM_LBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONUP()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

///////////////////////////////////////////////////////////////////////////////
#define	SCROLLWIDTH	100
int ShipBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialogBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	//LoadShips();

	int		width;
	// TODO: Add your specialized creation code here
	CRect	rect;
	GetParentFrame()->GetClientRect(&rect);
	width = rect.right;
	GetClientRect(&rect);
	rect.right = width - SCROLLWIDTH;
	m_shiplist.parent = this;
	if (!m_shiplist.Create(NULL, WS_CHILD|WS_VISIBLE, 
		rect, this, IDC_SHIPSHAPES))
	{
		AfxMessageBox("Failed to create shiplist");
	}
	rect.right = width;
	rect.left = rect.right - SCROLLWIDTH + 1;

	rect.top += 10;
	rect.bottom -= 10;
	if (!m_scrollbar.Create(SBS_HORZ | WS_CHILD|WS_VISIBLE,
		rect, this, IDC_SCROLLBAR1))
	{
		AfxMessageBox("Failed to create scrollbar");
	}

	if(!m_name.Create(SS_CENTER | WS_CHILD|ES_AUTOHSCROLL|WS_VISIBLE, rect, this, IDC_EDIT1))
	{
		// who cares.
	}
	m_name.LimitText(15);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	TRACE("LButtonDown %d @ %d/%d\n", nFlags, point.x, point.y);
	int		x = point.x / SHIPSPACING;
	if (x < shipsOnScreen)
	{
		int	newship = m_scrollbar.GetScrollPos() + x;
		if (newship > GetShipCount())
			newship = GetShipCount()-1;
		SetDefaultShip(newship);
		shipobj*	w = (shipobj*)m_ships.GetAt(newship);
		SaveNickName();
		m_name.SetWindowText(w->name);
		m_shiplist.Invalidate();
	}
	CDialogBar::OnLButtonDown(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnLButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	if (m_name.m_hWnd) m_name.SetWindowText(nick);
	CDialogBar::OnLButtonUp(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	TRACE("RButtonDown %d @ %d/%d\n", nFlags, point.x, point.y);
	oldCursor = SetCursor(NULL);
	int		x = point.x / SHIPSPACING;
	if (x < shipsOnScreen)
	{
		int	newship = m_scrollbar.GetScrollPos() + x;
		if (newship > GetShipCount())
			newship = GetShipCount()-1;
		shipobj*	w = (shipobj*)m_ships.GetAt(newship);
		SaveNickName();
		m_name.SetWindowText(w->name);
		aniInc = 0;
		AnimateOn(newship);
	}
	//if (m_name.m_hWnd) m_name.SetWindowText(((wireobj*)wDefaultShip)->name);
	CDialogBar::OnRButtonDown(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnRButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	SetCursor(oldCursor);
	if (m_name.m_hWnd) m_name.SetWindowText(nick);
	AnimateOff();
	m_shiplist.Invalidate();
	CDialogBar::OnRButtonUp(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnMButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	oldCursor = SetCursor(NULL);
	int		x = point.x / SHIPSPACING;
	if (x < shipsOnScreen)
	{
		int	newship = m_scrollbar.GetScrollPos() + x;
		if (newship > GetShipCount())
			newship = GetShipCount()-1;
		shipobj*	w = (shipobj*)m_ships.GetAt(newship);
		SaveNickName();
		m_name.SetWindowText(w->name);
		aniInc = 1;
		AnimateOn(newship);
	}
	//if (m_name.m_hWnd) m_name.SetWindowText(((wireobj*)wDefaultShip)->name);
	CDialogBar::OnMButtonDown(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnMButtonUp(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	
	SetCursor(oldCursor);
	if (m_name.m_hWnd) m_name.SetWindowText(nick);
	AnimateOff();
	CDialogBar::OnMButtonUp(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	TRACE("MouseMove %d @ %d/%d\n", nFlags, point.x, point.y);
#if 0
	int		x = point.x / SHIPSPACING;
	if (x < shipsOnScreen)
	{
		int	newship = m_scrollbar.GetScrollPos() + x;
		if (newship > GetShipCount())
			newship = GetShipCount()-1;
		wireobj*	w = (wireobj*)m_ships.GetAt(newship);
		m_name.SetWindowText(w->name);
	}
#endif
	CDialogBar::OnMouseMove(nFlags, point);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnSize(UINT nType, int cx, int cy) 
{
	CDialogBar::OnSize(nType, cx, cy);
	
	// TODO: Add your message handler code here
	if (!cx || !cy)
		return;
	CRect	rect(0,0,cx,cy);
	rect.right -= SCROLLWIDTH;
	m_shiplist.MoveWindow(rect);

	shipsOnScreen = rect.right/SHIPSPACING;
	m_scrollbar.SetScrollRange(0, GetShipCount()-shipsOnScreen);

	rect.right = cx - 2;
	rect.left = cx - SCROLLWIDTH + 2;
	rect.top = rect.bottom-20;
	m_scrollbar.MoveWindow(rect);

	rect.top = 0;
	rect.bottom = 20;
	m_name.MoveWindow(rect);
	m_name.SetWindowText(nick);
	SetDefaultShip(GetDefaultShip());
}

///////////////////////////////////////////////////////////////////////////////
int ShipBar::PaintShip(CPaintDC* dc, void* v, int dir, int c)
{
	shipobj*	w = (shipobj*)v;
	CPen		wpen;
	CRect		bigrect = dc->m_ps.rcPaint;
	int			yofs = (bigrect.bottom - bigrect.top) /2;
	int			xofs = (c)*SHIPSPACING + (SHIPSPACING/2);
	if (xofs > bigrect.right-(SHIPSPACING/2))
		return(FALSE);
	wpen.CreatePen(PS_SOLID, 1, RGB(255,255,255));

	CPen* open = dc->SelectObject(&wpen);
	for (int i=0; i<w->num_points; i++)
	{
		/* TRACE("%d, xyofs=%d/%d pts=%d/%d, dif=%d/%d\n", dir, xofs, yofs,
			(int)w->pts[i][dir].x, (int)w->pts[i][dir].y,
			(int)(xofs - w->pts[i][dir].x), (int)(yofs + w->pts[i][dir].y)); */
		if (!i)
			dc->MoveTo((int)(xofs - w->pts[i][dir].x), (int)(yofs + w->pts[i][dir].y));
		else
			dc->LineTo((int)(xofs - w->pts[i][dir].x), (int)(yofs + w->pts[i][dir].y));
	}
	dc->LineTo((int)(xofs - w->pts[0][dir].x), (int)(yofs + w->pts[0][dir].y));
	dc->SelectObject(open);
	wpen.DeleteObject();
	if (w == (shipobj*)wDefaultShip)
	{
		CPen	gpen;
		gpen.CreatePen(PS_SOLID, 1, RGB(0,255,0));
		open = dc->SelectObject(&gpen);
		dc->MoveTo(c*SHIPSPACING, bigrect.bottom-1);
		dc->LineTo((c+1)*SHIPSPACING, bigrect.bottom-1);
		dc->SelectObject(open);
		gpen.DeleteObject();
	}
	return(TRUE);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::AnimateOn(int ship)
{
	aniShip = ship;
	aniWire = GetShipAt(ship);
	aniCol  = ship-m_scrollbar.GetScrollPos();

	CRect	wr;
	m_shiplist.GetClientRect(&wr);
	aniRect = CRect(aniCol*SHIPSPACING-((SPINSPACING-SHIPSPACING)/2), wr.top, 
				   (aniCol+1)*SHIPSPACING+((SPINSPACING-SHIPSPACING)/2), wr.bottom);
	aniDir = RES*3/4;
#ifdef	_DEBUG
	aniTimer = SetTimer(SHIPTIMER, 200, NULL);
#else
	aniTimer = SetTimer(SHIPTIMER, 10, NULL);
#endif
	if (aniTimer)
		aniOn = TRUE;
}
void ShipBar::AnimateOff()
{
	KillTimer(aniTimer);
	aniOn = FALSE;
	m_shiplist.InvalidateRect(aniRect);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	if (nIDEvent == aniTimer)
	{
		m_shiplist.InvalidateRect(aniRect);
		if (aniInc)
		{
			if (--aniDir < 0)
				aniDir = RES-1;
		}
		else
		{
			if (++aniDir >= RES)
				aniDir = 0;
		}
	}
	CDialogBar::OnTimer(nIDEvent);
}

///////////////////////////////////////////////////////////////////////////////
#define	SHIPBUF		1024
void ShipBar::LoadShips(char* shipfile, char* shipname)
{
	FILE*	fp;
	char	buf[SHIPBUF];
	//char	shipname[64];
	m_ships.Add(Default_ship());

	if (shipfile[0] == '\0')
	{
		AfxMessageBox("Can't determine name of shipshape file");
		return;
	}
	if ((fp = fopen(shipfile, "r")) == NULL)
	{
		CString e;
		e.Format("Can't open shipshape file \"%s\"", shipfile);
		AfxMessageBox(e);
		return;
	}
	memset(buf, 0, SHIPBUF);
	int	i = 1;
	int	defaultIsSet=FALSE;
	while (fgets(buf, SHIPBUF-1, fp))
	{
		shipobj*	w;

		w = do_parse_shape(buf);
		m_ships.Add(w);
		if (!strcmp(shipname, w->name))
		{
			SetDefaultShip(i);
			defaultIsSet=TRUE;
		}
		memset(buf, 0, SHIPBUF);
		i++;
	}
	fclose(fp);
	if (!defaultIsSet)
		SetDefaultShip(0);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::SetDefaultShip(int i)
{
	nDefaultShip = i;
	wDefaultShip = m_ships.GetAt(i);

	if (!m_scrollbar.m_hWnd)
		return;					// too early to window
	int	curpos = m_scrollbar.GetScrollPos();
	if (i > curpos && i < curpos+shipsOnScreen)
		return;
	if (i < curpos)
		curpos = i;
	else
		curpos = i-shipsOnScreen;
	m_scrollbar.SetScrollPos(curpos);
	m_shiplist.Invalidate();
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::SetNick(CString cs)
{
	nick = cs;
	if (m_name.m_hWnd) m_name.SetWindowText(nick);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::SetNick(char* s)
{
	nick = s;
	if (m_name.m_hWnd) m_name.SetWindowText(nick);
}

///////////////////////////////////////////////////////////////////////////////
CString ShipBar::GetNick()
{
	return(nick);
}

///////////////////////////////////////////////////////////////////////////////
void ShipBar::SaveNickName()
{
	m_name.GetWindowText(nick);
}

/////////////////////////////////////////////////////////////////////////////
// ShipScrollBar

ShipScrollBar::ShipScrollBar()
{
}

ShipScrollBar::~ShipScrollBar()
{
}


BEGIN_MESSAGE_MAP(ShipScrollBar, CScrollBar)
	//{{AFX_MSG_MAP(ShipScrollBar)
	ON_WM_HSCROLL_REFLECT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShipScrollBar message handlers

void ShipScrollBar::HScroll(UINT nSBCode, UINT nPos) 
{
	// TODO: Add your message handler code here
	int		curpos = GetScrollPos();
	int		min, max;
	int		shipsOnScreen = ((ShipBar*)GetParent())->shipsOnScreen;
	bool	redraw = FALSE;
	TRACE("scroll: nSBCode=%d nPos=%d\n", nSBCode, nPos);
	GetScrollRange(&min, &max);
	switch (nSBCode)
	{
	case 0:		// scroll left
		if (curpos > 0)
		{
			SetScrollPos(curpos-1);
			redraw = TRUE;
		}
		break;
	case 1:		// scroll right
		if (curpos < max)
		{
			SetScrollPos(curpos+1);
			redraw = TRUE;
		}
		break;
	case 2:		// page left
		if (curpos > shipsOnScreen)
		{
			SetScrollPos(curpos-shipsOnScreen);
			redraw = TRUE;
		}
		else if (curpos > 0)
		{
			SetScrollPos(0);
			redraw = TRUE;
		}
		break;
	case 3:		// page right
		if (curpos+shipsOnScreen < max)
		{
			SetScrollPos(curpos+shipsOnScreen);
			redraw = TRUE;
		}
		else if (curpos != max)
		{
			SetScrollPos(max);
			redraw = TRUE;
		}
		break;
	case 5:		// thumb
		SetScrollPos(nPos);
		redraw = TRUE;
		break;
	}
	if (redraw)
		((ShipBar*)GetParent())->m_shiplist.Invalidate();
}
/////////////////////////////////////////////////////////////////////////////
// ShipList

ShipList::ShipList()
{
}

ShipList::~ShipList()
{
}


BEGIN_MESSAGE_MAP(ShipList, CStatic)
	//{{AFX_MSG_MAP(ShipList)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ShipList message handlers

void ShipList::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	
	// TODO: Add your message handler code here
	// Do not call CStatic::OnPaint() for painting messages
	if (parent->aniOn)
	{
		dc.FillSolidRect(parent->aniRect, RGB(0,0,0));
		parent->PaintShip(&dc, parent->aniWire, parent->aniDir, parent->aniCol);
	}
	else
	{
		CRect	rect;
		GetClientRect(&rect);
		dc.FillSolidRect(rect, RGB(0,0,0));
		int		start = parent->m_scrollbar.GetScrollPos();
		for (int i=start; i<parent->GetShipCount(); i++)
		{
			shipobj*	w = (shipobj*)parent->GetShipAt(i);
			if (!parent->PaintShip(&dc, w, RES*3/4, i-start))
				return;
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
void ShipList::OnLButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	AfxMessageBox("Beep");
	CStatic::OnLButtonDown(nFlags, point);
}



