/***************************************************************************\
*  XPwhoApp.cpp : Defines the class behaviors for the application           *
*  $Id$   				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.1.1.1  2001/04/04 14:01:08  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.5  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.4  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"
#include "XPwhoApp.h"

#include "MainFrame.h"
#include "XPwhoDoc.h"
#include "XPwhoView.h"
#include "xp-version.h"		/* yowsa! crossing project boundaries!!! */
#include "xp-winAbout.h"

#if		_USESOCK && defined(_MFC30)
#include "afxsock.h"
#endif

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#if 0
extern "C" {
//	void	_Trace(char* lpszFormat, ...);
	// #include "../xpilot/src/math.c"
	#include "xp-math.c"
}
#endif

/////////////////////////////////////////////////////////////////////////////
// XPwhoApp

BEGIN_MESSAGE_MAP(XPwhoApp, CWinApp)
	//{{AFX_MSG_MAP(XPwhoApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// XPwhoApp construction

XPwhoApp::XPwhoApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only XPwhoApp object

XPwhoApp theApp;

WSADATA wsadata;

/////////////////////////////////////////////////////////////////////////////
// XPwhoApp initialization

BOOL XPwhoApp::InitInstance()
{
#if	_USESOCK
#ifdef	_MFC30
	if (!AfxSocketInit(&wsadata))
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}
#else
	// from winsock.doc
	WORD wVersionRequested;
	int err;

	wVersionRequested = MAKEWORD( 1, 1 );

	err = WSAStartup( wVersionRequested, &wsadata );
	if ( err != 0 )
	{
		/* Tell the user that we couldn't find a useable */
		/* winsock.dll.                                  */
		return;
	}

	/* Confirm that the Windows Sockets DLL supports 1.1.*/
	/* Note that if the DLL supports versions greater    */
	/* than 1.1 in addition to 1.1, it will still return */
	/* 1.1 in wVersion since that is the version we      */
	/* requested.                                        */

	if (LOBYTE( wsaData.wVersion ) != 1 ||
		HIBYTE( wsaData.wVersion ) != 1 )
	{
		/* Tell the user that we couldn't find a useable */
		/* winsock.dll.                                  */
		WSACleanup( );
		return;   
	}

	/* The Windows Sockets DLL is acceptable.  Proceed.  */


	/* Make sure that the version requested is >= 1.1.   */
	/* The low byte is the major version and the high    */
	/* byte is the minor version.                        */

	if ( LOBYTE( wVersionRequested ) < 1 ||
		(LOBYTE( wVersionRequested ) == 1 &&
		HIBYTE( wVersionRequested ) < 1 )
	{
		return WSAVERNOTSUPPORTED;
	}

	/* Since we only support 1.1, set both wVersion and  */
	/* wHighVersion to 1.1.                              */

	lpWsaData->wVersion = MAKEWORD( 1, 1 );
	lpWsaData->wHighVersion = MAKEWORD( 1, 1 );
#endif
#endif

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef	_WIN32
	Enable3dControls();

	SetRegistryKey("BuckoSoft");
#endif

//	LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(XPwhoDoc),
		RUNTIME_CLASS(MainFrame),       // main SDI frame window
		RUNTIME_CLASS(XPwhoView));
	AddDocTemplate(pDocTemplate);

	// create a new (empty) document
	OnFileNew();

	if (m_lpCmdLine[0] != '\0')
	{
		// TODO: add command line processing here
	}

	return TRUE;
}

int XPwhoApp::ExitInstance()
{
	CWinApp::ExitInstance();
#if		_USESOCK
#ifndef	WIN32
	WSACleanup();
#endif
#endif
	return(0);
}

#if 0
/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
	SendDlgItemMessage(IDC_VERSION, WM_SETTEXT, 0, (LPARAM)("XPilot " TITLE));
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#endif

// App command to run the dialog
void XPwhoApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}
/////////////////////////////////////////////////////////////////////////////
// XPwhoApp commands

extern "C" 
void _Trace(char* lpszFormat, long a, long b, long c, long d, long e, long f, long g, long h, long i, long j, long k)
{
	AfxTrace(lpszFormat, a, b, c, d, e, f, g, h, i, j, k);
}
