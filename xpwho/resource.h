//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by XPwho.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_JOINBAR                     103
#define IDP_SOCKETS_INIT_FAILED         104
#define ID_VIEW_PLAYERS                 105
#define ID_VIEW_WATCH                   106
#define IDR_MAINFRAME                   128
#define IDI_XPMONTYPE                   129
#define IDD_CONFIG                      130
#define IDD_COLUMNS                     133
#define IDI_XPILOT                      134
#define IDD_CONFIG1                     134
#define IDD_SHIPBAR                     135
#define IDD_SYSINFO                     136
#define IDB_DING                        137
#define IDD_USERNAME                    139
#define IDC_HOSTNAME                    1000
#define IDC_SPIN                        1001
#define IDC_SOCKSHOSTNAME               1001
#define IDC_HOSTNAME2                   1001
#define IDC_MINUTES                     1002
#define IDC_MEASURE                     1003
#define IDC_UPDATE                      1004
#define IDC_MEASURE_ALL                 1005
#define IDC_MEASURE_PLAYERS             1006
#define IDC_MEASURE_NO_PLAYERS          1007
#define IDC_SORT_PLAYERS                1008
#define IDC_SORT_DISTANCE               1009
#define IDC_SORT_NONE                   1010
#define IDC_WATCH_TOP                   1011
#define ID_JOINBUTT                     1011
#define IDC_SOCKSSOCK                   1012
#define ID_JOINTEAM                     1012
#define IDC_XPILOTNT                    1014
#define IDC_USESOCKS                    1015
#define IDC_EXITLAUNCH                  1016
#define IDC_PLAYERCOUNT                 1017
#define IDC_BASES                       1018
#define IDC_SERVERNAME                  1019
#define IDC_MAPNAME                     1020
#define IDC_MAPAUTHOR                   1021
#define IDC_TEAMS                       1022
#define IDC_VERSION                     1023
#define IDC_FPS                         1024
#define IDC_SOUND                       1025
#define IDC_UPTIME                      1026
#define IDC_IP                          1027
#define IDC_MAPSIZE                     1028
#define IDC_TIMING                      1029
#define IDC_RADIO1                      1030
#define IDC_RADIO2                      1031
#define IDC_SCROLLBAR1                  1032
#define IDC_SHIPSHAPES                  1033
#define IDC_EDIT1                       1034
#define IDC_PORT                        1035
#define IDC_DT_DING                     1036
#define IDC_DT_MESSAGEBOX               1037
#define IDC_DT_FOCUS                    1038
#define IDC_DING_WAVE                   1040
#define IDC_DING_SELECT                 1050
#define IDC_EDIT_FULLNAME               1051
#define IDC_EDIT_HOST                   1052
#define IDC_EDIT_USER                   1053
#define IDC_EDIT_NICK                   1054
#define	IDC_CREDITS                     1055
#define ID_CONFIG                       32771
#define ID_VIEW_USERNAME                32772
#define ID_SERVER                       32773
#define ID_VIEW_DING                    32774
#define ID_VIEW_CONFIG                  32775
#define ID_VIEW_COLUMNS                 32776
#define ID_TEAM_1                       32777
#define ID_TEAM_2                       32778
#define ID_TEAM_3                       32779
#define ID_TEAM_4                       32780
#define ID_TEAM_5                       32781
#define ID_TEAM_6                       32782
#define ID_TEAM_7                       32783
#define ID_TEAM_8                       32784
#define ID_TEAM_9                       32785
#define ID_SERVER_TOGGLEWATCH           32786
#define ID_VIEW_INI                     32787
#define ID_APP_HOME                     32788
#define ID_APP_BUCKOHOME                32789
#define ID_VIEW_SHIPLIST                32790
#define ID_CONFIGURE_SYSINFO            32791
#define ID_SERVERCOUNT                  61446
#define ID_PLAYERCOUNT                  61447
#define ID_CONNSECONDS                  61448
#define ID_ACTIVESERVER                 61449

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32793
#define _APS_NEXT_CONTROL_VALUE         1056
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
