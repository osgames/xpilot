/***************************************************************************\
*  XPwhoMath.c : Localize access to xpilot project math.c                   *
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.1.1.1  2001/04/04 14:01:09  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.1  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

#include "../xpilot/src/common/portability.c"
#include "../xpilot/src/common/math.c"
#include "../xpilot/src/common/randommt.c"
#include "../xpilot/src/common/checknames.c"
