/***************************************************************************\
*  MainFrame.h : interface of the MainFrame class                           *
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2002/05/24 17:28:14  dik
 *  Make persistent the user's display of the statusbar and toolbar.
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:01  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

/////////////////////////////////////////////////////////////////////////////
#ifndef	_MAINFRM_H_
#define	_MAINFRM_H_

#include "ShipBar.h"

#define	WM_UPDATE_STATUS_TEXT	WM_USER+500

class MainFrame : public CFrameWnd
{
protected: // create from serialization only
	MainFrame();
	DECLARE_DYNCREATE(MainFrame)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MainFrame)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~MainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members
public:
	CDialogBar	m_joinbar;

	ShipBar		m_shipBar;
	CStatusBar  m_statusBar;
	CToolBar    m_toolBar;
	int			virgin;			// virgin?

	int			shipbarOnScreen;
	int			toolbarOnScreen;
	int			statusbarOnScreen;

// Generated message map functions
protected:
	//{{AFX_MSG(MainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSysColorChange();
	afx_msg void OnClose();
	//}}AFX_MSG

	afx_msg	LRESULT OnUpdateStatusText(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif		// _MAINFRM_H_
