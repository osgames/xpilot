/***************************************************************************\
*  Columns.h : Dialog box for selecting which columns to display            *
*  $Id$  					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

/////////////////////////////////////////////////////////////////////////////
// Columns dialog
#ifndef	_COLUMNS_H_
#define	_COLUMNS_H_

class Columns : public CDialog
{
// Construction
public:
	Columns(CWnd* pParent = NULL);		// standard constructor
	void operator=(const Columns&);	// copy

// Dialog Data
	//{{AFX_DATA(Columns)
	enum { IDD = IDD_COLUMNS };
	BOOL	m_bases;
	BOOL	m_fps;
	BOOL	m_ip;
	BOOL	m_mapauthor;
	BOOL	m_mapname;
	BOOL	m_mapsize;
	BOOL	m_servername;
	BOOL	m_playercount;
	BOOL	m_sound;
	BOOL	m_teams;
	BOOL	m_timing;
	BOOL	m_uptime;
	BOOL	m_version;
	BOOL	m_port;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Columns)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Columns)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#endif		// _COLUMNS_H_
