/***************************************************************************\
*  Config.h : Dialog box for selecting which columns to display             *
*  $Id$   					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#ifndef	_CONFIG_H_
#define	_CONFIG_H_
/////////////////////////////////////////////////////////////////////////////
// Config dialog

// The sort types
#define	SORT_NONE			0
#define	SORT_PLAYERCOUNT	1
#define	SORT_DISTANCE		2


class Config : public CDialog
{
// Construction
public:
	Config(CWnd* pParent = NULL);   // standard constructor
	void operator=(const Config&);		// copy
	
// Dialog Data
	//{{AFX_DATA(Config)
	enum { IDD = IDD_CONFIG };
	CString	m_hostname;
	CString	m_hostname2;
	BOOL	m_update;
	UINT	m_minutes;
	CString	m_xpilotntapp;
	int		m_primary;
	BOOL	m_dt_ding;
	BOOL	m_dt_focus;
	BOOL	m_dt_messagebox;
	CString	m_ding_wave;
	//}}AFX_DATA

	int		m_players;				// show players

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Config)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Config)
	afx_msg void OnDingSelect();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif		// _CONFIG_H_
