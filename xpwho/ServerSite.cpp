/***************************************************************************\
*  ServerSite.cpp : Describe a single server                                *
*  $Id$ 				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.1.1.1  2001/04/04 14:01:02  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.5  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.4  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols

#include "ServerSite.h"

ServerSite::~ServerSite()
{
	Player*	cp;

	while (!playerlist.IsEmpty())
	{
		cp = (Player*)playerlist.RemoveHead();
		delete cp;
	}
}

#ifdef _DEBUG
void ServerSite::AssertValid() const
{
	CObject::AssertValid();
}

void ServerSite::Dump(CDumpContext& dc) const
{
	CObject::Dump(dc);
	dc << "ServerSite " << servername;
}
#endif

// The list is in the following format:
// version:hostname:port number:number of users:map name:map size:map author
// :server status:number of home bases:frames per second:players list:sound
// :server uptime:number of team bases:timing:ip number:free bases:players queued
BOOL	ServerSite::ParseLine(CString* line)
{
//	TRACE("ServerSite::ParseLine <%.250s>\n", (const char*)*line);
	CString	par;
	int		i;

	watched = FALSE;
	map_changed = FALSE;

	i = line->Find(':');
	version = line->Left(i);
	par = line->Mid(i+1);
	
	i = par.Find(':');
	servername = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	serverport = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	playercount = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	mapname = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	mapsize = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	author = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	serverstatus = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	bases = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	fps = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	ParsePlayers(par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	sound = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	uptime = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	teams = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	timing = atoi((const char*)par.Left(i));
	par = par.Mid(i+1);

	i = par.Find(':');
	serverip = par.Left(i);
	par = par.Mid(i+1);

	i = par.Find(':');
	ParseFreeBases(par.Left(i));
	par = par.Mid(i+1);

	players_queued = atoi((const char*)par.Left(i));
//	serverip = par.Left(par.GetLength());
//	TRACE("ServerSite::ParseLine done\n");

	return(TRUE);
}
	
BOOL	ServerSite::ParseFreeBases(CString line)
{
	int		i;
	int		freet, freeb;

	for (i=0; i<MAX_TEAMS; i++)
		freebases[i] = 0;
	if (version < "4.0")
	{
		if (teams)
			for (i=0; i<MAX_TEAMS; i++)
				freebases[i] = 1;
	}
	while (!line.IsEmpty())
	{
		i = line.Find('=');
		if (i == -1)
			break;
		freet = atoi((const char*)line.Left(i));
		if (freet > MAX_TEAMS)
		{
			break;
		}
		line = line.Mid(i+1);
		i = line.Find(',');
		if (i == -1)
		{
			freeb = atoi((const char*)line);
			line.Empty();
		}
		else
		{
			freeb = atoi((const char*)line.Left(i));
			line = line.Mid(i+1);
		}
		freebases[freet] = freeb;
		
	}
	return(TRUE);
}

BOOL	ServerSite::ParsePlayers(CString line)
{
//	TRACE("ServerSite::ParsePlayers <%.250s>\n", (const char*)line);

	if (line.IsEmpty())
		return(TRUE);

	int		i;

	Player* cp;

	while (1)
	{
		cp = new Player;
		i = line.Find(',');
		if (i == -1)
			break;
		cp->name = line.Left(i);
		line = line.Mid(i+1);
		playerlist.AddTail(cp);
	}
	cp->name = line;
	playerlist.AddTail(cp);

//	TRACE("ServerSite::ParsePlayers done\n");
	return(TRUE);
}

#ifdef	_DEBUG
void	ServerSite::TRACEdump()
{
#if 0
	TRACE("ServerSite:: servername=<%s> map=<%s> version=<%s>\n", 
		(const char*)servername, (const char*)mapname, (const char*)version);
	TRACE("ServerSite:: playercount=%d, fps=%d serverip=<%s>\n", 
		playercount, fps, (const char*)serverip);
#endif
}
#endif

#ifdef _DEBUG
void Player::AssertValid() const
{
	CObject::AssertValid();
}

void Player::Dump(CDumpContext& dc) const
{
	CObject::Dump(dc);
	dc << "Player " << name;
}
#endif


Player::Player(const Player& cp)
{
	name = cp.name;
	SetNew();
}

#ifdef _DEBUG
void Watch::AssertValid() const
{
	CObject::AssertValid();
}

void Watch::Dump(CDumpContext& dc) const
{
	CObject::Dump(dc);
	dc << "CWatch " << servername;
}
#endif

Watch::Watch(ServerSite* csi)
{
	servername = csi->servername;
	serverport = csi->serverport;
}

bool Watch::operator==(ServerSite& csi)
{
	if (servername == csi.servername && serverport == csi.serverport)
		return(true);
	return(false);
}