/***************************************************************************\
*  UserName.h : Configure the nick=user@host thing                          *
*  $Id$ 					*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 */

/////////////////////////////////////////////////////////////////////////////
#ifndef	_USERNAME_H_
#define _USERNAME_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "XPwhoDoc.h"

/////////////////////////////////////////////////////////////////////////////
// UserName dialog

class UserName : public CDialog
{
// Construction
public:
	UserName(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(UserName)
	enum { IDD = IDD_USERNAME };
	CEdit	m_fullname;
	CEdit	m_nick;
	CEdit	m_user;
	CEdit	m_host;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UserName)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UserName)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditNick();
	afx_msg void OnChangeEditUser();
	afx_msg void OnChangeEditHost();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
	void SetDoc(XPwhoDoc* _doc)			{doc = _doc; };

	CString		sNick;
	CString		sUser;
	CString		sHost;

private:
	void		UpdateFullName();
	CString		GetDefaultUser();
	CString		GetDefaultHost();

	XPwhoDoc*	doc;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // _USERNAME_H_
