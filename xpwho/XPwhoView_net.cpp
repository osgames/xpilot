/***************************************************************************\
*  xpwhovwn.cpp : implementation of the XPwhoView class network functions	*
*  Copyrightę 1995-1998 - BuckoSoft, Inc.									*
*																			*
*  This file contains the network routines for XPwhoNT						*
*  These are all member functions of XPwhoView								*
*																			*
*  $Id$   			*
\***************************************************************************/

#include "stdafx.h"
#include "resource.h"       // main symbols

#include "ServerSite.h"
#include "config.h"
#include "XPwhoDoc.h"
#include "XPwhoView.h"

#if		_USESOCK && defined(MFC30)
#include "afxsock.h"
#endif


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#if _USESOCK

extern	const char*	GetWSockErrText(int	error);

const char* ws_states[] = {
	"WS_INIT",
	"WS_IDLE",
	"WS_RESOLVEHOST",
	"WS_CONNECT",
	"WS_RECV",
	"WS_XPILOT",
	"WS_ERROR"
	};


long XPwhoView::OnWSA_RECV(WPARAM wParam, LPARAM lParam)
{
#define	TEMPSIZE	20000
	char	szTemp[ TEMPSIZE+1 ];
	CString	s;
	int		status;
	int		error;

	if (WSAGETSELECTEVENT(lParam) == FD_READ)
	{
		status = recv((SOCKET)wParam, szTemp, TEMPSIZE, NO_FLAGS_SET );

		TRACE("OnWSA_RECV: received %d bytes\n", status);
		if (status == SOCKET_ERROR)
		{
			if ((error = WSAGetLastError()) == WSAEWOULDBLOCK)
			{
				szTemp[TEMPSIZE] = '\0';
				ProcessRecvData(szTemp);
			}
			else
			{
				s.Format("%s Error trying to receive data = %d <%s>",
						GetHost(), error, GetWSockErrText(error));
				SetStatus(s);
				closesocket(mysocket);
				mysocket = NULL;
				SetState(WS_IDLE);
			}
		}
		else
		if (status > TEMPSIZE)
		{
			// whoa! this is toooo big!
		}
		else
		if (status)
		{
			szTemp[ status ] = '\0';
			TRACE("Data=<%.100s>\n", szTemp);
			bytesread += status;
			s.Format("%s bytes received: %ld", GetHost(), bytesread);
			SetStatus(s);
			ProcessRecvData(szTemp);
		}
		else
		{
//			MessageBox( hWnd, "Connection broken", "Error", MB_OK);
			s.Format("%s Connection Broken", GetHost());
			SetStatus(s);
			closesocket(mysocket);
			mysocket = NULL;
			SetState(WS_IDLE);
		}
	}
	else
	{    /* FD_CLOSE -- connection dropped */
	}
	return(1);
}

long XPwhoView::OnWSA_RESOLVEHOST(WPARAM wParam, LPARAM lParam)
{
	struct hostent*	he = (struct hostent*)namebuf;
	int				ret;
	CString			s;

	if (WSAGETASYNCERROR(lParam))
	{
		XPwhoDoc*	pDoc = GetDoc();
		TRACE("OnWSA_RESOLVEHOST: %s\n", s);
		s.Format("%s Error resolving hostname. error=%d (%s)",
			GetHost(),
			WSAGETASYNCERROR(lParam), GetWSockErrText(WSAGETASYNCERROR(lParam)));
		SetStatus(s);
		SetState(WS_ERROR);
	}
	else
	{
		TRACE("OnWSA_RESOLVEHOST: Host resolved\n");
		TRACE("OnWSA_RESOLVEHOST: name=<%s> addr=<%s>\n",
			he->h_name,
			inet_ntoa(*((struct in_addr*)he->h_addr)));
		s.Format("%s Connecting to %s (%s)", 
			GetHost(), he->h_name, inet_ntoa(*(struct in_addr*)he->h_addr));
		SetStatus(s);

		if (mysocket)
		{
			closesocket(mysocket);
			mysocket = NULL;
		}

		if (!mysocket)
		{
			mysocket = socket(PF_INET, SOCK_STREAM, 0);
			if (mysocket == INVALID_SOCKET)
			{
				int error;
				error = WSAGetLastError();
				s.Format("%s Failed to create socket error=%d <%s>",
					GetHost(), error, GetWSockErrText(error));
				SetStatus(s);
				return(1);
			}
			///
			SOCKADDR_IN sin;
			u_short alport = IPPORT_RESERVED;

			sin.sin_family = AF_INET;
			sin.sin_addr.s_addr = 0;

			for (;;)
			{
				sin.sin_port = htons(alport);
				if (bind(mysocket, (LPSOCKADDR)&sin, sizeof (sin)) == 0)
				{
        	    	/* it worked */
					break;
				}
				if ( WSAGetLastError() != WSAEADDRINUSE)
				{
            		/* fail */
					continue;
				}
				alport--;
				if (alport == IPPORT_RESERVED/2 )
				{
					/* fail--all unassigned reserved ports are */
					/* in use. */
					s.Format("%s Can't bind socket", GetHost());
					SetStatus(s);
					SetState(WS_IDLE);
					return(1);
				}
			}
			ret = WSAAsyncSelect(mysocket, this->m_hWnd, WSA_EVENT, FD_CONNECT|FD_CLOSE|FD_READ);
			if (ret)
			{
				s.Format("%s AsyncSelect error=%d (%s)",
					GetHost(), WSAGetLastError(), GetWSockErrText(WSAGetLastError()));
				SetStatus(s);
				return(1);
			}
		}
		
		SOCKADDR_IN	sockaddr;
		sockaddr.sin_family = AF_INET;
		sockaddr.sin_port = htons(4401);
#ifdef	WIN32
		memcpy((char FAR *)&(sockaddr.sin_addr), he->h_addr, he->h_length);
#else
		_fmemcpy((LPVOID)&(sockaddr.sin_addr), (LPVOID)he->h_addr, he->h_length);
#endif
		ret = connect(mysocket, (PSOCKADDR)&sockaddr, sizeof(sockaddr));
		if (ret)
		{
			if (WSAGetLastError() != WSAEWOULDBLOCK)
			{
				// failed
				s.Format("%s Connect failed error=%d (%s)",
					GetHost(),
					WSAGetLastError(), GetWSockErrText(WSAGetLastError()));
				SetStatus(s);
				SetState(WS_IDLE);
				return(1);
			}
		}
	}
	return(1);
}

long XPwhoView::OnWSA_CONNECT(WPARAM wParam, LPARAM lParam)
{
	TRACE("OnWSA_CONNECT\n");
	return(1);
}

long XPwhoView::OnWSA_EVENT(WPARAM wParam, LPARAM lParam)
{
	CString	s;

	TRACE("OnWSA_EVENT: event=$%02x\n", WSAGETSELECTEVENT(lParam));

	switch(WSAGETSELECTEVENT(lParam))
	{
		case FD_CONNECT:
			if (WSAGETSELECTERROR(lParam))
			{
				s.Format("%s Connect failed. error=%d (%s)", 
					GetHost(),
					WSAGETSELECTERROR(lParam),
					GetWSockErrText(WSAGETSELECTERROR(lParam)));
				SetStatus(s);
				SetState(WS_ERROR);
				return(1);
			}
			s.Format("%s Receiving data...", GetHost());
			SetStatus(s);
			bytesread = 0;
			Prelude();
			ws_state = WS_RECV;					// it should just automatically read the data
			SetTimer(WS_TIMER, WST_RECV, NULL);	// time out if we don't
			break;
		case FD_READ:
			// whoa! we got data
			TRACE("We got Data!\n");
			OnWSA_RECV(wParam, lParam);
			break;
		case FD_CLOSE:
			TRACE("Close\n");
			closesocket(mysocket);
			mysocket = NULL;
			SetState(WS_IDLE);
			PostMortem();
			break;
	}
	return(1);
}

void XPwhoView::ResolveHostName()
{
	XPwhoDoc*	pDoc = GetDoc();
	HANDLE		ret;
	CString		s;
	if (!namebuf)
		namebuf = new char[MAXGETHOSTSTRUCT];
	
	s.Format("%s Finding host", GetHost());
	SetStatus(s);

	ret = WSAAsyncGetHostByName(this->m_hWnd, WSA_RESOLVEHOST, GetHost(), namebuf, MAXGETHOSTSTRUCT);

	if (!ret)
	{
		TRACE("%s ResolveHostName: WSAAsyncGetHostByName returned %d\n", 
			GetHost(), WSAGetLastError());
	}
	else
	{
		SetState(WS_RESOLVEHOST);
	}

}
#endif		// _USESOCK

void XPwhoView::PrintCountdown()
{
	char	s[50];
	sprintf(s, "%d", idlecount);
	SetStatusSecondCount(s);
}

void XPwhoView::OnServer() 
{
	ws_state = WS_INIT;
	OnTimer(myTimer);
}

static	UINT ws_timer[] = {
	WST_INIT, WST_IDLE, WST_RESOLVEHOST, WST_CONNECT, WST_RECV, WST_XPILOT, WST_ERROR
	};

void XPwhoView::SetState(WS_STATE st)
{
	XPwhoDoc*	pDoc = GetDoc();
	ws_state = st;

	if (ws_state == WS_ERROR)
	{
		activeMeta ^= TRUE;
		SetStatusActiveServer(GetHost());
		myTimer = SetTimer(WS_TIMER, 1000, NULL);
		idlecount = ws_timer[ws_state];
		PrintCountdown();
		return;
	}
	SetStatusActiveServer(GetHost());
	if (ws_state == WS_IDLE || ws_state == WS_ERROR)
	{
		CString	s;
		if (pDoc->ccfg.m_update)
		{
#if _USESOCK
			idlecount = pDoc->ccfg.m_minutes * 60;
#else
			idlecount = 15;
#endif
			myTimer = SetTimer(WS_TIMER, 1000, NULL);	// we count idle time in secs, not millisex
			PrintCountdown();
			if (ws_state == WS_IDLE)
				s.Format("%s Idle", GetHost());
		}
		else
		{
			if (ws_state == WS_IDLE)
				s.Format("%s Idle", GetHost());
		}
		if (ws_state == WS_IDLE)
			SetStatus(s);
		ws_state = WS_IDLE;
	}
	else
	{
		myTimer = SetTimer(WS_TIMER, ws_timer[ws_state], NULL);
//		PrintCountdown();
	}

}

void XPwhoView::OnTimer(UINT nIDEvent) 
{
	// Add your message handler code here and/or call default
//	XPwhoViewSUPERCLASS::OnTimer(nIDEvent);
	KillTimer(nIDEvent);
	CString	s;

	//TRACE("OnTimer: state = %s\n", ws_states[ws_state]);
	switch (ws_state)
	{
		case WS_XPILOT:
			if (FindWindow(NULL, "XPilot"))
			{
				SetState(WS_XPILOT);
				SetStatus("XPilot is running");
				SetStatusSecondCount("----");
				break;
			}
			else
			{
				if (GetDoc()->ccfg.m_update == TRUE)
					SetState(WS_INIT);
				else
					SetState(WS_IDLE);
			}
		case WS_ERROR:
		case WS_IDLE:
			if (FindWindow(NULL, "XPilot"))
			{
				SetState(WS_XPILOT);
				SetStatus("XPilot is running");
				SetStatusSecondCount("----");
				break;
			}
			if (--idlecount > 0)
			{
				myTimer = SetTimer(WS_TIMER, 1000, NULL);
				PrintCountdown();
				break;
			}
			SetStatusSecondCount("----");
		case WS_INIT:
			Prelude();
#if _FILETEST
			FileTest();
			SetState(WS_IDLE);
#else
			ResolveHostName();
#endif
			break;
		case WS_RESOLVEHOST:
			s.Format("%s Timed out trying to resolve hostname", GetHost());
			TRACE("%s\n", s);
			SetStatus(s);
			SetState(WS_ERROR);
			break;
		case WS_CONNECT:
			s.Format("%s Timed out trying to connect", GetHost());
			TRACE("%s\n", s);
			SetStatus(s);
			SetState(WS_ERROR);
			break;
		case WS_RECV:
			s.Format("%s Timed out waiting to receive data", GetHost());
			TRACE("%s\n", s);
			SetStatus(s);
			SetState(WS_ERROR);
			break;
	}
}

#if 0
//#else	// _USESOCK=0

// non socket version of our timer
void XPwhoView::OnTimer(UINT nIDEvent) 
{
	// Add your message handler code here and/or call default
	XPwhoViewSUPERCLASS::OnTimer(nIDEvent);
	KillTimer(nIDEvent);

	TRACE("OnTimer: state = %d\n", GetState());
	SetStatus("Calling Filetest");
	Prelude();
#if _FILETEST
	FileTest();
	SetState(WS_IDLE);
#endif
}
#endif

#if	_FILETEST
bool XPwhoView::FileTest()
{
#define	PASSCOUNT	3
	static	pass = 0;
	static	char*	fnames[] = {"meta1.txt", "meta2.txt", "meta3.txt", "meta4.txt" };
	
	XPwhoDoc*	pDoc = GetDocument();
	FILE	*fp;
	int		i;
	char	s[2049];

	if ((fp = fopen(fnames[pass], "r")) != NULL)
	{
		TRACE("FileTest: file <%s>\n", fnames[pass]);
		pass++;
		if (pass == PASSCOUNT)
			pass = 0;
		for (i=0; i<3; i++)
		{
			if (!fgets(s, 2048, fp))
			{
				fclose(fp);
				TRACE("Premature end of file\n");
				return(TRUE);
			}
			if (strchr(s, ':'))
			{
				TRACE("Unexpected ':' in first 3 lines...\n");
				break;
			}
		}

		size_t  count;
		while ((count = fread(s, 1, 2048, fp)) != NULL)
		{
			s[count] = '\0';
			ProcessRecvData(s);
		}
		fclose(fp);
		PostMortem();
		return(TRUE);
	}
	return(FALSE);
}
#endif
