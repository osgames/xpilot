/***************************************************************************\
*  XPwhoApp.h : Defines the class behaviors for the application             *
*  $Id$ 					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.3  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

#if _USESOCK
#include <winsock.h>
#endif

/////////////////////////////////////////////////////////////////////////////
// XPwhoApp:

class XPwhoApp : public CWinApp
{
public:
	XPwhoApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(XPwhoApp)
	public:
	virtual BOOL InitInstance();
	virtual int	 ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(XPwhoApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// The one and only XPwhoApp object

extern	XPwhoApp theApp;


#pragma warning(disable : 4237)	// "'bool' keyboard is reserved" baf!
/*typedef	unsigned char	bool; */
/////////////////////////////////////////////////////////////////////////////
