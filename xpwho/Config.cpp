/***************************************************************************\
*  Config.cpp : Dialog box for selecting which columns to display           *
*  $Id$ 					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.4  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.3  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "Config.h"
#include <mmsystem.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Config dialog


Config::Config(CWnd* pParent /*=NULL*/)
	: CDialog(Config::IDD, pParent)
{
	//{{AFX_DATA_INIT(Config)
	m_hostname = _T("");
	m_hostname2 = _T("");
	m_update = FALSE;
	m_minutes = 0;
	m_xpilotntapp = _T("");
	m_primary = -1;
	m_dt_ding = FALSE;
	m_dt_focus = FALSE;
	m_dt_messagebox = FALSE;
	m_ding_wave = _T("");
	//}}AFX_DATA_INIT

	m_players = FALSE;
}

void Config::operator=(const Config& cfg)
{
	m_hostname = cfg.m_hostname;
	m_hostname2 = cfg.m_hostname2;
	m_update = cfg.m_update;
	m_minutes = cfg.m_minutes;
	m_players = cfg.m_players;
	m_xpilotntapp = cfg.m_xpilotntapp;
	m_primary = cfg.m_primary;
	m_ding_wave = cfg.m_ding_wave;
	m_dt_ding = cfg.m_dt_ding;
	m_dt_messagebox = cfg.m_dt_messagebox;
	m_dt_focus = cfg.m_dt_focus;
}

void Config::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Config)
	DDX_Text(pDX, IDC_HOSTNAME, m_hostname);
	DDX_Text(pDX, IDC_HOSTNAME2, m_hostname2);
	DDX_Check(pDX, IDC_UPDATE, m_update);
	DDX_Text(pDX, IDC_MINUTES, m_minutes);
	DDX_Text(pDX, IDC_XPILOTNT, m_xpilotntapp);
	DDX_Radio(pDX, IDC_RADIO1, m_primary);
	DDX_Check(pDX, IDC_DT_DING, m_dt_ding);
	DDX_Check(pDX, IDC_DT_FOCUS, m_dt_focus);
	DDX_Check(pDX, IDC_DT_MESSAGEBOX, m_dt_messagebox);
	DDX_Text(pDX, IDC_DING_WAVE, m_ding_wave);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Config, CDialog)
	//{{AFX_MSG_MAP(Config)
	ON_BN_CLICKED(IDC_DING_SELECT, OnDingSelect)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Config message handlers

void Config::OnDingSelect() 
{
	// TODO: Add your control notification handler code here
	static char szFilter[] = "Wave Files (*.wav)|*.wav|All Files (*.*)|*.*||";
	CFileDialog cfd(TRUE, ".wav", NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		szFilter);
	int ret = cfd.DoModal();
	if (ret == IDOK)
	{
		CString cs = cfd.GetPathName();
		SendDlgItemMessage(IDC_DING_WAVE, WM_SETTEXT, 0, (LPARAM)(const char*)cs);
		PlaySound((const char*)cs, NULL, SND_FILENAME);
	}
}
