/***************************************************************************\
*  SysInfo.cpp : Gather system info to try to help stuck newbies            *
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.4  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.3  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "SysInfo.h"

#include <winsock.h>
#define	MAXHOSTNAMELEN	64

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SysInfo dialog


SysInfo::SysInfo(CWnd* pParent /*=NULL*/)
	: CDialog(SysInfo::IDD, pParent)
{
	//{{AFX_DATA_INIT(SysInfo)
	m_string = _T("");
	//}}AFX_DATA_INIT
}


void SysInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	SetupInfo();
	//{{AFX_DATA_MAP(SysInfo)
	DDX_Text(pDX, IDC_EDIT1, m_string);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SysInfo, CDialog)
	//{{AFX_MSG_MAP(SysInfo)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SysInfo message handlers

extern "C"	void	GetLocalHostName(char *name, unsigned size);

extern		WSADATA	wsadata;

void SysInfo::SetupInfo()
{
	char	s[256];
	CString	cs;
	int		ret = 0;
	CDC*	cdc = GetParent()->GetDC();
	HDC		hDC = cdc->m_hDC;

	GetLocalHostName(s, 256);
	m_string = "Hostname: <";
	if (ret)
		m_string += "Error getting hostname";
	else
		m_string += s;
	m_string += ">\r\n";

	cs.Format("Winsock: Version %d\r\n", wsadata.wVersion);
	m_string += cs;
	m_string +=	" Description: <";
	m_string += wsadata.szDescription;
	m_string += ">\r\n Status: <";
	m_string += wsadata.szSystemStatus;
	cs.Format(">\r\n MaxSockets: %d\r\n MaxUdp: %d\r\n", 
		wsadata.iMaxSockets, wsadata.iMaxUdpDg);
	m_string += cs;

	cs.Format("Display Driver: Version %d, RasterDisplay: %c\r\n", 
		GetDeviceCaps(hDC, DRIVERVERSION),
		GetDeviceCaps(hDC, DT_RASDISPLAY) ? 'Y' : 'N');
	m_string += cs;
	cs.Format(" Number of colors: %d, pens: %d, brushes: %d, fonts: %d\r\n",
		GetDeviceCaps(hDC, NUMCOLORS), GetDeviceCaps(hDC, NUMPENS),
		GetDeviceCaps(hDC, NUMBRUSHES), GetDeviceCaps(hDC, NUMFONTS));
	m_string += cs;
	cs.Format(" ColorRes: %d, SizePalette: %d, NumReserved: %d, BitsPixel: %d\r\n", 
		GetDeviceCaps(hDC, COLORRES), GetDeviceCaps(hDC, SIZEPALETTE),
		GetDeviceCaps(hDC, NUMRESERVED), GetDeviceCaps(hDC, BITSPIXEL));
	m_string += cs;
	cs.Format(" Supports: Palette: %c, BitBlt: %c, StretchBlt: %c, Scaling: %c\r\n",
		GetDeviceCaps(hDC, RC_PALETTE) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_BITBLT) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_STRETCHBLT) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_SCALING) ? 'Y' : 'N');
	m_string += cs;
	cs.Format("  DIBitmap: %c, DibToDev: %c, StretchDib: %c Bitmap64K: %c\r\n",
		GetDeviceCaps(hDC, RC_DI_BITMAP) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_DIBTODEV) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_STRETCHDIB) ? 'Y' : 'N',
		GetDeviceCaps(hDC, RC_BITMAP64) ? 'Y' : 'N');
	m_string += cs;
	cs.Format("  Curves: %c, Circles: %c, Ellipses: %c, Pie: %c\r\n",
		GetDeviceCaps(hDC, CC_NONE) ? 'Y' : 'N',
		GetDeviceCaps(hDC, CC_CIRCLES) ? 'Y' : 'N',
		GetDeviceCaps(hDC, CC_ELLIPSES) ? 'Y' : 'N',
		GetDeviceCaps(hDC, CC_PIE) ? 'Y' : 'N');
	m_string += cs;
	cs.Format("  Lines: %c, Wide: %c, Styled: %c WideStyled: %c\r\n",
		GetDeviceCaps(hDC, LC_NONE) ? 'Y' : 'N',
		GetDeviceCaps(hDC, LC_WIDE) ? 'Y' : 'N',
		GetDeviceCaps(hDC, LC_STYLED) ? 'Y' : 'N',
		GetDeviceCaps(hDC, LC_WIDESTYLED) ? 'Y' : 'N');
	m_string += cs;

	ReleaseDC(cdc);
}

void SysInfo::GetLocalHostName(char *name, unsigned size)
{
    struct hostent	*he = NULL;
	struct hostent  *xpilot_he = NULL;
#ifndef	_WINDOWS
	struct hostent  tmp;
#endif
    int			xpilot_len;
    char		*alias, *dot;
    char		xpilot_hostname[MAXHOSTNAMELEN];
    static const char	xpilot[] = "xpilot";
#ifdef VMS
    char                vms_inethost[MAXHOSTNAMELEN]   = "UCX$INET_HOST";
    char                vms_inetdomain[MAXHOSTNAMELEN] = "UCX$INET_DOMAIN";
    char                vms_host[MAXHOSTNAMELEN];
    char                vms_domain[MAXHOSTNAMELEN];
    int                 namelen;
#endif

    xpilot_len = strlen(xpilot);

#ifndef	_WINDOWS	/* this takes FOREVER! zzzz */
    /* Make a wild guess that a "xpilot" hostname or alias is in this domain */
    if ((xpilot_he = gethostbyname(xpilot)) != NULL) {
	strcpy(xpilot_hostname, xpilot_he->h_name);	/* copy data to buffer */
	tmp = *xpilot_he;
	xpilot_he = &tmp;
    }
#endif

    gethostname(name, size);
    if ((he = gethostbyname(name)) == NULL) {
	return;
    }
    strncpy(name, he->h_name, size);
    name[size - 1] = '\0';
    /*
     * If there are no dots in the name then we don't have the FQDN,
     * and if the address is of the normal Internet type
     * then we try to get the FQDN via the backdoor of the IP address.
     * Let's hope it works :)
     */

    if (strchr(he->h_name, '.') == NULL
	&& he->h_addrtype == AF_INET
	&& he->h_length == 4) {
	unsigned long a = 0;
	memcpy((void *)&a, he->h_addr_list[0], 4);
	if ((he = gethostbyaddr((char *)&a, 4, AF_INET)) != NULL
	    && strchr(he->h_name, '.') != NULL) {
	    strncpy(name, he->h_name, size);
	    name[size - 1] = '\0';
	}
	else {
#if !defined(VMS)
	    FILE *fp = fopen("/etc/resolv.conf", "r");
	    if (fp) {
		char *s, buf[256];
		while (fgets(buf, sizeof buf, fp)) {
		    if ((s = strtok(buf, " \t\r\n")) != NULL
			&& !strcmp(s, "domain")
			&& (s = strtok(NULL, " \t\r\n")) != NULL) {
			strcat(name, ".");
			strcat(name, s);
			break;
		    }
		}
		fclose(fp);
	    }
#else
            vms_trnlnm(vms_inethost, namelen, vms_host);
            vms_trnlnm(vms_inetdomain, namelen, vms_domain);
            strcpy(name, vms_host);
            strcat(name, ".");
            strcat(name, vms_domain);
#endif
	    return;
	}
    }

    /*
     * If a "xpilot" host is found compare if it's this one.
     * and if so, make the local name as "xpilot.*"
     */
    if (xpilot_he != NULL) {               /* host xpilot was found */
	if (strcmp(he->h_name, xpilot_hostname) == 0) {
	   /*
	    * Identical official names. Can they be different hosts after this?
	    * Find out the name which starts with "xpilot" and use it:
	    */
	    xpilot_he = gethostbyname(xpilot); /* read again the aliases info */
	    if (xpilot_he == NULL)       /* shouldn't happen */
		return;

	    if (strncmp(xpilot, xpilot_he->h_name, xpilot_len) != 0) {
		/*
		 * the official hostname doesn't begin "xpilot"
		 * so we'll find the alias:
		 */
		int i;
		for (i = 0; xpilot_he->h_aliases[i] != NULL; i++) {
		    alias = xpilot_he->h_aliases[i];
		    if (!strncmp(xpilot, alias, xpilot_len)) {
			strcpy(xpilot_hostname, alias);
			if (!strchr(alias, '.') && (dot = strchr(name, '.'))) {
			    strcat(xpilot_hostname + strlen(xpilot_hostname), dot);
			}
			strncpy(name, xpilot_hostname, size);
			return;
		    }
		}
	    } else {
		strncpy(name, xpilot_he->h_name, size);
		return;
	    }
	}
	/* NOT REATCHED */
    }
} /* GetLocalHostName */

