/***************************************************************************\
*  Columns.cpp : Dialog box for selecting which columns to display          *
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.5  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.4  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 *  Revision 1.3  2001/04/01 19:44:57  dick
 *  Prepare source for public release
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "Columns.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Columns dialog


Columns::Columns(CWnd* pParent /*=NULL*/)
	: CDialog(Columns::IDD, pParent)
{
	//{{AFX_DATA_INIT(Columns)
	m_bases = FALSE;
	m_fps = FALSE;
	m_ip = FALSE;
	m_mapauthor = FALSE;
	m_mapname = FALSE;
	m_mapsize = FALSE;
	m_servername = FALSE;
	m_playercount = FALSE;
	m_sound = FALSE;
	m_teams = FALSE;
	m_timing = FALSE;
	m_uptime = FALSE;
	m_version = FALSE;
	m_port = FALSE;
	//}}AFX_DATA_INIT
}

void Columns::operator=(const Columns& c)
{
	m_bases = c.m_bases;
	m_fps = c.m_fps;
	m_ip = c.m_ip;
	m_mapauthor = c.m_mapauthor;
	m_mapname = c.m_mapname;
	m_mapsize = c.m_mapsize;
	m_servername = c.m_servername;
	m_playercount = c.m_playercount;
	m_sound = c.m_sound;
	m_teams = c.m_teams;
	m_timing = c.m_timing;
	m_uptime = c.m_uptime;
	m_version = c.m_version;
	m_port = c.m_port;
}

void Columns::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(Columns)
	DDX_Check(pDX, IDC_BASES, m_bases);
	DDX_Check(pDX, IDC_FPS, m_fps);
	DDX_Check(pDX, IDC_IP, m_ip);
	DDX_Check(pDX, IDC_MAPAUTHOR, m_mapauthor);
	DDX_Check(pDX, IDC_MAPNAME, m_mapname);
	DDX_Check(pDX, IDC_MAPSIZE, m_mapsize);
	DDX_Check(pDX, IDC_SERVERNAME, m_servername);
	DDX_Check(pDX, IDC_PLAYERCOUNT, m_playercount);
	DDX_Check(pDX, IDC_SOUND, m_sound);
	DDX_Check(pDX, IDC_TEAMS, m_teams);
	DDX_Check(pDX, IDC_TIMING, m_timing);
	DDX_Check(pDX, IDC_UPTIME, m_uptime);
	DDX_Check(pDX, IDC_VERSION, m_version);
	DDX_Check(pDX, IDC_PORT, m_port);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(Columns, CDialog)
	//{{AFX_MSG_MAP(Columns)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Columns message handlers
