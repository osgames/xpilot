# Microsoft Developer Studio Generated NMAKE File, Based on xpwho.dsp
!IF "$(CFG)" == ""
CFG=XPwho - Win32 Debug
!MESSAGE No configuration specified. Defaulting to XPwho - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "XPwho - Win32 Debug" && "$(CFG)" != "XPwho - Win32 Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xpwho.mak" CFG="XPwho - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "XPwho - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "XPwho - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "XPwho - Win32 Debug"

OUTDIR=.\WinDebug
INTDIR=.\WinDebug
# Begin Custom Macros
OutDir=.\WinDebug
# End Custom Macros

ALL : "$(OUTDIR)\xpwho.exe" "$(OUTDIR)\xpwho.bsc"


CLEAN :
	-@erase "$(INTDIR)\Columns.obj"
	-@erase "$(INTDIR)\Columns.sbr"
	-@erase "$(INTDIR)\Config.obj"
	-@erase "$(INTDIR)\Config.sbr"
	-@erase "$(INTDIR)\MainFrame.obj"
	-@erase "$(INTDIR)\MainFrame.sbr"
	-@erase "$(INTDIR)\ServerSite.obj"
	-@erase "$(INTDIR)\ServerSite.sbr"
	-@erase "$(INTDIR)\ShipBar.obj"
	-@erase "$(INTDIR)\ShipBar.sbr"
	-@erase "$(INTDIR)\SysInfo.obj"
	-@erase "$(INTDIR)\SysInfo.sbr"
	-@erase "$(INTDIR)\UserName.obj"
	-@erase "$(INTDIR)\UserName.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\wsockerrs.obj"
	-@erase "$(INTDIR)\wsockerrs.sbr"
	-@erase "$(INTDIR)\xp-winAbout.obj"
	-@erase "$(INTDIR)\xp-winAbout.sbr"
	-@erase "$(INTDIR)\XPwho.res"
	-@erase "$(INTDIR)\XPwhoApp.obj"
	-@erase "$(INTDIR)\XPwhoApp.sbr"
	-@erase "$(INTDIR)\XPwhoDoc.obj"
	-@erase "$(INTDIR)\XPwhoDoc.sbr"
	-@erase "$(INTDIR)\XPwhoMath.obj"
	-@erase "$(INTDIR)\XPwhoMath.sbr"
	-@erase "$(INTDIR)\XPwhoView.obj"
	-@erase "$(INTDIR)\XPwhoView.sbr"
	-@erase "$(INTDIR)\XPwhoView_net.obj"
	-@erase "$(INTDIR)\XPwhoView_net.sbr"
	-@erase "$(OUTDIR)\xpwho.bsc"
	-@erase "$(OUTDIR)\xpwho.exe"
	-@erase "$(OUTDIR)\xpwho.ilk"
	-@erase "$(OUTDIR)\xpwho.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "_XPMONNT_" /D "WIN32" /D "_WINDOWS" /D _FILETEST=0 /D _USESOCK=1 /D "_MFC30" /D "_AFXDLL" /D "_MBCS" /D "_NAMEDSHIPS" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\XPwho.res" /d "_DEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\xpwho.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Columns.sbr" \
	"$(INTDIR)\Config.sbr" \
	"$(INTDIR)\MainFrame.sbr" \
	"$(INTDIR)\ServerSite.sbr" \
	"$(INTDIR)\ShipBar.sbr" \
	"$(INTDIR)\SysInfo.sbr" \
	"$(INTDIR)\UserName.sbr" \
	"$(INTDIR)\wsockerrs.sbr" \
	"$(INTDIR)\XPwhoApp.sbr" \
	"$(INTDIR)\XPwhoDoc.sbr" \
	"$(INTDIR)\XPwhoMath.sbr" \
	"$(INTDIR)\XPwhoView.sbr" \
	"$(INTDIR)\XPwhoView_net.sbr" \
	"$(INTDIR)\xp-winAbout.sbr"

"$(OUTDIR)\xpwho.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=winmm.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\xpwho.pdb" /debug /machine:I386 /out:"$(OUTDIR)\xpwho.exe" 
LINK32_OBJS= \
	"$(INTDIR)\Columns.obj" \
	"$(INTDIR)\Config.obj" \
	"$(INTDIR)\MainFrame.obj" \
	"$(INTDIR)\ServerSite.obj" \
	"$(INTDIR)\ShipBar.obj" \
	"$(INTDIR)\SysInfo.obj" \
	"$(INTDIR)\UserName.obj" \
	"$(INTDIR)\wsockerrs.obj" \
	"$(INTDIR)\XPwhoApp.obj" \
	"$(INTDIR)\XPwhoDoc.obj" \
	"$(INTDIR)\XPwhoMath.obj" \
	"$(INTDIR)\XPwhoView.obj" \
	"$(INTDIR)\XPwhoView_net.obj" \
	"$(INTDIR)\XPwho.res" \
	"$(INTDIR)\xp-winAbout.obj"

"$(OUTDIR)\xpwho.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "XPwho - Win32 Release"

OUTDIR=.\WinRel
INTDIR=.\WinRel
# Begin Custom Macros
OutDir=.\WinRel
# End Custom Macros

ALL : "$(OUTDIR)\xpwho.exe" "$(OUTDIR)\xpwho.bsc"


CLEAN :
	-@erase "$(INTDIR)\Columns.obj"
	-@erase "$(INTDIR)\Columns.sbr"
	-@erase "$(INTDIR)\Config.obj"
	-@erase "$(INTDIR)\Config.sbr"
	-@erase "$(INTDIR)\MainFrame.obj"
	-@erase "$(INTDIR)\MainFrame.sbr"
	-@erase "$(INTDIR)\ServerSite.obj"
	-@erase "$(INTDIR)\ServerSite.sbr"
	-@erase "$(INTDIR)\ShipBar.obj"
	-@erase "$(INTDIR)\ShipBar.sbr"
	-@erase "$(INTDIR)\SysInfo.obj"
	-@erase "$(INTDIR)\SysInfo.sbr"
	-@erase "$(INTDIR)\UserName.obj"
	-@erase "$(INTDIR)\UserName.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\wsockerrs.obj"
	-@erase "$(INTDIR)\wsockerrs.sbr"
	-@erase "$(INTDIR)\xp-winAbout.obj"
	-@erase "$(INTDIR)\xp-winAbout.sbr"
	-@erase "$(INTDIR)\XPwho.res"
	-@erase "$(INTDIR)\XPwhoApp.obj"
	-@erase "$(INTDIR)\XPwhoApp.sbr"
	-@erase "$(INTDIR)\XPwhoDoc.obj"
	-@erase "$(INTDIR)\XPwhoDoc.sbr"
	-@erase "$(INTDIR)\XPwhoMath.obj"
	-@erase "$(INTDIR)\XPwhoMath.sbr"
	-@erase "$(INTDIR)\XPwhoView.obj"
	-@erase "$(INTDIR)\XPwhoView.sbr"
	-@erase "$(INTDIR)\XPwhoView_net.obj"
	-@erase "$(INTDIR)\XPwhoView_net.sbr"
	-@erase "$(OUTDIR)\xpwho.bsc"
	-@erase "$(OUTDIR)\xpwho.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP_PROJ=/nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "_XPMONNT_" /D "WIN32" /D "_WINDOWS" /D _FILETEST=0 /D _USESOCK=1 /D "_MFC30" /D "_AFXDLL" /D "_MBCS" /D "_NAMEDSHIPS" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\XPwho.res" /d "NDEBUG" /d "_AFXDLL" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\xpwho.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Columns.sbr" \
	"$(INTDIR)\Config.sbr" \
	"$(INTDIR)\MainFrame.sbr" \
	"$(INTDIR)\ServerSite.sbr" \
	"$(INTDIR)\ShipBar.sbr" \
	"$(INTDIR)\SysInfo.sbr" \
	"$(INTDIR)\UserName.sbr" \
	"$(INTDIR)\wsockerrs.sbr" \
	"$(INTDIR)\XPwhoApp.sbr" \
	"$(INTDIR)\XPwhoDoc.sbr" \
	"$(INTDIR)\XPwhoMath.sbr" \
	"$(INTDIR)\XPwhoView.sbr" \
	"$(INTDIR)\XPwhoView_net.sbr" \
	"$(INTDIR)\xp-winAbout.sbr"

"$(OUTDIR)\xpwho.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=winmm.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\xpwho.pdb" /machine:I386 /out:"$(OUTDIR)\xpwho.exe" 
LINK32_OBJS= \
	"$(INTDIR)\Columns.obj" \
	"$(INTDIR)\Config.obj" \
	"$(INTDIR)\MainFrame.obj" \
	"$(INTDIR)\ServerSite.obj" \
	"$(INTDIR)\ShipBar.obj" \
	"$(INTDIR)\SysInfo.obj" \
	"$(INTDIR)\UserName.obj" \
	"$(INTDIR)\wsockerrs.obj" \
	"$(INTDIR)\XPwhoApp.obj" \
	"$(INTDIR)\XPwhoDoc.obj" \
	"$(INTDIR)\XPwhoMath.obj" \
	"$(INTDIR)\XPwhoView.obj" \
	"$(INTDIR)\XPwhoView_net.obj" \
	"$(INTDIR)\XPwho.res" \
	"$(INTDIR)\xp-winAbout.obj"

"$(OUTDIR)\xpwho.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL_PROJ=/mktyplib203 

!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("xpwho.dep")
!INCLUDE "xpwho.dep"
!ELSE 
!MESSAGE Warning: cannot find "xpwho.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "XPwho - Win32 Debug" || "$(CFG)" == "XPwho - Win32 Release"
SOURCE=..\xpilot\src\common\checknames.c
SOURCE=..\xpilot\src\common\error.c
SOURCE=..\xpilot\src\common\math.c
SOURCE=..\xpilot\src\common\portability.c
SOURCE=..\xpilot\src\common\randommt.c
SOURCE=..\xpilot\src\common\shipshape.c
SOURCE=..\xpilot\src\common\strdup.c
SOURCE=..\xpilot\src\common\strlcpy.c
SOURCE=..\xpilot\src\client\NT\winAbout.cpp
SOURCE=".\xp-shipshape.c"
SOURCE=".\xp-winAbout.cpp"

"$(INTDIR)\xp-winAbout.obj"	"$(INTDIR)\xp-winAbout.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Columns.cpp

"$(INTDIR)\Columns.obj"	"$(INTDIR)\Columns.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\Config.cpp

"$(INTDIR)\Config.obj"	"$(INTDIR)\Config.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\MainFrame.cpp

"$(INTDIR)\MainFrame.obj"	"$(INTDIR)\MainFrame.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ServerSite.cpp

"$(INTDIR)\ServerSite.obj"	"$(INTDIR)\ServerSite.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\ShipBar.cpp

"$(INTDIR)\ShipBar.obj"	"$(INTDIR)\ShipBar.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\SysInfo.cpp

"$(INTDIR)\SysInfo.obj"	"$(INTDIR)\SysInfo.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\UserName.cpp

"$(INTDIR)\UserName.obj"	"$(INTDIR)\UserName.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\wsockerrs.cpp

"$(INTDIR)\wsockerrs.obj"	"$(INTDIR)\wsockerrs.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\XPwho.rc

"$(INTDIR)\XPwho.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\XPwhoApp.cpp

"$(INTDIR)\XPwhoApp.obj"	"$(INTDIR)\XPwhoApp.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\XPwhoDoc.cpp

"$(INTDIR)\XPwhoDoc.obj"	"$(INTDIR)\XPwhoDoc.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\XPwhoMath.c

"$(INTDIR)\XPwhoMath.obj"	"$(INTDIR)\XPwhoMath.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\XPwhoView.cpp

"$(INTDIR)\XPwhoView.obj"	"$(INTDIR)\XPwhoView.sbr" : $(SOURCE) "$(INTDIR)"


SOURCE=.\XPwhoView_net.cpp

"$(INTDIR)\XPwhoView_net.obj"	"$(INTDIR)\XPwhoView_net.sbr" : $(SOURCE) "$(INTDIR)"



!ENDIF 

