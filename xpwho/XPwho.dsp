# Microsoft Developer Studio Project File - Name="XPwho" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=XPwho - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "xpwho.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "xpwho.mak" CFG="XPwho - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "XPwho - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "XPwho - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "XPwho - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\WinDebug"
# PROP BASE Intermediate_Dir ".\WinDebug"
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\WinDebug"
# PROP Intermediate_Dir ".\WinDebug"
# ADD BASE CPP /nologo /MD /W3 /GX /Zi /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "_XPMONNT_" /D "WIN32" /D "_WINDOWS" /D _FILETEST=0 /D _USESOCK=1 /D "_MFC30" /D "_AFXDLL" /D "_MBCS" /D "_NAMEDSHIPS" /FR /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD MTL /mktyplib203
# ADD BASE RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 winmm.lib /nologo /subsystem:windows /debug /machine:I386
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "XPwho - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\WinRel"
# PROP BASE Intermediate_Dir ".\WinRel"
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\WinRel"
# PROP Intermediate_Dir ".\WinRel"
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /Yu"stdafx.h" /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "_XPMONNT_" /D "WIN32" /D "_WINDOWS" /D _FILETEST=0 /D _USESOCK=1 /D "_MFC30" /D "_AFXDLL" /D "_MBCS" /D "_NAMEDSHIPS" /FR /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD MTL /mktyplib203
# ADD BASE RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 winmm.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "XPwho - Win32 Debug"
# Name "XPwho - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;for;f90"
# Begin Group "XPilot common files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\xpilot\src\common\checknames.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\checknames.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\commonproto.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\config.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\const.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\client\NT\credits.inc.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\draw.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\error.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\error.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\math.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\pack.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\portability.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\portability.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\randommt.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\shipshape.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\strdup.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\strlcpy.c
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\client\NT\winAbout.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\client\NT\winAbout.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\NT\winX.h
# End Source File
# Begin Source File

SOURCE=..\xpilot\src\common\NT\winX_.h
# End Source File
# End Group
# Begin Group "XPilot wrappers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=".\xp-checknames.h"
# End Source File
# Begin Source File

SOURCE=".\xp-const.h"
# End Source File
# Begin Source File

SOURCE=".\xp-draw.h"
# End Source File
# Begin Source File

SOURCE=".\xp-shipshape.c"
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=".\xp-version.h"
# End Source File
# Begin Source File

SOURCE=".\xp-winAbout.cpp"
# End Source File
# Begin Source File

SOURCE=".\xp-winAbout.h"
# End Source File
# End Group
# Begin Source File

SOURCE=.\Columns.cpp
# End Source File
# Begin Source File

SOURCE=.\Columns.h
# End Source File
# Begin Source File

SOURCE=.\Config.cpp
# End Source File
# Begin Source File

SOURCE=.\Config.h
# End Source File
# Begin Source File

SOURCE=.\MainFrame.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrame.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\ServerSite.cpp
# End Source File
# Begin Source File

SOURCE=.\ServerSite.h
# End Source File
# Begin Source File

SOURCE=.\ShipBar.cpp
# End Source File
# Begin Source File

SOURCE=.\ShipBar.h
# End Source File
# Begin Source File

SOURCE=.\stdafx.h
# End Source File
# Begin Source File

SOURCE=.\SysInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\SysInfo.h
# End Source File
# Begin Source File

SOURCE=.\UserName.cpp
# End Source File
# Begin Source File

SOURCE=.\UserName.h
# End Source File
# Begin Source File

SOURCE=.\wsockerrs.cpp
# End Source File
# Begin Source File

SOURCE=.\XPwho.rc
# End Source File
# Begin Source File

SOURCE=.\XPwhoApp.cpp
# End Source File
# Begin Source File

SOURCE=.\XPwhoApp.h
# End Source File
# Begin Source File

SOURCE=.\XPwhoDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\XPwhoDoc.h
# End Source File
# Begin Source File

SOURCE=.\XPwhoMath.c
# End Source File
# Begin Source File

SOURCE=.\XPwhoView.cpp
# End Source File
# Begin Source File

SOURCE=.\XPwhoView.h
# End Source File
# Begin Source File

SOURCE=.\XPwhoView_net.cpp
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dingnote.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\xpilot.ico
# End Source File
# Begin Source File

SOURCE=.\res\XPwho.ico
# End Source File
# Begin Source File

SOURCE=.\res\XPwho.rc2
# End Source File
# Begin Source File

SOURCE=.\res\XPwhoDoc.ico
# End Source File
# End Group
# End Target
# End Project
