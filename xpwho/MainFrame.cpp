/***************************************************************************\
*  MainFrame.cpp : implementation of the MainFrame class                    *
*  $Id$   				*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.3  2002/05/24 17:28:14  dik
 *  Make persistent the user's display of the statusbar and toolbar.
 *
 *  Revision 1.2  2001/05/07 08:10:56  dik
 *  Honor the "primary meta" setting.  Display which meta we are talking to in
 *  the status bar.
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:00  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.5  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 *  Revision 1.4  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols
#include "XPwhoDoc.h"

#include "MainFrame.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MainFrame

IMPLEMENT_DYNCREATE(MainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(MainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(MainFrame)
	ON_WM_CREATE()
	ON_WM_SYSCOLORCHANGE()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP

	ON_MESSAGE(WM_UPDATE_STATUS_TEXT, OnUpdateStatusText)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// arrays of IDs used to initialize control bars
	
// toolbar buttons - IDs are command buttons
static UINT BASED_CODE buttons[] =
{
	// same order as in the bitmap 'toolbar.bmp'
	ID_SERVER,
		ID_SEPARATOR,
	ID_FILE_PRINT,
		ID_SEPARATOR,
	ID_VIEW_USERNAME,
	ID_VIEW_CONFIG,
	ID_VIEW_COLUMNS,
		ID_SEPARATOR,
	ID_VIEW_PLAYERS,
	ID_VIEW_WATCH,
	ID_VIEW_INI,
	ID_VIEW_DING,
		ID_SEPARATOR,
	ID_APP_ABOUT,
	ID_CONTEXT_HELP,
	ID_APP_BUCKOHOME,
		ID_SEPARATOR,
		ID_SEPARATOR,
	ID_JOINBUTT,
	ID_TEAM_1,
	ID_TEAM_2,
	ID_TEAM_3,
	ID_TEAM_4,
	ID_TEAM_5,
	ID_TEAM_6,
	ID_TEAM_7,
	ID_TEAM_8,
	ID_TEAM_9,

};

///////////////////////////////////////////////////////////////////////////////
static UINT BASED_CODE indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_CONNSECONDS,
	ID_ACTIVESERVER,
	ID_SERVERCOUNT,
	ID_PLAYERCOUNT,
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

///////////////////////////////////////////////////////////////////////////////
// MainFrame construction/destruction

MainFrame::MainFrame()
{
	// TODO: add member initialization code here
	
}

///////////////////////////////////////////////////////////////////////////////
MainFrame::~MainFrame()
{
}

///////////////////////////////////////////////////////////////////////////////
int MainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_toolBar.Create(this) ||
		!m_toolBar.LoadBitmap(IDR_MAINFRAME) ||
		!m_toolBar.SetButtons(buttons,
		  sizeof(buttons)/sizeof(UINT)))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_statusBar.Create(this) ||
		!m_statusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
	m_toolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_toolBar);

	// TODO: Remove this if you don't want tool tips
#ifdef	_WIN32
	m_toolBar.SetBarStyle(m_toolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY);
#endif

	if (!m_shipBar.Create(this, IDD_SHIPBAR, CBRS_TOP, IDD_SHIPBAR))
	{
		TRACE0("Failed to create shipbar\n");
	}
	m_shipBar.SetBarStyle(CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_ALIGN_TOP);
	virgin = TRUE;			// Draw it open, and hide later if we want.


	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// MainFrame diagnostics

#ifdef _DEBUG
void MainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void MainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// MainFrame message handlers

afx_msg	LRESULT MainFrame::OnUpdateStatusText(WPARAM wParam, LPARAM lParam)
{
	LPCSTR	cs = (LPCSTR)lParam;

//	m_wndStatusBar.SetPaneText(0, (const char*)cs);
	if (m_statusBar)
		m_statusBar.SetPaneText(wParam, cs);

return(0);
}


///////////////////////////////////////////////////////////////////////////////
void MainFrame::OnSysColorChange() 
{
	CFrameWnd::OnSysColorChange();
	
	Invalidate();
	
}

///////////////////////////////////////////////////////////////////////////////
void MainFrame::OnClose() 
{
	toolbarOnScreen = m_toolBar.m_hWnd && m_toolBar.IsWindowVisible();
	shipbarOnScreen = m_toolBar.m_hWnd && m_shipBar.IsWindowVisible();
	statusbarOnScreen = m_statusBar && m_statusBar.IsWindowVisible();
	
	CFrameWnd::OnClose();
}
