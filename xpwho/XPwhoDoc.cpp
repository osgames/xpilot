/***************************************************************************\
*  XPwhoDoc.cpp : Windows Doc/View model Doc implementation                 *
*  $Id$   				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.1.1.1  2001/04/04 14:01:08  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.4  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

#include "stdafx.h"
#include "resource.h"       // main symbols

#include "ServerSite.h"
#include "XPwhoDoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// XPwhoDoc

IMPLEMENT_DYNCREATE(XPwhoDoc, CDocument)

BEGIN_MESSAGE_MAP(XPwhoDoc, CDocument)
	//{{AFX_MSG_MAP(XPwhoDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// XPwhoDoc construction/destruction

XPwhoDoc::XPwhoDoc()
{
	// TODO: add one-time construction code here

}

XPwhoDoc::~XPwhoDoc()
{
}

BOOL XPwhoDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

void XPwhoDoc::DeleteContents()
{
	ServerSite*	css;
	
	while (!serverlist.IsEmpty())
	{
		css = (ServerSite*)serverlist.RemoveHead();
		delete css;
	}
}

/////////////////////////////////////////////////////////////////////////////
// XPwhoDoc serialization

void XPwhoDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
#ifndef	_WIN32
	viewfont.Serialize(ar);
#endif
}

/////////////////////////////////////////////////////////////////////////////
// XPwhoDoc diagnostics

#ifdef _DEBUG
void XPwhoDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void XPwhoDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// XPwhoDoc commands

BOOL XPwhoDoc::IsDing(ServerSite* csi)
{
	POSITION	wpos;
	for (wpos=dinglist.GetHeadPosition(); wpos != NULL;)
	{
		Watch* cw = (Watch*)dinglist.GetNext(wpos);
		if (*cw == *csi)
		{
			return(TRUE);
		}
	}
	return(FALSE);
}

void XPwhoDoc::ClearDings()
{
	Watch*	cw;
	
	while (!curdings.IsEmpty())
	{
		cw = (Watch*)curdings.RemoveHead();
		delete cw;
	}

}

void XPwhoDoc::AddDings(ServerSite* csi, Player* cp)
{
	POSITION	wpos;
	for (wpos=curdings.GetHeadPosition(); wpos != NULL;)
	{
		Watch* cw = (Watch*)curdings.GetNext(wpos);
		if (*cw == *csi)
		{
			cw->players.AddTail(cp->name);
			return;
		}
	}
	Watch* cw = new Watch(csi);
	cw->players.AddTail(cp->name);
	curdings.AddTail(cw);
}
