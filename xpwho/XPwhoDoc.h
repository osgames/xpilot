/***************************************************************************\
*  XPwhoDoc.h : Windows Doc/View model Doc interface                        *
*  $Id$ 					*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2002/05/24 17:28:14  dik
 *  Make persistent the user's display of the statusbar and toolbar.
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:09  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.3  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

/////////////////////////////////////////////////////////////////////////////
#ifndef	_XPWHODOC_H_
#define	_XPWHODOC_H_

#include "config.h"
#include "columns.h"
#include "ServerSite.h"

#define	BSFONTheader	<bsfont.h>

#ifdef	WIN32
class CBSFont : public CObject
{
public:
	CBSFont() { memset(&lf, 0, sizeof(LOGFONT)); };
	LOGFONT		lf;
	CHOOSEFONT	cf;
};
#else
#include BSFONTheader
#endif

class XPwhoDoc : public CDocument
{
protected: // create from serialization only
	XPwhoDoc();
	DECLARE_DYNCREATE(XPwhoDoc)

// Attributes
public:
	CObList		serverlist;		// list of CServerSites as seen on the screen
	CObList		recvlist;		// new list just received, ready to be displayed
	CBSFont		viewfont;
	Config		ccfg;
	Columns	ccol;				// which columns to display
	CObList		watchlist;		// which servers have a watch on them
	CObList		inilist;		// which servers have custom INIs
	CObList		dinglist;		// which servers have a ding on them
	CObList		curdings;		// which servers have changes this time

	CString		userNick;
	CString		userUser;
	CString		userHost;

// Operations
public:
	void		AddDings(ServerSite* csi, Player* cp);
	void		ClearDings();
	BOOL		IsDing(ServerSite* csi);
	BOOL		HasDings() {return(!curdings.IsEmpty());};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(XPwhoDoc)
	public:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~XPwhoDoc();
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual	void DeleteContents();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(XPwhoDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif		// _XPWHODOC_H_
