/***************************************************************************\
*  UserName.cpp : Configure the nick=user@host thing                        *
*  $Id$    				*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 */


#include "stdafx.h"
#include "resource.h"
#include "XPwhoDoc.h"
#include "UserName.h"
#include "MainFrame.h"

#include "xp-checknames.h"

#include <winsock.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UserName dialog


UserName::UserName(CWnd* pParent /*=NULL*/)
	: CDialog(UserName::IDD, pParent)
{
	//{{AFX_DATA_INIT(UserName)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void UserName::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UserName)
	DDX_Control(pDX, IDC_EDIT_FULLNAME, m_fullname);
	DDX_Control(pDX, IDC_EDIT_NICK, m_nick);
	DDX_Control(pDX, IDC_EDIT_USER, m_user);
	DDX_Control(pDX, IDC_EDIT_HOST, m_host);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UserName, CDialog)
	//{{AFX_MSG_MAP(UserName)
	ON_EN_CHANGE(IDC_EDIT_NICK, OnChangeEditNick)
	ON_EN_CHANGE(IDC_EDIT_USER, OnChangeEditUser)
	ON_EN_CHANGE(IDC_EDIT_HOST, OnChangeEditHost)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UserName message handlers

BOOL UserName::OnInitDialog() 
{
	CDialog::OnInitDialog();

	/////////////////
	MainFrame* mf = (MainFrame*)GetParent();
	if (doc->userNick.GetLength())
		m_nick.SetWindowText(doc->userNick);
	else
		m_nick.SetWindowText(mf->m_shipBar.GetNick());

	/////////////////
	if (doc->userUser.GetLength())
		m_user.SetWindowText(doc->userUser);
	else
		m_user.SetWindowText(GetDefaultUser());

	/////////////////
	if (doc->userHost.GetLength())
		m_host.SetWindowText(doc->userHost);
	else
		m_host.SetWindowText(GetDefaultHost());

	/////////////////
	UpdateFullName();
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
CString UserName::GetDefaultUser()
{
	char	s[256];
	Get_login_name(s, 256);
	return(s);
}

CString UserName::GetDefaultHost()
{
	char	s[256];
    gethostname(s, 256);
	return(s);
}

///////////////////////////////////////////////////////////////////////////////
void UserName::UpdateFullName()
{
	CString	cs;
	cs = sNick + "=" + sUser + "@" + sHost;
	m_fullname.SetWindowText(cs);
}

///////////////////////////////////////////////////////////////////////////////
void UserName::OnChangeEditNick() 
{
	char	s[256];

	m_nick.GetWindowText(s, 256);
	if (!Check_nick_name(s))
		Fix_nick_name(s);
	sNick = s;
	UpdateFullName();
	
}

///////////////////////////////////////////////////////////////////////////////
void UserName::OnChangeEditUser() 
{
	char	s[256];

	m_user.GetWindowText(s, 256);
	if (!Check_real_name(s))
		Fix_real_name(s);
	sUser = s;
	UpdateFullName();
	
}

///////////////////////////////////////////////////////////////////////////////
void UserName::OnChangeEditHost() 
{
	char	s[256];

	m_host.GetWindowText(s, 256);
	if (!Check_host_name(s))
		Fix_host_name(s);
	sHost = s;
	UpdateFullName();
}
