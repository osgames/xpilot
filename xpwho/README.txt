This is the source package for XPwho, a Windows utility to monitor XPilot action.
The source must be built as a sibling to the XPilot source.
You must have the XPilot source installed first.

Your source tree should look like thus:
$
|-xpwho
      +-res
+-xpilot
      +-lib
      +-src

If your xpilot source directory is called something like
xpilot-4.3.1, then you will need to rename it to xpilot.


Good Luck

Dick Balaska
dick@xpilot.org
