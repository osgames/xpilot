/***************************************************************************\
*  ServerSite.h : Describe a single server                                  *
*  $Id$   				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.1.1.1  2001/04/04 14:01:02  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.4  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

#ifndef	_SERVERSITE_H_
#define	_SERVERSITE_H_

#define	MAX_TEAMS	10
class Watch;

class ServerSite : public CObject
{

public:
			~ServerSite();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL	ParseLine(CString* line);		// 
	BOOL	ParsePlayers(CString line);
	BOOL	ParseFreeBases(CString line);

#ifdef	_DEBUG
	void	TRACEdump();
#endif
// attributes
// 3.4.100:gotland.gmd.de:15345:2:TheBreeder's Bouncy Tank Party 3:100x100:Breeder & Hass:ok:6:14:Warp=andy@baltrum.gmd.de,TheBrave=dirk@sark.gmd.de:no:64:0:0:129.26.12.138
// 4.0.0beta-NT12:duke.buckosoft.com:15345:0:Wacky 3.8.0:100x100:Bucko:ok:14:12::no:61193:3:0:204.249.240.106:1=3,2=3:0
	CString	version;
	CString	servername;
	int		serverport;
	int		playercount;
	CString	mapname;
	CString	mapsize;
	CString	author;
	CString	serverstatus;
	int		bases;
	int		fps;
	CObList	playerlist;
	CString	sound;
	long	uptime;
	int		teams;
	int		timing;
	CString	serverip;
	int		freebases[MAX_TEAMS];
	int		players_queued;

// watch attributes
	bool	watched;

// ping attributes
	long	timesent;
	long	distance;				// #ms from server

// draw attributes
	int		playercount_changed;	// please draw highlighting
	bool	map_changed;

#define	CHANGEDNONE		0
#define	CHANGEDLESS		1
#define	CHANGEDMORE		2

	bool	refreshed;				// RecvData has touched this one
//	bool	drawn_refreshed;		// draw has displayed this in highlights

	int		status;
	bool	GetNew() { return(status == 2); };
	void	SetNew() { status = 2; };
	bool	GetDel() { return(status == 1); };
	void	SetDel() { status = 1; };
	bool	GetNormal() { return(status == 0); };
	void	SetNormal() { status = 0; };
};

///////////////////////////////////////////////////////////////////////////////
class Player : public CObject
{
public:
	Player() {};
	Player(const Player&);
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CString	name;
	int		status;
	bool	drawn_status;
	bool	GetNew() { return(status == 2); };
	void	SetNew() { status = 2; };
	bool	GetDel() { return(status == 1); };
	void	SetDel() { status = 1; };
	bool	GetNormal() { return(status == 0); };
	void	SetNormal() { status = 0; };
};

///////////////////////////////////////////////////////////////////////////////
class Watch : public CObject
{
public:
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	Watch() {};
	Watch(ServerSite* csi);
	bool		operator==(ServerSite& csi);

	CString		servername;
	int			serverport;
	CStringList	players;		// used by dings 
};

#endif
