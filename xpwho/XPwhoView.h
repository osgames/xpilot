/***************************************************************************\
*  XPwhoView.h : The View for XPwho                                         *
*  $Id$ 					*
*                                                                           *
*  Copyright� 1994-2002 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/05/07 08:10:56  dik
 *  Honor the "primary meta" setting.  Display which meta we are talking to in
 *  the status bar.
 *
 *  Revision 1.1.1.1  2001/04/04 14:01:13  dik
 *  Import XPwho to SourceForge.
 *
 *  Revision 1.4  2001/04/02 18:57:15  dick
 *  Prepare for public source release.
 *
 */

/////////////////////////////////////////////////////////////////////////////
#ifndef	_XPWHOVW_H_
#define	_XPWHOVW_H_

#if _USESOCK
#include <winsock.h>
#endif

#include "MainFrame.h"
#include "ServerSite.h"

/*typedef	unsigned char bool; */
#define	WSA_EVENT		WM_USER+300			// from WSAAsyncSelect
#define	WSA_RESOLVEHOST	WM_USER+301
#define	WSA_CONNECT		WM_USER+302
#define	WSA_RECV		WM_USER+303


#define NO_FLAGS_SET         0  /* Used with recv()/send()          */


enum WS_STATE {
	WS_INIT,
	WS_IDLE,
	WS_RESOLVEHOST,
	WS_CONNECT,
	WS_RECV,
	WS_XPILOT,
	WS_ERROR
	};

#define	WS_TIMER	1

#define	WST_INIT		  2000		// allow sys to stabilize before tcp
#define	WST_IDLE		    10		// wait between polls (in secs)
#define	WST_RESOLVEHOST	120000		// time to resolve hostname
#define	WST_CONNECT		 60000		// time allowed to connect
#define	WST_RECV		 30000		// time to receive the data
#define	WST_XPILOT		 10000		// if XPilot is running, how long between checks
#define	WST_ERROR		     3		// error state, display error message time in seconds

#ifndef	WIN32
#undef	WST_RESOLVEHOST
#define	WST_RESOLVEHOST	 65000
#endif

#define	XPwhoViewSUPERCLASS	CScrollView
//#define	XPwhoViewSUPERCLASS	CView

class XPwhoView : public XPwhoViewSUPERCLASS
{
protected: // create from serialization only
	XPwhoView();
	DECLARE_DYNCREATE(XPwhoView)

// Attributes
public:
	XPwhoDoc* GetDoc();

	WS_STATE	ws_state;
	char*		namebuf;
	char*		recvbuf;
	long		recvbuflen;
	long		bytesread;

	// columns in the view
#define	COL_NAME	0
#define	COL_MAP		1
#define	COL_AUTHOR	2
#define	COL_SIZE	3
#define	COL_PCOUNT	4
#define	COL_BASES	5
#define	COL_TEAMS	6
#define	COL_FPS		7
#define	COL_VERSION	8
#define	COL_SOUND	9
#define	COL_UPTIME	10
#define	COL_IP		11
#define	COL_TIMING	12
#define	COL_PORT	13
#define	MAXCOLUMNS	14
	int			columnx[MAXCOLUMNS];
	bool		resize_cursor_on;
	HCURSOR		resize_cursor;
	bool		resize_grabbed;
	int			resize_column;

	bool		virgin;			// first time ever reading the list

	int			lineheight;
	
	int			activeMeta;		// if an error, should we check alt meta?
	int			idlecount;		// # of secs left to idle
	int			myTimer;		// returned from SetTimer()
#if	_USESOCK
	SOCKET		mysocket;
#endif

	ServerSite*	selection;	// guy that the user clicked over

// Operations
public:

	void			ResolveHostName();
	void			Connect();
	void			ProcessRecvData(char*);
	PCSTR			GetHost() { return(activeMeta ? GetDoc()->ccfg.m_hostname2 
												: GetDoc()->ccfg.m_hostname); };

	void			SortList();
	void			DeleteList();
	void			UpdateScrollSizes();

	void			Prelude();			// before updating
	void			PostMortem();		// after updating
	
	void			SetState(WS_STATE s);
	WS_STATE		GetState() { return(ws_state); };
	void			PrintCountdown();

	ServerSite*		GetSelection() { return(selection); };
	void			SetSelection(ServerSite* csi);
	
	void			OnJoinWithTeam(int team);

	void			UpdateXPilotIni();	// set the ship and nick in XPilot.ini
	CString			GetExe();		// return full path of XPilot
	void			ProcessDings();

#if _FILETEST
	bool	FileTest();
#endif

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(XPwhoView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~XPwhoView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(XPwhoView)
	afx_msg void OnDestroy();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnFileOpen();
	afx_msg void OnUpdateViewUserName(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewConfig(CCmdUI* pCmdUI);
	afx_msg void OnViewUserName();
	afx_msg void OnViewConfig();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnUpdateViewPlayers(CCmdUI* pCmdUI);
	afx_msg void OnViewPlayers();
	afx_msg void OnUpdateViewWatch(CCmdUI* pCmdUI);
	afx_msg void OnViewWatch();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateServer(CCmdUI* pCmdUI);
	afx_msg void OnServer();
	afx_msg void OnUpdateViewDing(CCmdUI* pCmdUI);
	afx_msg void OnViewDing();
	afx_msg void OnUpdateJoinbutt(CCmdUI* pCmdUI);
	afx_msg void OnJoinbutt();
	afx_msg void OnUpdateJointeam(CCmdUI* pCmdUI);
	afx_msg void OnJointeam();
	afx_msg void OnUpdateViewColumns(CCmdUI* pCmdUI);
	afx_msg void OnViewColumns();
	afx_msg void OnUpdateTeam1(CCmdUI* pCmdUI);
	afx_msg void OnTeam1();
	afx_msg void OnUpdateTeam2(CCmdUI* pCmdUI);
	afx_msg void OnTeam2();
	afx_msg void OnUpdateTeam3(CCmdUI* pCmdUI);
	afx_msg void OnTeam3();
	afx_msg void OnUpdateTeam4(CCmdUI* pCmdUI);
	afx_msg void OnTeam4();
	afx_msg void OnUpdateTeam5(CCmdUI* pCmdUI);
	afx_msg void OnTeam5();
	afx_msg void OnUpdateTeam6(CCmdUI* pCmdUI);
	afx_msg void OnTeam6();
	afx_msg void OnUpdateTeam7(CCmdUI* pCmdUI);
	afx_msg void OnTeam7();
	afx_msg void OnUpdateTeam8(CCmdUI* pCmdUI);
	afx_msg void OnTeam8();
	afx_msg void OnUpdateTeam9(CCmdUI* pCmdUI);
	afx_msg void OnTeam9();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnUpdateViewIni(CCmdUI* pCmdUI);
	afx_msg void OnViewIni();
	afx_msg void OnAppHome();
	afx_msg void OnAppBuckohome();
	afx_msg void OnViewShiplist();
	afx_msg void OnUpdateViewShiplist(CCmdUI* pCmdUI);
	afx_msg void OnUpdateConfigureSysinfo(CCmdUI* pCmdUI);
	afx_msg void OnConfigureSysinfo();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG

	afx_msg void OnUpdateJointeamCommon(CCmdUI* pCmdUI, int t);

#if	_USESOCK
	afx_msg LONG OnWSA_RECV(UINT, LONG);
	afx_msg	LONG OnWSA_RESOLVEHOST(UINT, LONG);
	afx_msg	LONG OnWSA_CONNECT(UINT, LONG);
	afx_msg	LONG OnWSA_EVENT(UINT, LONG);
#endif

	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in xpwhovw.cpp
inline XPwhoDoc* XPwhoView::GetDoc()
   { return (XPwhoDoc*)m_pDocument; }
#endif

#define	SetStatus(__s)	GetParentFrame()->SendMessage(WM_UPDATE_STATUS_TEXT, 0, (LPARAM)(LPCSTR)__s)
#define	SetStatusSecondCount(__s)	GetParentFrame()->SendMessage(WM_UPDATE_STATUS_TEXT, 1, (LPARAM)(LPCSTR)__s)
#define	SetStatusActiveServer(__s)	GetParentFrame()->SendMessage(WM_UPDATE_STATUS_TEXT, 2, (LPARAM)(LPCSTR)__s)
#define	SetStatusServerCount(__s)	GetParentFrame()->SendMessage(WM_UPDATE_STATUS_TEXT, 3, (LPARAM)(LPCSTR)__s)
#define	SetStatusPlayerCount(__s)	GetParentFrame()->SendMessage(WM_UPDATE_STATUS_TEXT, 4, (LPARAM)(LPCSTR)__s)

/////////////////////////////////////////////////////////////////////////////

#endif		// _XPWHOVW_H_
