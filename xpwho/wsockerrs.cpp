/***************************************************************************\
*  wsockerrs.cpp : convert network err numbers into text					*
*  $Id$  				*
*                                                                           *
*  Copyright� 1994-2001 by                                                  *
*      Dick Balaska         <dick@xpilot.org>                               *
*      Bert Gijsbers        <bert@xpilot.org>                               *
*      Ken Ronny Schouten   <ken@xpilot.org>                                *
*      Bj�rn Stabell        <bjoern@xpilot.org>                             *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
*                                                                           *
* You should have received a copy of the GNU General Public License         *
* along with this program; if not, write to the Free Software               *
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                 *
\***************************************************************************/

/*
 *  $Log$
 *  Revision 1.2  2001/04/02 14:55:42  dick
 *  Prepare for public source release
 *
 */

#include "stdafx.h"

#include <winsock.h>

struct Wsockerrs {
	int		error;
	char*	text;
} Wsockerrs;
struct Wsockerrs wsockerrs[] = {
WSAEINTR, "WSAEINTR",
WSAEBADF, "WSAEBADF",
WSAEACCES, "WSAEACCES",
WSAEFAULT, "WSAEFAULT",
WSAEINVAL, "WSAEINVAL",
WSAEMFILE, "WSAEMFILE",

/*
 * Windows Sockets definitions of regular Berkeley error constants
 */
WSAEWOULDBLOCK, "WSAEWOULDBLOCK",
WSAEINPROGRESS, "WSAEINPROGRESS",
WSAEALREADY, "WSAEALREADY",
WSAENOTSOCK, "WSAENOTSOCK",
WSAEDESTADDRREQ, "WSAEDESTADDRREQ",
WSAEMSGSIZE, "WSAEMSGSIZE",
WSAEPROTOTYPE, "WSAEPROTOTYPE",
WSAENOPROTOOPT, "WSAENOPROTOOPT",
WSAEPROTONOSUPPORT, "WSAEPROTONOSUPPORT",
WSAESOCKTNOSUPPORT, "WSAESOCKTNOSUPPORT",
WSAEOPNOTSUPP, "WSAEOPNOTSUPP",
WSAEPFNOSUPPORT, "WSAEPFNOSUPPORT",
WSAEAFNOSUPPORT, "WSAEAFNOSUPPORT",
WSAEADDRINUSE, "WSAEADDRINUSE",
WSAEADDRNOTAVAIL, "WSAEADDRNOTAVAIL",
WSAENETDOWN, "WSAENETDOWN",
WSAENETUNREACH, "WSAENETUNREACH",
WSAENETRESET, "WSAENETRESET", 
WSAECONNABORTED, "WSAECONNABORTED",
WSAECONNRESET, "WSAECONNRESET",
WSAENOBUFS, "WSAENOBUFS",
WSAEISCONN, "WSAEISCONN",
WSAENOTCONN, "WSAENOTCONN",
WSAESHUTDOWN, "WSAESHUTDOWN",
WSAETOOMANYREFS, "WSAETOOMANYREFS",
WSAETIMEDOUT, "WSAETIMEDOUT",
WSAECONNREFUSED, "WSAECONNREFUSED",
WSAELOOP, "WSAELOOP",
WSAENAMETOOLONG, "WSAENAMETOOLONG",
WSAEHOSTDOWN, "WSAEHOSTDOWN",
WSAEHOSTUNREACH, "WSAEHOSTUNREACH",
WSAENOTEMPTY, "WSAENOTEMPTY",
WSAEPROCLIM, "WSAEPROCLIM",
WSAEUSERS, "WSAEUSERS",
WSAEDQUOT, "WSAEDQUOT",
WSAESTALE, "WSAESTALE",
WSAEREMOTE, "WSAEREMOTE",

WSAEDISCON, "WSAEDISCON",

/*
 * Extended Windows Sockets error constant definitions
 */
WSASYSNOTREADY, "WSASYSNOTREADY",
WSAVERNOTSUPPORTED, "WSAVERNOTSUPPORTED",
WSANOTINITIALISED, "WSANOTINITIALISED",

-1, "UNKNOWN"
};

const char*	GetWSockErrText(int	error)
{
	for (int i=0; wsockerrs[i].error != -1; i++)
		if (wsockerrs[i].error == error)
			break;
	return(wsockerrs[i].text);
}
