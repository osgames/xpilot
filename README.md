# XPilot

Originally developed at http://www.xpilot.org/ and https://sourceforge.net/projects/xpilotgame/ by [bjoerns](http://sourceforge.net/users/bjoerns), [dik](http://sourceforge.net/users/dik) and [xpilot](http://sourceforge.net/users/xpilot) and published under the GPL-2.0 license.